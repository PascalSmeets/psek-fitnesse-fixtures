@page tips

These pages contain some usage tips.


@subpage xpath2
## xPath axes2

With xPath axes it is possible to take *shortcuts* while defining an xPath expression.

A  nice resource with mote xPath information including a graphical representation op xPath axes can be found at: [http://www.xmlplease.com/axis](https://web.archive.org/web/20161125200548/http://www.xmlplease.com/axis)

Example using xPath to find an element:

    <div id="gwt-debug-filter">Filter</div>
    <table cellspacing="0" cellpadding="0" style="width: 100%;">
        <tbody>
            <tr>
                <td align="center" style="vertical-align: top;">
                    <button type="button" id="gwt-debug-filter">Filter</button>
                </td>
                <td align="center" style="vertical-align: top;">
                    <button type="button" id="gwt-debug-clear">Clear</button>
                </td>
                <td align="center" style="vertical-align: top;">
                    <button type="button" id="gwt-debug-cancel">Cancel</button>
                </td>
            </tr>
        </tbody>
    </table>

If there are more elements on the page with id=gwt-debug-filter using only the id as locator to find the button can result in errors.

    !|script			 	   |
    |click;|id=gwt-debug-filter|


Example xPath expressions to find the *Filter* button based on the *Clear* button. There are also more (and probably better) options.

    !|script                                                                                                  |
    |click;|//*[text()=’Clear’]/parent::td/preceding-sibling::td/child::button[text()=’Filter’]               | ← using xPath axes
    |click;|//*[@id=’gwt-debug-clear’]/parent::td/preceding-sibling::td/child::button[@id=’gwt-debug-filter’] | ← using xPath axes
    |click;|//button[text()=’Clear’]/ancestor::tr/descendant::button[text()=’Filter’]                         | ← using xPath axes
    |click;|//button[text()=’Clear’]/../../td/button[text()=’Filter’]                                         |
    |click;|//button[text()=’Clear’]/../../td[1]/button                                                       |



## Elements containing *br* tags

If an element contains a `<br/>` tag the text() option can not be used to
specify the complete text of the element.

    <html>
    <body>
    <table style="width:100%">
        <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Adress</th>
        </tr>
        <tr>
            <td>John</td>
            <td>27</td>
            <td>1 Infinite Loop<br/>
                Cupertino, CA 95014</td>
        </tr>
    </table>
    </body>
    </html>

To verify the presence of the adress the following xpath can be used:

    //td[contains(.,'1 Infinite Loop') and contains(.,'Cupertino, CA 95014')]

