@page pattern
## Commands that use a <em>pattern</em> as input can use regular expressions:

* If <em>regex:</em> is prepended to the input it will be matched as a regex.
* If <em>regexpi:</em> is prepended the input will be matched as a case-insensitive regex.
* Otherwise the <em>specified text</em> is used with String.equals.

You can use [regex101.com](https://regex101.com) to test your regular expression.

Source code:

	private static final String REGEXPI_SYNTAX = "regexpi:";
	private static final String REGEX_SYNTAX = "regex:";

	    public static boolean matchOnPattern(String input, String pattern) {
        LOG.debug("Starting pattern matcher complete match for input '" + input + "' and pattern '" + pattern + "'");
        if (pattern.startsWith(REGEXPI_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEXPI_SYNTAX.length()), Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
            boolean match = compiledPattern.matcher(input).matches();
            LOG.debug("Matching using regular expression, case-insensitive. Match = " + match);
            return match;
        } else if (pattern.startsWith(REGEX_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEX_SYNTAX.length()), Pattern.DOTALL);
            boolean match = compiledPattern.matcher(input).matches();
            LOG.debug("Matching using regular expression, case-sensitive. Match = " + match);
            return match;
        } else {
            boolean match = equal(input, pattern);
            LOG.debug("Matching using String.equals method. Match = " + match);
            return match;

        }

    }

        public static boolean containsPattern(String input, String pattern) {
        LOG.debug("Starting pattern matcher partial match for input '" + input + "' and pattern '" + pattern + "'");
        return containsPatternNoInputLogging(input, pattern);

    }

    public static boolean containsPatternNoInputLogging(String input, String pattern) {

        if (pattern.startsWith(REGEXPI_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEXPI_SYNTAX.length()), Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
            boolean partialmatch = compiledPattern.matcher(input).find();
            LOG.debug("Partial matching using regular expression, case-insensitive. Partial match = " + partialmatch);
            return partialmatch;
        } else if (pattern.startsWith(REGEX_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEX_SYNTAX.length()), Pattern.DOTALL);
            boolean partialmatch = compiledPattern.matcher(input).find();
            LOG.debug("Partial matching using regular expression, case-sensitive. Partial match = " + partialmatch);
            return partialmatch;
        } else {
            boolean partialmatch = input.contains(pattern);
            LOG.debug("Partial matching using String.contains method. Partial match = " + partialmatch);
            return partialmatch;
        }

    }