@page seleniumproperties

The template provides one selenium.properties file. But you can [use multiple](@ref nl.kvk.fitnesse.fixtures.selenium.SlimWebDriver.SlimWebDriver).

```
# The following options are allowed:
# firefox, chrome, safari, internet explorer, headless, remote
#
webdriver.browser=chrome
```
Defines the browser that will be used. If `remote` is selected, the property `webdriver.remote.browser` determines the remote browser.

```
# The location of screenshots made by the takeScreenshot command
# should be a relative or absolute path to FitnesseRoot/files/screenshots
#
# Windows: src\\test\\resources\\FitnesseRoot\\files\\screenshots
# OSX: src/test/resources/FitnesseRoot/files/screenshots/
#
screenshot.location=src/test/resources/FitnesseRoot/files/screenshots/
```
The template provides a folder screenshots, but you can specify any other location

```
#
# Timeout settings in seconden
#
command.timeout=30
pageload.timeout=60
```
These values are translated in the `Load selenium server properties` section of `TestSuite.SuiteSetUp`.
They can be used with the [defaultCommandTimeout](@ref nl.kvk.fitnesse.fixtures.selenium.SlimWebDriver.SlimWebDriver.defaultCommandTimeout) and [defaultPageLoadTimeout](@ref nl.kvk.fitnesse.fixtures.selenium.SlimWebDriver.SlimWebDriver.defaultPageLoadTimeout)commands 

<a name="chromeheadless"></a>
```
#
# If you are using Chrome as the webdriver.browser the setting below allows for headless operation of yout local Chrome browser.
#
webdriver.chrome.headless=false
webdriver.chrome.headless.window-size=1600x1600
```
When using chrome as a browser it is possible to use it headless using the settings below.


```
# When using firefox as your browser you can enable the following property to
# activate the usage of a custom firefox profile.
#
# Place the firefox profile somewhere on the resources folder with a unique folder name.
# Enter the name of the folder as the webdriver.firefox.profile value.
#
#webdriver.firefox.profile=customfirefoxprofile
```
If `browser=firefox` and this property is active the specified [firefox profile](https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data) will be used.

```
# It is also possible to use a proxy without setting a profile
# Enter the hostname and port (eg. localhost:808) as the firefox.proxy value
#
#webdriver.firefox.proxy=localhost:8080
```
Enables setting of a proxy for firefox.

```
#
# The following options are allowed:
# firefox, chrome, safari, internet explorer, headless
#
#webdriver.remote.browser=firefox
#
```
Sets the remote browser to use.

```
# The version of the browser, leave empty if unknown.
#
#webdriver.remote.browser.version=
#
```
Sets the remote browser version if needed.


```
# The URL to the server running the Selenium Server.
#
#webdriver.remote.server.url=http://localhost:4444/
```
Sets the url of the remote selenium server.

```
# Webdriver drivers
# The selenium fixture (SlimWebDriver) loads the required driver files from disk during startup.
# The properties below specify where the driver executables are located
#
webdriver.gecko.driver=src/test/tooling/win/geckodriver.exe
webdriver.chrome.driver=src/test/tooling/win/chromedriver.exe
webdriver.edge.driver=src/test/tooling/win/MicrosoftWebDriver.exe

webdriver.macos.gecko.driver=src/test/tooling/macos/geckodriver
webdriver.macos.chrome.driver=src/test/tooling/macos/chromedriver
```
The properties allow you to define the browser driver for Firefox, Chrome and Egde.
The slimWebDriver Fixture will fail gracefully if you don't supply a driver file for unused browsers or operating systems.