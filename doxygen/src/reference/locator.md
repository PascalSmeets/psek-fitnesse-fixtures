@page locator
## Commands that use a `locator` as input can use the following syntax to find an element on the page

This uses the  [constructLocatorFromString](@ref nl.kvk.fitnesse.fixtures.selenium.SlimWebDriver.constructLocatorFromString) method
-   xpath=
-   css=
-   id=
-   name=
-   classname=
-   link=
-   partiallink=

If none of the above is specified \xpath is assumed.
