@page manual
## Manual

### Generic functions

* The [OperationsFixture](@ref nl.psek.fitnesse.fixtures.general.util.OperationsFixture) contains commands that are available in every testcase. 
    <br/><em>These includes methods to</em> 
    * Compare values
    * encode to and decode from Base64
    * do simple calculations
    * generate a timestamp
    * log teststeps or output
    * do even more!
* The [PropertiesFixture](@ref nl.psek.fitnesse.fixtures.general.properties.PropertiesFixture) contains commands for the easy loading of java properties into [SlimSymbols](http://www.fitnesse.org/FitNesse.SuiteAcceptanceTests.SuiteSlimTests.SlimSymbols). 
  If the property values are encrypted they will be decrypted using the [EncryptionUtil](@ref nl.psek.fitnesse.fixtures.util.EncryptionUtil)     

### Generating testdata
There are multiple fixtures to generate testdata

* The [Variables Fixture](@ref nl.psek.fitnesse.fixtures.general.util.VariablesFixture) can generate random values, UUID's and more. Because the Variables Fixture includes the Date Fixture all of the date commands are directly accessible through it.
* The [DateFixture](@ref nl.psek.fitnesse.fixtures.general.util.DateFixture) contains multiple commands to generate a date in different formats with different offsets.
* The [LookupFixture](@ref nl.psek.fitnesse.fixtures.general.util.LookupFixture) allows for basic lookup functionality, i.e. setting the value of one [SlimSymbol](http://www.fitnesse.org/FitNesse.SuiteAcceptanceTests.SuiteSlimTests.SlimSymbols) based on the value of another. 
* The [BsnGenerator](@ref nl.psek.fitnesse.fixtures.general.bsn.BsnGenerator) can generate and verify (11-proef) a Dutch Burgerservicenummer.
* The [IbanGenerator](@ref nl.psek.fitnesse.fixtures.general.iban.IbanGenerator) generates random bank account numbers and random international bank account numbers.
* The [StringFixture](@ref nl.psek.fitnesse.fixtures.general.string.StringFixture) allows for manipulation of strings, concatenation, substring, replacements.


### Sending requests

* The [SOAPWebserviceFixture](@ref nl.psek.fitnesse.fixtures.webservice.SOAPWebserviceFixture) uses `Apache HttpClient` and a `SAX-parser` to make SOAP calls.
* The [SimpleSoapFixture](@ref nl.psek.fitnesse.fixtures.webservice.SimpleSoapFixture) uses `javax.xml.soap` without a `SAX-parser` to make SOAP calls. This enables the sending of invalid messages from testcases.
* The [RestFixture](@ref nl.psek.fitnesse.fixtures.rest.RestFixture) uses RestAssured to make api calls.
* The [BuildJsonStringFixture](@ref nl.psek.fitnesse.fixtures.general.json.BuildJsonStringFixture) can be used to generate a JSON message, thuis uses Stting concatenation and validates the result by generating a [org.json.JSONObject](https://mvnrepository.com/artifact/org.json/json) object. 
  If possible use your own POJO to generate a JSON string for use in the [RestFixture](@ref nl.psek.fitnesse.fixtures.rest.RestFixture)

### Checking output 

* The [XmlNavigateFixture](@ref nl.psek.fitnesse.fixtures.general.xml.XmlNavigateFixture) can be used to perform checks on and extract data from XML strings
* The [JsonNavigateFixture](@ref nl.psek.fitnesse.fixtures.general.json.JsonNavigateFixture) can be used to perform checks on and extract data from Json strings or files. [Jayway JsonPath](https://github.com/jayway/JsonPath) is used to navigate the json object.
* The [TextReaderFixture](@ref nl.psek.fitnesse.fixtures.general.text.TextReaderFixture) can read the last n-lines from a textfile. And perform checks on the returned lines. This can be used to perform checks in a logfile.
* The [PdfTextReaderFixture](@ref nl.psek.fitnesse.fixtures.general.pdf.PdfTextReaderFixture) can be used to read the text from a PDF file. And to perform checks on the text.

### Working with webpages

* The [SlimWebDriver](@ref nl.psek.fitnesse.fixtures.selenium.SlimWebDriver) uses Selenium 3 to automate webpages

### Working with files

* The [FileUtils](@ref nl.psek.fitnesse.fixtures.general.util.FileUtils) provide methods for reading from and writing to files. As well as local or SMB file manipulation.