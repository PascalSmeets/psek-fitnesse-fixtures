@page install_psek
## Generate a new testproject with demo testcases using the Maven archetype.  

### Generate the project
type `mvn archetype:generate -DarchetypeGroupId=nl.psek.fitnesse -DarchetypeArtifactId=psek-fitnesse-project-archetype -DarchetypeVersion=<LATEST ARCHETYPE VERSION>`
and answer the prompts about groupId, artifactId and version for your new testproject.

* Check Maven Central for the [latest version of the archetype](https://search.maven.org/#search%7Cga%7C1%7Ca%3A%22psek-fitnesse-project-archetype%22)
* Optionally edit the `pom.xml` file to use the [latest version of the fixtures](https://search.maven.org/#search%7Cga%7C1%7Ca%3A%22psek-fitnesse-fixtures%22)

```
<gx-fitnesse.version>2.0.0</gx-fitnesse.version>
```

### Run FitNesse
```
mvn clean install -P start-fitnesse-wiki
```

### Run a demo testcase
Open FitNesse at [http://localhost:9123](http://localhost:9123) and navigate to the demo testcases.
 
Just press 'Test'.