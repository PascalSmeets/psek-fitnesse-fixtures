@page install
## Generate a new testproject with demo testcases using the Maven archetype.  

This template sets up a basic project with some demo testcases.

### Generate the project
1. Check Maven Central for the [latest version of the archetype](https://search.maven.org/#search%7Cga%7C1%7Ca%3A%22psek-fitnesse-project-archetype%22)
2. Use the command

    `mvn archetype:generate -DarchetypeGroupId=nl.psek.fitnesse -DarchetypeArtifactId=psek-fitnesse-project-archetype -DarchetypeVersion=<LATEST ARCHETYPE VERSION>`
   
   and answer the prompts about groupId, artifactId and version for your new testproject.   

3. Optionally edit the `pom.xml` file to use the [latest version of the fixtures](https://search.maven.org/#search%7Cga%7C1%7Ca%3A%22psek-fitnesse-fixtures%22)
   
   `<psek-fitnesse-fixtures.version>2.0.0</psek-fitnesse-fixtures.version>`

### Or take a look at the demo project
https://gitlab.com/PascalSmeets/psek-fixtures-demoproject

### Run FitNesse

   `mvn clean install -P start-fitnesse-wiki`

### Run a demo testcase
Open FitNesse at [http://localhost:9123](http://localhost:9123) and navigate to the demo testcases.
 
Just press 'Test'.


## Source code

* [Git repository psek-fitnesse-fixtures](https://gitlab.com/PascalSmeets/psek-fitnesse-fixtures)
* [Git repository project template](https://gitlab.com/PascalSmeets/psek-fitnesse-project-archetype)
