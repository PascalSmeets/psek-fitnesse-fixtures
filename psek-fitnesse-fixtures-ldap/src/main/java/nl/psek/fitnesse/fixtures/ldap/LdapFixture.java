package nl.psek.fitnesse.fixtures.ldap;

import nl.psek.fitnesse.fixtures.general.properties.PropertiesFixture;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.query.LdapQueryBuilder;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.ModificationItem;
import java.util.List;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;

/**
 * Deze fixture representeert een low-level interface naar een LDAP. De fixture
 * biedt standaard CRUD functionaliteit.
 * <p>
 * Fitnesse projecten die gebruik maken van deze interface zullen over het algemeen
 * hun eigen specifieke fixtures bouwen bovenop deze. Deze fixture kan dan ook los
 * gebruikt worden als ldap-client in de implementatie van deze fixtures.
 */
public class LdapFixture {

    // De connectie naar de LDAP
    private LdapTemplate connection;

    // De query fragmenten
    private String base;
    private StringBuilder andFilters;

    /**
     * Construeert een {@code LdapFixture} object.
     */
    public LdapFixture() {
    }

    /**
     * Maakt een connectie met de LDAP. Dit is de eerste method die aangeroepen moet worden bij het
     * gebruik van de fixture.
     *
     * @param properties de configuratie van de connectie met de LDAP
     */
    @Deprecated
    public void createConnection(String properties) {
        PropertiesFixture propertiesFixture = new PropertiesFixture(properties);
        LdapContextSource lcs = new LdapContextSource();
        lcs.setUrl(propertiesFixture.getStringProperty("ldap.connectie.url"));
        lcs.setBase(propertiesFixture.getStringProperty("ldap.connectie.base"));
        lcs.setUserDn(propertiesFixture.getStringProperty("ldap.connectie.username"));
        lcs.setPassword(propertiesFixture.getStringProperty("ldap.connectie.password"));
        lcs.setPooled(false);
        lcs.afterPropertiesSet();
        this.connection = new LdapTemplate(lcs);
    }

    /**
     * Maakt een connectie met de LDAP. Dit is de eerste method die aangeroepen moet worden bij het
     * gebruik van de fixture.
     *
     * @param url      the url
     * @param base     the base
     * @param username the username
     * @param password the password
     */
    public void createConnection(String url,String base, String username, String password) {
        LdapContextSource lcs = new LdapContextSource();
        lcs.setUrl(getFromPropertyIfApplicable(url));
        lcs.setBase(getFromPropertyIfApplicable(base));
        lcs.setUserDn(getFromPropertyIfApplicable(username));
        lcs.setPassword(getFromPropertyIfApplicable(password));
        lcs.setPooled(false);
        lcs.afterPropertiesSet();
        this.connection = new LdapTemplate(lcs);
    }


    /**
     * Initialiseert de query waarmee de LDAP doorzocht gaat worden. De meegeven {@code base} mag
     * {@code null} zijn of een lege string.
     *
     * @param base de locatie in de LDAP waarvanaf gezocht moet worden naar entries, deze komt bovenop             de base die eventueel is meegegeven in de configuratie van de LDAP connectie zelf
     */
    public void initialiseQuery(String base) {
        this.base = base;
        this.andFilters = new StringBuilder("(&");
    }

    /**
     * Convenience methode voor het toevoegen van de "where objectClass is b"
     * clausule aan de query.
     *
     * @param objectClass de naam van de objectClass
     */
    public void addAndObjectClassIs(String objectClass) {
        addAndWhereIs("objectClass", objectClass);
    }

    /**
     * Voegt een algemene "where a is b" clausule toe aan de query.
     *
     * @param oneThing   de a in "where a is b"
     * @param otherThing de b in "where a is b"
     */
    public void addAndWhereIs(String oneThing, String otherThing) {
        this.andFilters.append("(" + oneThing + "=" + otherThing + ")");
    }

    /**
     * Voert de query uit tegen de LDAP.
     *
     * @return de lijst van entries die voldoen aan de criteria zoals opgebouwd in de query
     */
    public List<Attributes> executeQuery() {
        // Initialiseer de querybuilder
        LdapQueryBuilder query = LdapQueryBuilder.query();
        if (!isEmpty(this.base)) {
            query = query.base(this.base);
        }
        // Zet de filters
        String filterString = createFilter(this.andFilters);
        query.filter(filterString);
        return this.connection.search(query, new AttributesMapper<Attributes>() {
            @Override
            public Attributes mapFromAttributes(Attributes attributes) throws NamingException {
                return attributes;
            }
        });
    }

    /**
     * Ruimt de aangemaakte query op. Onder andere te gebruiken als men dezelfde connectie naar
     * de LDAP wil hergebruiken over meerdere queries.
     */
    public void destroyQuery() {
        this.base = null;
        this.andFilters = null;
    }

    /**
     * Convenience methode voor het snel kunnen opvragen van een specifieke entry in de LDAP als
     * de {@code dn} daarvan bekend is. Hiervoor hoeft geen query gebouwd te worden.
     *
     * @param dn de distinguished name van de entry
     * @return de attributen van de opgehaalde entry
     */
    public Attributes findEntry(String dn) {
        return findEntry(dn, new AttributesMapper<Attributes>() {
            @Override
            public Attributes mapFromAttributes(Attributes attributes) throws NamingException {
                return attributes;
            }
        });
    }

    /**
     * Convenience methode voor het snel kunnen opvragen van een specifieke entry in de LDAP als
     * de {@code dn} daarvan bekend is. Hiervoor hoeft geen query gebouwd te worden.
     *
     * @param <T>    the type parameter
     * @param dn     de distinguished name van de entry
     * @param mapper mapt de entry die terugkomt uit de LDAP naar custom type T
     * @return entry zoals getransformeerd door de meegegeven mapper
     */
    public <T> T findEntry(String dn, AttributesMapper<T> mapper) {
        return this.connection.lookup(dn, mapper);
    }

    /**
     * Voert een entry op in de LDAP op locatie {@code dn}.
     *
     * @param dn    de distinguished name van de entry
     * @param entry de entry
     */
    public void addEntry(String dn, Attributes entry) {
        this.connection.bind(dn, null, entry);
    }

    /**
     * Verwijdert de entry op locatie {@code dn} uit de LDAP. De method verwijdert ook recursief
     * alle children onder de bewuste entry (wanneer aanwezig).
     *
     * @param dn de distinguished name van de entry
     */
    public void removeEntry(String dn) {
        this.connection.unbind(dn, true);
    }

    /**
     * Wijzigt de entry op locatie {@code dn} met de meegegeven lijst van modificaties
     *
     * @param modifications de mutaties die uitgevoerd moeten worden op de entry
     * @param dn            de distinguished name van de entry
     */
    public void wijzigEntry(ModificationItem[] modifications, String dn) {
        if (modifications.length > 0) {
            this.connection.modifyAttributes(dn, modifications);
        }
    }

    /**
     * Ruimt de connectie naar de LDAP op.
     */
    public void destroyConnection() {
        this.connection = null;
    }

    private String createFilter(StringBuilder sb) {
        return sb.append(")").toString();
    }

    private boolean isEmpty(String value) {
        return value == null || "".equals(value.trim());
    }
}
