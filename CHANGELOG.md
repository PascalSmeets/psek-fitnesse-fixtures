@page changelog
## Changelog


## [2.5.0] 2022-01-20
### Updated lots of documentation

## BREAKING change

Module psek-fitnesse-runner is removed, methods are present in the gx-fitnesse-fixtures-util module. Please update your **pom.xml**

replace
```xml
<dependency>
    <groupId>nl.psek.fitnesse</groupId>
    <artifactId>psek-fitnesse-runner</artifactId>
    <version>...</version>
</dependency>
```
with

```xml
<dependency>
  <groupId>nl.psek.fitnesse</groupId>
  <artifactId>psek-fitnesse-fixtures-util</artifactId>
  <version>2.3.0</version>
</dependency>

```
### Added

* **KeepassFixture** to load secrets from a keepass file. Contains methods to store values from a keepass file in System.properties

### Updated

* Multiple methods are updated with support for the loading of secrets from a System.property. 
Where applicable: these classes have been updated with methods to load configuration from supplied strings 
(if used with the prefix `p:` the value will be read from a system property).
  * SOAPWebserviceFixture
  * RestFixture
  * DatabaseFixture
  * PdfTextReaderFixture
  * SeleniumFixture
  * EncryptionUtil
* General cleanup

## [2.3.0] 2021-03-12

### Added 
**SeleniumFixture** containing all the commands of SlimWebDriver, initialization is no longer dependent on a properties file but done programatically.
Look at the manual for more setup config

example:

```
|start                  |Slim Web Driver|selenium.properties  |
|circumventSelenium3Limitations;                              |
|defaultCommandTimeout; |$default_command_timeout             |
|defaultPageLoadTimeout;|$default_pageload_timeout            |

```

```
|start                  |SeleniumFixture                                              |
|setDriverLocation;     |webdriver.chrome.driver|src/test/tooling/win/chromedriver.exe|
|#addChromeOption;      |option=value                                                 |
|#setChromeHeadless;    |true                                                         |
|#setChromeProxy;       |host                   |port                                 |
|startWebDriver;        |chrome                                                       |
|circumventSelenium3Limitations;                                                      |
|defaultCommandTimeout; |$default_command_timeout                                     |
|defaultPageLoadTimeout;|$default_pageload_timeout                                    |

```

## [2.1.0] 2020-07-12
### Added 
**SimpleHttpPoster** class to POST files to an upload form

### Updated 
Fitnesse version to 20200501 - check the FitNesseRoot folder for unwanted content.txt and properties.xml files after the update if you're using the new .wiki format.
Remove the content.txt and properties.xml created by the update.

**SlimWebDriver** HtmlUnitDriver version to 2.40.0
**SlimWebDriver** new methods to adjust default Selenium3 behaviour. circumventSelenium3Limitations / restoreSelenium3Limitations. In this version implemented in the click(locator) command.
**SlimWebDriver** new method to download a file (any resource) from a link using the selenium session: downloadUrl   
**SlimWebDriver** support for saving of one screenshot to multiple locations.   
Change the path in selenium.properties to a ;-separated list. the example below saves the same file to three locations
```  
screenshot.location=src/test/resources/FitnesseRoot/files/screenshots;target/fitnesse-test-results/files/screenshots;c:/tmp/screenshots
```
The return value of the method contains a link to the file in the FitNesse-accessible location `src/test/resources/FitnesseRoot/files/screenshots`
**SlimWebDriver** removed not working aShot library for screenshots, reverted to Selenium.TakesScreenshot
**SlimWebDriver** added methods typeUsingJavascript(locator, value), tab(locator)

**DateFixture** uses LocalDateTime instead of localDate → revert to LocalDate for getDate(String value)
**DateFixture** uses LocalDateTime instead of localDate, can be used to generate a date wih a timestamp (using LocalDateTime.now())

**OperationsFixture** added method getSubstringWithRegex(String input, String regex)
**OperationsFixture** added method *addDecimalNumbersInCsv* <br/>

**JsonNavigateFixture** moved some logging from info to debug level

**RestFixture** added a reference to logging of request and response to match both<br/>

### Fixed 
**DatabaseFixture** fix timestamp format in 'getTimestamp' to take into account 24h instead of 12h system


## [2.0.3]
### Updated 
**SlimWebDriver** with code method to extract the current webdriver

## [2.0.2]
### Updated 
**SlimWebDriver** with code to satisfy instantiation without triggering unwanted side effects while inheriting or unit testing.

## [2.0.1] 2019-05-16 
### Updated
**FIX** guava dependency update to 25.0-jre, matching the version used in selenium 3.141.59 <br/>
**general** FitNesse verson update to 20190428

## [2.0.0] 2019-04-29
### Updated 
**general** uses OpenJDK11  <br/>
**general** restructured -rest module to include soap functionality, renamed to -api  <br/>
**general** updated documentation - Doxygen  <br/>
**SoapMessageBuilder** added the command to add `attributes` to SOAP eleemmts

### Removed
**RestassuredFixture** was @Deprecated <br/>
**StringUtilFixture** was @Deprecated <br/>
**documentation module and documentation-plugin** <br/>




## [1.2.1] 2019-02-04 
### Updated
GENERAL - replaced ConditionalExceptionType.fail with ConditionalException;
GENERAL - removed fitnesse.responders.editing.fixture.Command/Fixture/Start imports and annotations
REMOVED - all classes in package fitnesse.responders.editing.fixture
GENERAL - switched from org.apache.log4j to org.slf4j for logging
GENERAL - refactor after SonarLint analysis

FileUtils - added createTempFile
FileUtils - when a file is loaded the last CR/LF characters are removed (chomp)

XmlNavigateFixture - updated returntype to void with methods that needlessly returned a boolean
XmlNavigateFixture - more logging

OperationsFixture - refactor

SlimWebDriver - removed dependency on org.reflection and implementation of InteractionAwareFixture for SlimWebDriver
FitnesseSystemSettings - refactor

VariablesFixture - added generateUnicodeCodepointString / generateUnicodeCodepointStringFromWithLength / generateUnicodeCodepointStringFromTo



## [1.2.0] 2018-03-18

### Updated
- Java classes that use selenium and fitnesse were both present in the module psek-fitnesse-runner. This has been refactored, the selenium classes were moved to module psek-fitnesse-fixtures-selenium.

  *To use this version in your existing project you need to update the following* (Or use version 1.2.0 of the archetype https://gitlab.com/PascalSmeets/psek-fitnesse-project-archetype)

1) add the following to the pom.xml of your testproject when you update to `1.2.0`.

  ```
   <properties>
      <psek-fitnesse-fixtures-selenium.version>${psek-fitnesse-fixtures.version}</psek-fitnesse-fixtures-selenium.version>
   </properties>

   <dependencies>
      <dependency>
         <groupId>nl.psek.fitnesse</groupId>
         <artifactId>psek-fitnesse-fixtures-selenium</artifactId>
         <version>${psek-fitnesse-fixtures-selenium.version}</version>
      </dependency>
   </dependencies>
   ```
2) In the SuiteSetup add this line to the `import` table: `|nl.psek.fitnesse.fixtures.selenium|`

3) In the `plugins.properties` file `\src\test\resources\plugins.properties` remove this line: `Responders = commandData:fitnesse.responders.editing.CommandResponder`


- Selenium updated to <a href="https://raw.githubusercontent.com/SeleniumHQ/selenium/master/java/CHANGELOG" target="_blank">3.11.0</a>
- FitNesse updated to <a href="http://fitnesse.org/.FrontPage.FitNesseDevelopment.FitNesseRelease20180127" target="_blank">20180127</a>
- Removed some customization of FitNesse
- Refactored documentation module to work with the new .wiki style pages and the older content.txt files

### Fixed
- **JsonNavigateFixture** fixed method countNumberOfTimesPresent

## [1.1.11] 2018-03-08
### Updated
- **<a href="/reference/soap/soapwebservice-fixture/">SOAPWebserviceFixture</a>** added SSL support using JKS KeyStores and TrustStores. Fixed logging of exceptions while sending the message.
- **RestFixture** support for multi-value parameters (key=[value1,value2,value3])

### Added
- **BuildJsonStringFixture** new fixture to generate a json message in a testcase.

## [1.1.10] 2018-01-12
## Added
- **SOAPWebserviceFixture** new commands: 'addNamespaceToEnvelope, addChildElementToHeader, setAttribute'
- **VariablesFixture** command to give a random entry from a comma-separated-list `|$randomentry=|giveEntry;|fiets,auto,boot,trein,tram,metro|`
## Updated
- **DatabaseFixture/SqlExecutor** updated executeSqlString to return SQLExceptions to FitNesse; added executeSqlStringWithReturnValue, this allows for checking the number of affected rows in a Fitnesse test; added more logging tot FitNesse execution log

    |check |executeSqlStringWithReturnValue; |h2 |!-DELETE FROM Persons WHERE Address LIKE '%Alder%'-! |>0 |


## [1.1.9] 2017-12-15

## Added
- **SlimWebDriver** added Chrome headless mode. Chrome browser can be used in headless mode on the local system. This is controlled by the following properties in [selenium.properties](/project-template/selenium-properties/#chromeheadless): ['webdriver.chrome.headless=true'](/project-template/selenium-properties/#chromeheadless) and ['webdriver.chrome.headless.window-size=1600x1600'](/project-template/selenium-properties/#chromeheadless).
- **SlimWebdriver** added logThis (equal to logThis from OperationsFixture) and logPageSource
- **SlimWebDriver** added setLoopCommand toegevoegd fot SlimWebDriver command's with 3 parameters like |selectUsingLabel;|id=mijnlijst|Deze|allowDefault|
- **SlimWebDriver** added takeScreenshotOnException, allows for control over the automatic screenshots on exceptions
- **SlimWebDriver** added verifyPatternPresent, this method can use a <a href='/reference/parameters/pattern/'>regular expression</a> to find text on the webpage
- **Stringfixture** added getSubstringWithRegex toegevoegd

## Updated
- **DatabaseFixture** fix for errors with push fixture / pull fixture and the import table after using the DatabaseFixture. Removed usage of SystemUnderTest.
- **DatabaseFixture**  updated activateResultSet to lof the resultSet to the System.out logging
- **Documentation plugin** All !- and -! character sequences are removed in the output of the following doenumentation parts: TESTCASE, PUROPOSE en DOCUMENTATION BLOCK. This allows for using HTML in a testcase (between !- -!) that will be rendered correctly on the FitNesse wiki page and in the generated documentation file.
- **OperationsFixture** updated logThis to support linebreaks, use the string **#LINEBREAK#** in your input.
- **SlimWebDriver** updated waitForTextPresent with support for regular expressions using the (<a href='/reference/parameters/pattern/'>pattern</a>) parameter
- **SlimWebdriver** updated more logging with errors (url, cookies en pagesource)
- **SlimWebDriver** the click command tries to scroll to an element is an ElementNotVisibleException occurs.
- **Variables Fixture** convertNumberCharsToAlpha now supports Strings that already contain Alphabetic characters
- other small changes

## [1.1.8] 2017-10-10

### Changed
- **SlimWebDriver** removed auto-adjustment of scaling of the screenshot for high-resolution screens. Added support for customization using a property value "screen.dpr" in the selenium.properties file. Alternatively the value can be set during test execution using the command `setScreenDpr`.

## [1.1.7] 2017-09-30
### Added

- new fixture **LdapFixture** for basic communication with LDAP servers. Requires a specific implementation in your own project.
- **JsonNavigateFixture** kan use <a href='/reference/parameters/pattern/'>regular expressions</a> in methods _checkValueOfJsonElement_, _verifyValueOfJsonElement_ and _verifyJsonElementContainsString_
- **FileUtils** added [copyFile](/reference/filesanddata/fileutils-fixture/#manipulating-files) method
- **FileUtils** added for files on samba shares using NTLM authentication: copyFileToSMBShare and verifySMBFileExists. See [Networked Files](/reference/filesanddata/fileutils-fixture/#networked-files)
- **RestFixture** proxy support [description in 'specifying-the-request'] (/reference/rest/rest-fixture/#specifying-the-request)

### Changed
- update RestAssured version to 3.0.3


## [1.1.6] 2017-07-04
### Fixed
- **SlimWebDriver** fix in WebDriverScreenshot method determineDevicePixelRatio, casting of double to float

### Added
- **SlimWebdriver** added command takeScreenshotOnException to enable this, the new default value (if left unset) is false

## [1.1.5] 2017-06-21
### Fixed
- **SlimWebdriver** fix in WebDriverScreenshot method determineDevicePixelRatio

## [1.1.4] 2017-06-15
### Added
- **RestFixture** added option to use [relaxedHTTPSValidation](https://github.com/rest-assured/rest-assured/wiki/Usage#ssl)
- **FileUtils** added verifyFileExists and getFileSize methods

### Changed
- updated log messages for FixtureOperations.matchOnPattern and FixtureOperations.containsPattern
- **SlimWebDriver** can use multiple properties files, just enumarate them in a comma-separated list when starting SlimWebDriver

## [1.1.3] 2017-05-26
### Added

- added debug logging to *FixtureOperations.matchOnPattern*
- added *FixtureOperations.containsPattern* (used in PdfTextReaderFixture)
- added *OperationsFixture.getLength*

### Changed

- refactored taking screenshots using AShot:
  - added WebdriverScreenshot class for AShot methods
  - auto detection of DevicePixelRatio
  - auto selection of AShot ShootingStrategy based on WebDriver Browser setting
  - added customizable ScrollTimeout option used in takeADefaultScreenshot methods
- **SlimWebDriver** updated filenames of Screenshots taken when an exception occurs to indicate that testing was continued or stopped.
- update org.apache.httpcomponents.httpclient to version 4.5.3
- update org.apache.pdfbox.pdfbox to version 2.0.5
- updated nl.psek.fitnesse.Downloader with support for downloads from other fixtures than SlimWebDriver using download(URL)
- updated nl.psek.fitnesse.Downloader with support for other download folder using 'setDownloadPath'
- updated nl.psek.fitnesse.fixtures.general.util.PDFUtils.loadPdf to work with nl.psek.fitnesse.Downloader (load from URL was removed in PDFBox 2.x)
- updated nl.psek.fitnesse.fixtures.general.util.PDFUtils removed encoding from 'PDFTextStripper'
- updated nl.psek.fitnesse.fixtures.general.pdf.PdfTextReaderFixture

### Deprecated
- removed deprecated code from **SOAPWebserviceFixture**
- removed unused imports

## [1.1.2] 2017-05-12
### Fixed
- **PdfTextReaderFixture** pdfContainsTextLineThatContainsText corrected command name

### Added
- **PDFUtils** - added encoding to PDFTextStripper

### Changed
- **SlimWebDriver** - refactored file download commands: ‘downloadFile’, ‘downloadImage’, support for proxy’s with basic and NTLM authentication.


