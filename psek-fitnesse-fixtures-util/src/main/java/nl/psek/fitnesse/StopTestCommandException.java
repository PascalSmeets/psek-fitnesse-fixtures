package nl.psek.fitnesse;

public class StopTestCommandException extends CommandException {

    public StopTestCommandException(String message, Object... args) {
        super(message, args);
    }

    public StopTestCommandException(Exception exception) {
        super(exception);
    }
}
