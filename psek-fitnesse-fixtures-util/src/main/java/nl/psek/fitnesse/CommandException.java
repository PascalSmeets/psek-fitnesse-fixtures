package nl.psek.fitnesse;

/**
 * @author Ronald Mathies
 */
public class CommandException extends RuntimeException {

    public CommandException(String message, Object... args) {
        super("message:<< " + String.format(message, args) + " >>");
    }

    public CommandException(Exception exception) {
        super("message:<<" + String.format(exception.toString()) + ">>");
    }

}
