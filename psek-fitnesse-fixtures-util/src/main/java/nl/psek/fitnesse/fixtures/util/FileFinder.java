package nl.psek.fitnesse.fixtures.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Recursively searches the user.dir path for the given filename and calls
 * the FileHandler when the file is found.
 *
 * @author Ronald Mathies
 */
public final class FileFinder {

    private enum Type {
        /**
         * File type.
         */
        FILE,
        /**
         * Folder type.
         */
        FOLDER
    }

    /**
     * The constant PROP_USER_DIR.
     */
    public static final String PROP_USER_DIR = "user.dir";

    private FileFinder() {
    }

    /**
     * Find files matching expression.
     *
     * @param expression the expression
     * @param handler    the handler
     */
    public static void findFilesMatchingExpression(final String expression, final FileHandler handler) {
        findFilesMatchingExpression(System.getProperty(PROP_USER_DIR), expression, handler);
    }

    /**
     * Find files matching expression.
     *
     * @param directory  the directory
     * @param expression the expression
     * @param handler    the handler
     */
    public static void findFilesMatchingExpression(final String directory, final String expression, final FileHandler handler) {
        final Path startPath = Paths.get(new File(directory).toURI());

        try {
            Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                    File file = path.toFile();

                    if (file.getName().matches(expression)) {
                        handler.handle(file);
                    }

                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new IllegalArgumentException("FileFinder: error finding files matching '" + expression + "'.");
        }
    }

    /**
     * Recursively searches the user.dir path for the given filename and calls
     * the FileHandler when the file is found.
     *
     * @param filename the filename to search for.
     * @param handler  the handler which will be called when the file is found.
     * @throws IllegalArgumentException when the file cannot be located.
     */
    public static void findFileInUserDirectory(final String filename, final FileHandler handler) {
        findObjectInUserDirectory(Type.FILE, filename, handler);
    }

    /**
     * Recursively searches the user.dir path for the given directory name and calls
     * the FileHandler when the file is found.
     *
     * @param directory the directory to search for.
     * @param handler   the handler which will be called when the file is found.
     * @throws IllegalArgumentException when the file cannot be located.
     */
    public static void findDirectoryInUserDirectory(final String directory, final FileHandler handler) {
        findObjectInUserDirectory(Type.FOLDER, directory, handler);
    }

    /**
     * Find object in user directory.
     *
     * @param type    the type
     * @param name    the name
     * @param handler the handler
     */
    public static void findObjectInUserDirectory(final Type type, String name, final FileHandler handler) {
        Path startPath = getPathToFolder(System.getProperty(PROP_USER_DIR));

        if (name.startsWith("/")) {
            startPath = new File(startPath.toFile(), name.substring(0, name.lastIndexOf("/"))).toPath();
            name = name.substring(name.lastIndexOf("/") + 1);
        }

        final String[] filename = {name};
        final boolean[] result = {false};
        try {
            Files.walkFileTree(startPath, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    if (type == Type.FOLDER) {
                        if (attrs.isDirectory() && dir.getFileName().toString().equalsIgnoreCase(filename[0])) {
                            result[0] = true;
                        }

                        if (result[0]) {
                            handler.handle(dir.toFile());
                        }
                    }

                    return result[0] ? FileVisitResult.TERMINATE : FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                    File file = path.toFile();
                    if (type == Type.FILE) {
                        if (attrs.isRegularFile() && file.getName().equalsIgnoreCase(filename[0])) {
                            result[0] = true;
                        }
                    }

                    if (result[0]) {
                        handler.handle(file);
                    }

                    return result[0] ? FileVisitResult.TERMINATE : FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new IllegalArgumentException("FileFinder: error finding " + (type == Type.FOLDER ? "directory" : "file") + " with name '" + filename[0] + "'.");
        }

        if (!result[0]) {
            throw new IllegalArgumentException("FileFinder: error finding " + (type == Type.FOLDER ? "directory" : "file") + " with name '" + filename[0] + "'.");
        }
    }

    private static Path getPathToFolder(String directory) {
        return Paths.get(new File(directory).toURI());
    }


}
