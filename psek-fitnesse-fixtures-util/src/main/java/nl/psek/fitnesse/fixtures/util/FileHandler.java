package nl.psek.fitnesse.fixtures.util;

import java.io.File;

/**
 * Must be used in conjunction with the <code>FileFinder</code>.
 *
 * @author Ronald Mathies
 */
public interface FileHandler {

    /**
     * Method is called when the <code>filename</code> passed into the <code>FileFinder</code>
     * is found in the user.dir path.
     *
     * @param file the file that was found.
     */
    void handle(File file);
}
