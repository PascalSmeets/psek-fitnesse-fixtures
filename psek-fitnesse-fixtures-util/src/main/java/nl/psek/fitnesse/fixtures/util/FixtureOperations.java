package nl.psek.fitnesse.fixtures.util;


import nl.psek.fitnesse.StopTestCommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A helper for multiple fixtures
 */
public class FixtureOperations {

    private static final Logger LOG = LoggerFactory.getLogger(FixtureOperations.class);
    private static final String REGEXPI_SYNTAX = "regexpi:";
    private static final String REGEX_SYNTAX = "regex:";
	private static final String PROP_SYNTAX = "p:";


	public static String getFromPropertyIfApplicable(String input) {
		if (input.startsWith(PROP_SYNTAX)) {
			String key = input.substring(PROP_SYNTAX.length());
			try {
				return System.getProperty(key);
			} catch (NullPointerException e) {
				throw new StopTestCommandException("Loading of a system property with key {} failed with a NullPointerException.", key);
			}
		}
		return input;
	}

    public static boolean equal(final Object input1, final Object input2) {
        if (input1 == null && input2 == null) {
            LOG.info("Both inputs are 'null'. We'll return TRUE because null == null");
            return true;
        }

        if (input1 != null && input2 == null) {
            throw new IllegalArgumentException("The second input is 'null'.");
        }

        if (input1 == null) {
            throw new IllegalArgumentException("The first input is 'null'.");
        }

        return input1.toString().equals(input2.toString());

    }

    /**
     * Match on pattern boolean.
     *
     * @param input   the input
     * @param pattern the pattern
     * @return the boolean
     */
    public static boolean matchOnPattern(String input, String pattern) {
        LOG.debug("Starting pattern matcher complete match for input '" + input + "' and pattern '" + pattern + "'");
        if (pattern.startsWith(REGEXPI_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEXPI_SYNTAX.length()), Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
            boolean match = compiledPattern.matcher(input).matches();
            LOG.debug("Matching using regular expression, case-insensitive. Match = " + match);
            return match;
        } else if (pattern.startsWith(REGEX_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEX_SYNTAX.length()), Pattern.DOTALL);
            boolean match = compiledPattern.matcher(input).matches();
            LOG.debug("Matching using regular expression, case-sensitive. Match = " + match);
            return match;
        } else {
            boolean match = equal(input, pattern);
            LOG.debug("Matching using String.equals method. Match = " + match);
            return match;

        }

    }

    /**
     * Find on regex and return result string.
     *
     * @param input   the input
     * @param pattern the pattern
     * @return the string
     */
    public static String findOnRegexAndReturnResult(String input, String pattern) {
        // Compile and use regular expression
        Pattern patt = Pattern.compile(pattern);
        Matcher matcher = patt.matcher(input);
        boolean matchFound = matcher.find();
        String groupStr = "";

        if (matchFound) {
            // Get all groups for this match
            for (int i = 0; i <= matcher.groupCount(); i++) {
                groupStr = matcher.group(i);
            }

        }
        return groupStr;
    }

    /**
     * Contains pattern boolean.
     *
     * @param input   the input
     * @param pattern the pattern
     * @return the boolean
     */
    public static boolean containsPattern(String input, String pattern) {
        LOG.debug("Starting pattern matcher partial match for input '" + input + "' and pattern '" + pattern + "'");
        return containsPatternNoInputLogging(input, pattern);

    }

    /**
     * Contains pattern no input logging boolean.
     *
     * @param input   the input
     * @param pattern the pattern
     * @return the boolean
     */
    public static boolean containsPatternNoInputLogging(String input, String pattern) {

        if (pattern.startsWith(REGEXPI_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEXPI_SYNTAX.length()), Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
            boolean partialmatch = compiledPattern.matcher(input).find();
            LOG.debug("Partial matching using regular expression, case-insensitive. Partial match = " + partialmatch);
            return partialmatch;
        } else if (pattern.startsWith(REGEX_SYNTAX)) {
            Pattern compiledPattern = Pattern.compile(pattern.substring(REGEX_SYNTAX.length()), Pattern.DOTALL);
            boolean partialmatch = compiledPattern.matcher(input).find();
            LOG.debug("Partial matching using regular expression, case-sensitive. Partial match = " + partialmatch);
            return partialmatch;
        } else {
            boolean partialmatch = input.contains(pattern);
            LOG.debug("Partial matching using String.contains method. Partial match = " + partialmatch);
            return partialmatch;
        }

    }

}
