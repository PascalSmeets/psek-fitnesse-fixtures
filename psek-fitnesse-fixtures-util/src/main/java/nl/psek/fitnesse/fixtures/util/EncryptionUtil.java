package nl.psek.fitnesse.fixtures.util;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;

/**
 * A fixture that can be used to encrypt data using AES encryption
 */
public class EncryptionUtil {

    private static final String DIGEST_METHOD = "SHA-1";
    private static final String ENCRYPTION_ALGORITHM = "AES";
    private static final String CIPHER_ALGORITHM_SETTINGS = "AES/CBC/PKCS5Padding";
    private String encryptionPassword;
    private String base64InitVector;

    /**
     * Ingang om het wachtwoord dat gebruikt wordt om de k
     * ey op te bouwen in te stellen via de FitNesse gui
     *
     * @param encryptionPassword het wachtwoord dat gebruikt wordt om de AES key op te bouwen
     */
    public void setPassword(String encryptionPassword) {
        this.encryptionPassword = getFromPropertyIfApplicable(encryptionPassword);
    }

    /**
     * Opslaan van de initialization vector (https://en.wikipedia.org/wiki/Initialization_vector) in eeb Base64 string om mee te geven
     *
     * @param initVectorByteArray initialization vector
     */
    private void setInitVector(byte[] initVectorByteArray) {
        this.base64InitVector = encodeToBase64(initVectorByteArray);
    }

    /**
     * Genereren van de IvParameterSpec die gebruikt wordt in de AES encryptie,
     * hierbij genereren we ook een random initialization vector (ook wel starting variable of nonce genoemd) die we later ook nodig hebben bij de decryptie
     *
     * @return de AES IvParameterSpec
     */
    private IvParameterSpec generateIvParamSpec() {
        SecureRandom random = new SecureRandom();
        byte[] initVectorByteArray = new byte[128 / 8];
        random.nextBytes(initVectorByteArray);
        setInitVector(initVectorByteArray);
        return new IvParameterSpec(initVectorByteArray);
    }

    /**
     * Het encryptionPassword wordt met SHA-1 gehashed, van deze hash worden de eerste 128 bits (16 bytes) gebruikt als secret key.
     *
     * @param encryptionPassword het wachtwoord dat gebruikt wordt om de AES key op te bouwen
     * @return de AES SecretKeySpec
     */
    private SecretKeySpec loadSecretKey(String encryptionPassword) {
        try {
            byte[] key = (encryptionPassword).getBytes(StandardCharsets.UTF_8);
            MessageDigest sha = MessageDigest.getInstance(DIGEST_METHOD);
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16); // use only first 128 bit
            return new SecretKeySpec(key, ENCRYPTION_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    /**
     * Bouw het AES Cipher voor de gespecificeerde modus en geef het terug
     * Cipher.ENCRYPT_MODE = int 1
     * Cipher.DECRYPT_MODE = int 2
     *
     * @param secretKeySpec   de AES SecretKeySpec
     * @param ivParameterSpec de AES IvParameterSpec
     * @return het AES cypher
     */
    private Cipher getCipher(int cipherMode, SecretKeySpec secretKeySpec, IvParameterSpec ivParameterSpec) {
        try {
            Cipher ci = Cipher.getInstance(CIPHER_ALGORITHM_SETTINGS);
            ci.init(cipherMode, secretKeySpec, ivParameterSpec);
            return ci;
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchPaddingException | NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    /**
     * Voer de daadwerkelijke AES-128 bit encoding uit
     *
     * @param plainText de te versleutelen tekst
     * @return versleutelde tekst die bestaat uit `base64 initialization vector` + `:` + `base64 cypherText`
     */
    public String encode(String plainText) {
        try {
            // opbouwen van het cypher, met random IvParameterSpec
            SecretKeySpec secretKeySpec = loadSecretKey(this.encryptionPassword);
            IvParameterSpec ivParameterSpec = generateIvParamSpec();
            Cipher cipher = getCipher(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            // plainText omzetten naar byte []
            byte[] input = plainText.getBytes(StandardCharsets.UTF_8);

            // de daadwerkelijke versleuteling
            byte[] cypherText = cipher.doFinal(input);

            // als antwoord voegen we de initialization vector en cypherText samen, base64 gecodeerd
            return this.base64InitVector + ":" + encodeToBase64(cypherText);

        } catch (BadPaddingException | IllegalBlockSizeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }

    }

    /**
     * Voer de daadwerkelijke AES-128 bit decodering uit
     *
     * @param ivAndEncodedTextInBase64 de versleutelde tekst die bestaat uit `base64 initialization vector` + `:` + `base64 cypherText`
     * @return plainText string
     */
    public String decode(String ivAndEncodedTextInBase64) {
        try {
            // splitsen van initialization vector en cypherText
            String[] parts = ivAndEncodedTextInBase64.split(":");

            // base64 decodering
            byte[] iv = decodeFromBase64(parts[0]);
            byte[] cypherText = decodeFromBase64(parts[1]);

            // opbouwen van het cypher, met IvParameterSpec gebaseerd op meegeleverde initialization vector
            SecretKeySpec secretKeySpec = loadSecretKey(this.encryptionPassword);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);
            Cipher cipher = getCipher(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

            // de daadwerkelijke ontsleuteling
            byte[] decoded = cipher.doFinal(cypherText);

            // als antwoord geven we de plaintext terug
            return new String(decoded, StandardCharsets.UTF_8);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private String encodeToBase64(byte[] input) {
        return Base64.getEncoder().encodeToString(input);
    }

    private byte[] decodeFromBase64(String input) {
        return Base64.getDecoder().decode(input);
    }

}
