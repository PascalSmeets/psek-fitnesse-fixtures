package nl.psek.fitnesse;

import nl.psek.fitnesse.util.FitnesseSystemSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ConditionalException extends RuntimeException {

    private static final Logger LOG = LoggerFactory.getLogger(ConditionalException.class);

    public ConditionalException(String message, Object... args) {
        if (FitnesseSystemSettings.stopTestOnException()) {
            LOG.error("Stopping test execution, throwing StopTestCommandException: {} ", message);
            throw new StopTestCommandException(message, args);
        } else {
            LOG.error("Continuing test execution, throwing CommandException: {} ", message);
            throw new CommandException(message, args);
        }
    }

    public ConditionalException(Exception ex) {
        if (FitnesseSystemSettings.stopTestOnException()) {
            LOG.error("Stopping test execution, throwing StopTestCommandException: {} ", ex.getMessage());
            throw new StopTestCommandException(ex);
        } else {
            LOG.error("Continuing test execution, throwing CommandException: {} ", ex.getMessage());
            throw new CommandException(ex);
        }
    }
}
