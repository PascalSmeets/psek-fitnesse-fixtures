package nl.psek.fitnesse.util;

/**
 * Created by Ronald on 13-11-2015.
 */
public class FitnesseSystemSettings {

    private static final String STOPTESTPROPERTYKEY = "FSTOPONEXCEPTION";

    private static final String SYSTEM_FITNESSE_SKIP_TEST_KEY = "fitnesseSkipTest";


    /**
     * returns the value of the STOPTESTPROPERTYKEY System.Property
     * If not set, false will be returned
     *
     * @return boolean
     */
    public static boolean stopTestOnException() {
        return Boolean.valueOf((System.getProperty(STOPTESTPROPERTYKEY)));
    }

    /**
     * Sets the value of the System.Property for STOPTESTPROPERTYKEY
     *
     * @param value boolean
     */
    public static void setStopTestOnException(boolean value) {
        System.setProperty(STOPTESTPROPERTYKEY, String.valueOf(value));
    }


    public static boolean executeTests() {
        String property = System.getProperty(SYSTEM_FITNESSE_SKIP_TEST_KEY);
        if (property == null) {
            return false;
        }

        return !Boolean.valueOf(property);
    }

}
