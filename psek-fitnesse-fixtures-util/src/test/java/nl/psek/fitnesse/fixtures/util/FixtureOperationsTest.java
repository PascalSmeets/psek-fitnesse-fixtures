package nl.psek.fitnesse.fixtures.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.equal;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Pascal on 21-7-2015.
 */
public class FixtureOperationsTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void equalTest() {
        assertTrue(equal(1, 1));
        assertFalse(equal(1, 0));
    }

    @Test
    public void equalThrowsExceptionTest() throws IllegalArgumentException {
        thrown.expect(IllegalArgumentException.class);
        assertTrue(equal(1, null));
    }
}