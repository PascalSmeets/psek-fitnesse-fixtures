package nl.psek.fitnesse.fixtures.util;

import org.junit.Assert;
import org.junit.Test;

public class EncryptionUtilTest {

    private String plaintext = "this is the plaintext, eęciī";
    private EncryptionUtil util = new EncryptionUtil();

    @Test
    public void checkThatDecodedTextIsSameAsInputTest() {
        util.setPassword("PoDfV3U1r0ACWrP92IhQx6yrAeOeuR58KiyCaerWw3KJbVDwLF7MJbcLr7WCVYb");
        String encoded = util.encode(plaintext);
        String decoded = util.decode(encoded);
        Assert.assertEquals("plaintext is  the same as the decoded text", plaintext, decoded);
    }

    @Test(expected = IllegalArgumentException.class)
    public void checkThatDecryptionFailsWithWrongPassword() {
        util.setPassword("aaaaaaaaaaaa");
        String encoded = util.encode(plaintext);

        util.setPassword("aaaaaaaaaaab");
        String decoded = util.decode(encoded);

    }
}

