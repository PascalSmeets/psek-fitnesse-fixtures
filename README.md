<em>Fixtures for use in a java based testproject.</em>

The code is tailored for FitNesse, but almost all methods can be used by any testproject.

<strong>from version 1.2.1 Browserstack support is added</strong>
To use this now read [these instructions](https://gitlab.com/PascalSmeets/psek-fitnesse-fixtures/commit/bc247d53fba44f5dc4470ec3340080589bd9eb02) or check out the reference site when the new version is released to Maven Central.

_Using browserstack is as simple as defining the correct selenium hub (with your credentials) and setting the desired capabilities!_

<p align="center"><a href="http://browserstack.com/" target="_blank"><img src="https://gitlab.com/PascalSmeets/psek-fitnesse-fixtures/raw/master/resources/browserstack-logo-600x315.png" /></a></p>


More information on the specific fixtures can be found in the [Manual](@ref manual).

<em>The modules provide functionality for:</em>
<dl>
    <dt><strong>api module</strong></dt>
    <dd>REST API and SOAP webservice tests</dd>
    <dt><strong>database module</strong></dt>
    <dd>Databases and SQL</dd>
    <dt><strong>selenium module</strong></dt>
    <dd>Selenium Webdriver</dd>
    <dt><strong>general module</strong></dt>
    <dd>Generating testdata, using json and xml, working with files, pdf documents ... and more</dd>
    <dt><strong>ldap module</strong></dt>
    <dd>Basic LDAP functionality that you can extend in your own classes</dd>
    <dt><strong>util module</strong></dt>
    <dd>classes used by the other modules</dd>
    <dt><strong>runner module</strong></dt>
    <dd>classes specific for FitNesse</dd>
</dl>
