package nl.psek.fitnesse.fixtures.database;

import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.fixtures.general.properties.PropertiesFixture;
import nl.psek.fitnesse.fixtures.general.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.matchOnPattern;

/**
 * A fixture to query and update a database using a jdbc connection.
 *
 * @author Ronald Mathies
 */
public class DatabaseFixture {

    private static final Logger LOG = LoggerFactory.getLogger(DatabaseFixture.class);
    private static final String CONNECTION_DATABASE_DRIVER = "database.driver";
    private static final String CONNECTION_DATABASE_URL = "database.url";
    private static final String CONNECTION_DATABASE_USERNAME = "database.username";
    private static final String CONNECTION_DATABASE_PASSWORD = "database.password";
	private static long pollingTime;
    /**
     * The Active result set manager.
     */
    public ResultSetManager activeResultSetManager = null;
	public static String resultSetString = "";
    private Map<String, Connection> connections = new HashMap<String, Connection>();

    private Map<String, ResultSetManager> resultSetManagers = new HashMap<>();

    private List<String> parameters = new ArrayList<>();

    /**
     * Constructs a new database fixture.
     */
    public DatabaseFixture() {
    }

    /**
     * Creates a new database connection with the specified name and properties.
     *
     * @param name       the name for the connection.
     * @param properties the properties file containing the connection settings.
     */
    @Deprecated
    public void createConnection(String name, String properties) {
        Connection connection = createAConnection(properties);
        connections.put(name, connection);
    }


    /**
     * Creates a new database connection with the specified name the credentials are taken directly OR from system.properties
     *
     * @param name an alias for this connection
     * @param databaseDriver the database driver name as a string
     * @param url the URL to connect to, provided als plain text or with prefix 'p:' with a string that points to a System.property
     * @param username the username, provided als plain text or with prefix 'p:' with a string that points to a System.property
     * @param password the password , provided als plain text or with prefix 'p:' with a string that points to a System.property
     */
    public void createConnection(String name, String databaseDriver, String url, String username, String password) {
        Connection connection = createAConnection(databaseDriver, url, username, password);
        connections.put(name, connection);
    }


    /**
     * Creates a new database connection with the specified name the credentials are taken from system.properties
     * @param name an alias for this connection
     * @param databaseDriver the database driver name as a string
     * @param systemPropertyForUrl the key of the system property that contains the url
     * @param systemPropertyForUsername the key of the system property that contains the username
     * @param systemPropertyForPassword the key of the system property that contains the password
     */
    @Deprecated
    public void createConnectionUsingSystemProperties(String name, String databaseDriver, String systemPropertyForUrl, String systemPropertyForUsername, String systemPropertyForPassword) {
        Connection connection = createAConnectionUsingSystemProperties(databaseDriver,systemPropertyForUrl, systemPropertyForUsername, systemPropertyForPassword);
        connections.put(name, connection);
    }

    /**
     * Performs a query on the database using the connection with the specified name.
     *
     * @param name the name of the connection to use.
     * @param sql  the sql statement to execute.
     */
    public void performSelect(String name, String sql) {
        Connection connection = findConnectionByName(name);

        try {
            PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            setPreparedStatementParameters(statement);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSetManagers.containsKey(name)) {
                resultSetManagers.put(name, new ResultSetManager());
            }

            ResultSetManager resultSetManager = resultSetManagers.get(name);
            resultSetManager.setResultSet(resultSet);

            this.activeResultSetManager = resultSetManager;
        } catch (SQLException e) {
            throw new ConditionalException("Failed to execute query '%s' due to the following '%s'.", sql, e.getMessage());
        }
    }

    private void setPreparedStatementParameters(PreparedStatement statement) throws SQLException {
        for (int i = 0; i < parameters.size(); i++) {
            statement.setObject(i + 1, parameters.get(i));
        }
        parameters.clear();
    }

    /**
     * Activates a result set, that was initiated by a previous query executed.
     *
     * @param name the name of the connection that initiated a result set.
     */
    public void activateResultSet(String name) {
        if (!resultSetManagers.containsKey(name)) {
            throw new ConditionalException("No active result set found for connection '%s'.", name);
        }

        this.activeResultSetManager = resultSetManagers.get(name);
        try {
            LOG.info("===== BEGIN Resultset " + name + " =====");
            LOG.info("aantal rijen = " + getNumberOfRows());
            printResultSet(activeResultSetManager.getResultSet());
            LOG.info("======= END Resultset " + name + " =====");
        } catch (Exception e) {
        }

    }


    /**
     * Print result set.
     *
     * @param rs the rs
     * @throws SQLException the sql exception
     */
    public static void printResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        rs.beforeFirst();
        int columnsNumber = rsmd.getColumnCount();
        LOG.info("aantal kolommen = " + columnsNumber);
        StringBuilder line = new StringBuilder();

		for (int i = 1; i <= columnsNumber; i++) {
			String name = rsmd.getColumnName(i);

			if (i > 1) {
				line.append(" | ");
			}
			line.append(rsmd.getColumnLabel(i));

		}
		line.append(System.lineSeparator());

        while (rs.next()) {
            for (int i = 1; i <= columnsNumber; i++) {

                if (i > 1) {
                    line.append(" | ");
                }

                line.append(rs.getString(i));
            }
            line.append(System.lineSeparator());
        }
        LOG.info(line.toString());
		resultSetString = line.toString();

	}

	public String getActiveResultSetAsString() throws SQLException {
		return resultSetString;
    }


    /**
     * Retreives a value (the specified column) from the resultset at the given row..
	 * <p>
	 */
	public Object getValue(int index, String column) throws ConditionalException {
		return this.activeResultSetManager.getValue(index,column);
	}


	/**
	 * Gets date.
     *
     * @param index  the index
     * @param column the column
	 * @return the date
	 * @throws ConditionalException the conditional exception
     */
	public String getDate(int index, String column) throws ConditionalException {
		return this.activeResultSetManager.getDate(index,column);
    }


    /**
	 * Gets timestamp.
     *
     * @param index   the index
     * @param column  the column
	 * @return the timestamp
	 * @throws ConditionalException the conditional exception
     */
	public String getTimestamp(int index, String column) throws ConditionalException {
		return this.activeResultSetManager.getTimestamp(index,column);
	}

    /**
     * Verifies a value (the specified column) from the resultset at the given row..
     *
     * @return true / false
     */
	public boolean verifyValue(int index, String column, String pattern) throws ConditionalException {
        return this.activeResultSetManager.verifyValue(index, column, pattern);
    }

    /**
     * Retreives a value (the specified column) from the resultset at the given row..
     *
     * @return the number of rows
     */
    public int getNumberOfRows() {
        return this.activeResultSetManager.getNumberOfRows();
    }


    /**
     * Verify number of rows boolean.
     *
     * @param expectedNumberOfRows the expected number of rows
     * @return the boolean
     */
    public boolean verifyNumberOfRows(int expectedNumberOfRows) {
        return this.activeResultSetManager.verifyNumberOfRows(expectedNumberOfRows);
    }

    /**
     * Performs a query on the database using the connection with the specified name.
     * <p>
     * The query should result in a single row with a single column, in case of more
     * columns then only the first column will be returned.
     *
     * @param name the name of the connection to use.
     * @param sql  the sql statement to execute.
     * @return the resultset.
     */
    public Object performSelectSingleResult(String name, String sql) {
        Connection connection = findConnectionByName(name);

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            setPreparedStatementParameters(statement);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getObject(1);
            }

            return "#[NULL]#";
        } catch (SQLException | NullPointerException e) {
            throw new ConditionalException("Failed to execute query '%s' due to the following '%s'.", sql, e.getMessage());
        }
    }


    /**
     * Perform select single result until result present boolean.
     *
     * @param name           the name
     * @param sql            the sql
     * @param expectedResult the expected result
     * @param maxAttempts    the max attempts
     * @return the boolean
     * @throws InterruptedException the interrupted exception
     * @throws ConditionalException the conditional exception
     */
    public boolean performSelectSingleResultUntilResultPresent(String name, String sql, String expectedResult, int maxAttempts) throws InterruptedException, ConditionalException {
        int milliSecondsBetweenAttempt = 1000;
        return performSelectSingleResultUntilResultPresent(name, sql, expectedResult, maxAttempts, milliSecondsBetweenAttempt);

    }


    /**
     * Perform select single result until result present boolean.
     *
     * @param name                       the name
     * @param sql                        the sql
     * @param pattern                    the pattern
     * @param maxAttempts                the max attempts
     * @param milliSecondsBetweenAttempt the milli seconds between attempt
     * @return the boolean
     * @throws InterruptedException the interrupted exception
     * @throws ConditionalException the conditional exception
     */
    public boolean performSelectSingleResultUntilResultPresent(String name, String sql, String pattern, int maxAttempts, int milliSecondsBetweenAttempt) throws InterruptedException, ConditionalException {

		LOG.info("POLLING IN DATABASE WITH QUERY ");
		logQuery(sql);
		LOG.info("POLLING IN DATABASE WITH COMPARISON PATTERN: " + pattern);
		LOG.info("POLLING IN DATABASE WITH TIME BETWEEN ATTEMPTS: " + milliSecondsBetweenAttempt + " milliseconds.");

		long startTime = System.nanoTime();

        Boolean isPresent;

        try {
            isPresent = matchOnPattern(performSelectSingleResult(name, sql).toString(), pattern);
        } catch (NullPointerException e) {//catch any null results, they are likely because we are polling
            isPresent = false;
        }

		int currentAttempt = 1;
		while (isPresent == false && currentAttempt <= maxAttempts) {

            // als het er niet is doen we dit
            Thread.sleep(milliSecondsBetweenAttempt);                 // wait milliSecondsBetweenAttempt milliseconds
			String result;
            try {
				LOG.info("POLLING ATTEMPT " + (currentAttempt + 1) + " OF MAX ATTEMPTS " + maxAttempts);
				result = performSelectSingleResult(name, sql).toString();
				LOG.info("POLLING RESULT: " + result);

            } catch (NullPointerException e) { //catch any null results
				result = "#NULL#";
            }
			isPresent = matchOnPattern(result, pattern);      // execute the query again
			LOG.info("POLLING MATCH FOUND: " + isPresent.toString());
            currentAttempt++;
        }

		long endTime = System.nanoTime();
		pollingTime = (endTime - startTime) / 1000000;
		LOG.info("POLLING IN DATABASE TOOK " + pollingTime + " milliseconds.");
        return isPresent;
    }

	public long getPollingTime() {
		return pollingTime;
	}


    /**
     * Perform select save blob to disk boolean.
     *
     * @param name     the name
     * @param sql      the sql
     * @param fileName the file name
     * @return the boolean
     */
    public Boolean performSelectSaveBlobToDisk(String name, String sql, String fileName) {
        try {
            saveInputStreamToDisk(getBlobAsStream(name, sql), fileName);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    /**
     * Verify result perform select single result boolean.
     *
     * @param name    the name
     * @param sql     the sql
     * @param pattern the pattern
     * @return the boolean
     */
    public Boolean verifyResultPerformSelectSingleResult(String name, String sql, String pattern) {
        if (pattern.length() == 0) {
            pattern = null;
        }
        return matchOnPattern(performSelectSingleResult(name, sql).toString(), pattern);
    }

    /**
     * Gets blob as stream.
     *
     * @param name the name
     * @param sql  the sql
     * @return the blob as stream
     */
    public InputStream getBlobAsStream(String name, String sql) {
        Connection connection = findConnectionByName(name);

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            setPreparedStatementParameters(statement);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                if (Types.BLOB == resultSet.getMetaData().getColumnType(1)) {
                    return resultSet.getBlob(1).getBinaryStream();
                } else {
                    throw new ConditionalException("The column that was found is not a BLOB");
                }
            } else {
                throw new ConditionalException("No rows were found");
            }
        } catch (SQLException e) {
            throw new ConditionalException("Failed to execute query '%s' due to the following '%s'.", sql, e.getMessage());
        }
    }


    /**
     * Gets clob as string.
     *
     * @param name the name
     * @param sql  the sql
     * @return the clob as string
     */
    public String getClobAsString(String name, String sql) {
        Connection connection = findConnectionByName(name);

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            setPreparedStatementParameters(statement);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                if (Types.CLOB == resultSet.getMetaData().getColumnType(1)) {
                    Clob cXML = resultSet.getClob(1);

                    String sXML = cXML.getSubString(1, (int) cXML.length());
                    return sXML;
                } else {
                    throw new ConditionalException("The column that was found is not a CLOB");
                }
            } else {
                throw new ConditionalException("No rows were found");
            }
        } catch (SQLException e) {
            throw new ConditionalException("Failed to execute query '%s' due to the following '%s'.", sql, e.getMessage());
        }
    }


    private void saveInputStreamToDisk(InputStream inputStream, String targetFileName) throws IOException, SQLException {
        File file = new File(targetFileName);
        if (!file.exists()) {
            file.createNewFile();

        } else {
            System.out.println("WARNING! File already exists, it will be overwritten: " + targetFileName);
        }
        FileUtils.writeStreamToFile(inputStream, file);
    }


    /**
     * Executes the contents of an SQL file using the connection with the specified name.
     *
     * @param name    the name of the connection to use.
     * @param sqlFile the file containing the sql statements to execute.
     * @return flag indicating if the execution was successful.
     */
    public boolean executeSqlFile(String name, String sqlFile) {
        Connection connection = findConnectionByName(name);

        SqlExecutor sqlExecutor = new SqlExecutor();
        sqlExecutor.execute(connection, sqlFile);

        return true;
    }


    /**
     * Execute sql string.
     *
     * @param name      the name
     * @param sqlString the sql string
     */
    public void executeSqlString(String name, String sqlString) {
        Connection connection = findConnectionByName(name);

        SqlExecutor sqlExecutor = new SqlExecutor();
        try {
            sqlExecutor.processString(connection, sqlString);
        } catch (SQLException e) {
            throw new ConditionalException("An exception occurred: '%s'.", e.getMessage());
        }
    }


    /**
     * Execute sql string with return value int.
     *
     * @param name      the name
     * @param sqlString the sql string
     * @return the int
     */
    public int executeSqlStringWithReturnValue(String name, String sqlString) {
        String results = "";
        Connection connection = findConnectionByName(name);
        SqlExecutor sqlExecutor = new SqlExecutor();

        return sqlExecutor.processStringWithResults(connection, sqlString);
    }

    /**
     * Closes a database connection with the specified name.
     *
     * @param name the name of the database connection.
     */
    public void closeConnection(String name) {
        Connection connection = findConnectionByName(name);
        try {
            connection.close();
        } catch (SQLException sqlException) {
            // Ignore this message.
        } finally {
            connections.remove(name);
        }
    }

    /**
     * Close all open database connections.
     */
    public void closeAllConnections() {
        for (String name : connections.keySet()) {
            try {
                Connection connection = connections.get(name);
                connection.close();
            } catch (SQLException sqlException) {
                // Ignore this message.
            } finally {
                connections.remove(name);
            }
        }
    }

    /**
     * Finds a database connection with the specified name.
     *
     * @param name the name of the database connection.
     * @return the database connection.
     * @throws ConditionalException when the database connection with the specified name could not be found.
     */
    private Connection findConnectionByName(String name) {
        if (connections.containsKey(name)) {
            return connections.get(name);
        }
        throw new ConditionalException("No database connection found with name '%s'.", name);
    }

    /**
     * Creates a new database connection with the specified properties file.
     *
     * @param properties the properties file containing the connection settings.
     * @return the database connection.
     * @throws ConditionalException when the driver class could not be found or when the connection could not be opened.
     */
    @Deprecated
    private Connection createAConnection(String properties) {
        PropertiesFixture propertiesFixture = new PropertiesFixture(properties);

        String databaseDriver = propertiesFixture.getStringProperty(CONNECTION_DATABASE_DRIVER);
        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new ConditionalException("Cannot find database driver class '%s'.", databaseDriver);
        }

        String databaseUrl = propertiesFixture.getStringProperty(CONNECTION_DATABASE_URL);
        String databaseUsername = propertiesFixture.getStringProperty(CONNECTION_DATABASE_USERNAME);
        String databasePassword = propertiesFixture.getStringProperty(CONNECTION_DATABASE_PASSWORD);

        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException sqlException) {
            throw new ConditionalException("Cannot open connection to '%s' because '%s'.", databaseUrl, sqlException.getMessage());
        }
    }

    private Connection createAConnection(String databaseDriver, String url, String username, String password) {

        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new ConditionalException("Cannot find database driver class '%s'.", databaseDriver);
        }


        String databaseUrl = getFromPropertyIfApplicable(url);
        String databaseUsername = getFromPropertyIfApplicable(username);
        String databasePassword = getFromPropertyIfApplicable(password);

        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException sqlException) {
            throw new ConditionalException("Cannot open connection to '%s' because '%s'.", databaseUrl, sqlException.getMessage());
        }
    }


    private Connection createAConnectionUsingSystemProperties(String databaseDriver, String systemPropertyForUrl, String systemPropertyForUsername, String systemPropertyForPassword) {

        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new ConditionalException("Cannot find database driver class '%s'.", databaseDriver);
        }


        String databaseUrl = System.getProperty(systemPropertyForUrl);
        String databaseUsername = System.getProperty(systemPropertyForUsername);
        String databasePassword = System.getProperty(systemPropertyForPassword);

        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException sqlException) {
            throw new ConditionalException("Cannot open connection to '%s' because '%s'.", databaseUrl, sqlException.getMessage());
        }
    }




    /**
     * Sets a parameter that will be put in a "?" placeholder in the SQL statement.
     * See <a href="https://docs.oracle.com/javase/7/docs/api/java/sql/PreparedStatement.html">prepared statement</a>.
     *
     * @param parameter the parameter
     * @return the boolean
     */
    public boolean selectionParameter(String parameter) {
        parameters.add(parameter);
        return true;
    }

	private void logQuery(String sql){
		LOG.info("===== BEGIN Query =====" );
		LOG.info(sql);
		LOG.info("===== END Query =======" );


	}
}
