package nl.psek.fitnesse.fixtures.database;

import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.StopTestCommandException;
import nl.psek.fitnesse.fixtures.util.FileFinder;
import nl.psek.fitnesse.fixtures.util.FileHandler;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Helper class for the DatabaseFixture
 *
 * @author Ronald Mathies, Pascal Smeets
 */
public class SqlExecutor {


    private static final Logger LOG = LoggerFactory.getLogger(SqlExecutor.class);

    /**
     * Executes the contents of the sql file using the specified connection.
     *
     * @param connection the connection to use.
     * @param sqlFile    the sql file to execute.
     * @throws ConditionalException when the execution fails.
     */
    public static void execute(final Connection connection, String sqlFile) {
        FileFinder.findFileInUserDirectory(sqlFile, new FileHandler() {
            @Override
            public void handle(File file) {
                try {
                    process(connection, file);
                } catch (IOException e) {
                    throw new StopTestCommandException("Failed to import file '%s' due to the following reason '%s'.", file, e.getMessage());
                }
            }
        });
    }

    private static void process(Connection connection, File sqlFile) throws IOException {
        ScriptRunner runner = new ScriptRunner(connection);
        runner.setAutoCommit(true);

        InputStream commentInputStream = new ByteArrayInputStream("-- Empty comment for SQL Server.\n\r".getBytes());
        SequenceInputStream sequenceInputStream = new SequenceInputStream(commentInputStream, new FileInputStream(sqlFile));
        BufferedReader reader = new BufferedReader(new InputStreamReader(sequenceInputStream));

        runner.runScript(reader);
    }

    public static void processString(Connection connection, String sqlString) throws SQLException, ConditionalException {
        LOG.info("Processing SQL query '" + sqlString + "'");

        try (PreparedStatement statement = connection.prepareStatement(sqlString)) {
            statement.execute();

        } catch (SQLException e) {
            throw new ConditionalException(e.getMessage());
        }
    }


    public static int processStringWithResults(Connection connection, String sqlString) {
        int rowsAffected = 0;
        LOG.info("Processing SQL query '" + sqlString + "'");

        try (PreparedStatement statement = connection.prepareStatement(sqlString)) {

			LOG.info("===== BEGIN Query =====");
			LOG.info(statement.toString());
			LOG.info("===== END Query =======");

            rowsAffected = statement.executeUpdate();
            LOG.info("Number of rows affected for query '" + sqlString + "' is: " + rowsAffected);

        } catch (SQLException e) {
            throw new ConditionalException(e.getMessage());
        }
        return rowsAffected;
    }

}



