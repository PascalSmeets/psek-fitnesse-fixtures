package nl.psek.fitnesse.fixtures.database;

import nl.psek.fitnesse.ConditionalException;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import static nl.psek.fitnesse.fixtures.general.util.OperationsFixture.areValuesEqual;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.matchOnPattern;

/**
 * Helper class for the DatabaseFixture
 *
 * @author Ronald Mathies
 */
public class ResultSetManager {

    private ResultSet resultSet;

    public ResultSetManager() {
    }

    public void setResultSet(ResultSet resultSet) {
        try {
            if (this.resultSet != null && !this.resultSet.isClosed()) {
                this.resultSet.close();
            }

            this.resultSet = resultSet;
        } catch (SQLException sqlException) {
            throw new ConditionalException("Could not close resultset due to exception '%s'.", sqlException.getMessage());
        }
    }

    public ResultSet getResultSet() {
        return this.resultSet;
    }

    public int getNumberOfRows() {
        isOpen();

        try {
            this.resultSet.last();
            return this.resultSet.getRow();
        } catch (SQLException sqlException) {
            throw new ConditionalException("There was a problem counting the number of rows in the resultset '%s'.", sqlException.getMessage());

        } finally {
            try {
                this.resultSet.beforeFirst();
            } catch (SQLException e) {
                throw new ConditionalException(e.getMessage());
            }
        }
    }

    public boolean verifyNumberOfRows(int expectedNumberOfRows) {
        return areValuesEqual(getNumberOfRows(), expectedNumberOfRows);
    }

	/**
	 * Gets value.
	 *
	 * @param index  the index
	 * @param column the column
	 * @return the value
	 * @throws ConditionalException the conditional exception
	 */
	public Object getValue(int index, String column) throws ConditionalException {
        isOpen();

        if (index == 0) {
            throw new ConditionalException("Cannot get value from row 0, the row count starts with 1.");
        }

        try {
            this.resultSet.absolute(index);
            return this.resultSet.getObject(column);
        } catch (SQLException sqlException) {
            throw new ConditionalException("There was a problem getting a value out of the resultset '%s'.", sqlException.getMessage());
        }
     }

	/**
	 * Gets date.
	 *
	 * @param index  the index
	 * @param column the column
	 * @return the date
	 * @throws ConditionalException the conditional exception
	 */
	public String getDate(int index, String column) throws ConditionalException {

		try {
			if (getValue(index, column) instanceof Date) {
				Date date = this.resultSet.getDate(column);
				return new SimpleDateFormat("yyyyMMdd").format(date);

			} else {
				throw new ConditionalException("Column is not a  java.sql.Date.");
			}
		} catch (SQLException sqlException) {
			throw new ConditionalException("There was a problem getting a value out of the resultset '%s'.", sqlException.getMessage());
		}

	}

	/**
	 * Gets timestamp.
	 *
	 * @param index  the index
	 * @param column the column
	 * @return the timestamp
	 * @throws ConditionalException the conditional exception
	 */
	public String getTimestamp(int index, String column) throws ConditionalException {

		try {
			if (getValue(index, column) instanceof Timestamp) {
				Timestamp timestamp = this.resultSet.getTimestamp(column);
				return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(timestamp);

			} else {
				throw new ConditionalException("Column is not a java.sql.Timestamp.");
			}
		} catch (SQLException sqlException) {
			throw new ConditionalException("There was a problem getting a value out of the resultset '%s'.", sqlException.getMessage());
		}

	}


	public boolean verifyValue(int index, String column, String pattern) throws ConditionalException {
        String foundValue = getValue(index, column).toString();
        return matchOnPattern(foundValue, pattern);
    }

    private void isOpen() {
        try {
            if (resultSet.isClosed()) {
                throw new ConditionalException("Resultset is closed, cannot retrieve any information.");
            }
        } catch (SQLException sqlException) {
            throw new ConditionalException("There was a problem checking if the resultset is still open '%s'.", sqlException.getMessage());
        }
    }

}
