create table mytable (
    integer_col int,
    double_col float,
    float_col float,
    string_col varchar2(128)
);

insert into mytable
    values(10, 10.5, 11.5, 'String value');

insert into mytable
    values(20, 20.5, 21.5, 'String value again');

