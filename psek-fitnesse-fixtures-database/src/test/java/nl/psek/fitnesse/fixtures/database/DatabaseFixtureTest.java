package nl.psek.fitnesse.fixtures.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * @author Ronald Mathies
 */
public class DatabaseFixtureTest {

    private DatabaseFixture dbFixture;

    @Before
    public void setup() {
        System.setProperty("dbURL","jdbc:h2:mem:so");
        System.setProperty("dbUSER","root");
        System.setProperty("dbPWD","");

        dbFixture = new DatabaseFixture();
        dbFixture.createConnection("h2", "org.h2.Driver", "p:dbURL", "p:dbUSER", "p:dbPWD");
        dbFixture.executeSqlFile("h2", "table.sql");
    }

    @After
    public void after() {
        dbFixture.closeConnection("h2");
    }

    @Test
    public void performSelectSingleResult() throws SQLException {
        Long count = (Long) dbFixture.performSelectSingleResult("h2", "select count(*) from mytable where integer_col = 10");
        assertEquals("There should be one row.", 1l, count.longValue());
    }

    @Test
    public void useAPreparedStatement() throws SQLException {
        dbFixture.selectionParameter("10");
        Long count = (Long) dbFixture.performSelectSingleResult("h2", "select count(*) from mytable where integer_col = ?");
        assertEquals("There should be one row.", 1l, count.longValue());
    }

    @Test
    public void verifyPerformSelectSingleResultTest() {
        assertTrue(dbFixture.verifyResultPerformSelectSingleResult("h2", "select string_col from mytable where integer_col = 20", "String value again"));
        assertFalse(dbFixture.verifyResultPerformSelectSingleResult("h2", "select string_col from mytable where integer_col = 20", "String value"));
    }

    @Test
    public void getValue() throws SQLException {
        dbFixture.performSelect("h2", "select * from mytable");
        String value = (String) dbFixture.activeResultSetManager.getValue(2, "string_col");
        assertEquals("There should be one row.", "String value again", value);
        value = (String) dbFixture.activeResultSetManager.getValue(1, "string_col");
        assertEquals("There should be one row.", "String value", value);
    }

    @Test
    public void getNumberOfRows() {
        dbFixture.performSelect("h2", "select * from mytable");
        assertEquals("Number of rows should be 2.", 2, dbFixture.activeResultSetManager.getNumberOfRows());
    }

}
