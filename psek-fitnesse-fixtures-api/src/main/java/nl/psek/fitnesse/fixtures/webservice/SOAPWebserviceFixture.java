package nl.psek.fitnesse.fixtures.webservice;

import jakarta.xml.soap.MessageFactory;
import jakarta.xml.soap.MimeHeaders;
import jakarta.xml.soap.SOAPException;
import jakarta.xml.soap.SOAPMessage;
import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.fixtures.general.xml.XmlNavigateFixture;
import nl.psek.fitnesse.fixtures.util.FileFinder;
import nl.psek.fitnesse.fixtures.util.FileHandler;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import javax.net.ssl.SSLContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;
import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Map;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;


/**
 * A fixture that acts as a SAAJ client that can send SOAP messages to a webservice and return the response.
 *
 * Deze Fitnesse Fixture representeerd een eenvoudige SAAJ client, die
 * SOAP berichten naar een WebService kan versturen en de response
 * kan retourneren. Ik ondersteun momenteel nog geen attachments.
 */
public class SOAPWebserviceFixture {

    private static final Logger LOG = LoggerFactory.getLogger(SOAPWebserviceFixture.class);
    private static long responseTime;
    private MessageFactory messageFactory;
    private SoapMessageBuilder builder;
    private ArrayList<Header> headerList;
    private String url;

    // Ondersteund trustmaterial
    private KeyStore keyStore = null;
    private File keystorefile = null;
    private String keystorepassword = null;
    private String keystorealias = null;
    private boolean useKeystore = false;
    private KeyStore trustStore = null;
    private File truststorefile = null;
    private String truststorepassword = null;
    private boolean useTruststore = false;


    private Document responseDocument = null;
    private boolean tidyXml = true;
    private int statusCode = -1;
    private Header[] responseHeaders;
    private boolean enableSystemOutLog = false;

    /**
     * Constructor voor het starten van de Fixture.
     *
     * @param url               - De URL van de WebService
     * @param characterEncoding - De gebruikte character encoding voor de HTTP request en SOAP bericht. Bijvoorbeeld ISO-8859-1
     */
    public SOAPWebserviceFixture(String url, String characterEncoding) {
        resetKeyAndTrustStores();

        try {
            messageFactory = MessageFactory.newInstance();
            this.headerList = new ArrayList<Header>();
            this.url = url;
            this.builder = new SoapMessageBuilder(characterEncoding);

        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }


    /**
     * Gets response time.
     *
     * @return the response time
     */
    public static long getResponseTime() {
        return responseTime;
    }

    /**
     * Eventuele HTTP headers kun je via mij toevoegen
     *
     * @param naam   - De naam van de header
     * @param waarde - De waarde van de header
     */
    public void addHttpHeader(String naam, String waarde) {
		this.headerList.add(new BasicHeader(naam, getFromPropertyIfApplicable(waarde)));
    }

    /**
     * Eventueel toegevoegde HTTP headers kun je via mij weer weggooien.
     */
    public void emptyHttpHeaders() {
        this.headerList.clear();
    }

    /**
     * Ik verstuur een SOAP bericht naar een Webservice. Ik doe dit op basis van een aangeleverd SOAP XML bericht, die
     * als String is aangeleverd. Daarnaast gebruik ik de eventueel toegevoegde HTTP headers, om samen met het bericht
     * een HTTP POST methode mee uit te voeren. Eventuele response op deze POST methode van de Web Service kun je opvragen
     * via de response() methode.
     *
     * @param soapXmlBericht    - Het SOAP bericht inclusief headers en envelope in String formaat.
     * @param characterEncoding - De character encoding (bijvoorbeeld ISO-8859-1)
     */
    public void sendSoapMessageToWebservice(String soapXmlBericht, String characterEncoding) {
		soapXmlBericht = getFromPropertyIfApplicable(soapXmlBericht);

        soapXmlBericht = tidyXml(soapXmlBericht);
        try {
            logRequestToSystemOut(soapXmlBericht);
            this.responseDocument = callSoapWebService(createHttpEntityFromSOAPMessage(createSOAPMessageFromXmlString(soapXmlBericht, characterEncoding), characterEncoding), this.headerList.toArray(new Header[this.headerList.size()]), url);
            logResponseToSystemOut(responseDocument.asXML());
        } catch (Exception e) {
            throw new CommandException("Fout bij versturen naar webservice: exception = '" + e.getClass() + "' message = '" + e.getMessage() + "'");
        }
    }

	/**
	 * Ik verstuur een SOAP bericht naar een Webservice. Ik doe dit op basis van een aangeleverd SOAP XML bericht, die
	 * als String is aangeleverd. Daarnaast gebruik ik de eventueel toegevoegde HTTP headers, om samen met het bericht
	 * een HTTP POST methode mee uit te voeren.
	 * Als er een HTTP504 optreedt dan probeer ik het nogmaals na een pauze van 2 seconden
	 * <p>
	 * Eventuele response op deze POST methode van de Web Service kun je opvragen
	 * via de response() methode.
	 *
	 * @param soapXmlBericht    - Het SOAP bericht inclusief headers en envelope in String formaat.
	 * @param characterEncoding - De character encoding (bijvoorbeeld ISO-8859-1)
	 */
	public void sendSoapMessageToWebserviceRetryOn504(String soapXmlBericht, String characterEncoding) {
		soapXmlBericht = getFromPropertyIfApplicable(soapXmlBericht);

		soapXmlBericht = tidyXml(soapXmlBericht);
		try {
			logRequestToSystemOut(soapXmlBericht);
			this.responseDocument = callSoapWebServiceRetryOn504(createHttpEntityFromSOAPMessage(createSOAPMessageFromXmlString(soapXmlBericht, characterEncoding), characterEncoding), this.headerList.toArray(new Header[this.headerList.size()]), url);
			logResponseToSystemOut(responseDocument.asXML());
		} catch (Exception e) {
			throw new CommandException("Fout bij versturen naar webservice: exception = '" + e.getClass() + "' message = '" + e.getMessage() + "'");
		}
	}

    /**
     * Verstuurt een soap bericht naar de webservice en probeert dit totdat de responseUniekeXpath een positieve
     * match heeft op de soap response of totdat de maxRetryNr is bereikt. Ik geef true terug als de response de
     * xpath matched, in alle andere gevallen false.
     *
     * @param soapXmlBericht      the soap xml bericht
     * @param characterEncoding   the character encoding
     * @param maxRetryNr          the max retry nr
     * @param responseUniekeXpath the response unieke xpath
     * @param intervalMs          the interval ms
     * @return boolean
     */
    public boolean sendSoapMessageUntilResponseHas(String soapXmlBericht, String characterEncoding, int maxRetryNr, String responseUniekeXpath, long intervalMs) {
        try {

            boolean valid = false;
            int currentTry = 0;
            while (!valid && currentTry < maxRetryNr) {
                sendSoapMessageToWebservice(soapXmlBericht, characterEncoding);
                XmlNavigateFixture xmlFixture = new XmlNavigateFixture();
                xmlFixture.loadXmlFromString(response());
                int resultCount = xmlFixture.xpathIsTimesPresentInDocument(responseUniekeXpath);
                valid = (resultCount == 1);
                if (!valid) {
                    LOG.info("Thread" + Thread.currentThread().getId() + " Versturen van soap bericht gaf niet het verwachte resultaat. Er was/waren " + resultCount + " matches op de xpath: " + responseUniekeXpath + " We proberen het nog maximaal " + (maxRetryNr - (++currentTry)) + " keer.");
                    try {
                        Thread.sleep(intervalMs);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    LOG.info("Thread" + Thread.currentThread().getId() + " Versturen van het soap bericht gelukt, response is valide.");
                }
            }
            return valid;
        } catch (XPathExpressionException | SAXException | ParserConfigurationException | IOException e) {
            throw new ConditionalException(e.getMessage());
        }
    }

    /**
     * Ik verstuur een nieuw aangemaakt SOAP bericht naar een Webservice. Om een nieuw bericht aan te maken
     * kan de maakNieuwBericht() methode worden aangeroepen. Vervolgens kunnen de volgende methoden worden gebruikt om het bericht aan te vullen:
     * - voegNamespaceAanHeaderToe(String prefix, String namespaceUri)
     * - voegNamespaceAanHeaderToe(prefix, namespaceUri)
     * - voegBodyElementAanEnvelopeToe(String naam)
     * - voegBodyElementAanEnvelopeToe(String naam, String prefix, String namespaceUri)
     * - voegChildElementToeAanElement(String parentElementNaam, String elementNaam, String value)
     * - voegChildElementToeAanElement(String parentElementNaam, String elementNaam, String prefix, String namespaceUri, String value)
     */
    public void sendNewSoapMessageToWebservice() {
        try {

            logRequestToSystemOut(getSoapMessageAsString());
            this.responseDocument = callSoapWebService(createHttpEntityFromSOAPMessage(builder.getSOAPMessage(), builder.getCharacterEncoding()), this.headerList.toArray(new Header[this.headerList.size()]), url);
            logResponseToSystemOut(responseDocument.asXML());
        } catch (Exception e) {
            throw new CommandException("Fout bij versturen naar webservice: exception = '" + e.getClass() + "' message = '" + e.getMessage() + "'");
        }
    }


    /**
     * Send new soap message to webservice until response has.
     *
     * @param responseXpath the response xpath
     * @param maxRetryNr    the max retry nr
     * @param intervalMs    the interval ms
     * @throws SOAPException the soap exception
     * @throws ConditionalException the conditional exception
     */
    public void sendNewSoapMessageToWebserviceUntilResponseHas(String responseXpath, int maxRetryNr, long intervalMs) throws SOAPException, ConditionalException {

        logRequestToSystemOut(getSoapMessageAsString());
        try {
            boolean valid = false;
            int currentTry = 0;
            while (!valid && currentTry < maxRetryNr) {
                this.responseDocument = callSoapWebService(createHttpEntityFromSOAPMessage(builder.getSOAPMessage(), builder.getCharacterEncoding()), this.headerList.toArray(new Header[this.headerList.size()]), url);
                XmlNavigateFixture xmlFixture = new XmlNavigateFixture();
                xmlFixture.loadXmlFromString(response());
                int resultCount = xmlFixture.xpathIsTimesPresentInDocument(responseXpath);
                valid = (resultCount == 1);
                if (!valid) {
                    LOG.info("Thread" + Thread.currentThread().getId() + " Sending SOAP message did not give the expected result. There were " + resultCount + " matches on xpath: " + responseXpath + " Will retry maximally " + (maxRetryNr - (++currentTry)) + " times.");
                    try {
                        Thread.sleep(intervalMs);
                    } catch (InterruptedException e) {
                        throw new CommandException(e);
                    }
                } else {
                    LOG.info("Thread" + Thread.currentThread().getId() + " Sending SOAP message did give the expected result. Response is valid.");
                }
            }
        } catch (XPathExpressionException | SAXException | GeneralSecurityException | IOException | ParserConfigurationException e) {
            throw new ConditionalException(e.getMessage());
        }

        logResponseToSystemOut(responseDocument.asXML());
    }

    private String getSoapMessageAsString() throws SOAPException {
        StringWriter sw = new StringWriter();
        try {
            TransformerFactory.newInstance().newTransformer().transform(
                    new DOMSource(builder.getSOAPMessage().getSOAPPart()),
                    new StreamResult(sw));
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }

        return sw.toString();
    }

    /**
     * Ik geef een eventueel response bericht als tekst retour. Als er geen response is, geef
     * ik de String "null" terug.
     *
     * @return string
     */
    public String response() {
        return this.responseDocument != null ? this.responseDocument.asXML() : "null";
    }


    /**
     * Status code int.
     *
     * @return the int
     */
    public int statusCode() {
        return this.statusCode;
    }


    /**
     * Verify status code boolean.
     *
     * @param expectedStatusCode the expected status code
     * @return the boolean
     */
    public Boolean verifyStatusCode(int expectedStatusCode) {
        return (expectedStatusCode == this.statusCode());

    }

    private Document callSoapWebService(HttpEntity entity, Header[] headers, String url)
            throws GeneralSecurityException, IOException, ConditionalException {
        CloseableHttpClient httpClient = getHttpClient();
        HttpPost post = getPostMethod(url, entity, headers);
        Document result = extractResponse(post, httpClient);
        return result;
    }

	private Document callSoapWebServiceRetryOn504(HttpEntity entity, Header[] headers, String url)
			throws GeneralSecurityException, IOException, ConditionalException {
		CloseableHttpClient httpClient = getHttpClient();
		HttpPost post = getPostMethod(url, entity, headers);
		Document result = extractResponseRetryOn504(post, httpClient);
		return result;
	}


    private Document extractResponse(HttpPost post, CloseableHttpClient client) throws IOException, ConditionalException {
        try {
            Document document = null;
            long startTime = System.nanoTime();
            HttpResponse response = client.execute(post);
            long endTime = System.nanoTime();
            responseHeaders = response.getAllHeaders();
            responseTime = (endTime - startTime) / 1000000;
            statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream stream = entity.getContent();
                try {
                    if (stream != null) {
                        document = new SAXReader().read(stream);
                    }

                } catch (DocumentException ex) {
					LOG.info("DocumentException, trying to print the input");
					LOG.info(convertInputstreamToString(stream));
                    throw ex;

                } catch (RuntimeException ex) {
					LOG.info("RuntimeException, trying to print the input");
					LOG.info(convertInputstreamToString(stream));
                    post.abort();
                    throw ex;

                } finally {
                    stream.close();
                }
            }
            return document;
        } catch (DocumentException | IOException e) {
            throw new ConditionalException(e.getMessage());
        } finally {
            client.close();
			resetKeyAndTrustStores();
		}
	}

	private Document extractResponseRetryOn504(HttpPost post, CloseableHttpClient client) throws IOException, ConditionalException {
		try {
			Document document = null;
			long startTime = System.nanoTime();
			HttpResponse response = client.execute(post);
			long endTime = System.nanoTime();
			responseHeaders = response.getAllHeaders();
			responseTime = (endTime - startTime) / 1000000;
			statusCode = response.getStatusLine().getStatusCode();

			if (HttpStatus.SC_GATEWAY_TIMEOUT == statusCode) {

				HttpEntity entity = response.getEntity();
				// Read the contents of an entity and return it as a String.
				String content = EntityUtils.toString(entity);
				LOG.info("Response content = " + System.lineSeparator() + content);

				LOG.info("HTTP status 504, retrying the operation once after two seconds");
				Thread.sleep(2000);
				startTime = System.nanoTime();
				response = client.execute(post);
				endTime = System.nanoTime();
			}


			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream stream = entity.getContent();
				try {
					if (stream != null) {
						document = new SAXReader().read(stream);
					}

				} catch (DocumentException ex) {
					LOG.info("DocumentException, trying to print the input");
					LOG.info(convertInputstreamToString(stream));
					throw ex;

				} catch (RuntimeException ex) {
					LOG.info("RuntimeException, trying to print the input");
					LOG.info(convertInputstreamToString(stream));
					post.abort();
					throw ex;

				} finally {
					stream.close();
				}
			}
			return document;
		} catch (DocumentException | IOException | InterruptedException e) {
			throw new ConditionalException(e.getMessage());
		} finally {
			client.close();
			resetKeyAndTrustStores();
        }
    }


	private String convertInputstreamToString(InputStream inputStream) {
		StringBuilder textBuilder = new StringBuilder();
		try {
			try (Reader reader = new BufferedReader(new InputStreamReader
					(inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
				int c = 0;
				while ((c = reader.read()) != -1) {
					textBuilder.append((char) c);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return textBuilder.toString();

	}


    private HttpPost getPostMethod(String url, HttpEntity entity, Header[] headers) {
        HttpPost httppost = new HttpPost(url);
        httppost.setEntity(entity);
        httppost.setHeaders(headers);
        return httppost;
    }


    // SSL keystore for double-sided SSL

    /**
     * Sets key store.
     *
     * @param inputKeystorefile the input keystorefile
     * @param keystorepassword  the keystorepassword
     * @param keystorealias     the keystorealias
     */
    public void setKeyStore(final String inputKeystorefile, String keystorepassword, String keystorealias) {

        //try to find the file in the project folder
        try {
            FileFinder.findFileInUserDirectory(inputKeystorefile, new FileHandler() {
                @Override
                public void handle(File file) {
                    try {
                        SOAPWebserviceFixture.this.keystorefile = file;
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException(("SOAPWebserviceFixture cannot load file '" + inputKeystorefile + "', please make sure the file exists and is on the classpath. "));
                    }
                }
            });
        } catch (IllegalArgumentException e) {
            //do nothing
        }

        // if not found use inputKeystorefile as a path (relative to project root or absolute)
        if (this.keystorefile == null) {
            this.keystorefile = new File(inputKeystorefile);

			this.keystorepassword = getFromPropertyIfApplicable(keystorepassword);
			this.keystorealias = getFromPropertyIfApplicable(keystorealias);
            this.useKeystore = true;
            LOG.info("SSL: Setting java Keystore file to: " + this.keystorefile.getAbsolutePath());
            LOG.info("SSL: Setting java Keystore alias to: " + keystorealias);

        }
    }


    // SSL keystore for single-sided SSL

    /**
     * Sets trust store.
     *
     * @param inputTruststorefile the input truststorefile
     * @param truststorepassword  the truststorepassword
     */
    public void setTrustStore(final String inputTruststorefile, String truststorepassword) {

        //try to find the file in the project folder
        try {
            FileFinder.findFileInUserDirectory(inputTruststorefile, new FileHandler() {
                @Override
                public void handle(File file) {
                    try {
                        SOAPWebserviceFixture.this.truststorefile = file;
                    } catch (IllegalArgumentException e) {
                        throw new IllegalArgumentException(("SOAPWebserviceFixture cannot load file '" + inputTruststorefile + "', please make sure the file exists and is on the classpath. "));
                    }
                }
            });
        } catch (IllegalArgumentException e) {
            //do  nothing
        }

        // if not found use inputKeystorefile as a path (relative to project root or absolute)
        if (this.truststorefile == null) {
            this.truststorefile = new File(inputTruststorefile);


			this.truststorepassword = getFromPropertyIfApplicable(truststorepassword);
            this.useTruststore = true;
            LOG.info("SSL: Setting java TrustStore file to: " + this.truststorefile.getAbsolutePath());
        }
    }

    /**
     * Reset key and trust stores.
     */
    public void resetKeyAndTrustStores() {
        this.trustStore = null;
        this.keyStore = null;
        this.useTruststore = false;
        this.useKeystore = false;
    }

    private KeyStore loadKeyStore() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        this.keyStore = KeyStore.getInstance("JKS");

        keyStore.load(new FileInputStream(this.keystorefile), this.keystorepassword.toCharArray());
        LOG.info("SSL: Loaded previously defined KeyStore for SSL connection");
        return keyStore;
    }

    private KeyStore loadTrustStore() throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException {
        this.trustStore = KeyStore.getInstance("JKS");

        trustStore.load(new FileInputStream(this.truststorefile), this.truststorepassword.toCharArray());
        LOG.info("SSL: Loaded previously defined TrustStore for SSL connection");
        return trustStore;
    }

    private CloseableHttpClient getHttpClient() throws GeneralSecurityException, IOException {

        SSLContext context = null;
        if (!useKeystore && useTruststore) {
            LOG.info("SSL: Using only TrustStore for SSL connection");
            context = SSLContexts.custom()
                    .loadTrustMaterial(loadTrustStore(), new TrustSelfSignedStrategy())
                    .build();
            return HttpClients.custom().setSSLContext(context).build();

        } else if (useKeystore && useTruststore) {
            LOG.info("SSL: Using Keystore and TrustStore for SSL connection");
            context = SSLContexts.custom()
                    .loadKeyMaterial(loadKeyStore(), this.keystorepassword.toCharArray(), new PrivateKeyStrategy() {
                        @Override
                        public String chooseAlias(Map<String, PrivateKeyDetails> aliases, Socket socket) {
                            return keystorealias;
                        }
                    })
                    .loadTrustMaterial(loadTrustStore(), new TrustSelfSignedStrategy())
                    .build();
            return HttpClients.custom().setSSLContext(context).build();
        } else {
            return HttpClients.createDefault();
        }
    }

    private SOAPMessage createSOAPMessageFromXmlString(String soapDocument, String characterEncoding) {
        try {
            InputStream inputStream = new ByteArrayInputStream(soapDocument.getBytes(characterEncoding));
            return messageFactory.createMessage(new MimeHeaders(), inputStream);
        } catch (SOAPException | IOException e) {
            throw new ConditionalException(e.getMessage());
        }
    }

    private HttpEntity createHttpEntityFromSOAPMessage(SOAPMessage soapMessage, String characterEncoding) {
        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMessage.saveChanges();
            soapMessage.writeTo(out);
            return new StringEntity(new String(out.toByteArray(), characterEncoding), characterEncoding);
        } catch (SOAPException | IOException e) {
            throw new ConditionalException(e.getMessage());
        }
    }

    /**
     * Ik maak een nieuw, leeg SOAP bericht aan. Eventueel eerder aangemaakte
     * berichten worden hiermee overschreven.
     */
    public void createNewMessage() {
        try {
            builder.createNewMessage();
        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }


    /**
     * Add namespace to envelope.
     *
     * @param prefix       the prefix
     * @param namespaceUri the namespace uri
     */
    public void addNamespaceToEnvelope(String prefix, String namespaceUri) {
        try {
            builder.addNamespaceToenvelope(prefix, namespaceUri);
        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }


    /**
     * Ik voeg een Namespace declaratie aan de SOAP header van een nieuw aangemaakt bericht toe.
     *
     * @param prefix       the prefix
     * @param namespaceUri the namespace uri
     */
    public void addNamespaceToHeader(String prefix, String namespaceUri) {
        try {
            builder.addNamespaceToHeader(prefix, namespaceUri);
        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }


    /**
     * Add child element to header.
     *
     * @param name         the name
     * @param prefix       the prefix
     * @param namespaceUri the namespace uri
     * @param value        the value
     */
    public void addChildElementToHeader(String name, String prefix, String namespaceUri, String value) {
        try {
            builder.addChildElementToHeader(name, prefix, namespaceUri, value);
        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }


    /**
     * Clear attributes.
     */
    public void clearAttributes() {
        builder.clearAttributes();
    }


    /**
     * Sets attribute.
     *
     * @param key   the key
     * @param value the value
     */
    public void setAttribute(String key, String value) {
        builder.setAttribute(key, value);
    }

    /**
     * Ik voeg een body element (op root niveau) aan de envelope van het nieuw aangemaakte bericht toe.
     *
     * @param naam the naam
     */
    public void addBodyElementToEnvelope(String naam) {
        try {
            builder.addBodyElementToEnvelope(naam);
        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }

    /**
     * Ik voeg een body element (op root niveau) aan de envelope van het nieuw aangemaakte bericht toe.
     *
     * @param naam         the naam
     * @param prefix       the prefix
     * @param namespaceUri the namespace uri
     */
    public void addBodyElementToEnvelope(String naam, String prefix, String namespaceUri) {
        try {
            builder.addBodyElementToEnvelope(naam, prefix, namespaceUri);
        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }

    /**
     * Ik voeg een child element toe, aan een ander eerder gemaakt element. Dit mag ook een body element zijn.
     * Geef de volledige naam van het parent element op, inclusief parent namen van de parent, etc, gescheiden door een "."
     *
     * @param parentElementNaam - bijvoorbeeld bodyElementNaam.childElementNaam.childElementNaam
     * @param elementNaam       - de naam van het toe te voegen element
     * @param value             - de eventuele waarde van het toe te voegen element.
     */
    public void addChildElementToElement(String parentElementNaam, String elementNaam, String value) {
        addChildElementToElement(parentElementNaam, elementNaam, "", "", value);
    }

    /**
     * Ik voeg een child element toe, aan een ander eerder gemaakt element. Dit mag ook een body element zijn.
     * Geef de volledige naam van het parent element op, inclusief parent namen van de parent, etc, gescheiden door een "."
     *
     * @param parentElementNaam - bijvoorbeeld bodyElementNaam.childElementNaam.childElementNaam
     * @param elementNaam       - de naam van het toe te voegen element
     * @param prefix            - namespace prefix
     * @param namespaceUri      - de namespace uri
     * @param value             - de eventuele waarde van het toe te voegen element.
     */
    public void addChildElementToElement(String parentElementNaam, String elementNaam, String prefix, String namespaceUri, String value) {
        try {
            builder.addChildElementToElement(parentElementNaam, elementNaam, prefix, namespaceUri, value);
        } catch (SOAPException e) {
            throw new ConditionalException(e.getMessage());
        }
    }

    /**
     * Ik geef het tot nu toe aangemaakte bericht terug als tekst.
     *
     * @return string
     */
    public String generatedMessage() {
        return builder.toString();
    }

    private String tidyXml(String xml) {
        if (tidyXml) {
            return xml.replace("</br>", "").replace("<br>", "").replace("</BR>", "").replace("<BR>", "").replace("<br/>", "").replace("<BR/>", "").replace("$amp;", "&").replace("&", "&amp;");
        }
        return xml;
    }

    /**
     * Sets tidy xml.
     *
     * @param tidy the tidy
     */
    public void setTidyXml(boolean tidy) {
        this.tidyXml = tidy;
    }


    /**
     * Enablelog messages to system out.
     */
    public void enablelogMessagesToSystemOut() {
        enableSystemOutLog = true;
    }


    /**
     * Disablelog messages to system out.
     */
    public void disablelogMessagesToSystemOut() {
        enableSystemOutLog = false;
    }

    private void logRequestToSystemOut(String input) {
        if (enableSystemOutLog) {
            LOG.info(System.lineSeparator());
            LOG.info("BEGIN : LOADED SOAP REQUEST: ");
            try {
                logMessageTypeToSystemOut(input);
            } catch (Exception e) {
                LOG.info(e.getMessage());
            }
            LOG.info("ENDPOINT           = " + url);
            LOG.info("REQUEST      = " + input);
            LOG.info("END   : LOADED SOAP REQUEST. " + System.lineSeparator());
        }
    }

    private void logResponseToSystemOut(String input) {
        if (enableSystemOutLog) {
            LOG.info(System.lineSeparator());
            LOG.info("BEGIN : RECEIVED SOAP RESPONSE: ");
            LOG.info("RESPONSESTATUSCODE = " + statusCode);

            for (Header header : responseHeaders) {
                LOG.info("RESPONSEHEADER     = Key   : " + header.getName());
                LOG.info("                     Value : " + header.getValue());
            }
            LOG.info("RESPONSETIME = " + responseTime + " milliseconds");
            try {
                logMessageTypeToSystemOut(input);
            } catch (Exception e) {
                LOG.info(e.getMessage());
            }
            LOG.info("RESPONSE     = " + input);
            LOG.info("END   : RECEIVED SOAP RESPONSE. " + System.lineSeparator());
        }
    }

    private void logMessageTypeToSystemOut(String input) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        XmlNavigateFixture fixture = new XmlNavigateFixture();
        fixture.loadXmlFromString(input);
        String type = fixture.getXpathNodeName("//*[contains(translate(local-name(), 'ABCDEFGHJIKLMNOPQRSTUVWXYZ', 'abcdefghjiklmnopqrstuvwxyz'), 'body')]/child::*[local-name()]");
        LOG.info(" MESSAGETYPE  = " + type);
    }

    private boolean isEmpty(String value) {
        return value == null || "".equals(value.trim());
    }
}