package nl.psek.fitnesse.fixtures.webservice;


import jakarta.xml.soap.*;
import nl.psek.fitnesse.CommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.text.StringEscapeUtils.unescapeXml;


/**
 * A very basic fixture that can send a SOAP message, no validation of the sent message, can be used to test the response of your testobject to invalid input
 * <p>
 * Created by pascal on 22/06/16.
 */
public class SimpleSoapFixture {

    /**
     * The constant TIDY_XML.
     */
    public static final boolean TIDY_XML = true;
    private static final Logger LOG = LoggerFactory.getLogger(SimpleSoapFixture.class);
    private SOAPConnection soapConnection;
    private SOAPMessage request;
    private SOAPMessage soapResponse;
    private String responseXmlString;
    private boolean enableSystemOutLog = false;


    /**
     * Instantiates a new Simple soap fixture.
     */
    public SimpleSoapFixture() {
        //default constructor
    }

    /**
     * Tidy xml string.
     *
     * @param xml the xml
     * @return the string
     */
    public static String tidyXml(String xml) {
        if (TIDY_XML) {
            LOG.info("TIDY_XML input = " + xml);
            xml = xml.replace("</br>", "").replace("<br>", "").replace("</BR>", "").replace("<BR>", "").replace("<br/>", "").replace("<BR/>", "").replace("$amp;", "&").replace("&", "&amp;");
        }
        LOG.info("TIDY_XML output = " + xml);
        return xml;
    }

    /**
     * Load soap message from string boolean.
     *
     * @param xml the xml
     * @return the boolean
     * @throws IOException the io exception
     */
    public boolean loadSoapMessageFromString(String xml) throws IOException {
        loadSoapMessageFromString(xml, "UTF-8");
        return true;
    }

    /**
     * Load soap message from string boolean.
     *
     * @param xml      the xml
     * @param encoding the encoding
     * @return the boolean
     * @throws IOException the io exception
     */
    public boolean loadSoapMessageFromString(String xml, String encoding) throws IOException {

        InputStream is = new ByteArrayInputStream(xml.getBytes(Charset.forName(encoding)));

        try {
            request = MessageFactory.newInstance().createMessage(null, is);
            logRequestToSystemOut();
        } catch (SOAPException e) {
            throw new CommandException("A SOAPException has occurred with message: " + e.getMessage());
        }
        return true;
    }

    /**
     * Load soap message from file boolean.
     *
     * @param fileName the file name
     * @param encoding the encoding
     * @return the boolean
     * @throws IOException   the io exception
     * @throws SOAPException the soap exception
     */
    public boolean loadSoapMessageFromFile(String fileName, String encoding) throws IOException, SOAPException {
        Path path = Paths.get(fileName);
        byte[] bytes = Files.readAllBytes(path);
        String xml = new String(bytes, Charset.forName(encoding));

        loadSoapMessageFromString(xml, encoding);
        return true;
    }

    /**
     * Send soap message.
     *
     * @param url the url
     * @throws SOAPException the soap exception
     * @throws IOException   the io exception
     */
    public void sendSoapMessage(String url) throws SOAPException, IOException {
        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
        soapConnection = soapConnectionFactory.createConnection();
        try {
            soapResponse = soapConnection.call(request, url);
        } catch (Exception e) {
            logErrorToSystemOut(e);
            throw new CommandException("The request resulted in an error: " + e.getMessage());
        }
        logResponseToSystemOut();
        closeSoapConnection();
        storeResponseAsString();
    }

    private void storeResponseAsString() {
        final StringWriter sw = new StringWriter();
        try {
            TransformerFactory.newInstance().newTransformer().transform(
                    new DOMSource(soapResponse.getSOAPPart()),
                    new StreamResult(sw));
        } catch (TransformerException e) {
            throw new CommandException(e.getMessage());
        }

        // Now you have the XML as a String:
        responseXmlString = sw.toString();
    }

    /**
     * Returns the xml response
     *
     * @return string xml
     */
    public String getResponseXmlString() {
        return responseXmlString;
    }

    /**
     * Cleans - unescapes - the xml reponse and returns it
     *
     * @return string xml
     */


    public String getResponseXmlStringCleaned() {
        return unescapeXml(responseXmlString);
    }

    private void closeSoapConnection() throws SOAPException {
        soapConnection.close();
    }


    /**
     * Enablelog messages to system out.
     */
    public void enablelogMessagesToSystemOut() {
        enableSystemOutLog = true;
    }


    /**
     * Disablelog messages to system out.
     */
    public void disablelogMessagesToSystemOut() {
        enableSystemOutLog = false;
    }

    private void logRequestToSystemOut() throws IOException, SOAPException {
        if (enableSystemOutLog) {
            LOG.info("BEGIN : LOADED SOAP REQUEST: ");
            request.writeTo(System.out);
            LOG.info("END : LOADED SOAP REQUEST. " + System.lineSeparator());
        }
    }

    private void logResponseToSystemOut() throws IOException, SOAPException {
        if (enableSystemOutLog) {
            LOG.info("BEGIN : RECIEVED SOAP RESPONSE: ");
            soapResponse.writeTo(System.out);
            LOG.info("END : RECIEVED SOAP RESPONSE. " + System.lineSeparator());
        }
    }

    private void logErrorToSystemOut(Exception e) {
        LOG.info("BEGIN : RECIEVED ERROR RESPONSE: ");
        LOG.info(e.getMessage());
        LOG.info("END : RECIEVED ERROR RESPONSE. " + System.lineSeparator());
    }

    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }


}
