package nl.psek.fitnesse.fixtures.rest;

import io.restassured.authentication.AuthenticationScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.ProxySpecification;
import io.restassured.specification.RequestSpecification;
import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.StopTestCommandException;
import nl.psek.fitnesse.fixtures.util.FileFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;

/**
 * A fixture that uses RestAssured to make REST calls
 * <p>
 * [REST Assured](http://rest-assured.io/) is used in this fixture.
 */
public class RestFixture {

    private static final String EMPTYMESSAGE = "Response is null";
    private static final Logger LOG = LoggerFactory.getLogger(RestFixture.class);
    /**
     * The Form parameter set.
     */
    public Map<String, String> formParameterSet = new HashMap<String, String>();
    /**
     * The Query parameter set.
     */
    public Map<String, String> queryParameterSet = new HashMap<String, String>();
    /**
     * The Multi value parameter set.
     */
    public Map<String, List<String>> multiValueParameterSet = new HashMap<>();
    /**
     * The Header set.
     */
    public Map<String, String> headerSet = new HashMap<String, String>();
    /**
     * The Body.
     */
    public String body = "";

    private RequestSpecification requestSpec;
    private ProxySpecification proxySpecification = null;
    private Response response = null;
    /**
     * The constant responseBodyAsString.
     */
    public static String responseBodyAsString = null;
    /**
     * The Response body as byte array.
     */
    public static byte[] responseBodyAsByteArray = null;
    private static long responseTime;

    private boolean tidyInput = true;
    private boolean enableSystemOutLog = false;
    private boolean filterSystemOutLog = false;
    private List<String> filterlist = new ArrayList<>();
    private boolean relaxedHttpsValidation = false;

    /**
     * The Rest assured config.
     */
    private String keystorefilelocation = null;
    private String keystorefile = null;
    private KeyStore keystore = null;
    private String keystorepassword = null;
    private boolean useKeystore = false;
    private String trustorefilelocation = null;
    private File truststorefile = null;
    private KeyStore truststore = null;
    private String truststorepassword = null;
    private boolean useTruststore = false;
    private AuthenticationScheme authentication = null;


    /**
     * Instantiates a new Rest fixture. This is a wrapper for RestAssured.
     */

    public RestFixture() {
        //default contructor
    }

    /**
     * Sets base uri.
     *
     * @param uri the uri
     */
    public static void setBaseUri(String uri) {
        baseURI = getFromPropertyIfApplicable(uri);
    }

    /**
     * Adds a requestheader to the set that will be sent with the request
     *
     * @param name  the header name
     * @param value the header value
     */

    public void addHeader(String name, String value) {
        headerSet.put(name, getFromPropertyIfApplicable(value));
    }

    /**
     * Adds a query parameter to the set that will be sent with the request
     *
     * @param name  the query parameter name
     * @param value the query parameter value
     */

    public void addQueryParameter(String name, String value) {
        queryParameterSet.put(name, getFromPropertyIfApplicable(value));
    }

    /**
     * Adds one value to a multi-value query parameter to the set that will be sent with the request
     * <p>
     * example, with:
     * addMultiQueryParameter(id,a);
     * addMultiQueryParameter(id,b);
     * The resulting request would look like this http://server/action?id=a,b
     *
     * @param name  the name
     * @param value the value
     */

    public void addMultiQueryParameter(String name, String value) {
        addMultiValueParameter(name, getFromPropertyIfApplicable(value));
    }

    /**
     * Add one value to a multi-value form parameter to the set that will be sent with the request.
     *
     * @param name  the name
     * @param value the value
     */

    public void addMultiValueParameter(String name, String value) {
        multiValueParameterSet.computeIfAbsent(name, k -> new ArrayList<>()).add(getFromPropertyIfApplicable(value));
    }

    /**
     * Adds a form parameter to the set that will be sent with the request.
     *
     * @param name  the name
     * @param value the value
     */

    public void addFormParameter(String name, String value) {
        formParameterSet.put(name, getFromPropertyIfApplicable(value));
    }

    /**
     * Add a body that will be sent with the request
     *
     * @param input the input
     */

    public void addBody(String input) {
        this.body = input;
    }

    /**
     * Set relaxed https validation.
     */

    public void setRelaxedHTTPSValidation() {
        relaxedHttpsValidation = true;

    }

    /**
     * Sets proxy configuration.
     *
     * @param proxyHost   the proxy host
     * @param proxyPort   the proxy port
     * @param proxyScheme the proxy scheme
     */

    public void setProxyConfiguration(String proxyHost, int proxyPort, String proxyScheme) {
        proxySpecification = new ProxySpecification(proxyHost, proxyPort, proxyScheme);
    }

    /**
     * Sets proxy configuration.
     *
     * @param proxyHost     the proxy host
     * @param proxyPort     the proxy port
     * @param proxyScheme   the proxy scheme
     * @param proxyUsername the proxy username
     * @param proxyPassword the proxy password
     */

    public void setProxyConfiguration(String proxyHost, int proxyPort, String proxyScheme, String proxyUsername, String proxyPassword) {
        proxySpecification = new ProxySpecification(proxyHost, proxyPort, proxyScheme).withAuth(getFromPropertyIfApplicable(proxyUsername), getFromPropertyIfApplicable(proxyPassword));
    }

    /**
     * Sets trust store.
     *
     * @param truststorefile     the truststorefile
     * @param truststorepassword the truststorepassword
     */
    public void setTrustStore(String truststorefile, String truststorepassword) {
        //try to find the file in the project folder
        try {
            FileFinder.findFileInUserDirectory(truststorefile, file -> {
                try {
                    RestFixture.this.truststorefile = file;
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(("RestFixture cannot load file '" + truststorefile + "', please make sure the file exists and is on the classpath. "));
                }
            });
        } catch (IllegalArgumentException e) {
            //negeren
        }

        // if not found use inputKeystorefile as a path (relative to project root or absolute)
        if (this.truststorefile == null) {
            this.truststorefile = new File(truststorefile);
        }

        this.truststorepassword = getFromPropertyIfApplicable(truststorepassword);
        this.useTruststore = true;
        LOG.info("SSL: Setting java TrustStore file to: " + this.truststorefile.getAbsolutePath());
    }

    /**
     * Sets key store.
     *
     * @param keystorefile     the keystorefile
     * @param keystorepassword the keystorepassword
     * @throws IOException               the io exception
     * @throws CertificateException      the certificate exception
     * @throws NoSuchAlgorithmException  the no such algorithm exception
     * @throws KeyStoreException         the key store exception
     * @throws UnrecoverableKeyException the unrecoverable key exception
     * @throws KeyManagementException    the key management exception
     */
// SSL keystore for double-sided SSL
    public void setKeyStore(final String keystorefile, String keystorepassword) throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, KeyManagementException {
        this.keystorefile = keystorefile;
        this.keystorepassword = getFromPropertyIfApplicable(keystorepassword);
        this.useKeystore = true;
        LOG.info("SSL: Setting java Keystore file to: " + new File(keystorefile).getAbsolutePath());
    }


    /**
     * Sets the keystore based on a byte[] with the bytes of the file, and the password (using prefix p: the value is rettieved from a system.property)
     *
     * @param bytes    the keystore file as a byte[]
     * @param password the password of the keystore (using prefix p: the value is rettieved from a system.property)
     */
    public void setKeyStore(byte[] bytes, String password) {
        char[] pass = getFromPropertyIfApplicable(password).toCharArray();

        try (InputStream is = new ByteArrayInputStream(bytes)) {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(is, pass);
            this.keystore = ks;
            this.useKeystore = true;
            LOG.info("SSL: Loaded java Keystore from a byte[]");
        } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
            throw new CommandException("loading of the KeyStore failed", e.getMessage());
        }
    }

    /**
     * Sets the truststore based on a byte[] with the bytes of the file, and the password (using prefix p: the value is rettieved from a system.property)
     *
     * @param bytes    the truststore file as a byte[]
     * @param password the password of the truststore (using prefix p: the value is rettieved from a system.property)
     */
    public void setTrustStore(byte[] bytes, String password) {
        char[] pass = getFromPropertyIfApplicable(password).toCharArray();

        try (InputStream is = new ByteArrayInputStream(bytes)) {
            KeyStore ts = KeyStore.getInstance(KeyStore.getDefaultType());
            ts.load(is, pass);
            this.truststore = ts;
            this.useTruststore = true;
            LOG.info("SSL: Loaded java TrustStore from a byte[]");
        } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
            throw new CommandException("loading of the TrustStore failed", e.getMessage());
        }
    }

    /**
     * Sets the truststore based on a byte[], this method creates a temporary file with the truststore and sets it for use when sending the request
     *
     * @param bytes    the truststore file as a byte[]
     * @param password the password of the truststore (using prefix p: the value is rettieved from a system.property)
     */
    public void setTrustStoreFromByteArray(byte[] bytes, String password) throws IOException {
        this.truststorepassword = getFromPropertyIfApplicable(password);

        if ((bytes.length > 0)) {
            Path truststore = Files.createTempFile("truststore", ".truststore");
            Files.write(truststore, bytes);
            this.trustorefilelocation = truststore.toAbsolutePath().toString();
        } else {
            throw new IllegalArgumentException("byte[] was empty");
        }
        this.useTruststore = true;

    }


    /**
     * Sets the keystore based on a byte[], this method creates a temporary file with the keystore and sets it for use when sending the request
     *
     * @param bytes    the keystore file as a byte[]
     * @param password the password of the keystore (using prefix p: the value is rettieved from a system.property)
     */
    public void setKeyStoreFromByteArray(byte[] bytes, String password) throws IOException {
        this.keystorepassword = getFromPropertyIfApplicable(password);

        if ((bytes.length > 0)) {
            Path truststore = Files.createTempFile("keystore", ".keystore");
            Files.write(truststore, bytes);
            this.keystorefilelocation = truststore.toAbsolutePath().toString();
        } else {
            throw new IllegalArgumentException("byte[] was empty");
        }
        this.useKeystore = true;

    }


    // Building the RequestSpecification

    private RequestSpecification buildRequest() {
        RequestSpecBuilder builder = new RequestSpecBuilder();


        builder.setBaseUri(baseURI);

        if (!headerSet.isEmpty()) {
            builder.addHeaders(headerSet);
        }
        if (!queryParameterSet.isEmpty()) {
            builder.addQueryParams(queryParameterSet);
        }
        if (!multiValueParameterSet.isEmpty()) {
            builder.addParams(multiValueParameterSet);
        }
        if (!formParameterSet.isEmpty()) {
            builder.addFormParams(formParameterSet);
        }
        if (body.length() > 0) {
            body = tidyInput(body);
            builder.setBody(body);
        }
        if (relaxedHttpsValidation) {
            builder.setRelaxedHTTPSValidation();
        }
        if (this.useTruststore) {
            if (truststore != null) {
                LOG.debug("setting truststore from byte[]");
                builder.setTrustStore(truststore);
            } else if (trustorefilelocation != null) {
                LOG.debug("setting truststore from temp file file and password");
                builder.setTrustStore(this.trustorefilelocation, this.truststorepassword);
            } else if (truststorefile != null) {
                LOG.debug("setting truststore from file and password");
                builder.setTrustStore(this.truststorefile, this.truststorepassword);
            }
        }
        if (this.useKeystore) {
            if (keystore != null) {
                LOG.debug("setting keystore from byte[]");
                builder.setKeyStore(keystore);
            } else if (keystorefilelocation != null) {
                LOG.debug("setting keystore from temp file file and password");
                builder.setKeyStore(this.keystorefilelocation, this.keystorepassword);
            } else if (keystorefile != null) {
                LOG.debug("setting keystore from file and password");
                builder.setKeyStore(this.keystorefile, this.keystorepassword);
            }
        }
        if (proxySpecification != null) {
            builder.setProxy(proxySpecification);
        }
        return requestSpec = builder.build();
    }

    /**
     * Send request.
     * Builds the request, sends it, logs a unique test reference to combine the response with the request and the responsetime and response.
     *
     * @param verb the verb
     * @param path the path
     */
    public void sendRequest(String verb, String path) {


        String testRef = "REF_" + Instant.now().toEpochMilli() + "_" + new BigInteger((6 + 5) * 5, new SecureRandom()).toString(32).substring(0, 6);

        buildRequest();
        logRequestToSystemOut(testRef, verb, path);
        long startTime = System.nanoTime();
        this.response = given().spec(requestSpec).when().request(verb, path);
        long endTime = System.nanoTime();
        responseTime = (endTime - startTime) / 1000000;
        logResponseToSystemOut(testRef);
    }


//	Working with the response

    /**
     * Gets response.
     *
     * @return the response
     */
    public String getResponse() {
        if (response != null)
            return response.asString();
        else throw new CommandException(EMPTYMESSAGE);
    }


    /**
     * Gets response as Object.
     *
     * @return the response as object
     */
    public Response getResponseAsObject() {
        if (response != null)
            return response;
        else throw new CommandException(EMPTYMESSAGE);
    }

    /**
     * Gets response body.
     *
     * @return the response body
     */
    public String getResponseBody() {
        if (response != null) {
            this.responseBodyAsString = response.getBody().asString();
            return this.responseBodyAsString;
        } else throw new CommandException(EMPTYMESSAGE);
    }

    /**
     * Get response body as uft 8 byte array byte[].
     *
     * @return the byte [ ]
     */
    public byte[] getResponseBodyAsUFT8ByteArray() {
        if (response != null) {
            this.responseBodyAsByteArray = response.getBody().asString().getBytes(StandardCharsets.UTF_8);
            return this.responseBodyAsByteArray;
        } else throw new CommandException(EMPTYMESSAGE);
    }

    /**
     * Store response body.
     */
    public void storeResponseBody() {
        if (response != null) {
            this.responseBodyAsString = response.getBody().asString();
        } else throw new CommandException(EMPTYMESSAGE);
    }

    /**
     * Write response body to temp file string.
     *
     * @return the string
     */
    public String writeResponseBodyToTempFile() {
        if (response != null) {
            this.responseBodyAsString = response.getBody().asString();
            Path tempDirWithPrefix;
            try {
                tempDirWithPrefix = Files.createTempDirectory("jsontemp");
            } catch (IOException e) {
                throw new StopTestCommandException(e.getMessage());
            }

            String fileName = tempDirWithPrefix.toString() + System.nanoTime() + ".temp.json";

            Path path = Paths.get(fileName);

            byte[] strToBytes = this.responseBodyAsString.getBytes(StandardCharsets.UTF_8);

            try {
                Files.write(path, strToBytes);
            } catch (IOException e) {
                throw new StopTestCommandException(e.getMessage());
            }

            return fileName;


        } else throw new CommandException(EMPTYMESSAGE);

    }

    /**
     * Gets response status code.
     *
     * @return the response status code
     */
    public int getResponseStatusCode() {
        if (response != null)
            return response.getStatusCode();
        else throw new CommandException(EMPTYMESSAGE);
    }

    /**
     * Gets headers.
     *
     * @return the headers
     */
    public String getHeaders() {
        return response.getHeaders().toString();
    }

    /**
     * Gets header.
     *
     * @param name the name
     * @return the header
     */
    public String getHeader(String name) {
        return response.getHeader(name);
    }


//	helper methods

    /**
     * Removes break tags generated by FitNesse
     *
     * @param input
     * @return cleaned input if tidyInput boolean is set to true
     */
    private String tidyInput(String input) {
        if (tidyInput) {
            return input.replace("</br>", "").replace("<br>", "").replace("</BR>", "").replace("<BR>", "").replace("<br/>", "").replace("<BR/>", "").replace("<pre>", "").replace("</pre>", "").replace("<PRE>", "").replace("</PRE>", "");
        }
        return input;
    }

    /**
     * Enable log messages to system out.
     */
//	Logging tot sout
    public void enablelogMessagesToSystemOut() {
        enableSystemOutLog = true;
    }

    public void filterOutHeaderInLogging(String headername) {
        filterSystemOutLog = true;
        filterlist.add(headername);
    }

    /**
     * Disable log messages to system out.
     */
    public void disablelogMessagesToSystemOut() {
        enableSystemOutLog = false;
    }


    private void logRequestToSystemOut(String testRef, String verb, String path) {

        if (enableSystemOutLog) {
            LOG.info("BEGIN              : REQUEST TO SEND : ");
            LOG.info(" + TEST-REFERENCE  : " + testRef);
            LOG.info(" + VERB            : " + verb);
            LOG.info(" + URL/PATH        : " + baseURI + path);

            if (filterSystemOutLog) {
                for (String header : filterlist) {
                    headerSet.replace(header, "this header was cleaned");
                }
            }
            LOG.info(" + HEADERS         : " + headerSet.toString());
            LOG.info(" + QUERYPARAMETERS : " + queryParameterSet.toString());
            LOG.info(" + MULTIVALUEPARAMETERS : " + multiValueParameterSet.toString());
            LOG.info(" + FORMPARAMETERS  : " + formParameterSet.toString());
            LOG.info(" + BODY            : " + body);
            LOG.info(" + RELAXED HTTPS VALIDATION : " + relaxedHttpsValidation);
            LOG.info("END                : REQUEST TO SEND. ");
        }
    }

    private void logResponseToSystemOut(String testRef) {
        if (enableSystemOutLog) {
            LOG.info("BEGIN              : RECEIVED RESPONSE: ");
            LOG.info(" + TEST-REFERENCE  : " + testRef);
            LOG.info(" + STATUS          : " + getResponseStatusCode());
            LOG.info(" + RESPONSETIME    : " + responseTime + " milliseconds");
            LOG.info(" + HEADERS         : " + getHeaders());
            LOG.info(" + RESPONSE        : " + getResponse());
            LOG.info("END                : RECEIVED RESPONSE. ");
        }
    }


}
