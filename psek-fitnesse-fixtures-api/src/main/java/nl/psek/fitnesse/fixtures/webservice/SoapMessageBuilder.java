package nl.psek.fitnesse.fixtures.webservice;

import jakarta.xml.soap.*;

import javax.xml.namespace.QName;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * Helper class used by SOAPWebserviceFixture
 */
public class SoapMessageBuilder {

    private static HashMap<QName, String> attributes = new HashMap<>();
    private final MessageFactory messageFactory;
    private SOAPMessage soapMessage;
    private String charset;
    private HashMap<String, SOAPElement> elements;

    public SoapMessageBuilder(String charset) throws SOAPException {
        this.messageFactory = MessageFactory.newInstance();
        this.charset = charset;
    }


    public void setAttribute(String key, String value) {
        QName name = new QName(key);
        attributes.put(name, value);
    }

    public void clearAttributes() {
        attributes.clear();
    }


    public void createNewMessage() throws SOAPException {
        this.soapMessage = messageFactory.createMessage();
        this.elements = new HashMap<>();
    }

    public void addNamespaceToenvelope(String prefix, String namespaceUri) throws SOAPException {
        this.soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration(prefix, namespaceUri);
    }

    public void addNamespaceToHeader(String prefix, String namespaceUri) throws SOAPException {
        this.soapMessage.getSOAPHeader().addNamespaceDeclaration(prefix, namespaceUri);
    }

    public void addBodyElementToEnvelope(String naam, String prefix, String namespaceUri) throws SOAPException {
        SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
        SOAPElement bodyElement = soapMessage.getSOAPBody().addBodyElement(envelope.createName(naam, prefix, namespaceUri));
        addAttributes(bodyElement);
        this.elements.put(naam, bodyElement);
    }

    public void addChildElementToHeader(String naam, String prefix, String namespaceUri, String value) throws SOAPException {
        SOAPHeader header = soapMessage.getSOAPHeader();
        SOAPElement headerElement = header.addChildElement(naam, prefix, namespaceUri);
        addAttributes(headerElement);
        if (value.length() > 0) {
            headerElement.addTextNode(value);
        }
        this.elements.put(naam, headerElement);
    }

    public void addBodyElementToEnvelope(String naam) throws SOAPException {
        SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
        SOAPElement bodyElement = soapMessage.getSOAPBody().addBodyElement(envelope.createName(naam));
        addAttributes(bodyElement);
        this.elements.put(naam, bodyElement);
    }

    public void addChildElementToElement(String parentElementNaam, String elementNaam, String prefix, String namespaceUri, String value) throws SOAPException {
        SOAPEnvelope envelope = soapMessage.getSOAPPart().getEnvelope();
        Name name;
        if (namespaceUri.isEmpty() && prefix.isEmpty()) {
            name = envelope.createName(elementNaam);

        } else {
            name = envelope.createName(elementNaam, prefix, namespaceUri);
    }
        addElementToElement(parentElementNaam, elementNaam, name, value);
    }


    private void addElementToElement(String parentIdentifier, String childIdentifier, Name childElementName, String value) throws SOAPException {
        SOAPElement parentElement = findElement(parentIdentifier);
        SOAPElement childElement = parentElement.addChildElement(childElementName);
        addAttributes(childElement);
        if (value != null && value.length() > 0) {
            childElement.setValue(value);
        }
        this.elements.put(parentIdentifier.concat(".").concat(childIdentifier), childElement);
    }

    private SOAPElement findElement(String identifier) throws SOAPException {
        SOAPElement element = this.elements.get(identifier);
        if (element != null) {
            return element;
        } else {
            throw new SOAPException("SOAP Envelope element met naam " + identifier + " niet gevonden.");
        }
    }

    public SOAPMessage getSOAPMessage() throws SOAPException {
        this.soapMessage.saveChanges();
        return this.soapMessage;
    }

    public String getCharacterEncoding() {
        return this.charset;
    }

    private void addAttributes(SOAPElement soapElement) throws SOAPException {
        if (!attributes.isEmpty()) {
            for (HashMap.Entry<QName, String> entry : attributes.entrySet()) {
                soapElement.addAttribute(entry.getKey(), entry.getValue());
            }
        }
        attributes.clear();
    }

    @Override
    public String toString() {
        String output = null;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            this.soapMessage.saveChanges();
            this.soapMessage.writeTo(out);
            output = out.toString(this.charset);
            out.close();
        } catch (IOException | SOAPException ex) {

        }
        return output;
    }

}
