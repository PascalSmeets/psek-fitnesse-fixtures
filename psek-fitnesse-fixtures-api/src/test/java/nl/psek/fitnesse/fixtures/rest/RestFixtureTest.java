package nl.psek.fitnesse.fixtures.rest;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertTrue;

/**
 * Created by pascal on 18-04-2017.
 */
public class RestFixtureTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089); // No-args constructor defaults to port 8080

    @Before
    public void setUp() {

//        setup voor GET
        stubFor(get(urlPathEqualTo("/test"))
                .withQueryParam("search_term", equalTo("WireMock"))
                .withHeader("Accept", containing("xml"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/xml")
                        .withHeader("Allow", "GET")
                        .withBody("<response>Some content</response>")));

//        setup voor POST
        stubFor(post(urlPathEqualTo("/test2"))
                .withRequestBody(equalToJson("{\"store\":{\"reason\":\"test\",\"data\":\"Store this string.\",\"validate\":true}}"))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "text/xml")
                        .withHeader("Allow", "POST, GET")
                        .withBody("POST successful")));

    }


    @Test
    public void getWithQueryParametersTest() {
        RestFixture fixture = new RestFixture();
        fixture.enablelogMessagesToSystemOut();
        fixture.setBaseUri("http://localhost:8089");
        fixture.addQueryParameter("search_term", "WireMock");
        fixture.addHeader("Accept", "xml");
        fixture.sendRequest("get", "/test");

        assertTrue("Statuscode is 200", fixture.getResponseStatusCode() == 200);
        assertTrue("Body is '<response>Some content</response>' ", fixture.getResponseBody().matches("<response>Some content</response>"));
    }

    @Test
    public void postWithBodyTest() {
        RestFixture fixture = new RestFixture();
        fixture.enablelogMessagesToSystemOut();
        fixture.setBaseUri("http://localhost:8089");
        fixture.addHeader("Accept", "xml");
        fixture.addBody("{\n" +
                "  \"store\": {\n" +
                "    \"reason\": \"test\",\n" +
                "    \"data\": \"Store this string.\",\n" +
                "    \"validate\": true\n" +
                "  }\n" +
                "}");
        fixture.sendRequest("post", "/test2");


        assertTrue("Statuscode is 201", fixture.getResponseStatusCode() == 201);
        assertTrue("Body is 'POST successful' ", fixture.getResponseBody().matches("POST successful"));
    }

}