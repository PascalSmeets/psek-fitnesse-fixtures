package nl.psek.fitnesse.fixtures.selenium.browsers;

import nl.psek.fitnesse.fixtures.selenium.SeleniumFixtureException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class with the logic to initialize a new Remote Webdriver for use with a Selenium Grid
 */
public class initRemoteWebDriver {

	private static DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	private static ChromeOptions chromeOptions = new ChromeOptions();
	private static boolean useremotechromeoptions = false;
	private static boolean useproxy = false;
	private static String proxyHost = "";
	private static String proxyPort = "";
	private static final Logger LOG = LoggerFactory.getLogger(initRemoteWebDriver.class);


	private static URL remoteServerUrl;

	public static void setRemoteServerUrl(String url) {
		url = url + (url.endsWith("/") ? "" : "/") + "wd/hub";
		try {
			remoteServerUrl = new URL(url);
		} catch (MalformedURLException e) {
			throw new SeleniumFixtureException(e.getMessage());
		}
	}

	public static void setRemoteBrowser(String browsername, String version) {
		desiredCapabilities.setBrowserName(browsername);
		desiredCapabilities.setVersion(version);
		desiredCapabilities.setPlatform(Platform.ANY);
	}

	public static void addRemoteChromeOption(String option) {
		useremotechromeoptions = true;
		LOG.info("added Chrome option: " + option);
		chromeOptions.addArguments(option);
	}

	public static void setRemoteChromePageLoadStrategy(String strategy) {
		useremotechromeoptions = true;
		chromeOptions.setPageLoadStrategy(PageLoadStrategy.fromString(strategy));
	}

	public static void addDesiredCapability(String key, String value) {
		desiredCapabilities.setCapability(key, value);
	}

	public static void setRemoteProxy(String proxyhost, String proxyport) {
		//for chrome
		proxyHost = proxyhost;
		proxyPort = proxyport;
		useproxy = true;

		//for the rest
		Proxy proxy = new Proxy();
		proxy.setHttpProxy(proxyhost + ":" + proxyport);
		desiredCapabilities.setCapability("proxy", proxy);

	}

	public static RemoteWebDriver getRemoteWebDriver() {
		RemoteWebDriver rwd;
		if (useremotechromeoptions) {
			if (useproxy) {
				Proxy proxy = new Proxy();
				proxy.setHttpProxy(proxyHost + ":" + proxyPort);
				chromeOptions.setProxy(proxy);
			}
			rwd = new RemoteWebDriver(remoteServerUrl, chromeOptions);

			chromeOptions = new ChromeOptions();
			useremotechromeoptions = false;
			useproxy = false;
		} else {
			rwd = new RemoteWebDriver(remoteServerUrl, desiredCapabilities);

			desiredCapabilities = new DesiredCapabilities();
		}
		return rwd;
	}

}
