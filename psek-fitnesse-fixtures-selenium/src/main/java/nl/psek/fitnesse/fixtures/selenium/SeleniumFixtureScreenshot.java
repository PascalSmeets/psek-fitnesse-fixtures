package nl.psek.fitnesse.fixtures.selenium;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static nl.psek.fitnesse.fixtures.selenium.SeleniumFixtureCommon.*;

/**
 * Methods for taking a screenshot of the currently loaded webpage
 */
public class SeleniumFixtureScreenshot {


	public static String makeScreenshotAndReturnLink(WebDriver driver) {

		if (!notNullOrEmpty(SCREENSHOTLOCATION)) {
			throw new SeleniumFixtureException("Please set the default screenshot location. Multiple locations can be separated with ; "
					+ System.lineSeparator()
					+ " Example = src/test/resources/FitnesseRoot/files/screenshots;target/fitnesse-test-results/files/screenshots"
					+ System.lineSeparator()
					+ "|setScreenshotLocation; |src/test/resources/FitnesseRoot/files/screenshots;target/fitnesse-test-results/files/screenshots|"
					+ System.lineSeparator()
					+ "If this location is included in the list: 'src/test/resources/FitnesseRoot/files/screenshots', the method will return a clickable link in the testoutput.");
		}
		
		//take the screenshot

		byte[] scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		String fileName = getTimeStamp() + "_screenshot" + ".png";

		// check if we want to save to multiple locations
		List<Path> saveLocations = new ArrayList<>();

		if (SCREENSHOTLOCATION.contains(";")) {
			LOG.info("Using multiple screenshot locations");
			List<String> locationslist = new ArrayList<>(Arrays.asList(SCREENSHOTLOCATION.split(";")));
			for (String location : locationslist) {
				LOG.info("screenshot location = " + location);

				try {
					Files.createDirectories(Paths.get(location));
				} catch (IOException e) {
					throw new SeleniumFixtureException(e.getMessage());
				}
				saveLocations.add(Paths.get(location, fileName));
			}

		} else {
			saveLocations.add(Paths.get(SCREENSHOTLOCATION, fileName));
			LOG.info("Using one screenshot location");
			LOG.info("screenshot location = " + SCREENSHOTLOCATION);
		}

		try {
			for (Path location : saveLocations) {
				Files.write(location, scrFile);
				LOG.info("File saved at: " + location.toString());

			}
		} catch (IOException e) {
			throw new SeleniumFixtureException(e.getMessage());
		}

		// returning DEFAULT location
		return "<a href=\"files/screenshots/" + fileName + "\" target=\"_blank\"><img src=./files/screenshots/" + fileName + " width=\"300\"></a>";

	}

}
