package nl.psek.fitnesse.fixtures.selenium;

import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.StopTestCommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SeleniumFixtureException extends RuntimeException {
	private final Logger LOG = LoggerFactory.getLogger(SeleniumFixtureException.class);

	public SeleniumFixtureException(String message, Object... args) {
		if (Boolean.parseBoolean(System.getProperty("FSTOPONEXCEPTION"))) {
			LOG.error("Stopping test execution, throwing StopTestCommandException: {} ", message);
			throw new StopTestCommandException(message, args);
		} else {
			LOG.error("Continuing test execution, throwing CommandException: {} ", message);
			throw new CommandException(message, args);
		}
	}

	public SeleniumFixtureException(Exception ex) {
		if (Boolean.parseBoolean(System.getProperty("FSTOPONEXCEPTION"))) {
			LOG.error("Stopping test execution, throwing StopTestCommandException: {} ", ex.getMessage());
			throw new StopTestCommandException(ex);
		} else {
			LOG.error("Continuing test execution, throwing CommandException: {} ", ex.getMessage());
			throw new CommandException(ex);
		}
	}
}
