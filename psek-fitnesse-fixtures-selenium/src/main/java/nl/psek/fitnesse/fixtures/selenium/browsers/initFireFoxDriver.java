package nl.psek.fitnesse.fixtures.selenium.browsers;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.io.File;

/**
 * Class with the logic to initialize a new FirefoxDriver
 */
public class initFireFoxDriver {

	private static FirefoxOptions firefoxOptions = new FirefoxOptions();


	public static void addFireFoxOption(String argument) {
		firefoxOptions.addArguments(argument);
	}

	public static void addFireFoxStringPreference(String key, String value) {
		firefoxOptions.addPreference(key, value);
	}

	public static void addFireFoxIntPreference(String key, int value) {
		firefoxOptions.addPreference(key, value);
	}

	public static void addFireFoxBooleanPreference(String key, boolean value) {
		firefoxOptions.addPreference(key, value);
	}


	public static void setFireFoxProxy(String proxyhost, String proxyport) {
		String proxystring = proxyhost + ":" + proxyport;
		Proxy proxy = new Proxy();
		proxy.setHttpProxy(proxystring).setSslProxy(proxystring);

		firefoxOptions.setProxy(proxy);
	}

	public static void setFireFoxProfile(String profileFolder) {
		FirefoxProfile firefoxProfile = new FirefoxProfile(new File(profileFolder));
		firefoxOptions.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
	}


	public static FirefoxDriver getFireFoxDriver() {
		FirefoxOptions usedOptions = firefoxOptions;
		firefoxOptions = new FirefoxOptions();
		return new FirefoxDriver(usedOptions);

	}

}
