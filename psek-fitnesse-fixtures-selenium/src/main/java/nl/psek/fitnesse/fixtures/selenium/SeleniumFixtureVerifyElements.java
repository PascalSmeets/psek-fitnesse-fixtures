package nl.psek.fitnesse.fixtures.selenium;

import nl.psek.fitnesse.fixtures.util.FixtureOperations;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;

import static nl.psek.fitnesse.fixtures.selenium.SeleniumFixtureCommon.*;

/**
 * Methods concerning the presence and value of elements on the currently loaded webpage
 */
public class SeleniumFixtureVerifyElements {


	static boolean verifyElementPresent(String locator) {
		return !driver.findElements(constructLocatorFromString(locator)).isEmpty();
	}

	static boolean verifyElementNotPresent(String locator) {
		return driver.findElements(constructLocatorFromString(locator)).isEmpty();
	}

	static boolean verifyText(String locator, String pattern) {
		WebElement element = findElement(locator);
		String value = element.getText();

		return FixtureOperations.matchOnPattern(value, pattern);
	}

	static boolean verifyTextPresent(String text) {
		return driver.getPageSource().contains(text);
	}

	static boolean verifyTextNotPresent(String text) {
		return !driver.getPageSource().contains(text);
	}

	static boolean verifyValue(String locator, String pattern) {
		WebElement element = findElement(locator);
		String value = getValueElement(element);

		return FixtureOperations.matchOnPattern(value, pattern);
	}

	static boolean verifyValueEmpty(String locator) {
		return getValueElement(findElement(locator)).isEmpty();
	}

	static boolean verifyValueNotEmpty(String locator) {
		return !verifyValueEmpty(locator);
	}

	static Boolean verifyAttributeValue(String locator, String attribute, String pattern) {
		WebElement element = findElement(locator);
		String value = element.getAttribute(attribute);

		return FixtureOperations.matchOnPattern(value, pattern);
	}

	static Boolean verifyAttributePresent(String locator, String attribute) {
		WebElement element = findElement(locator);
		try {
			String value = element.getAttribute(attribute);
			if (value != null) {
				return true;
			}
		} catch (Exception e) {
			return false;

		}
		return false;
	}



	static boolean verifyOptionSelected(String locator, String value) {
		Select select = findSelect(locator);
		WebElement option = findOptionInSelectByValue(select, value);
		if (option.isSelected()) {
			return true;
		}
		throw new SeleniumFixtureException("The option with value '%s' is not selected, while this test suggested it should be selected.", value);
	}

	static boolean verifyOptionNotSelected(String locator, String value) {
		Select select = findSelect(locator);
		WebElement option = findOptionInSelectByValue(select, value);
		if (!option.isSelected()) {
			return true;
		}
		throw new SeleniumFixtureException("The option with value '%s' is selected, while this test suggested it should be deselected.", value);
	}

	static boolean verifyOptionSelectedByLabel(String locator, String label) {
		Select select = findSelect(locator);
		WebElement option = findOptionInSelectByLabel(select, label);
		if (option.isSelected()) {
			return true;
		}
		throw new SeleniumFixtureException("The option with label '%s' is not selected, while this test suggested it should be selected.", label);
	}

	static boolean verifyOptionNotSelectedByLabel(String locator, String label) {
		Select select = findSelect(locator);
		WebElement option = findOptionInSelectByLabel(select, label);
		if (!option.isSelected()) {
			return true;
		}
		throw new SeleniumFixtureException("The option with label '%s' is selected, while this test suggested it should be deselected.", label);
	}

	static boolean verifyAllOptionsSelected(String locator) {
		Select select = findSelect(locator);

		ArrayList<String> labels = new ArrayList<>();
		for (WebElement option : select.getOptions()) {
			if (!option.isSelected()) {
				labels.add(option.getText());
			}
		}

		if (labels.isEmpty()) {
			return true;
		}
		throw new SeleniumFixtureException("The option(s) with label(s) '%s' are not selected, while this test suggested it should all be selected.", labels);
	}

	static boolean verifyAllOptionsNotSelected(String locator) {
		Select select = findSelect(locator);

		ArrayList<String> labels = new ArrayList<>();
		for (WebElement option : select.getOptions()) {
			if (option.isSelected()) {
				labels.add(option.getText());
			}
		}

		if (labels.isEmpty()) {
			return true;
		}
		throw new SeleniumFixtureException("The option(s) with label(s) '%s' are selected, while this test suggested it should all be deselected.", labels);
	}

	static boolean verifyPatternPresent(String pattern) {
		return FixtureOperations.containsPatternNoInputLogging(driver.getPageSource(), pattern);
	}

	static boolean verifyTextContainsString(String locator, String pattern) {
		WebElement element = findElement(locator);
		String value = element.getText();
		return value.contains(pattern);
	}

	static boolean isElementVisible(String locator) {
		WebElement element = findElement(locator);
		if (element.isDisplayed()) {
			return true;
		}
		throw new SeleniumFixtureException("Element with locator '%s' is not visible (visible but also width and height != 0)", locator);
	}

	static boolean isElementEnabled(String locator) {
		WebElement element = findElement(locator);
		if (element.isEnabled()) {
			return true;
		} else
			throw new SeleniumFixtureException("Element with locator '%s' is not enabled", locator);
	}

	static int countTimesElementPresent(String locator) {
		return findElements(locator).size();
	}

	static boolean verifyTimesElementPresent(String locator, int timesPresent) {
		return countTimesElementPresent(locator) == timesPresent;
	}

	static boolean checkTimesElementPresent(String locator, int timesPresent) {
		if (verifyTimesElementPresent(locator, timesPresent)) {
			return true;
		}
		throw new SeleniumFixtureException("Element with locator '%s' is '%s' times present on the page", locator, countTimesElementPresent(locator));
	}

	static boolean checkWebsiteTitle(String title) {
		if (driver.getTitle().equals(title)) {
			return true;
		}
		throw new SeleniumFixtureException("Website title does not match, expected '%s', found '%s'.", title, driver.getTitle());
	}

}
