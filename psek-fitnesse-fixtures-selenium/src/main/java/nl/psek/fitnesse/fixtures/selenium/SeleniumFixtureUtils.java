package nl.psek.fitnesse.fixtures.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static nl.psek.fitnesse.fixtures.selenium.SeleniumFixtureCommon.CtrlOrCmd;
import static nl.psek.fitnesse.fixtures.selenium.SeleniumFixtureCommon.SYSTEMISMACOS;
import static nl.psek.fitnesse.fixtures.selenium.SeleniumFixturePageInteractions.getAllCookies;
import static nl.psek.fitnesse.fixtures.selenium.SeleniumFixturePageInteractions.getCurrentUrl;

/**
 * Utility methods (mainly logging)
 */
public class SeleniumFixtureUtils {

	protected static final Logger LOG = LoggerFactory.getLogger(SeleniumFixtureUtils.class);

	static boolean isSystemMacOs() {
		String os = System.getProperty("os.name");

		if (os.toLowerCase().startsWith("mac")) {
			LOG.info("Looks like we're running on macOs!");
			return true;
		} else {
			LOG.info("Looks like we're running on Windows or linux!");
			return false;
		}
	}


	/**
	 * Will check is the OS name starts with 'mac'. If this is the case the COMMAND key  will be used instead of the CONTROL key if 'CtrlOrCmd' is used.
	 */
	static void setCmdKeyifMacOs() {
		if (SYSTEMISMACOS) {
			CtrlOrCmd = Keys.COMMAND;
		}
	}

	/**
	 * Log page source.
	 */
	static void logPageSource(WebDriver driver) {
		LOG.info("====== BEGIN LOGGING PAGE SOURCE ======");
		LOG.info(driver.getPageSource());
		LOG.info("======= END LOGGING PAGE SOURCE =======");
	}


	static void logThis(String input) {
		String parsed = input.replaceAll("#LINEBREAK#", "\n");
		LOG.info(parsed);
	}


	static String getPageDebugInformation() {
		String currentUrl = "";
		String allcookies = "";
		String pageSource = "";
		try {
			currentUrl = getCurrentUrl();
		} catch (Exception e) {
		}

		try {
			allcookies = getAllCookies();
		} catch (Exception e) {
		}

		try {
			pageSource = SeleniumFixtureCommon.driver.getPageSource();
		} catch (Exception e) {
		}


		return "BEGIN TEST DEBUG INFORMATION===========================================================" +
				System.lineSeparator() +
				"URL = " + currentUrl +
				System.lineSeparator() +
				"COOKIES = " + allcookies +
				System.lineSeparator() +
				"PAGESOURCE = " + pageSource +
				System.lineSeparator() +
				"END TEST DEBUG INFORMATION=============================================================";
	}


	static String getSupportedXPathVersion(WebDriver driver) {
		try {
			By.xpath("/nobody[@attr=('A'||'')]").findElement(driver);
			return "3.0";
		} catch (Exception e1) {
			try {
				By.xpath("/nobody[@attr=lower-case('A')]").findElement(driver);
				return "2.0";
			} catch (Exception e2) {
				return "1.0";
			}
		}

	}

	static void sleep(long millis) {
		Actions action = new Actions(SeleniumFixtureCommon.driver);
		action.pause(millis);
		action.perform();
		LOG.info("Performed a (thread)Sleep for " + millis + " milliseconds.");

	}

	static String getWorkingDir() {
		return System.getProperty("user.dir");
	}


}
