package nl.psek.fitnesse.fixtures.selenium;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.StopTestCommandException;
import nl.psek.fitnesse.fixtures.util.FileFinder;
import nl.psek.fitnesse.fixtures.util.FileHandler;
import nl.psek.fitnesse.fixtures.util.FixtureOperations;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static nl.psek.fitnesse.util.FitnesseSystemSettings.setStopTestOnException;
import static nl.psek.fitnesse.util.FitnesseSystemSettings.stopTestOnException;


/**
 *  Deprecated: This class uses Selenium 3 to automate webpages, relies on properties files for selenium settings.
 * <p>
 * <p>
 * ## Properties
 * <p>
 * The selenium.properties file is used to define several settings:
 * <p>
 * - browser specification (firefox, chrome, safari, internet explorer, headless or remote)
 * - save location of screenshots made with this fixture
 * - custom local firefox profile
 * - custom local firefox proxy
 * - remote browser setting
 * - remote webdriver server url
 * - custom remote firefox profile
 * <p>
 * <p>
 * ## Initialization
 * <p>
 * Start the fixture by referencing one or more properties files. All property files will be combined to one Java Properties object. This way you could define some general settings (where is the selenium hub) and some project specific settings (which browser to use).
 *
 * @author Ronald Mathies
 * @author Pascal Smeets
 */

@Deprecated
public class SlimWebDriver {


    public final static String BROWSER_FIREFOX = "firefox";
    public final static String BROWSER_CHROME = "chrome";
    public final static String BROWSER_REMOTE_CHROME = "remote.chrome";
    public final static String BROWSER_SAFARI = "safari";
    public final static String BROWSER_INTERNET_EXPLORER = "internet explorer";
    public final static String BROWSER_EDGE = "edge";
    public final static String BROWSER_HEADLESS = "headless";
    public final static String BROWSER_REMOTE = "remote";
    public final static String BROWSER_BROWSERSTACK = "browserstack";
    /**
     * The constant ELEMENT_INPUT.
     */
    public final static String ELEMENT_INPUT = "input";
    /**
     * The constant ELEMENT_TEXTAREA.
     */
    public final static String ELEMENT_TEXTAREA = "textarea";
    /**
     * The constant ATTR_TYPE.
     */
    public final static String ATTR_TYPE = "type";
    /**
     * The constant TYPE_CHECKBOX.
     */
    public final static String TYPE_CHECKBOX = "checkbox";
    /**
     * The constant TYPE_RADIO.
     */
    public final static String TYPE_RADIO = "radio";
    /**
     * The constant ATTR_VALUE.
     */
    public final static String ATTR_VALUE = "value";
    private static final Logger LOG = LoggerFactory.getLogger(SlimWebDriver.class);
    private static final String REGEXPI_SYNTAX = "regexpi:";
    private static final String REGEX_SYNTAX = "regex:";
    private static final String LOCATOR_XPATH = "xpath=";
    private static final String LOCATOR_CSS = "css=";
    private static final String LOCATOR_ID = "id=";
    private static final String LOCATOR_NAME = "name=";
    private static final String LOCATOR_CLASSNAME = "classname=";
    private static final String LOCATOR_LINKTEXT = "link=";
    private static final String LOCATOR_PARTIAlLINKTEXT = "partiallink=";
    private static final String MAC_OS_NAME = "mac";
    private static final String HEADLESSCHROMEWINDOWSIZE = "1600x1200";
    /**
     * The constant SCREENSHOTLOCATION.
     */
    public static String SCREENSHOTLOCATION;
    /**
     * The constant remoteProfile.
     */
    public static FirefoxProfile remoteProfile;
    /**
     * The Dpr.
     */
    static float DPR = 1;
    /**
     * The Useusersupplieddpr.
     */
    static boolean USEUSERSUPPLIEDDPR = false;
    /**
     * The Usepropertyfilesupplieddpr.
     */
    static boolean USEPROPERTYFILESUPPLIEDDPR = false;
    /**
     * The Browser.
     */
    static String BROWSER;
    /**
     * The Webdrivermanagerpropertiesfile.
     */
    static String WEBDRIVERMANAGERPROPERTIESFILE;
    private static String proxyurl;
    private static String noproxylist;
    private static Proxy proxy = null;
    private static boolean TAKESCREENSHOTONEXCEPTION = false;
    private static boolean SYSTEMISMACOS = false;
    private static ArrayList<WebdriverCommand> loopCommands = new ArrayList<>();
    /**
     * The Driver.
     */
    public WebDriver driver = null;
    /**
     * The Ctrl or cmd.
     */
    Keys CtrlOrCmd = Keys.CONTROL;
    private int seconds;
    private Properties properties = null;
    private boolean circumventSelenium3 = false;

    /**
     * Instantiate a new Slim Web Driver without configuration loading
     */
    protected SlimWebDriver() {
        /*
         * To satisfy instantiation without triggering unwanted side effects while inheriting or unit testing
         */
        LOG.info("Starting SlimWebDriver with inherited driver");
    }

    /**
     * Instantiates a new Slim web driver.
     *
     * @param configuration the configuration, this is a reference to one or more `selenium.properties` files.
     *                      If multiple files are used they must be specified in a comma-separated list
     */
    public SlimWebDriver(String configuration) {
        checkIfSystemisMacOs();
        setCmdKeyifMacOs();
        initDriver(configuration);
        LOG.info("This driver supports xpath version: " + getSupportedXPathVersion());
    }

    /**
     * Returns the currect webdriver for use in other fixture
     *
     * @return
     */
    public WebDriver giveWebDriver() {
        return this.driver;
    }

    /**
     * Condition to check if the text is present anywhere on the page.
     *
     * @param pattern the regular expression to search for.
     * @return flag indicating if it did or did not become visible.
     */
    public static ExpectedCondition<Boolean> textToBePresent(final String pattern) {

        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {

                    String input = driver.getPageSource();
                    return FixtureOperations.containsPatternNoInputLogging(input, pattern);
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("text ('%s') to be present anywhere on the page", pattern);
            }
        };
    }

    /**
     * Condition to check if the text is present anywhere on the page.
     *
     * @param locator the \locator to use to find the element.
     * @return flag indicating if it did or did not become visible.
     */
    public static ExpectedCondition<Boolean> elementNotEmpty(final String locator) {

        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    WebElement element = driver.findElement(constructLocatorFromString(locator));
                    String valueElement = getValueElement(element);
                    return valueElement != null && !valueElement.isEmpty();
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("locator '%s' to be present on the page and have a value.", locator);
            }
        };
    }

    /**
     * Condition to check if the text is present anywhere on the page.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value to look for.
     * @return flag indicating if it did or did not become visible.
     */
    public static ExpectedCondition<Boolean> valueToBePresentInElement(final String locator, final String value) {

        return new ExpectedCondition<>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    WebElement element = driver.findElement(constructLocatorFromString(locator));
                    String valueElement = getValueElement(element);
                    return valueElement != null && valueElement.equalsIgnoreCase(value);
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("locator '%s' to be present on the page and have a value.", locator);
            }
        };
    }

    /**
     * Take screenshot on exception.
     *
     * @param TAKESCREENSHOTONEXCEPTION the takescreenshotonexception
     */
    public static void takeScreenshotOnException(boolean TAKESCREENSHOTONEXCEPTION) {
        SlimWebDriver.TAKESCREENSHOTONEXCEPTION = TAKESCREENSHOTONEXCEPTION;
    }

    /**
     * Construct locator from string by.
     *
     * @param locator the \locator
     * @return the by
     */
    static By constructLocatorFromString(String locator) {

        LOG.info("Looking for element: " + locator);

        if (locator.startsWith(LOCATOR_XPATH)) {
            return By.xpath(locator.substring(LOCATOR_XPATH.length()));
        } else if (locator.startsWith(LOCATOR_CSS)) {
            return By.cssSelector(locator.substring(LOCATOR_CSS.length()));
        } else if (locator.startsWith(LOCATOR_ID)) {
            return By.id(locator.substring(LOCATOR_ID.length()));
        } else if (locator.startsWith(LOCATOR_NAME)) {
            return By.name(locator.substring(LOCATOR_NAME.length()));
        } else if (locator.startsWith(LOCATOR_CLASSNAME)) {
            return By.className(locator.substring(LOCATOR_CLASSNAME.length()));
        } else if (locator.startsWith(LOCATOR_LINKTEXT)) {
            return By.linkText(locator.substring(LOCATOR_LINKTEXT.length()));
        } else if (locator.startsWith(LOCATOR_PARTIAlLINKTEXT)) {
            return By.partialLinkText(locator.substring(LOCATOR_PARTIAlLINKTEXT.length()));
        } else {
            return By.xpath(locator);
        }
    }

    /**
     * Get the actual value of an element, for checkboxes and radio buttons it will return <code>true</code> or <code>false</code>.
     *
     * @param element the element
     * @return the value or null.
     */
    public static String getValueElement(WebElement element) {
        String value = null;

        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT)) {
            String type = element.getAttribute(ATTR_TYPE);
            if (type.equalsIgnoreCase(TYPE_CHECKBOX) || type.equalsIgnoreCase(TYPE_RADIO)) {
                value = element.isSelected() ? Boolean.TRUE.toString() : Boolean.FALSE.toString();
            } else {
                value = element.getAttribute(ATTR_VALUE);
            }
        } else if (tagName.equalsIgnoreCase(ELEMENT_TEXTAREA)) {
            return element.getAttribute(ATTR_VALUE);
        } else {
            throw new ConditionalException("Element is not an INPUT element, can only get value of elements of type INPUT.");
        }

        return value;
    }

    private static void initProxyForWebdriver(Properties props) {
        proxyurl = props.getProperty("proxy.url");
        noproxylist = props.getProperty("proxy.noproxylist", "");
        proxy = new Proxy().setHttpProxy(proxyurl).setSslProxy(proxyurl).setFtpProxy(proxyurl);

        LOG.info("Set proxy to: " + proxyurl);
        if (!noproxylist.isEmpty()) {
            proxy.setNoProxy(noproxylist);
            LOG.info("Set noproxylist to: " + noproxylist);
        }
    }

    /**
     * Log page source.
     */
    public void logPageSource() {
        LOG.info("====== BEGIN LOGGING PAGE SOURCE ======");
        LOG.info(driver.getPageSource());
        LOG.info("======= END LOGGING PAGE SOURCE =======");
    }

    /**
     * Log this.
     *
     * @param input the input
     */
    public void logThis(String input) {
        String parsed = input.replaceAll("#LINEBREAK#", "\n");
        LOG.info(parsed);
    }

    /**
     * Gets page debug information.
     *
     * @return the page debug information
     */
    public String getPageDebugInformation() {
        String currentUrl = "";
        String allcookies = "";
        String pageSource = "";
        try {
            currentUrl = getCurrentUrl();
        } catch (Exception e) {
        }

        try {
            allcookies = getAllCookies();
        } catch (Exception e) {
        }

        try {
            pageSource = driver.getPageSource();
        } catch (Exception e) {
        }


        return "BEGIN TEST DEBUG INFORMATION===========================================================" +
                System.lineSeparator() +
                "URL = " + currentUrl +
                System.lineSeparator() +
                "COOKIES = " + allcookies +
                System.lineSeparator() +
                "PAGESOURCE = " + pageSource +
                System.lineSeparator() +
                "END TEST DEBUG INFORMATION=============================================================";
    }

    /**
     * Register the default timeout that will apply to all commands.
     *
     * @param seconds the amount of seconds to wait until the test fails.
     * @throws ConditionalException when the specified seconds is a negative number.
     */
    public void defaultCommandTimeout(int seconds) {
        if (seconds < 1) {
            throw new ConditionalException("Unable to set a negative amount of seconds as the default command timeout.");
        }

        this.seconds = seconds;

        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    /**
     * Register the default timeout that will apply to page loading actions.
     *
     * @param seconds the amount of seconds to wait until the page loads
     * @throws ConditionalException when the specified seconds is a negative number.
     */
    public void defaultPageLoadTimeout(int seconds) {
        if (seconds < 1) {
            throw new ConditionalException("Unable to set a negative amount of seconds as the default page load timeout.");
        }

        driver.manage().timeouts().pageLoadTimeout(seconds, TimeUnit.SECONDS);
    }

    /**
     * Gets all cookies for the current website and returns them as a string
     *
     * @return all cookies
     */
    public String getAllCookies() {
        return driver.manage().getCookies().toString();
    }

    /**
     * Deletes all cookies.
     */
    public void deleteAllCookies() {
        driver.manage().deleteAllCookies();
        LOG.info("Deleted all cookies");
    }

    /**
     * Delete the cookie with the specified name.
     *
     * @param name the name of the cookie.
     */
    public void deleteCookieByName(String name) {
        driver.manage().deleteCookieNamed(name);
        LOG.info("Deleted cookie with name: " + name);
    }

    /**
     * Checks if a cookie exists with the specified <code>name</code>.
     *
     * @param name the name of the cookie.
     * @return the boolean
     * @throws ConditionalException When the cookie with the specified name does not exist.
     */
    public boolean checkIfCookieExists(String name) {
        boolean found = false;
        for (Cookie cookie : driver.manage().getCookies()) {
            if (cookie.getName().equalsIgnoreCase(name)) {
                found = true;
            }
        }

        if (found) {
            return true;
        }
        throw new ConditionalException("Cookie with name '%s' does not exist.", name);
    }

    /**
     * Verifies the value of a cookie  with the specified <code>name</code> using the supplied expected <code>value</code>
     *
     * @param name  the name of the cookie.
     * @param value the expexted value
     * @return the boolean
     * @throws ConditionalException When the cookie with the specified name does not exist.
     * @throws ConditionalException When the cookie with the specified name does not have the specified value.
     */
    public boolean verifyValueOfCookie(String name, String value) {
        for (Cookie cookie : driver.manage().getCookies()) {
            if (cookie.getName().equalsIgnoreCase(name)) {
                if (cookie.getValue().equals(value)) {
                    return true;
                } else {
                    throw new ConditionalException("The value of the cookie with the name '%s' is '%s', but we expected '%s'.", name, cookie.getValue(), value);
                }
            }
        }
        throw new ConditionalException("Cookie with name '%s' does not exist.", name);
    }

    /**
     * Sets a cookie for the curent domain and path with specified name and value
     *
     * @param name  the name of the cookie.
     * @param value the value of the cookie.
     */
    public void setCookieForThisDomain(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        driver.manage().addCookie(cookie);
        LOG.info("Set cookie with name: " + name + " to value: " + value);
    }

    /**
     * Open a new window with the given <code>url</code>
     *
     * @param url the url to call upon opening the new window.
     * @throws ConditionalException when the specified URL is not valid.
     */
    public void openWebsite(String url) {
        try {
            new URL(url);
        } catch (MalformedURLException e) {
            throw new ConditionalException("The specified URL '%s' is not a valid URL.", url);
        }

        driver.get(url);
    }

    /**
     * Gets current url.
     *
     * @return the current url
     */
    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return driver.getTitle();
    }

    /**
     * Maximizes the window
     */
    public void maximizeWindow() {
        driver.manage().window().maximize();
    }

    /**
     * Resizes the window to the specified dimensions
     *
     * @param pixelsWidth  the pixels width
     * @param pixelsHeight the pixels height
     */
    public void resizeWindow(int pixelsWidth, int pixelsHeight) {
        Dimension dimension = new Dimension(pixelsWidth, pixelsHeight);
        driver.manage().window().setSize(dimension);
    }

    /**
     * Moves the window
     *
     * @param horizontal shift in pixels
     * @param vertical   shift in pixels
     */
    public void moveWindow(int horizontal, int vertical) {
        //get position
        int x = driver.manage().window().getPosition().getX();
        int y = driver.manage().window().getPosition().getY();
        int xPos = x + horizontal;
        int yPos = y + vertical;
        //set new position
        driver.manage().window().setPosition(new Point(xPos, yPos));
    }

    /**
     * Sets the window position
     *
     * @param x the x
     * @param y the y
     */
    public void setWindowPosition(int x, int y) {
        driver.manage().window().setPosition(new Point(x, y));
    }

    /**
     * Quit the browser.
     */
    public void closeWebsite() {
        driver.quit();
    }

    /**
     * Close the current window
     */
    public void closeWindow() {
        driver.close();
    }

    /**
     * Pushes a new frame as the current driver.
     *
     * @param locator the \locator to use to find the web element.
     */
    public void switchToFrameUsingLocator(String locator) {
        WebDriver drv = this.driver.switchTo().frame(findElement(locator));
        if (drv == null) {
            throw new ConditionalException("No frame found matching the locator '%s'.", locator);
        }
        this.driver = drv;
    }

    /**
     * Switch to frame using index.
     *
     * @param index the index
     */
    public void switchToFrameUsingIndex(int index) {
        WebDriver drv = this.driver.switchTo().frame(index);
        if (drv == null) {
            throw new ConditionalException("No frame found matching the index '%s'.", index);
        }
        this.driver = drv;

    }

    /**
     * Go back to the initial state.
     */
    public void switchToStart() {
        driver = driver.switchTo().defaultContent();
    }

    /**
     * Pushes a new window as the current driver.
     *
     * @param nameOrHandle the name or id attribute.
     */
    public void switchToWindow(String nameOrHandle) {
        WebDriver drv = this.driver.switchTo().window(nameOrHandle);
        if (drv == null) {
            throw new ConditionalException("No window found where the name or handle corresponds with the value '%s'", nameOrHandle);
        }
        this.driver = drv;
    }

    /**
     * Pushes a new window as the current driver using the window title or a regexp as input
     * Will retrieve all window handles and switch between them. If the title of the window is found we stop. Otherwise switch back to the original window
     *
     * @param pattern the expected window title (optionally witg REGEXPI: prefix)
     */
    public void switchToWindowByTitle(String pattern) {
        // Store the current window handle- the parent one
        String winHandleBefore = driver.getWindowHandle();
        boolean foundCorrectWindow = false;
        // store all the windows
        Set<String> handle = driver.getWindowHandles();
        LOG.info("Set of found handles: " + handle.toString());

        // iterate over window handles
        for (String mywindows : handle) {
            // store window title
            String windowTitleForHandle = driver.switchTo().window(mywindows).getTitle();
            LOG.info("Now trying windows with windowHandle '" + mywindows + "' and title/pattern '" + pattern + "'.");

            // if a REGEXP pattern was supplied use as a regular expression and match against the window title of the current window handle
            if (pattern.startsWith(REGEXPI_SYNTAX)) {
                Pattern compiledPattern = Pattern.compile(pattern.substring(REGEXPI_SYNTAX.length()), Pattern.CASE_INSENSITIVE + Pattern.DOTALL);
                foundCorrectWindow = compiledPattern.matcher(windowTitleForHandle).matches();
            } else if (pattern.startsWith(REGEX_SYNTAX)) {
                Pattern compiledPattern = Pattern.compile(pattern.substring(REGEX_SYNTAX.length()), Pattern.DOTALL);
                foundCorrectWindow = compiledPattern.matcher(windowTitleForHandle).matches();

            }
            // otherwise do a direct comparison
            else {
                foundCorrectWindow = windowTitleForHandle.equals(pattern);
            }

            //Stop if the window was found, otherwise go back to the original
            if (foundCorrectWindow) {
                LOG.info("Found window with title '" + windowTitleForHandle + "' and handle '" + mywindows + "'.");
                break;
            } else {
                LOG.info(" Window with title/pattern '" + pattern + "' not found (yet). Switching back to the original window.");
                driver.switchTo().window(winHandleBefore);
            }
        }
    }

    /**
     * Switches to the opened window that is not the curent window for webdriver.
     */
    public void switchToUnfocussedWindow() {
        String currentHandle = driver.getWindowHandle();
        Set<String> allHandles = driver.getWindowHandles();
        allHandles.remove(currentHandle);

        if (!allHandles.isEmpty()) {
            driver.switchTo().window(allHandles.iterator().next());
        }
    }

    /**
     * Returns the current windowhandle
     *
     * @return the window handle
     */
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    /**
     * Deprecated command, should wait for the page to load but is now implicit.
     */
    public void waitForPageLoad() {
    }

    /**
     * Refreshes the current web site..
     */
    public void refreshWebsite() {
        driver.navigate().refresh();
    }

    /**
     * Presses the browser back button.
     */
    public void browserBack() {
        driver.navigate().back();
    }

    /**
     * Presses the browser back button.
     */
    public void browserForward() {
        driver.navigate().forward();
    }

    /**
     * Checks if the title of the website matches the title supplied.
     *
     * @param title the title of the website.
     * @return true if it matches.
     * @throws ConditionalException when the element can not be found or the value does not match.
     */
    public boolean checkWebsiteTitle(String title) {
        if (driver.getTitle().equals(title)) {
            return true;
        }
        throw new ConditionalException("Website title does not match, expected '%s', found '%s'.", title, driver.getTitle());
    }

    /**
     * Pause is not allowed anymore.
     *
     * @param milliseconds the milliseconds
     */
    public void pause(String milliseconds) {
    }

    /**
     * Pause is not allowed anymore.
     *
     * @param milliseconds the milliseconds
     */
    public void waitForSeconds(String milliseconds) {
    }

    /**
     * Clicks on an element in the page.
     *
     * @param locator the \locator to use to find the element.
     */
    public void click(String locator) {

        try {
            findElement(locator).click();
        } catch (ElementNotVisibleException e) {
            scrollTo(locator);
            findElement(locator).click();
        } catch (ElementClickInterceptedException e) {
            if (circumventSelenium3Mode()) {
                LOG.info("We went into circumventSelenium3Mode and clicked using JavaScript on locator: " + locator + ". An ElementClickInterceptedException was caught.");
                clickUsingJavascript(locator);
            } else {
                throw e;
            }
        }
    }

    /**
     * Click using action.
     *
     * @param locator the \locator
     */
    public void clickUsingAction(String locator) {
        WebElement element = findElement(locator);
        Actions action = new Actions(driver);
        action.moveToElement(element).click().perform();
    }

    /**
     * Clicks on an element in the page that is not visible i.e. hidden
     *
     * @param locator the \locator
     */
    public void clickUsingJavascript(String locator) {
        WebElement element = findElement(locator);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", element);
    }

    /**
     * Richt click / context click action
     *
     * @param locator the \locator
     * @throws ConditionalException when the element can not be found
     */
    public void contextClick(String locator) {
        WebElement element = findElement(locator);
        Actions action = new Actions(driver);
        action.contextClick(element);
        action.perform();
    }

    /**
     * Doubleclicks on an element in the page
     *
     * @param locator the \locator to use to find the element
     * @throws ConditionalException when the element can not be found                                                                   Pascal: DONE
     */
    public void doubleClick(String locator) {
        WebElement element = findElement(locator);
        Actions action = new Actions(driver);
        action.doubleClick(element);
        action.perform();
    }

    /**
     * Clicks on an element in the page to trigger a page load.
     *
     * @param locator the \locator to use to find the element.
     * @throws ConditionalException when the element can not be found.
     */
    public void clickAndWait(String locator) {
        click(locator);
    }

    /**
     * Clicks on an element in the page for a certain number of times..
     *
     * @param locator                the \locator to use to find the element.
     * @param numberOfTimesToExecute the number of times to click the element.
     * @throws ConditionalException when the element can not be found.                                                                   Ronald: DONE
     */
    public void clickUpToTimes(String locator, String numberOfTimesToExecute) {
        int totalNumberOfTimes = Integer.parseInt(numberOfTimesToExecute);
        for (int times = 1; times <= totalNumberOfTimes; times++) {
            findElement(locator).click();
        }
    }

    /**
     * Performs a pause action using the Actions class
     *
     * @param millis the time to pause in milliseconds
     *               meer informatie               http://selenium.googlecode.com/git/docs/api/java/org/openqa/selenium/interactions/Actions.html
     *               let op gebruik alleen als het echt nodig is of voor de debug van je fitnesse test.
     */
    public void sleep(long millis) {
        Actions action = new Actions(driver);
        action.pause(millis);
        action.perform();
        LOG.info("Performed a (thread)Sleep for " + millis + " milliseconds.");

    }

    /**
     * Checks a checkbox (or radiobutton), if it was checked then nothing will be done.
     *
     * @param locator the \locator to use to find the element.
     */
    public void makeChecked(String locator) {
        WebElement element = findElement(locator);

        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT)) {
            String type = element.getAttribute(ATTR_TYPE);
            if (type.equalsIgnoreCase(TYPE_CHECKBOX) || type.equalsIgnoreCase(TYPE_RADIO)) {
                if (!element.isSelected()) {
                    element.click();
                }
            } else {
                throw new ConditionalException("Element found using the locator '%s' is not a checkbox or a radio button, makeChecked only works on a checkbox or radio button.");
            }
        } else {
            throw new ConditionalException("Element found using the locator '%s' is not an input element but a '%s' element.", locator, tagName);
        }
    }

    /**
     * Unchecks a checkbox (or radiobutton), if it was checked then nothing will be done.
     *
     * @param locator the \locator to use to find the element.
     */
    public void makeNotChecked(String locator) {
        WebElement element = findElement(locator);
        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT)) {
            String type = element.getAttribute(ATTR_TYPE);
            if (type.equalsIgnoreCase(TYPE_CHECKBOX) || type.equalsIgnoreCase(TYPE_RADIO)) {
                if (element.isSelected()) {
                    element.click();
                }
            } else {
                throw new ConditionalException("Element found using the locator '%s' is not a checkbox, makeNotChecked only works on a checkbox.");
            }
        } else {
            throw new ConditionalException("Element found using the locator '%s' is not an input element but a '%s' element.", locator, tagName);
        }

    }

    /**
     * Writes the specified value to the element found using the \locator. Will select text in the element first and type over it.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value to write to the element.
     * @throws ConditionalException when the element can not be found or is not an INPUT or TEXTAREA element.
     */
    public void type(String locator, String value) {

        if (SYSTEMISMACOS) {
            typeWithClear(locator, value);
            return;
        } else {
            WebElement element = findElement(locator);
            String tagName = element.getTagName();
            if (tagName.equalsIgnoreCase(ELEMENT_INPUT) || tagName.equalsIgnoreCase(ELEMENT_TEXTAREA)) {
                element.sendKeys(Keys.chord(CtrlOrCmd, "a"), value);
                return;
            }
            throw new ConditionalException("Element found using the locator '%s' is not an input or textarea element but a '%s' element.", locator, tagName);
        }
    }

    /**
     * Types a value in an element on the page using Javascript
     *
     * @param locator the \locator
     * @param value   the text to be typed
     */
    public void typeUsingJavascript(String locator, String value) {
        WebElement element = findElement(locator);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute(\"value\",\"" + value + "\");", element);
    }

    /**
     * Tab out of the element found using the locator
     *
     * @param locator the locator to use to find the element
     * @throws ConditionalException when the element can not be found
     */
    public void tab(String locator) {
        WebElement element = findElement(locator);
        element.sendKeys(Keys.TAB);
    }

    /**
     * Scrolls up to the element using the \locator.
     *
     * @param locator the \locator to use to find the element.
     */
    public void scrollUpTo(String locator) {
        WebElement element = findElement(locator);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(false);", element);
    }

    /**
     * Scrolls down to the element using the \locator.
     *
     * @param locator the \locator to use to find the element.
     */
    public void scrollDownTo(String locator) {
        WebElement element = findElement(locator);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    /**
     * Scroll to.
     *
     * @param locator the \locator
     */
    public void scrollTo(String locator) {
        WebElement element = findElement(locator);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;

        try {
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
            isElementVisible(locator);
        } catch (ConditionalException e) {
            javascriptExecutor.executeScript("arguments[0].scrollIntoView(false);", element);
            isElementVisible(locator);
        }
    }


    /**
     * Writes the specified value to the element found using the \locator.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value to write to the element.
     * @throws ConditionalException when the element can not be found or is not an INPUT or TEXTAREA element.
     */
    public void typeWithClear(String locator, String value) {
        WebElement element = findElement(locator);
        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT) || tagName.equalsIgnoreCase(ELEMENT_TEXTAREA)) {
            element.clear();
            element.sendKeys(value);
            return;
        }
        throw new ConditionalException("Element found using the locator '%s' is not an input or textarea element but a '%s' element.", locator, tagName);
    }

    /**
     * Types the specified value if the element is present. Can not be used as a test condition - returntype is void. Method will catch all exceptions
     * Use in the test preparation steps.
     *
     * @param locator the \locator
     * @param value   the value
     */
    public void typeIfElementPresent(String locator, String value) {
        Boolean valueBefore = Boolean.getBoolean(System.getProperty("FSTOPONEXCEPTION"));
        System.setProperty("FSTOPONEXCEPTION", "false");
        try {
            if (isElementVisible(locator) && isElementEnabled(locator)) {
                type(locator, value);
                LOG.info("Element " + locator + " was visible, value " + value + " was entered");
            }
        } catch (Exception ignored) {
            LOG.info("Element " + locator + " was not visible or present, no action was taken");
        }
        System.setProperty("FSTOPONEXCEPTION", valueBefore.toString());
    }

    /**
     * Type without clear.
     *
     * @param locator the \locator
     * @param value   the value
     */
    public void typeWithoutClear(String locator, String value) {
        WebElement element = findElement(locator);
        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT) || tagName.equalsIgnoreCase(ELEMENT_TEXTAREA)) {
            element.sendKeys(value);

            return;
        }
        throw new ConditionalException("Element found using the locator '%s' is not an input or textarea element but a '%s' element.", locator, tagName);
    }

    /**
     * Type key code without clear.
     *
     * @param locator the \locator
     * @param keycode the keycode
     */
    public void typeKeyCodeWithoutClear(String locator, Keys keycode) {
        WebElement element = findElement(locator);
        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase(ELEMENT_INPUT) || tagName.equalsIgnoreCase(ELEMENT_TEXTAREA)) {
            element.sendKeys(keycode);
            return;

        }
        throw new ConditionalException("Element found using the locator '%s' is not an input or textarea element but a '%s' element.", locator, tagName);
    }

    /**
     * Type key code using actions.
     *
     * @param keycode the keycode
     */
    public void typeKeyCodeUsingActions(Keys keycode) {
        Actions action = new Actions(driver);
        action.sendKeys(keycode).build().perform();
    }

    /** @name Getting Data From A Page
     *  The following methods are used to get data from a webpage
     */
    ///@{

    /**
     * Returns the text that is present in the specified \locator.
     *
     * @param locator the \locator to use to find the element.
     * @return the text
     */
    public String getText(String locator) {
        WebElement element = findElement(locator);

        return element.getText();
    }

    /**
     * Returns the value that is present in the specified \locator.
     *
     * @param locator the \locator to find the element
     * @return the value
     */
    public String getValue(String locator) {
        WebElement element = findElement(locator);

        return getValueElement(element);

    }

    ///@}


    /** @name Verifying Data On A Page
     *  The following methods are used to verify data on a webpage
     */
    ///@{

    /**
     * Verifies if the text at the specified \locator is compliant with the given pattern.
     * If the \locator refers to an input element it will check if it is a checkbox or radio button, if so it will use "true" or "false", if it
     * is any other input element it will use the value of the attribute "value".
     *
     * @param locator the \locator to use to find the element.
     * @param pattern the pattern.
     * @return the boolean
     */
    public boolean verifyText(String locator, String pattern) {
        WebElement element = findElement(locator);
        String value = element.getText();

        return FixtureOperations.matchOnPattern(value, pattern);
    }

    /**
     * Verifies if the text at the specified \locator is compliant with the given pattern.
     * If the \locator refers to an input element it will check if it is a checkbox or radio button, if so it will use "true" or "false", if it
     * is any other input element it will use the value of the attribute "value".
     *
     * @param locator the \locator to use to find the element.
     * @param pattern the pattern.
     * @return the boolean
     */
    public boolean verifyValue(String locator, String pattern) {
        WebElement element = findElement(locator);
        String value = getValueElement(element);

        return FixtureOperations.matchOnPattern(value, pattern);
    }
    ///@}


    /**
     * Returns the value of an attribute at a specific \locator
     *
     * @param locator   the \locator to find the element on the page.
     * @param attribute the name of the attribute.
     * @return the attribute
     */
    public String getAttribute(String locator, String attribute) {
        WebElement element = findElement(locator);
        return element.getAttribute(attribute);
    }

    /**
     * Returns the CSS value at a specific \locator
     *
     * @param locator the \locator to find the element on the page.
     * @param style   the css style to check, e.g. color, background-color, font-size.
     * @return the css value
     */
    public String getCssValue(String locator, String style) {
        WebElement element = findElement(locator);
        return element.getCssValue(style);
    }

    /**
     * Verify attribute present boolean.
     *
     * @param locator   the \locator
     * @param attribute the attribute
     * @return the boolean
     */
    public Boolean verifyAttributePresent(String locator, String attribute) {
        WebElement element = findElement(locator);
        try {
            String value = element.getAttribute(attribute);
            if (value != null) {
                return true;
            }
        } catch (Exception e) {
            return false;

        }
        return false;
    }

    /**
     * Verify attribute value boolean.
     *
     * @param locator   the \locator
     * @param attribute the attribute
     * @param pattern   the pattern
     * @return the boolean
     */
    public Boolean verifyAttributeValue(String locator, String attribute, String pattern) {
        WebElement element = findElement(locator);
        String value = element.getAttribute(attribute);

        return FixtureOperations.matchOnPattern(value, pattern);
    }

    /**
     * Verifies if the given text is present anywhere in the source of the page.
     *
     * @param text the value to verify.
     * @return the boolean
     */
    public boolean verifyTextPresent(String text) {
        return driver.getPageSource().contains(text);
    }

    /**
     * Verify pattern present boolean.
     *
     * @param pattern the pattern
     * @return the boolean
     */
    public boolean verifyPatternPresent(String pattern) {
        return FixtureOperations.containsPatternNoInputLogging(driver.getPageSource(), pattern);
    }

    /**
     * Verifies if the given text is not present anywhere in the source of the page.
     *
     * @param text the value to verify.
     * @return the boolean
     */
    public boolean verifyTextNotPresent(String text) {
        return !driver.getPageSource().contains(text);
    }

    /**
     * Verifies that a specific String is present in a specific part of the page
     *
     * @param locator the \locator
     * @param pattern the pattern
     * @return boolean
     */
    public Boolean verifyTextContainsString(String locator, String pattern) {
        WebElement element = findElement(locator);
        String value = element.getText();
        return value.contains(pattern);
    }

    /**
     * Waits for an element to become editable.
     *
     * @param locator the \locator to use to find the element.
     * @throws ConditionalException when the element was not found or the element did not become editable.
     */
    public void waitForEditable(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (element != null && element.isDisplayed() && element.isEnabled()) {
            LOG.info("Element " + locator + "is now editable");
        } else try {
            WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(element));
            if (webElement == null) {
                throw new ConditionalException("Element with locator '%s' did not become editable within '%d' seconds.", locator);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("Element with locator '%s' did not become editable within '%d' seconds, a timeout occurred.", locator, seconds);
        }


    }

    /**
     * Waits for an element to become not editable.
     *
     * @param locator the \locator to use to find the element.
     * @throws ConditionalException when the element was not found or the element did  not become un-editable.
     */
    public void waitForNotEditable(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (element != null && element.isDisplayed() && !element.isEnabled()) {
            LOG.info("Element " + locator + "is now not editable");
        } else try {
            Boolean notClickable = wait.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(element)));
            if (!notClickable) {
                throw new ConditionalException("Element with locator '%s' did not become un-editable within '%d' seconds.", locator);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("Element with locator '%s' did not become un-editable within '%d' seconds, a timeout occurred.", locator, seconds);
        }

    }

    /**
     * Waits for an element to become present in the dom tree.
     * Will also return true if the element was already present.
     *
     * @param locator the \locator to use to find the element.
     * @return the boolean
     * @throws ConditionalException when the element was not found or the element did not appear in the dom tree.
     */
    public boolean waitForElementPresent(String locator) {
        if (verifyElementPresent(locator)) {
            return true;
        } else {

            WebDriverWait wait = new WebDriverWait(driver, seconds);
            try {
                WebElement webElement = wait.until(ExpectedConditions.presenceOfElementLocated(constructLocatorFromString(locator)));
                if (webElement == null) {
                    throw new ConditionalException("Element with locator '%s' did not become present within '%d' seconds", locator);
                }
            } catch (TimeoutException timeoutException) {
                throw new ConditionalException("Element with locator '%s' did not become present within '%d' seconds, a timeout occurred.", locator, seconds);
            }
            return true;
        }
    }

    /**
     * Mag nog mooier
     * <p>
     * Clicks on an element until a different element becomes present
     *
     * @param elementToClick   the element to click
     * @param elementToWaitFor the element to wait for
     * @param commandTimeout   - alternative command timeout for this command, needs to be set
     * @param maxAttempts      the max attempts
     */
    public void clickUntilElementPresent(String elementToClick, String elementToWaitFor, long commandTimeout, int maxAttempts) {

        Boolean isPresent = findElements((elementToWaitFor)).size() > 0;   //controleer of het element er al is, return true
        driver.manage().timeouts().implicitlyWait(commandTimeout, TimeUnit.SECONDS);                        // zet de command timeout op de waarde die is meegegeven

        int currentAttempt = 0;
        while (isPresent == false && currentAttempt < maxAttempts) {                                          // als het er niet is doen we dit
            findElement(elementToClick).click();                    // klik op een element
            isPresent = findElements((elementToWaitFor)).size() > 0;       //controleer of het element er al is, return true
            currentAttempt++;
        }

        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);                               // zet de command timeout weer op de default waarde
        if (currentAttempt >= maxAttempts) {
            LOG.info("The max number of attempts has been reached");
        }
        LOG.info("It took approximately: " + String.valueOf((commandTimeout) * currentAttempt) + "seconds.");
    }

    /**
     * Click if other element present.
     *
     * @param elementToClick   the element to click
     * @param elementToWaitFor the element to wait for
     */
    public void clickIfOtherElementPresent(String elementToClick, String elementToWaitFor) {
        Boolean isPresent = findElements((elementToWaitFor)).size() > 0;
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (isPresent == true) {
            click(elementToClick);
        } else try {
            WebElement webElement = wait.until(ExpectedConditions.presenceOfElementLocated(constructLocatorFromString(elementToWaitFor)));
            if (webElement == null) {  //Als het element niet gevonden wordt na het wachten hoeft er ook niet geklikt te worden
            } else click(elementToClick); // als het element wel aanwezig is na het wachten: voer een click actie uit}
        } catch (TimeoutException timeoutException) { // In het geval van een TimeoutException geven niets terug, het element is niet gevonden in de gestelde tijd
        }
    }

    /**
     * Click if other element not present.
     *
     * @param elementToClick           the element to click
     * @param elementThatShoudBeAbsent the element that shoud be absent
     */
    public void clickIfOtherElementNotPresent(String elementToClick, String elementThatShoudBeAbsent) {
        if (verifyElementNotPresent(elementThatShoudBeAbsent)) {
            click(elementToClick);
        }
    }

    /**
     * Check that the element is not present
     *
     * @param locator the \locator to find the element.
     * @return true if there are no elements present with the specified \locator.
     */
    public boolean verifyElementNotPresent(String locator) {
        return driver.findElements(constructLocatorFromString(locator)).isEmpty();
    }

    /**
     * Check that the element is present
     *
     * @param locator the \locator
     * @return true if there are elements present with the specified \locator.
     */
    public boolean verifyElementPresent(String locator) {

        List<WebElement> elements = driver.findElements(constructLocatorFromString(locator));
        if (elements.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Counts the number of times an element is present on the page
     *
     * @param locator the \locator
     * @return int with the times the element is present
     */
    public int countTimesElementPresent(String locator) {
        return findElements(locator).size();
    }

    /**
     * Verifies that a specific element is x times present on the page
     *
     * @param locator      the \locator
     * @param timesPresent the times present
     * @return true or false
     */
    public boolean verifyTimesElementPresent(String locator, int timesPresent) {
        return countTimesElementPresent(locator) == timesPresent;
    }

    /**
     * Checks that a specific element is x times present on the page. If the element is not x times present a ConditionalExceptionType is thrown.
     *
     * @param locator      the \locator
     * @param timesPresent the times present
     * @return true or a ConditionalExceptionType with the number of times the element was found
     */
    public boolean checkTimesElementPresent(String locator, int timesPresent) {
        if (verifyTimesElementPresent(locator, timesPresent)) {
            return true;
        }
        throw new ConditionalException("Element with locator '%s' is '%s' times present on the page", locator, countTimesElementPresent(locator));
    }

    /**
     * Returns true if the element is not present, otherwise
     * Waits for an element to become detached from the dom tree.
     *
     * @param locator the \locator to use to find the element.
     * @return the boolean
     * @throws ConditionalException when the element was not found or the element did not detach itself from the dom tree.
     */
    public boolean waitForElementNotPresent(String locator) {
        if (!verifyElementPresent(locator)) {
            return true;
        } else {

            WebElement element = findElement(locator);

            WebDriverWait wait = new WebDriverWait(driver, seconds);
            try {
                Boolean stale = wait.until(ExpectedConditions.stalenessOf(element));
                if (!stale) {
                    throw new ConditionalException("Element with locator '%s' did not become absent within '%d' seconds", locator);
                }
            } catch (TimeoutException timeoutException) {
                throw new ConditionalException("Element with locator '%s' did not become absent within '%d' seconds, a timeout occurred.", locator, seconds);
            }

            return true;
        }
    }

    /**
     * Waits for an element to become visible (meaning visible and width and height != 0).
     *
     * @param locator the \locator to use to find the element.
     * @throws ConditionalException when the element was not found or the element did not become visible.
     */
    public void waitForVisible(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (element != null && element.isDisplayed()) {
            LOG.info("Element: " + locator + " is now present and displayed");
        } else try {
            WebElement webElement = wait.until(ExpectedConditions.visibilityOf(element));
            if (webElement.isDisplayed()) {
                throw new ConditionalException("Element with locator '%s' did not become visible (visible but also width and height != 0) within '%d' seconds.", locator, seconds);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("Element with locator '%s' did not become visible (visible but also width and height != 0) within '%d' seconds, a timeout occurred.", locator, seconds);
        }

    }

    /**
     * Checks if the element is visible
     *
     * @param locator the \locator
     * @return the boolean
     * @throws ConditionalException when the element was not visible.
     */
    public boolean isElementVisible(String locator) {
        WebElement element = findElement(locator);
        if (element.isDisplayed()) {
            return true;
        }
        throw new ConditionalException("Element with locator '%s' is not visible (visible but also width and height != 0)", locator);
    }

    /**
     * Is element enabled boolean.
     *
     * @param locator the \locator
     * @return the boolean
     */
    public boolean isElementEnabled(String locator) {
        WebElement element = findElement(locator);
        if (element.isEnabled()) {
            return true;
        } else
            throw new ConditionalException("Element with locator '%s' is not enabled", locator);
    }

    /**
     * Wait for element to be clickable.
     *
     * @param locator the \locator
     */
    public void waitForElementToBeClickable(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(element));
            if (webElement.isDisplayed()) {
                throw new ConditionalException("Element with locator '%s' did not become clickable within '%d' seconds.", locator, seconds);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("Element with locator '%s' did not become clickable within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        LOG.info("Element: " + locator + " is now clickable");
    }


    /**
     * Wait for an alert dialog and when it appears it will accept it.
     *
     * @throws ConditionalException when a timeout occurs, a webdriver exception occurs or when the dialog is not appearing.
     */
    public void waitForAlertAndAccept() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            alert.accept();
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("Timeout while waiting for message dialog.");
        } catch (WebDriverException webdriverException) {
            throw new ConditionalException("WebDriver exception while waiting for message dialog.");
        }

    }

    /**
     * Wait for an alert dialog and when it appears it will accept it.
     * If it doesn't appear in the wait time of 2 seconds, we continue
     *
     * @throws ConditionalException when a timeout occurs, a webdriver exception occurs or when the dialog is not appearing.
     */
    public void waitForPossibleAlertAndAccept() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 2);
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            alert.accept();
        } catch (TimeoutException timeoutException) {
            LOG.info("No alert during wait time, continuing");
        } catch (WebDriverException webdriverException) {
            throw new ConditionalException("WebDriver exception while waiting for message dialog.");
        }

    }

    /**
     * Wait for an alert dialog and when it appears it will dismiss it.
     *
     * @throws ConditionalException when a timeout occurs, a webdriver exception occurs or when the dialog is not appearing.
     */
    public void waitForAlertAndDismiss() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            alert.dismiss();
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("Timeout while waiting for message dialog.");
        } catch (WebDriverException webdriverException) {
            throw new ConditionalException("WebDriver exception while waiting for message dialog.");
        }
    }

    /**
     * Wait for an alert dialog and when it appears it will dismiss it.
     * If it doesn't appear in the wait time of 2 seconds, we continue
     *
     * @throws ConditionalException when a timeout occurs, a webdriver exception occurs or when the dialog is not appearing.
     */
    public void waitForPossibleAlertAndDismiss() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 2);
            Alert alert = wait.until(ExpectedConditions.alertIsPresent());
            alert.dismiss();
        } catch (TimeoutException timeoutException) {
            LOG.info("No alert during wait time, continuing");
        } catch (WebDriverException webdriverException) {
            throw new ConditionalException("WebDriver exception while waiting for message dialog.");
        }
    }

    /**
     * Executes the specified javascript in the browser.
     *
     * @param script the javascript to execute.
     * @throws ConditionalException when the javascript failed to execute.
     */
    public void executeJavaScript(String script) {
        try {
            JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
            javascriptExecutor.executeScript(script);
        } catch (Exception e) {
            throw new ConditionalException("Failed to execute javascript.");
        }

    }

    /**
     * Execute java script with return value string.
     *
     * @param script the script
     * @return the string
     */
    public String executeJavaScriptWithReturnValue(String script) {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        return javascriptExecutor.executeScript(script).toString();
    }

    /**
     * Waits for an element to become invisible (meaning visible and width and height != 0).
     *
     * @param locator the \locator to use to find the element.
     * @throws ConditionalException when the element was not found or the element did not become visible.
     */
    public void waitForNotVisible(String locator) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (element != null && !element.isDisplayed()) {
            LOG.info("Element: " + locator + " is already not visible");
        } else try {
            Boolean visible = wait.until(ExpectedConditions.invisibilityOfElementLocated(constructLocatorFromString(locator)));
            if (!visible) {
                throw new ConditionalException("Element with locator '%s' did not become invisible (visible but also width and height != 0) within '%d' seconds.", locator, seconds);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("Element with locator '%s' did not become invisible (visible but also width and height != 0) within '%d' seconds, a timeout occurred.", locator, seconds);
        }

        LOG.info("Element: " + locator + " is now not visible");
    }

    /**
     * Waits for the text to become visible anywhere on the page.
     *
     * @param text the text to search for.
     * @return the boolean
     * @throws ConditionalException when the text did not become visible.
     */
    public boolean waitForTextPresent(String text) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (verifyPatternPresent(text)) {
            return true;
        } else try {
            Boolean present = wait.until(textToBePresent(text));
            if (!present) {
                throw new ConditionalException("The text '%s' was not visible after '%d' seconds.", text, seconds);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("The text '%s' did not become present within '%d' seconds, a timeout occurred.", text, seconds);
        }

        return true;
    }

    /**
     * Waits for the text to become absent anywhere on the page.
     *
     * @param text the text to search for.
     * @return the boolean
     * @throws ConditionalException when the text did not become visible.
     */
    public boolean waitForTextNotPresent(String text) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (verifyTextNotPresent(text)) {
            return true;
        } else try {
            Boolean present = wait.until(ExpectedConditions.not(textToBePresent(text)));
            if (!present) {
                throw new ConditionalException("The text '%s' was still visible after '%d' seconds.", text, seconds);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("The text did was still present after '%d' seconds, a timeout occurred.", text, seconds);
        }

        return true;
    }

    /**
     * Waits for the text to become visible in the specified \locator.
     *
     * @param locator the \locator to use to find the element.
     * @param text    the text to search for.
     * @return the boolean
     * @throws ConditionalException when the text did not become visible.
     */
    public boolean waitForText(String locator, String text) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (verifyText(locator, text)) {
            return true;
        } else try {
            Boolean present = wait.until(ExpectedConditions.textToBePresentInElement(element, text));
            if (!present) {
                throw new ConditionalException("The element with locator '%s' did not have the text '%s'.", locator, text);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("The element with locator '%s' did not have the text '%s', a timeout occurred.", locator, text);
        }

        return true;
    }

    /**
     * Waits for the specified text to be removed / adjusted.
     *
     * @param locator the \locator to use to find the element.
     * @param text    the text to search for.
     * @return the boolean
     * @throws ConditionalException when the text did not change.
     */
    public boolean waitForNotText(String locator, String text) {
        WebElement element = findElement(locator);
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (!verifyText(locator, text)) {
            return true;
        } else try {
            Boolean present = wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(element, text)));
            if (!present) {
                throw new ConditionalException("The element with locator '%s' still has the text '%s'.", locator, text);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("The element with locator '%s' still has the text '%s', a timeout occurred.", locator, text);
        }

        return true;
    }

    /**
     * Waits for the text to become absent anywhere on the page.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value to search for.
     * @return the boolean
     * @throws ConditionalException when the text did not become visible.
     */
    public boolean waitForValue(String locator, String value) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (verifyValue(locator, value)) {
            return true;
        } else try {
            Boolean present = wait.until(valueToBePresentInElement(locator, value));
            if (!present) {
                throw new ConditionalException("The element with locator '%s' did not have the value '%s'.", locator, value);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("The element with locator '%s' did not have the value '%s', a timeout occurred.", locator, value);
        }

        return true;
    }

    /**
     * Waits for the element to have a value.
     *
     * @param locator the \locator to use to find the element.
     * @return the boolean
     * @throws ConditionalException when the text did not become visible.
     */
    public boolean waitForValueNotEmpty(String locator) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        if (verifyValueNotEmpty(locator)) {
            return true;
        } else try {
            Boolean present = wait.until(elementNotEmpty(locator));
            if (!present) {
                throw new ConditionalException("The element at the locator '%s' did not have a value.", locator);
            }
        } catch (TimeoutException timeoutException) {
            throw new ConditionalException("The element with locator '%s' still not has a value after '%d' seconds, a timeout occurred.", locator, seconds);
        }

        return true;
    }

    /**
     * Checks whether a value is empty.
     *
     * @param locator the \locator
     * @return the boolean
     */
    public boolean verifyValueEmpty(String locator) {
        return getValueElement(findElement(locator)).isEmpty();
    }

    /**
     * Checks whether a value is not empty.
     *
     * @param locator the \locator
     * @return the boolean
     */
    public boolean verifyValueNotEmpty(String locator) {
        return !verifyValueEmpty(locator);
    }

    /**
     * Selects an option in a select box using the specified label.
     *
     * @param locator the \locator to use to find the element.
     * @param label   the label of the option within the select box.
     * @return true if successful
     * @throws ConditionalException when the element could not be found, is not an SELECT element, there is no option with the specified label.
     */
    public boolean selectUsingLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);

        if (option.isSelected()) {
            quitDriverIfRequired();
            throw new ConditionalException("The option with label '%s' cannot be selected since the option was already selected. Add 'allowDefault' to prevent this exception.", label);
        }

        select.selectByVisibleText(label);
        return true;
    }

    /**
     * Select using label.
     *
     * @param locator      the \locator
     * @param label        the label
     * @param keywordAllow the keyword allow
     */
    public void selectUsingLabel(String locator, String label, String keywordAllow) {
        Boolean allowDefault = false;

        if (keywordAllow.equalsIgnoreCase("allowdefault")) {
            allowDefault = true;
        }

        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);

        if (!allowDefault && option.isSelected()) {
            throw new ConditionalException("The option with label '%s' cannot be selected since the option was already selected and the keyword 'allowDefault' was not  added to the command. Instead '%s' was added to the command.", label, keywordAllow);
        }

        select.selectByVisibleText(label);
    }

    /**
     * Selects the specified value if the element is present. Can not be used as a test condition - returntype is void. Method will catch all exceptions
     * Use in the test preparation steps.
     *
     * @param locator the \locator
     * @param label   the label
     */
    public void selectUsingLabelIfElementPresent(String locator, String label) {
        Boolean valueBefore = stopTestOnException();
        setStopTestOnException(false);
        try {
            Select select = findSelect(locator);
            WebElement option = findOptionInSelectByLabel(select, label);

            if (select != null && option != null) {
                select.selectByVisibleText(label);
            }
        } catch (Exception ignored) {
        }
        setStopTestOnException(valueBefore);
    }

    /**
     * Deselects an option in a select box using the specified label.
     *
     * @param locator the \locator to use to find the element.
     * @param label   the label of the option within the select box.
     * @throws ConditionalException when the element could not be found, is not an SELECT element,                                  there is no option with the specified label or when the option was not selected in the first place.
     */
    public void deselectUsingLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);

        if (!option.isSelected()) {
            throw new ConditionalException("The option with label '%s' cannot be deselected since the option was not selected.", label);
        }

        select.deselectByVisibleText(label);
    }

    /**
     * Selects an option in a select box using the specified value.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value of the option within the select box.
     * @return true if successful
     * @throws ConditionalException when the element could not be found, is not an SELECT element, there is no option with the specified value.
     */
    public boolean select(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);

        if (option.isSelected()) {
            throw new ConditionalException("The option with value '%s' cannot be selected since the option was already selected.", value);
        }

        select.selectByValue(value);
        return true;
    }

    /**
     * Selects an option in a select box using the specified value to trigger a page load.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value of the option within the select box.
     * @throws ConditionalException when the element could not be found, is not an SELECT element, there is no option with the specified value.
     */
    public void selectAndWait(String locator, String value) {
        select(locator, value);
    }

    /**
     * Deselects an option in a select box using the specified value.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value of the option within the select box.
     * @throws ConditionalException when the element could not be found, is not an SELECT element,                                  there is no option with the specified value or when the option was not selected in the first place.
     */
    public void deselect(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);

        if (!option.isSelected()) {
            throw new ConditionalException("The option with value '%s' cannot be deselected since the option was not selected.", value);
        }

        select.deselectByValue(value);
    }

    /**
     * Selects all options in a select.
     *
     * @param locator the \locator to use to find the element.
     * @throws ConditionalException when the element could not be found or when the element is not an SELECT element.
     */
    public void selectAllOptions(String locator) {
        Select select = findSelect(locator);
        for (WebElement option : select.getOptions()) {
            if (!option.isSelected()) {
                select.selectByVisibleText(option.getText());
            }
        }

    }

    /**
     * Deselects all options in a select box.
     *
     * @param locator the \locator to use to find the element.
     * @return true if successful
     * @throws ConditionalException when the element could not be found.
     */
    public boolean deselectAllOptions(String locator) {
        Select select = findSelect(locator);
        select.deselectAll();
        return true;
    }

    /**
     * Verifies if an option in a select box is selected.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value
     * @return the boolean
     */
    public boolean verifyOptionSelected(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);
        if (option.isSelected()) {
            return true;
        }
        throw new ConditionalException("The option with value '%s' is not selected, while this test suggested it should be selected.", value);
    }

    /**
     * Verifies if an option in a select box is not selected.
     *
     * @param locator the \locator to use to find the element.
     * @param value   the value
     * @return the boolean
     */
    public boolean verifyOptionNotSelected(String locator, String value) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByValue(select, value);
        if (!option.isSelected()) {
            return true;
        }
        throw new ConditionalException("The option with value '%s' is selected, while this test suggested it should be deselected.", value);
    }

    /**
     * Verifies if an option in a select box is selected.
     *
     * @param locator the \locator to use to find the element.
     * @param label   the label of the option.
     * @return the boolean
     */
    public boolean verifyOptionSelectedByLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);
        if (option.isSelected()) {
            return true;
        }
        throw new ConditionalException("The option with label '%s' is not selected, while this test suggested it should be selected.", label);
    }

    /**
     * Returns the selected label of the select box
     *
     * @param locator the \locator to use to find the element.
     * @return the option selected
     */
    public String getOptionSelected(String locator) {
        Select select = findSelect(locator);
        WebElement option = select.getFirstSelectedOption();

        return option.getText();

    }

    /**
     * Verifies if an option in a select box is not selected.
     *
     * @param locator the \locator to use to find the element.
     * @param label   the label
     * @return the boolean
     */
    public boolean verifyOptionNotSelectedByLabel(String locator, String label) {
        Select select = findSelect(locator);
        WebElement option = findOptionInSelectByLabel(select, label);
        if (!option.isSelected()) {
            return true;
        }
        throw new ConditionalException("The option with label '%s' is selected, while this test suggested it should be deselected.", label);
    }

    /**
     * Verifies if all the options in a select box are selected.
     *
     * @param locator the \locator to use to find the element.
     * @return the boolean
     */
    public boolean verifyAllOptionsSelected(String locator) {
        Select select = findSelect(locator);

        ArrayList<String> labels = new ArrayList<String>();
        for (WebElement option : select.getOptions()) {
            if (!option.isSelected()) {
                labels.add(option.getText());
            }
        }

        if (labels.isEmpty()) {
            return true;
        }
        throw new ConditionalException("The option(s) with label(s) '%s' are not selected, while this test suggested it should all be selected.", labels);
    }

    /**
     * Verifies if all the options in a select box are not selected.
     *
     * @param locator the \locator to use to find the element.
     * @return the boolean
     */
    public boolean verifyAllOptionsNotSelected(String locator) {
        Select select = findSelect(locator);

        ArrayList<String> labels = new ArrayList<>();
        for (WebElement option : select.getOptions()) {
            if (option.isSelected()) {
                labels.add(option.getText());
            }
        }

        if (labels.isEmpty()) {
            return true;
        }
        throw new ConditionalException("The option(s) with label(s) '%s' are selected, while this test suggested it should all be deselected.", labels);
    }

    /**
     * Takes a screenshot using Selenium.TakesScreenshot
     *
     * @return the string
     * @throws IOException the io exception
     */
    public String takeScreenshot() throws IOException {
        //take the screenshot

        byte[] scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        String fileName = getTimeStamp() + "_screenshot" + ".png";

        // check if we want to save to multiple locations
        List<Path> saveLocations = new ArrayList<>();

        if (SCREENSHOTLOCATION.contains(";")) {
            LOG.info("Using multiple screenshot locations");
            List<String> locationslist = new ArrayList<>(Arrays.asList(SCREENSHOTLOCATION.split(";")));
            for (String location : locationslist) {
                LOG.info("screenshot location = " + location);

                Files.createDirectories(Paths.get(location));
                saveLocations.add(Paths.get(location, fileName));
            }

        } else {
            saveLocations.add(Paths.get(SCREENSHOTLOCATION, fileName));
            LOG.info("Using one screenshot location");
            LOG.info("screenshot location = " + SCREENSHOTLOCATION);
        }

        try {
            for (Path location : saveLocations) {
                Files.write(location, scrFile);
                LOG.info("File saved at: " + location.toString());

            }
        } catch (IOException e) {
            throw new CommandException(e.getMessage());
        }

        // returning DEFAULT location
        return "<a href=\"files/screenshots/" + fileName + "\" target=\"_blank\"><img src=./files/screenshots/" + fileName + " width=\"300\"></a>";

    }

    // BEGIN
    // Looping with webdriver commands

    /**
     * Sets loop command.
     *
     * @param command the command
     */
    public void setLoopCommand(String command) {
        loopCommands.add(new WebdriverCommand(command));
    }

    /**
     * Sets loop command.
     *
     * @param command the command
     * @param target  the target
     */
    public void setLoopCommand(String command, String target) {
        loopCommands.add(new WebdriverCommand(command, target));
    }

    /**
     * Sets loop command.
     *
     * @param command the command
     * @param target  the target
     * @param value   the value
     */
    public void setLoopCommand(String command, String target, String value) {
        loopCommands.add(new WebdriverCommand(command, target, value));
    }

    /**
     * Sets loop command.
     *
     * @param command the command
     * @param target  the target
     * @param value   the value
     * @param param   the param
     */
    public void setLoopCommand(String command, String target, String value, String param) {
        loopCommands.add(new WebdriverCommand(command, target, value, param));
    }


    // END
    // Looping with webdriver commands

    /**
     * Reset loop commands.
     */
    public void resetLoopCommands() {
        loopCommands.clear();
    }

    /**
     * Run loop.
     *
     * @param numberOfTimes            the number of times
     * @param timeBetweenLoopsInMillis the time between loops in millis
     */
    public void runLoop(int numberOfTimes, int timeBetweenLoopsInMillis) {
        for (int x = 0; x < numberOfTimes; x++) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
            try {
                Thread.sleep(timeBetweenLoopsInMillis);
            } catch (InterruptedException e) {
                //do nothing
            }
        }
    }

    /**
     * Run loop command if element is present.
     *
     * @param locator the \locator
     */
    public void runLoopCommandIfElementIsPresent(String locator) {
        if (verifyElementPresent(locator)) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
        }
    }

    /**
     * Run loop until element present boolean.
     *
     * @param locator                  the \locator
     * @param maxTimesToLoop           the max times to loop
     * @param timeBetweenLoopsInMillis the time between loops in millis
     * @return the boolean
     */
    public boolean runLoopUntilElementPresent(String locator, int maxTimesToLoop, int timeBetweenLoopsInMillis) {
        int current = 0;
        while (current < maxTimesToLoop && !verifyElementPresent(locator)) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
            try {
                Thread.sleep(timeBetweenLoopsInMillis);
            } catch (InterruptedException e) {
                //do nothing
            }
            current++;
        }
        return verifyElementPresent(locator);
    }

    /**
     * Run loop until javascript return value boolean.
     *
     * @param javascript                    the javascript
     * @param expectedExpressionReturnValue the expected expression return value
     * @param maxTimesToLoop                the max times to loop
     * @param timeBetweenLoopsInMillis      the time between loops in millis
     * @return the boolean
     */
    public boolean runLoopUntilJavascriptReturnValue(String javascript, String expectedExpressionReturnValue, int maxTimesToLoop, int timeBetweenLoopsInMillis) {
        int current = 0;
        while (current < maxTimesToLoop && !((JavascriptExecutor) driver).executeScript(javascript).equals(expectedExpressionReturnValue)) {
            for (WebdriverCommand command : loopCommands) {
                command.execute();
            }
            try {
                Thread.sleep(timeBetweenLoopsInMillis);
            } catch (InterruptedException e) {
                //do nothing
            }
            current++;
        }
        return ((JavascriptExecutor) driver).executeScript(javascript).equals(expectedExpressionReturnValue);
    }

    /**
     * Enable textbox.
     *
     * @param locator the \locator
     */
    public void enableTextbox(String locator) {
        JavascriptExecutor javascript = (JavascriptExecutor) driver;
        String toenable = "document.getElementsByName('lname')[0].removeAttribute('disabled');";
        javascript.executeScript(toenable);
    }

    /**
     * Disable textbox.
     *
     * @param locator the \locator
     */
    public void disableTextbox(String locator) {
        JavascriptExecutor javascript = (JavascriptExecutor) driver;
        String todisable = "document.getElementsByName('fname')[0].setAttribute('disabled', '');";
        javascript.executeScript(todisable);
    }


    /**
     * Find select select.
     *
     * @param locator the \locator
     * @return the select
     */
//ifndef DOXYGEN_SHOULD_SKIP_THIS
    public Select findSelect(String locator) {
        WebElement element = findElement(locator);

        String tagName = element.getTagName();
        if (tagName.equalsIgnoreCase("select")) {
            return new Select(element);
        }
        throw new ConditionalException("Element found using the locator '%s' is not a select element but a '%s' element.", locator, tagName);
    }

    /**
     * Find option in select by label web element.
     *
     * @param select the select
     * @param label  the label
     * @return the web element
     */
    public WebElement findOptionInSelectByLabel(Select select, String label) {
        for (WebElement option : select.getOptions()) {
            if (option.getText().equals(label)) {
                return option;
            }
        }
        throw new ConditionalException("The select element does not have an option with label '%s'", label);
    }

    /**
     * Find option in select by value web element.
     *
     * @param select the select
     * @param value  the value
     * @return the web element
     */
    public WebElement findOptionInSelectByValue(Select select, String value) {
        for (WebElement option : select.getOptions()) {
            if (option.getAttribute(ATTR_VALUE).equals(value)) {
                return option;
            }
        }
        throw new ConditionalException("The select element does not have an option with value '%s'", value);
    }

    /**
     * Find element web element.
     *
     * @param locator the \locator
     * @return the web element
     */
    public WebElement findElement(String locator) {
        try {
            return driver.findElement(constructLocatorFromString(locator));
        } catch (NoSuchElementException e) {
            throw new ConditionalException("No element found using the locator '%s'.", locator);
        }
    }

    /**
     * Find elements list.
     *
     * @param locator the \locator
     * @return the list
     */
    public List<WebElement> findElements(String locator) {
        try {

            List<WebElement> elements = driver.findElements(constructLocatorFromString(locator));
            if (elements.size() > 0) {
                return elements;

            } else throw new ConditionalException("No element found using the locator '%s'.", locator);

        } catch (NoSuchElementException e) {
            throw new ConditionalException("No element found using the locator '%s'.", locator);
        }
    }
//endif /* DOXYGEN_SHOULD_SKIP_THIS */

    /**
     * Method to send inut to a INPUT element with TYPE="FILE". If a RemoteWebdriver is used the file will be streamed to the selenium node. If a local Webdriver is used this command will type the filename in the input field.
     *
     * @param locator  the \locator
     * @param fileName the file name
     */
    public void uploadFile(final String locator, final String fileName) {
        WebElement element = findElement(locator);
        if (properties.get("webdriver.browser").toString().equalsIgnoreCase(BROWSER_REMOTE)) {
            ((RemoteWebElement) element).setFileDetector(new LocalFileDetector());
            element.sendKeys(fileName);
        } else {
            element.sendKeys(fileName);
        }
    }

    /**
     * Gets working dir.
     *
     * @return the working dir
     */
    public String getWorkingDir() {
        return System.getProperty("user.dir");
    }

    /**
     * Upload file.
     *
     * @param locator      the \locator
     * @param fileLocation the file location
     * @param fileName     the file name
     */
    public void uploadFile(final String locator, String fileLocation, String fileName) {
        final String fileToUpload = getWorkingDir() + fileLocation + fileName;

        WebElement element = findElement(locator);
        if (properties.get("webdriver.browser").toString().equalsIgnoreCase(BROWSER_REMOTE)) {
            ((RemoteWebElement) element).setFileDetector(new LocalFileDetector());
            element.sendKeys(fileToUpload);
        } else {
            element.sendKeys(fileToUpload);
        }

    }


//ifndef DOXYGEN_SHOULD_SKIP_THIS

    /**
     * Loads the specified properties into memory, supports multiple properties files if they are comma-separated
     *
     * @param configuration single or multiple properties files, if multiple then comma-separated
     * @return properties object
     */
    public Properties loadPropertiesFromFiles(String configuration) {
        LOG.info("Loading properties for SlimWebDriver");
        Properties properties = new Properties();
//		Split configuration into array if it contains a comma character
        if (configuration.contains(",")) {
            LOG.info("Using multiple properties files: " + configuration);
            String[] configfiles = configuration.split(Pattern.quote(","));

//			For each configfile in the array
            for (String configfile : configfiles) {
                FileFinder.findFileInUserDirectory(configfile, new FileHandler() {
                    @Override
                    public void handle(File file) {
                        try {
                            // load the properties
                            LOG.info("Found properties file " + file.getAbsolutePath());
                            properties.load(new FileInputStream(file));
                        } catch (IOException ie) {
                            throw new StopTestCommandException("Error while reading files: " + configuration + ".");
                        }
                    }
                });
            }
        } else {
            LOG.info("Using a single properties file: " + configuration);
            FileFinder.findFileInUserDirectory(configuration, new FileHandler() {
                @Override
                public void handle(File file) {
                    try {
                        // load the properties
                        LOG.info("Found properties file " + file.getAbsolutePath());
                        properties.load(new FileInputStream(file));
                    } catch (IOException ie) {
                        throw new StopTestCommandException("Error while reading file: " + configuration + ".");
                    }
                }
            });
        }
        logProperties(properties);
        return properties;
    }

    /**
     * Log properties.
     *
     * @param properties the properties
     */
    public void logProperties(Properties properties) {
        StringWriter writer = new StringWriter();
        try {
            properties.store(writer, "");
        } catch (IOException e) {
        }
        LOG.info("Loaded properties: ");
        LOG.info(writer.getBuffer().toString());
    }
//endif /* DOXYGEN_SHOULD_SKIP_THIS */

    /**
     * Initialize the correct WebDriver for the browser specified.
     *
     * @param configuration the configuration to use.
     */
    private void initDriver(String configuration) {

        properties = loadPropertiesFromFiles(configuration);

        try {

            BROWSER = (String) properties.get("webdriver.browser");
            LOG.info("Using browser: " + BROWSER);

            // set the screenshotlocation for this driver and create the folder if it does not exist
            SCREENSHOTLOCATION = (String) properties.get("screenshot.location");
//			Files.createDirectories(Paths.get(SCREENSHOTLOCATION));

            // Setup of webdriver custom drivers
            setWebdriverDriverProperties(properties);

            //prepare proxy if it is defined in the properties file(s)
            if (properties.containsKey("proxy.url")) {
                initProxyForWebdriver(properties);
            }
            if (BROWSER.equalsIgnoreCase(BROWSER_FIREFOX)) {
                initFirefoxDriver();
            } else if (BROWSER.equalsIgnoreCase(BROWSER_CHROME)) {
                initChromeDriver();
            } else if (BROWSER.equalsIgnoreCase(BROWSER_REMOTE_CHROME)) {
                initRemoteChromeDriver();
            } else if (BROWSER.equalsIgnoreCase(BROWSER_SAFARI)) {
                this.driver = new SafariDriver();
            } else if (BROWSER.equalsIgnoreCase(BROWSER_INTERNET_EXPLORER)) {
                this.driver = new InternetExplorerDriver();
            } else if (BROWSER.equalsIgnoreCase(BROWSER_EDGE)) {
                this.driver = new EdgeDriver();
            } else if (BROWSER.equalsIgnoreCase(BROWSER_REMOTE)) {
                initRemoteDriver();
            } else if (BROWSER.equalsIgnoreCase(BROWSER_HEADLESS)) {
                HtmlUnitDriver webdriver = new HtmlUnitDriver(BrowserVersion.getDefault());
                webdriver.setJavascriptEnabled(true);
                this.driver = webdriver;
            } else if (BROWSER.equalsIgnoreCase(BROWSER_BROWSERSTACK)) {
                this.driver = initBrowserstackDriver(properties);
            } else {
                throw new CommandException("The browser '%s' is not supported, please use any of the following '%s','%s', '%s' or '%s'.", BROWSER, BROWSER_FIREFOX, BROWSER_CHROME, BROWSER_SAFARI, BROWSER_HEADLESS);
            }
        } catch (IOException ie) {
            throw new ConditionalException("Error while reading file 'selenium.properties'.");
        }
    }

    private void initChromeDriver() throws RuntimeException {
        if (properties.containsKey("chrome.options") && properties.containsKey("chrome.options.separator.regex")) {
            ChromeOptions options = new ChromeOptions();

            LOG.info("Instantiated chromeoptions");
            String allchromeoptions = properties.getProperty("chrome.options");
            String optionsregex = properties.getProperty("chrome.options.separator.regex");
            LOG.info("loaded list and separator");


            if (allchromeoptions.contains(optionsregex)) {
                LOG.info("it's a list, splitting");
                List<String> chromeoptionslist = new ArrayList<>(Arrays.asList(allchromeoptions.split(optionsregex)));
                options.addArguments(chromeoptionslist);
            } else {
                LOG.info("it's not a list, using as a whole");
                options.addArguments(allchromeoptions);
            }

            this.driver = new ChromeDriver(options);
        } else {

            this.driver = new ChromeDriver();
        }
    }

    private void initRemoteChromeDriver() {
        MutableCapabilities options = null;

        // set chrome options if there are any defined in the properties file
        if (properties.containsKey("chrome.options") && properties.containsKey("chrome.options.separator.regex")) {
            options = addChromeOptionsFromProperties(properties);
        }
        // add proxy to options if it is set
        if (proxy != null) {
            if (options == null) {                   // for the remote driver without previous options!
                options = new ChromeOptions();
            }
            ((ChromeOptions) options).setProxy(proxy);
        }
        if (options != null) {
            this.driver = new RemoteWebDriver(giveRemoteServerUrl(), options);
        } else {
            DesiredCapabilities capabilities = new DesiredCapabilities("chrome", "", Platform.ANY);
            this.driver = new RemoteWebDriver(giveRemoteServerUrl(), capabilities);
        }
    }

    private void initRemoteDriver() {
        if (properties.containsKey("webdriver.remote.firefox.profile")) {
            FileFinder.findDirectoryInUserDirectory((String) properties.get("webdriver.remote.firefox.profile"), new FileHandler() {
                @Override
                public void handle(File firefoxProfileDir) {
                    remoteProfile = new FirefoxProfile(firefoxProfileDir);
                }
            });
            String remoteBrowser = (String) properties.get("webdriver.remote.browser");
            String remoteBrowserVersion = (String) properties.get("webdriver.remote.browser.version");
            DesiredCapabilities capabilities = new DesiredCapabilities(remoteBrowser, remoteBrowserVersion, Platform.ANY);
            capabilities.setCapability(FirefoxDriver.PROFILE, remoteProfile);
            this.driver = new RemoteWebDriver(giveRemoteServerUrl(), capabilities);
        } else {
            String remoteBrowser = (String) properties.get("webdriver.remote.browser");
            String remoteBrowserVersion = (String) properties.get("webdriver.remote.browser.version");

            DesiredCapabilities capabilities = new DesiredCapabilities(remoteBrowser, remoteBrowserVersion, Platform.ANY);
            this.driver = new RemoteWebDriver(giveRemoteServerUrl(), capabilities);
        }
    }

    private void initFirefoxDriver() {
        if (properties.containsKey("webdriver.firefox.profile")) {
            FileFinder.findDirectoryInUserDirectory((String) properties.get("webdriver.firefox.profile"), firefoxProfileDir -> {
                FirefoxProfile profile = new FirefoxProfile(firefoxProfileDir);
                FirefoxOptions firefoxOptions = new FirefoxOptions();

                firefoxOptions.setCapability(FirefoxDriver.PROFILE, profile);
                this.driver = new FirefoxDriver(firefoxOptions);

                LOG.info("Using " + BROWSER_FIREFOX + " with a profile from folder: " + (String) properties.get("webdriver.firefox.profile"));
            });
        } else if (properties.containsKey("webdriver.firefox.proxy")) {
            String PROXY = (String) properties.get("webdriver.firefox.proxy");
            Proxy proxy = new Proxy();
            proxy.setHttpProxy(PROXY).setFtpProxy(PROXY).setSslProxy(PROXY);
            DesiredCapabilities cap = new DesiredCapabilities();
            cap.setCapability(CapabilityType.PROXY, proxy);
            FirefoxDriver firefoxDriver = new FirefoxDriver(cap);
            this.driver = firefoxDriver;
            LOG.info("Using PROXY from property file " + PROXY);
        } else {
            FirefoxDriver firefoxDriver = new FirefoxDriver();
            this.driver = firefoxDriver;
        }

    }

    private WebDriver initBrowserstackDriver(Properties properties) throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        Properties desiredcapabilitieslist;
        String USERNAME = (String) properties.get("browserstack.username");
        String AUTOMATE_KEY = (String) properties.get("browserstack.automate.key");
        String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";


        // Generate desired capabilities from list in properties file
        String BROWSERSTACK_CAPABILITIES_LIST = (String) properties.get("browserstack.capabilities");
        String BROWSERSTACK_CAPABILITIES_SEPARATOR = (String) properties.get("browserstack.capabilities.separator");

        try {
            desiredcapabilitieslist = buildProperties(BROWSERSTACK_CAPABILITIES_LIST, BROWSERSTACK_CAPABILITIES_SEPARATOR);
        } catch (IOException e) {
            throw new ConditionalException("Could not read desired capabilitie list from properties file. " + e.getMessage());
        }

        Enumeration enukeys = desiredcapabilitieslist.keys();
        while (enukeys.hasMoreElements()) {
            String key = (String) enukeys.nextElement();
            String value = desiredcapabilitieslist.getProperty(key);
            caps.setCapability(key, value);
        }

        return new RemoteWebDriver(new URL(URL), caps);

    }

    private ChromeOptions addChromeOptionsFromProperties(Properties props) {
        ChromeOptions options = new ChromeOptions();
        LOG.info("Instantiated chromeoptions");
        String allchromeoptions = props.getProperty("chrome.options");
        String optionsregex = props.getProperty("chrome.options.separator.regex");
        LOG.info("loaded list and separator");


        if (allchromeoptions.contains(optionsregex)) {
            LOG.info("it's a list, splitting");
            List<String> chromeoptionslist = new ArrayList<>(Arrays.asList(allchromeoptions.split(optionsregex)));
            options.addArguments(chromeoptionslist);
        } else {
            LOG.info("it's not a list, using as a whole");
            options.addArguments(allchromeoptions);
        }

        return options;
    }

    private URL giveRemoteServerUrl() {
        String url = (String) properties.get("webdriver.remote.server.url");
        url = url + (url.endsWith("/") ? "" : "/") + "wd/hub";
        return makeUrl(url);

    }

    private Properties buildProperties(String propertiesFromString, String entrySeparator) throws IOException {
        Properties properties = new Properties();
        properties.load(new StringReader(propertiesFromString.replaceAll(entrySeparator, "\n")));
        return properties;
    }


    private void setWebdriverDriverProperties(Properties properties) {

        LOG.info("Loading and setting webdriver driver properties");

        if (SYSTEMISMACOS) {
            LOG.info("webdriver.gecko.driver = " + (String) properties.get("webdriver.macos.gecko.driver"));
            LOG.info("webdriver.chrome.driver = " + (String) properties.get("webdriver.macos.chrome.driver"));
            try {
                System.setProperty("webdriver.gecko.driver", (String) properties.get("webdriver.macos.gecko.driver"));
            } catch (Exception e) {
                LOG.info("ERROR Loading and setting webdriver.gecko.driver driver property. Will try to continue.");
            }
            try {
                System.setProperty("webdriver.chrome.driver", (String) properties.get("webdriver.macos.chrome.driver"));
            } catch (Exception e) {
                LOG.info("ERROR Loading and setting webdriver.chrome.driver driver property. Will try to continue.");
            }

        } else {
            LOG.info("webdriver.gecko.driver = " + (String) properties.get("webdriver.gecko.driver"));
            LOG.info("webdriver.chrome.driver = " + (String) properties.get("webdriver.chrome.driver"));
            LOG.info("webdriver.edge.driver = " + (String) properties.get("webdriver.edge.driver"));
            LOG.info("webdriver.ie.driver = " + (String) properties.get("webdriver.ie.driver"));
            try {
                System.setProperty("webdriver.gecko.driver", (String) properties.get("webdriver.gecko.driver"));
            } catch (Exception e) {
                LOG.info("ERROR Loading and setting webdriver.gecko.driver driver property. Will try to continue.");
            }
            try {
                System.setProperty("webdriver.chrome.driver", (String) properties.get("webdriver.chrome.driver"));
            } catch (Exception e) {
                LOG.info("ERROR Loading and setting webdriver.chrome.driver driver property. Will try to continue.");
            }
            try {
                System.setProperty("webdriver.edge.driver", (String) properties.get("webdriver.edge.driver"));
            } catch (Exception e) {
                LOG.info("ERROR Loading and setting webdriver.edge.driver driver property. Will try to continue.");
            }
            try {
                System.setProperty("webdriver.ie.driver", (String) properties.get("webdriver.ie.driver"));
            } catch (Exception e) {
                LOG.info("ERROR Loading and setting webdriver.ie.driver driver property. Will try to continue.");
            }
        }
        if (properties.containsKey("screen.dpr")) {
            try {
                DPR = Float.parseFloat(properties.getProperty("screen.dpr"));

                USEPROPERTYFILESUPPLIEDDPR = true;
                LOG.info("Found property key 'screen.dpr' in the supplied selenium properties file. Using value " + DPR);
            } catch (Exception e) {
            }
        }

    }

    /**
     * Called before a StopTestException is thrown to quit the current driver
     */
    private void quitDriverIfRequired() {
        if (stopTestOnException()) {
            driver.quit();
        }
    }

    /**
     * Will check is the OS name starts with 'mac'. If this is the case the COMMAND key  will be used instead of the CONTROL key if 'CtrlOrCmd' is used.
     */
    private void setCmdKeyifMacOs() {
        if (SYSTEMISMACOS) {
            CtrlOrCmd = Keys.COMMAND;
        }
    }

    private void checkIfSystemisMacOs() {
        String os = System.getProperty("os.name");

        if (os.toLowerCase().startsWith(MAC_OS_NAME)) {
            SYSTEMISMACOS = true;
            LOG.info("Looks like we're running on macOs!");
        } else {
            LOG.info("Looks like we're running on Windows or linux!");
        }
    }

    /**
     * Generates a URL object, stops testing if URL is malformed
     *
     * @param input string
     * @return URL
     * @throws StopTestCommandException if URL is malformed
     */
    private URL makeUrl(String input) {
        URL url = null;
        try {
            url = new URL(input);
        } catch (MalformedURLException e) {
            throw new StopTestCommandException(e.getMessage());
        }
        return url;
    }

    /**
     * Gets supported x path version.
     *
     * @return the supported x path version
     */
    public String getSupportedXPathVersion() {
        try {
            By.xpath("/nobody[@attr=('A'||'')]").findElement(driver);
            return "3.0";
        } catch (Exception e1) {
            try {
                By.xpath("/nobody[@attr=lower-case('A')]").findElement(driver);
                return "2.0";
            } catch (Exception e2) {
                return "1.0";
            }
        }
    }

    /**
     * Sets download path.
     *
     * @param path the path
     */
    public void setDownloadPath(String path) {
        Downloader.setLocalDownloadPath(path);
    }

    /**
     * Sets download proxy.
     *
     * @param proxyHost the proxy host
     * @param proxyport the proxyport
     */
    public void setDownloadProxy(String proxyHost, int proxyport) {
        Downloader.downloadProxyHost = proxyHost;
        Downloader.downloadProxyPort = proxyport;
    }

    /**
     * Sets download proxy basic credentials.
     *
     * @param username the username
     * @param password the password
     */
    public void setDownloadProxyBasicCredentials(String username, String password) {
        Downloader.downloadProxyUsernameBasic = username;
        Downloader.downloadProxyPasswordBasic = password;
    }

    /**
     * Sets download proxy ntlm credentials.
     *
     * @param username    the username
     * @param password    the password
     * @param workstation the workstation
     * @param domain      the domain
     */
    public void setDownloadProxyNtlmCredentials(String username, String password, String workstation, String domain) {
        Downloader.downloadProxyUsernameNtlm = username;
        Downloader.downloadProxyPasswordNtlm = password;
        Downloader.downloadProxyWorstationNtlm = workstation;
        Downloader.downloadProxyDomainNtlm = domain;
    }

    /**
     * Download a file from a \locator using the selenium session (cookies, login state, etc..) and save it in the java TEMP folder using the specified name.
     * This method resolves the 'href' attribute of the locator and downloads the file
     *
     * @param locator  the \locator
     * @param filename the filename
     * @return full path to the file
     * @throws Exception the exception
     */
    public String downloadFile(String locator, String filename) throws Exception {
        Downloader downloader = new Downloader(this.driver);
        return downloader.downloadFile(locator, filename);

    }

    /**
     * Download a file from a URL using the selenium session (cookies, login state, etc..) and save it in the java TEMP folder using the specified name.
     *
     * @param url
     * @param filename
     * @return full path to the file
     */
    public String downloadUrl(String url, String filename) {
        Downloader downloader = new Downloader(this.driver);
        return downloader.downloadUrl(url, filename);
    }


    /**
     * Download an image from a \locator using the selenium session (cookies, login state, etc..) and save it in the java TEMP folder using the specified name.
     * This method resolves the 'src' attribute of the locator and downloads the image
     *
     * @param locator  the \locator
     * @param filename the filename
     * @return full path to the image
     * @throws Exception the exception
     */
    public String downloadImage(String locator, String filename) throws Exception {
        Downloader downloader = new Downloader(driver);
        return downloader.downloadFile(locator, filename);

    }

    private String getTimeStamp() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssS");
        Date date = new Date();
        return (dateFormat.format(date));
    }

    /**
     * The type Webdriver command.
     */
    protected class WebdriverCommand {
        private String command;
        private String target;
        private String value;
        private String param;

        /**
         * Instantiates a new Webdriver command.
         *
         * @param command the command
         * @param target  the target
         * @param value   the value
         * @param param   the param
         */
        protected WebdriverCommand(String command, String target, String value, String param) {
            this(command, target, value);
            this.param = param;
        }

        /**
         * Instantiates a new Webdriver command.
         *
         * @param command the command
         * @param target  the target
         * @param value   the value
         */
        protected WebdriverCommand(String command, String target, String value) {
            this(command, target);
            this.value = value;
        }

        /**
         * Instantiates a new Webdriver command.
         *
         * @param command the command
         */
        protected WebdriverCommand(String command) {
            this.command = command.replaceAll(";", "");
        }

        /**
         * Instantiates a new Webdriver command.
         *
         * @param command the command
         * @param target  the target
         */
        protected WebdriverCommand(String command, String target) {
            this(command);
            this.target = target;
        }

        /**
         * Execute.
         */
        protected void execute() {
            try {
                if (target == null && value == null) {
                    try {
                        Method method = SlimWebDriver.this.getClass().getMethod(command);
                        method.invoke(SlimWebDriver.this);
                    } catch (NoSuchMethodException e) {
                        throw new ConditionalException(e.getMessage());
                    }

                } else if (target != null && value == null) {
                    try {
                        Method method = SlimWebDriver.this.getClass().getMethod(command, String.class);
                        method.invoke(SlimWebDriver.this, target);
                    } catch (NoSuchMethodException e) {
                        throw new ConditionalException(e.getMessage());
                    }

                } else if (target != null && value != null && param == null) {
                    try {
                        Method method = SlimWebDriver.this.getClass().getMethod(command, String.class, String.class);
                        method.invoke(SlimWebDriver.this, target, value);
                    } catch (NoSuchMethodException e) {
                        throw new ConditionalException(e.getMessage());
                    }
                } else {
                    try {
                        Method method = SlimWebDriver.this.getClass().getMethod(command, String.class, String.class, String.class);
                        method.invoke(SlimWebDriver.this, target, value, param);
                    } catch (NoSuchMethodException e) {
                        throw new ConditionalException(e.getMessage());
                    }
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new ConditionalException(e.getMessage());

            }

        }
    }

    public void circumventSelenium3Limitations() {
        LOG.info("One some methods Selenium3 limitations will be overridden (overlay on element you want to click)");
        circumventSelenium3 = true;
    }

    public void restoreSelenium3Limitations() {
        LOG.info("Selenium3 default behaviour restored");
        circumventSelenium3 = false;
    }

    private boolean circumventSelenium3Mode() {
        return circumventSelenium3;
    }

}