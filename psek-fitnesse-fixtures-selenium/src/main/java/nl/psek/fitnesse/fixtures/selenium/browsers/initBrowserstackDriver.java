package nl.psek.fitnesse.fixtures.selenium.browsers;

import nl.psek.fitnesse.StopTestCommandException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class with the logic to initialize a new Remote Webdriver for use with BrowserStack
 */
public class initBrowserstackDriver {

    private static DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
    private static URL remoteurl;

    /**
     * Sets the url to the browserstack instance, should look like this
     * https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
     *
     * @param url the url to the browserstack instance
     */
    public static void setBrowserStackUrl(String url) {
        try {
            remoteurl = new URL(url);
        } catch (MalformedURLException e) {
            throw new StopTestCommandException("The provided URL is malformed: " + url);
        }
    }

    public static void addDesiredCapability(String key, String value) {
        desiredCapabilities.setCapability(key, value);
    }

    public static RemoteWebDriver getBrowserstackDriver() {
        RemoteWebDriver rd = new RemoteWebDriver(remoteurl, desiredCapabilities);
        reset();
        return rd;
    }

    private static void reset() {
        desiredCapabilities = new DesiredCapabilities();
        remoteurl = null;

    }


}
