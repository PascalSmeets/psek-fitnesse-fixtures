package nl.psek.fitnesse.fixtures.selenium.browsers;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class with the logic to initialize a new SafariDriver
 */
public class initSafariDriver {

    private static final Logger LOG = LoggerFactory.getLogger(initSafariDriver.class);
    private static SafariOptions safariOptions = new SafariOptions();


    public static void addSafariCapability(String key, String value) {
        LOG.info("added Safari capability: " + key + ": " + value);
        safariOptions.setCapability(key, value);
    }

    public static void addSafariBooleanCapability(String key, boolean value) {
        LOG.info("added Safari capability: " + key + ": " + value);
        safariOptions.setCapability(key, value);
    }

    public static void setSafariAutomaticInspection(boolean value) {
        LOG.info("added Safari setAutomaticInspection: " + value);
        safariOptions.setAutomaticInspection(value);
    }

    public static void setSafariAutomaticProfiling(boolean value) {
        LOG.info("added Safari setAutomaticProfiling: " + value);
        safariOptions.setAutomaticProfiling(value);

    }

    public static void setSafariProxy(String proxyhost, String proxyport) {
        String proxystring = proxyhost + ":" + proxyport;
        Proxy proxy = new Proxy();
        proxy.setHttpProxy(proxystring).setSslProxy(proxystring);
        safariOptions.setProxy(proxy);
    }

    public static SafariDriver getSafariDriver() {
        SafariDriver sd = new SafariDriver(safariOptions);
        safariOptions = new SafariOptions();
        return sd;
    }

}
