package nl.psek.fitnesse.fixtures.selenium.browsers;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class with the logic to initialize a new ChromeDriver
 */
public class initChromeDriver {

	private static ChromeOptions chromeOptions = new ChromeOptions();
	private static final Logger LOG = LoggerFactory.getLogger(initChromeDriver.class);


	public static void addChromeOption(String option) {
		LOG.info("added Chrome option: " + option);
		chromeOptions.addArguments(option);
	}

	public static void addChromeExperimentalOption(String name, Object value){
		LOG.info("added Chrome experimental option: " + name + " with value: " + value);
		chromeOptions.setExperimentalOption(name, value);
	}
	public static void addChromeExperimentalOptionAsArray(String name, Object value){
		LOG.info("added Chrome experimental option: " + name + " with value []: " + value);
		chromeOptions.setExperimentalOption(name, new String[]{String.valueOf(value)});
	}

	public static void setChromeHeadless(boolean headless){
		chromeOptions.setHeadless(headless);
	}

	public static void setChromeProxy(String proxyhost, String proxyport){
		String proxystring = proxyhost+":"+proxyport;
		Proxy proxy = new Proxy();
		proxy.setHttpProxy(proxystring).setSslProxy(proxystring);
		chromeOptions.setProxy(proxy);

	}

	public static ChromeDriver getChromeDriver(){
		ChromeDriver cd = new ChromeDriver(chromeOptions);
		chromeOptions = new ChromeOptions();
		return cd;

	}




}
