package nl.psek.fitnesse.fixtures.general.util;

import java.io.File;
import java.io.IOException;
import java.util.TreeSet;

/**
 * Helper class for FileUtils.
 *
 * Mij kun je voorzien van 1 of meerdere
 * {@code FileCriteria} en ik beoordeel
 * vervolgens of de onderhanden file aan
 * deze criteria voldoet.
 *
 */
public class FileEvaluator {

    private File file;

    private TreeSet<FileCriterium> criteria = new TreeSet<FileCriterium>();

    /**
     * Ik evalueer onderhanden {@code File} door
     * van alle gesette {@code FileCriterium} de
     * .valid(File file) methode aan te roepen. De
     * volgorde van criteria is niet gegarandeerd, behalve
     * als de prioriteit van een {@code FileCriterium} is
     * geset. In dat geval worden de criterium met de hoogste
     * prioreiten eerst geevalueerd. Indien het criterium kritiek is,
     * en de .valid methode hiervan false opleverd, zullen de overige
     * criterium niet meer geevalueerd worden, anders wordt wel doorgegegaan.
     *
     * @return
     * @throws java.io.IOException
     */
    public boolean valid() throws IOException {
        if (this.file != null) {
            boolean v = true;
            outerloop:
            for (FileCriterium criterium : criteria) {
                if (!criterium.valid(this.file)) {
                    v = false;
                    if (criterium.critical()) {
                        break outerloop;
                    }
                }
            }
            return v;
        } else {
            throw new IOException("Must provide a file to evaluate");
        }
    }

    /**
     * Add file criterium boolean.
     *
     * @param criterium the criterium
     * @return the boolean
     */
    public boolean addFileCriterium(FileCriterium criterium) {
        return criteria.add(criterium);
    }

    /**
     * Remove file criterium.
     *
     * @param criterium the criterium
     */
    public void removeFileCriterium(FileCriterium criterium) {
        criteria.remove(criterium);
    }

    /**
     * Clear criteria.
     */
    public void clearCriteria() {
        criteria.clear();
    }

    /**
     * Gets criteria.
     *
     * @return the criteria
     */
    public TreeSet<FileCriterium> getCriteria() {
        return this.criteria;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Gets file.
     *
     * @return the file
     */
    public File getFile() {
        return file;
    }
}
