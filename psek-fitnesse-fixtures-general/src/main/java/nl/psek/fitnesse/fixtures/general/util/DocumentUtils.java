package nl.psek.fitnesse.fixtures.general.util;


import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Helper class used in nl.psek.fitnesse.fixtures.general.xml.XmlNavigateFixture
 */
public class DocumentUtils {

    private static XPathFactory xpathFactory = XPathFactory.newInstance();

    private static DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();

    private static SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");


    public static Schema loadSchemasFromFile(Source[] schemaSource) throws SAXException {
        return schemaFactory.newSchema(schemaSource);
    }

    public static boolean validateDocument(Document document, Schema schema) throws IOException, SAXException {
        schema.newValidator().validate(new DOMSource(document));
        return true;
    }

    public static Document parseDocument(String xml) throws ParserConfigurationException, IOException, SAXException {
        docBuilderFactory.setNamespaceAware(true);
        DocumentBuilder parser = docBuilderFactory.newDocumentBuilder();
        byte[] xmlBytes = xml.getBytes(Charset.forName("UTF-8"));
        return parser.parse(new ByteArrayInputStream(xmlBytes));
    }

    public static NodeList findNodes(Document doc, String xpath) throws XPathExpressionException {
        XPath xp = xpathFactory.newXPath();
        XPathExpression expr = xp.compile(xpath);
        return (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
    }

    public static String documentToString(Document doc) throws XmlException {
        XmlObject xmlObj = XmlObject.Factory.parse(doc);
        return xmlObj.toString();
    }
}