package nl.psek.fitnesse.fixtures.general.file;

import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.fixtures.util.FileFinder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * A fixture for loading files that reside within the fitnesse root folder.
 */
public class FileReaderFixture {

    private File file = null;

    /**
     * Initializes the file fixture with the given file.
     *
     * @param file the properties file.
     */

    public FileReaderFixture(String file) {
        FileFinder.findFileInUserDirectory(file, file1 -> FileReaderFixture.this.file = file1);

        if (this.file == null) {
            throw new CommandException("Cannot find file '%s'", file);
        }
    }


    public String load() {
        try {
            return new String(Files.readAllBytes(file.toPath()), "UTF-8");
        } catch (IOException e) {
            throw new CommandException("Failed to load file.");
        }
    }
}
