package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.StopTestCommandException;
import org.linguafranca.pwdb.kdbx.KdbxCreds;
import org.linguafranca.pwdb.kdbx.simple.SimpleDatabase;
import org.linguafranca.pwdb.kdbx.simple.SimpleEntry;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static nl.psek.fitnesse.fixtures.general.util.FileUtils.createBinaryTempFile;

/**
 * A fixture that can be used to retrieve username, credential and url from a keepass store.
 * It uses https://github.com/jorabin/KeePassJava2
 *
 * @author Pascal Smeets
 */
public class KeepassFixture {

	private SimpleDatabase database;

	/**
	 * Loads a keepass store into memory
	 *
	 * @param keepassStoreFile                               the path to the keepass store
	 * @param systemPropertyThatContainsThePassphraseOrAFile the Java system property that contains the passphrase for the keepass store
	 * @param useAFile                                       the use a file
	 */
	public KeepassFixture(String keepassStoreFile, String systemPropertyThatContainsThePassphraseOrAFile, boolean useAFile) {
		KdbxCreds creds;

		if (useAFile) {
			creds = initializeKdbxCredentialsFromAFile(systemPropertyThatContainsThePassphraseOrAFile);

		} else {
			creds = initializeKdbxCredentials(systemPropertyThatContainsThePassphraseOrAFile);
		}

		database = initializeSimpleDatabase(keepassStoreFile, creds);

	}


	/**
	 * This method returns the username from the specified entry
	 *
	 * @param entryName in the Keepass store, eg. "application.x.acp.database"
	 * @return the username
	 */
	public String getUsername(String entryName) {
		checkThatEntryNameIsUnique(entryName);
		return parseResult(getEntry(entryName).getUsername());
	}

	/**
	 * This method stores the username from the specified entry in a java System.property
	 *
	 * @param entryName          in the Keepass store, eg. "application.x.acp.database"
	 * @param systemPropertyName the key for the system property
	 */
	public void storeUsernameInProperty(String entryName, String systemPropertyName) {
		System.setProperty(systemPropertyName, getUsername(entryName));

	}

	/**
	 * This method returns the credential/password from the specified entry
	 *
	 * @param entryName in the Keepass store, eg. "application.x.acp.database"
	 * @return the credential
	 */
	public String getCredential(String entryName) {
		checkThatEntryNameIsUnique(entryName);
		return parseResult(getEntry(entryName).getPassword());
	}

	/**
	 * This method stores the credential/password from the specified entry in a java System.property
	 *
	 * @param entryName          in the Keepass store, eg. "application.x.acp.database"
	 * @param systemPropertyName the key for the system property
	 */
	public void storeCredentialInProperty(String entryName, String systemPropertyName) {
		System.setProperty(systemPropertyName, getCredential(entryName));
	}

	/**
	 * This method returns the url from the specified entry
	 *
	 * @param entryName in the Keepass store, eg. "application.x.acp.database"
	 * @return the url
	 */
	public String getUrl(String entryName) {
		checkThatEntryNameIsUnique(entryName);
		return parseResult(getEntry(entryName).getUrl());
	}

	/**
	 * This method stores the url from the specified entry in a java System.property
	 *
	 * @param entryName          in the Keepass store, eg. "application.x.acp.database"
	 * @param systemPropertyName the key for the system property
	 */
	public void storeUrlInProperty(String entryName, String systemPropertyName) {
		System.setProperty(systemPropertyName, getUrl(entryName));
	}

	/**
	 * This method returns the custom string property value that is present in the Entry
	 *
	 * @param entryName in the Keepass store, eg. "application.x.acp.database"
	 * @param fieldName the custom field in the entryName
	 * @return the value of the custom field
	 */
	public String getField(String entryName, String fieldName) {
		checkThatEntryNameIsUnique(entryName);
		return parseResult(getEntry(entryName).getProperty(fieldName));
	}


	/**
	 * This method returns the file attachment of an entry as a byte[]
	 *
	 * @param entryName          the entry name
	 * @param fileAttachmentName the name of the file attachment
	 * @return the byte [ ], contents of the file attachment
	 */
	public byte[] getBinaryProperty(String entryName, String fileAttachmentName) {
		checkThatEntryNameIsUnique(entryName);
		return getEntry(entryName).getBinaryProperty(fileAttachmentName);
	}

	/**
	 * This method returns the file attachment of an entry as a byte[]
	 *
	 * @param entryName          the entry name
	 * @param fileAttachmentName the name of the file attachment
	 * @return the byte [ ], contents of the file attachment
	 */
	public byte[] getBinaryAttachment(String entryName, String fileAttachmentName){
		return getBinaryProperty(entryName,fileAttachmentName);
	}

	/**
	 * This method stores the file attachment of an entry as java temp file
	 *
	 * @param entryName          the entry name
	 * @param fileAttachmentName the name of the file attachment
	 * @param filenamePrefix     filename part added to the beginning of the name of the temporary file
	 * @param filenameSuffix     filename part added to the end of the name of the temporary file
	 * @return the location of the created temo file
	 */
	public String storeBinaryAttachmentInTempFile(String entryName, String fileAttachmentName, String filenamePrefix, String filenameSuffix) {
		checkThatEntryNameIsUnique(entryName);
		byte[] bytes = getEntry(entryName).getBinaryProperty(fileAttachmentName);
		String location = createBinaryTempFile(filenamePrefix, filenameSuffix, bytes);

		return location;
	}

	/**
	 * This method stores the custom string property value that is present in the Entry from the specified entry in a java System.property
	 *
	 * @param entryName          in the Keepass store, eg. "application.x.acp.database"
	 * @param fieldName          the custom field in the entryName, eg. "schema"
	 * @param systemPropertyName the key for the system property
	 */
	public void storeFieldInProperty(String entryName, String fieldName, String systemPropertyName) {
		System.setProperty(systemPropertyName, getField(entryName, fieldName));
	}


	private String parseResult(String result) {
		if (result.isEmpty()) {
			throw new StopTestCommandException("The requested result from the entry was empty");
		}
		return result;
	}

	private SimpleEntry getEntry(String entryName) {
		SimpleEntry entry = database.findEntries(entryName).get(0);
		return entry;
	}

	private void checkThatEntryNameIsUnique(String entryName) {

		int size = database.findEntries(entryName).size();

		if (size > 1) {
			throw new StopTestCommandException("Finding entries is done with a matcher, please make your entries unique. The supplied key is not unique in the database, but present %s times", size);
		}
	}

	private KdbxCreds initializeKdbxCredentials(String systemPropertyThatContainsThePassphrase) {
		String passphrase = System.getProperty(systemPropertyThatContainsThePassphrase);

		if (passphrase == null || ((String) passphrase).isEmpty()) {
			throw new StopTestCommandException("The system property containing the passphrase has not been set: " + systemPropertyThatContainsThePassphrase);
		}
		return new KdbxCreds(passphrase.getBytes());
	}

	private KdbxCreds initializeKdbxCredentialsFromAFile(String filename) {

		Path file = Paths.get(filename);

		String passphrase = null;
		try {
			passphrase = Files.readString(file);
		} catch (IOException e) {
			throw new StopTestCommandException("Could not read fom file: " + filename);
		}


		if (passphrase == null || ((String) passphrase).isEmpty()) {
			throw new StopTestCommandException("Passphrase is not present in file: " + filename);
		}
		return new KdbxCreds(passphrase.getBytes());
	}

	private SimpleDatabase initializeSimpleDatabase(String keepassStoreFile, KdbxCreds credentials) {
		//File opzoeken
		File initialFile = new File(keepassStoreFile);
		//TODO close this try with resources
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(initialFile);
		} catch (FileNotFoundException e) {
			throw new StopTestCommandException("The keepass datastore could not be found: " + keepassStoreFile);
		}

		SimpleDatabase database;
		try {
			database = SimpleDatabase.load(credentials, inputStream);
		} catch (Exception e) {
			throw new StopTestCommandException("The keepass datastore could not be loaded: " + keepassStoreFile + System.lineSeparator() + e.getMessage());
		}
		return database;
	}

}
