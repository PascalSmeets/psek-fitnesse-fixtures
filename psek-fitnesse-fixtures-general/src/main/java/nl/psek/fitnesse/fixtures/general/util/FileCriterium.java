package nl.psek.fitnesse.fixtures.general.util;

import java.io.File;
import java.io.IOException;

/**
 * Helper class for FileUtils.
 *
 * Ik representeer een File Criterium voor het evalueren van
 * fysieke {@code File} bestanden. Je kunt mij voorzien van een
 * prioriteit en aanmerken als kritiek criterium. Ik ben bedoelt
 * voor gebruik met de {@code FileEvaluator}
 *
 */
public abstract class FileCriterium implements Comparable<FileCriterium> {

    public abstract boolean valid(File file) throws IOException;

    public abstract boolean critical();

    public abstract int priority();

    public int compareTo(FileCriterium fc) {
        return this.priority() - fc.priority();
    }

}
