package nl.psek.fitnesse.fixtures.general.util;

import com.github.f4b6a3.uuid.UuidCreator;
import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.fixtures.util.FileFinder;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.*;

/**
 * \brief     A fixture that contains commands that can be used to generate testdata or manipulate testdata.
 * \details   The variables fixture extends the date fixture, this means that all commands from the date fixture can be used in the variables fixture.
 * \author    Pascal Smeets
 * \date      2018
 */
public class VariablesFixture extends DateFixture {

    private static SecureRandom secureRandom = new SecureRandom();
    private static Random random = new Random();
    private StringBuilder sb = new StringBuilder("");

    /**
     * Instantiates a new Variables fixture.
     */
    public VariablesFixture() {
        //default constructor
    }

    /**
     * Returns the specified text. Can be used to assign a value to a variable in FitNesse.
     *
     * <table>
     * <caption>implementation example</caption>
     * <tr>
     * <td>$variable&lt;-[&nbsp;&nbsp;leading and trailing whitespace&nbsp;&nbsp;]=</td>
     * <td>giveText;</td>
     * <td>!-&nbsp;&nbsp;leading and trailing whitespace&nbsp;&nbsp;-!</td>
     * </tr>
     * <tr>
     * <td>$variable&lt;-[leading and trailing whitespace]=</td>
     * <td>trimText;</td>
     * <td>!-&nbsp;&nbsp;leading and trailing whitespace&nbsp;&nbsp;-!</td>
     * </tr>
     * </table>
     *
     * @param text the text to return.
     * @return the text that was passed in as an argument.
     */
    public static String giveText(String text) {
        return text;
    }


    /**
     * Returns one random entry from a supplied comma-separated-list
     *
     *
     * <table>
     *   <caption>implementation example</caption>
     *   <tr>
     *     <td>$randomentry&lt;-[car]=</td>
     *     <td>giveEntry;</td>
     *     <td>bike,car,boat,train,tram,subway</td>
     *   </tr>
     *   <tr>
     *     <td>$randomentry&lt;-[subway]=</td>
     *     <td>giveEntry;</td>
     *     <td>bike,car,boat,train,tram,subway</td>
     *   </tr>
     *   <tr>
     *     <td>$randomentry&lt;-[boat]=</td>
     *     <td>giveEntry;</td>
     *     <td>bike,car,boat,train,tram,subway</td>
     *   </tr>
     * </table>
     *
     * @param entries comma-separated-list of entries
     * @return one of the list entries
     */
    public static String giveEntry(String entries) {
        //converting comma separate String to array of String
        String[] elements = entries.split(",");
        //convert String array to list of String
        List<String> fixedLenghtList = Arrays.asList(elements);

        //get a random entry
        int index = random.nextInt(fixedLenghtList.size());

        return fixedLenghtList.get(index);
    }

    /**
     * Give entry from csv file string.
     *
     * @param filename the filename
     * @return the string
     */
    public static String giveEntryFromCsvFile(String filename) {
        //try to find the file in the project folder
        final String[] entry = new String[1];
        try {
            FileFinder.findFileInUserDirectory(filename, file -> {
                try {
                    Path path = file.toPath();
                    byte[] allBytes = Files.readAllBytes(path);
                    entry[0] = giveEntry(new String(allBytes, StandardCharsets.UTF_8.name()));

                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(("VariablesFixture cannot load file '" + filename + "', please make sure the file exists and is on the classpath. "));
                } catch (IOException e) {
                    throw new CommandException(e.getMessage());
                }
            });
        } catch (IllegalArgumentException e) {
            throw new CommandException(e.getMessage());
        }

        return entry[0];

    }


    /**
     * Returns the text as has been passed in as an argument, will trim leading and trailing whitespace.
     * <p>
     * This can be useful when concatenating multiple values, if values that are concatenated at the end can be empty.
     * <p>
     * The table below illustrates the difference between <em>trimText</em> and <em>giveText</em>.
     *
     * <table>
     * <caption>implementation example</caption>
     * <tr>
     * <td>$variable&lt;-[leading and trailing whitespace]=</td>
     * <td>trimText;</td>
     * <td>!-&nbsp;&nbsp;leading and trailing whitespace&nbsp;&nbsp;-!</td>
     * </tr>
     * <tr>
     * <td>$variable&lt;-[&nbsp;&nbsp;leading and trailing whitespace&nbsp;&nbsp;]=</td>
     * <td>giveText;</td>
     * <td>!-&nbsp;&nbsp;leading and trailing whitespace&nbsp;&nbsp;-!</td>
     * </tr>
     * </table>
     *
     * @param text the text to return.
     * @return the text that was passed in as an argument, trimmed.
     */
    public static String trimText(String text) {
        return text.trim();
    }

    /**
     * Returns a random number between the two given arguments.
     *
     * @param from the from value (inclusive).
     * @param to   the until value (inclusive).
     * @return a random number.
     */
    public static long giveRandomNumber(long from, long to) {
        return from + ((long) (Math.random() * (to - from)));
    }

    public static BigDecimal generateRandomBigDecimalFromRange(String low, String high) {
        BigDecimal min = new BigDecimal(low);
        BigDecimal max = new BigDecimal(high);

        BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2, RoundingMode.HALF_UP);
    }


    /**
     * Generates a cryptographically strong random string.
     *
     * @param length desired length of output string
     * @return a random string of the specified length
     */
    public static String giveSecureRandomString(int length) {
        return new BigInteger((length + 5) * 5, secureRandom).toString(32).substring(0, length);
    }


    public static String giveSecureRandomStringCaps(int length) {
        String rnd = new BigInteger((length + 5) * 5, secureRandom).toString(32).substring(0, length);
        return rnd.toUpperCase(Locale.ROOT);
    }


    /**
     * Generates a random hexadecimal string.
     *
     * @param length desired length of output string
     * @return a random hex string of the specified length
     */
    public static String giveRandomHexString(int length) {
        StringBuilder stringBuffer = new StringBuilder();
        while (stringBuffer.length() < length) {
            stringBuffer.append(Integer.toHexString(random.nextInt()));
        }

        return stringBuffer.toString().substring(0, length).toUpperCase();
    }

    /**
     * Generates a random uuid, according to the UUID specification (RFC-4122).
     * source https://github.com/f4b6a3/uuid-creator#uuid-creator
     *
     * @return a random uuid
     */
    public static String giveRandomUuid() {
        return UuidCreator.getRandomBased().toString();
    }

    /**
     * Generates a time based uuid, according to the UUID specification (RFC-4122).
     * source https://github.com/f4b6a3/uuid-creator#uuid-creator
     *
     * @return time based uuid
     */
    public static String giveTimebasedUuid() {
        return UuidCreator.getTimeBased().toString();
    }


    public static String roundOnTwoDecimals(String input) {
        if (input.contains(".") && input.contains(",")) {
            input = input.replace(".", "");
        }
        BigDecimal amount = new BigDecimal(input.replace(",", "."));

        return amount.setScale(2, RoundingMode.HALF_UP).toPlainString();
    }

    /**
     * Returns a unique string.
     * <p>
     * Based on Date.getTime, if you use this command immediately again it is possible to get the same string.
     * Because not enough time has passed.
     *
     * @return unique string.
     */
    public static String giveUniqueString() {
        return (Long.valueOf(new Date().getTime())).toString();
    }

    /**
     * Returns a unique string containing charaters [A-J]
     * <p>
     * Based on Date.getTime, if you use this command immediately again it is possible to get the same string.
     * Because not enough time has passed.
     *
     * @return unique alpha string.
     */
    public static String giveUniqueAlphaString() {
        return convertNumberCharsToAlpha(giveUniqueString());
    }

    /**
     * Convert to uppercase string.
     *
     * @param input the input
     * @return the string
     */
    public static String convertToUppercase(String input) {
        return input.toUpperCase();
    }

    /**
     * Converts a number string to an aplha string [0-9] is converted into [A-J].
     *
     * @param input String input containing only numbers
     * @return the converted string
     * @throws CommandException if the input string contains other characters than [0-9]
     */
    public static String convertNumberCharsToAlpha(String input) {
        StringBuilder converted = new StringBuilder();

        if (input.length() < 1) {
            throw new CommandException("Error - Input length was zero");
        }

        for (int i = 0; i < (input.length()); i++) {

            Character ch = input.charAt(i);

            if (Character.isAlphabetic(ch)) {

                converted.append(ch);
            } else if (Character.isDigit(ch)) {

                String alphaValue = getCharForNumber(Character.getNumericValue(ch));
                converted.append(alphaValue);

            } else {
                throw new CommandException("Error - unsupported characer was used. Supported are Alphabetic and Digit characters");
            }
        }
        return converted.toString();
    }

    /**
     * Helper method to convert an int to an alpha character
     *
     * @param input
     * @return
     */
    private static String getCharForNumber(int input) {
        int i = input + 1;
        return i > 0 && i < 27 ? String.valueOf((char) (i + 64)) : null;
    }

    /**
     * Returns a base64 encoded hash of the input string
     *
     * @param input    the input
     * @param hashType the type of MessageDigest to use, Java is required to support at least: MD5, SHA-1 and SHA-256
     * @return base 64 encoded - hashed - value
     */
    public static String giveBase64EncodedHash(String input, String hashType) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(hashType);
        } catch (NoSuchAlgorithmException e) {
            throw new CommandException("MessageDigest met type: " + hashType + " is niet bekend.");
        }
        byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(hash);
    }

    /**
     * Generates a string based on codePoint inputs
     *
     * @param csvList ,-separated list OR a single entry
     * @return unicode string
     */
    public static String generateUnicodeCodePointString(String csvList) {
        StringBuilder str = new StringBuilder();

        if (csvList.contains(",")) {

            List<String> intList = Arrays.asList(csvList.split("'"));

            intList.forEach(codePointValue -> str.appendCodePoint(Integer.parseInt(codePointValue)));
        } else {
            str.appendCodePoint(Integer.parseInt(csvList));
        }
        return str.toString();
    }

    /**
     * Generates a UnicodeCodepoint string for testing purposes
     *
     * @param startingPosition the starting position
     * @param length           of the string to return
     * @return a string
     */
    public static String generateUnicodeCodepointStringFromWithLength(int startingPosition, int length) {
        StringBuilder str = new StringBuilder();

        for (int i = startingPosition; i < (startingPosition + length); i++) {

            str.appendCodePoint(i);
        }

        return str.toString();
    }

    /**
     * Generates a UnicodeCodepoint string for testing purposes
     *
     * @param startingPosition the starting position
     * @param endingPosition   (inclusive)
     * @return a string
     */
    public static String generateUnicodeCodepointStringFromTo(int startingPosition, int endingPosition) {
        StringBuilder str = new StringBuilder();

        for (int i = startingPosition; i <= (endingPosition); i++) {

            str.appendCodePoint(i);
        }

        return str.toString();
    }

    public static String getSystemProperties() {
        return System.getProperties().toString();
    }

    /**
     * Raises the number by one.
     *
     * @param value the number to raise by one.
     * @return the result.
     */
    public int increaseValueByOne(int value) {
        return ++value;
    }

    /**
     * Raises the number by the amount specified.
     *
     * @param value  the number that will be added to.
     * @param amount the amount to raise.
     * @return the result.
     */
    public int add(int value, int amount) {
        return value + amount;
    }

    /**
     * Substract the amount specified from the number.
     *
     * @param value  the number that subtract will be subtracted from.
     * @param amount the amount to subtract.
     * @return the result.
     */
    public int subtract(int value, int amount) {
        return value - amount;
    }

    /**
     * Concats the string to the existing value hold by the fixture.
     *
     * @param string the string to append.
     * @see #emptyConcatItems() #emptyConcatItems()#emptyConcatItems()
     * @see #concat() #concat()#concat()
     */
    public void addConcatItem(String string) {
        this.sb.append(string);
    }

    /**
     * Adds a specific unicode character tot the concatenation
     *
     * @param unicodeCodePoint the unicode code point
     */
    public void addConcatUnicodeCodepoint(int unicodeCodePoint) {
        this.sb.appendCodePoint(unicodeCodePoint);
    }

    /**
     * Empties the current buffer.
     *
     * @see #addConcatItem(String) #addConcatItem(String)#addConcatItem(String)
     * @see #concat() #concat()#concat()
     */
    public void emptyConcatItems() {
        this.sb = new StringBuilder();
    }

    /**
     * Returns the current buffer.
     *
     * @return the current buffer.
     * @see #addConcatItem(String) #addConcatItem(String)#addConcatItem(String)
     * @see #emptyConcatItems() #emptyConcatItems()#emptyConcatItems()
     */
    public String concat() {
        return this.sb.toString();
    }

}
