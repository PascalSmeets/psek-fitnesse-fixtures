package nl.psek.fitnesse.fixtures.general.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.equal;

/**
 * A fixture to generate a java.security.MessageDigest from String input using the specified encoding
 *
 * Created by Pascal on 5-9-2016.
 */
public class DigestXmlFixture {

    private static final String XML_LINARIZATION_REGEX = "(>|&gt;){1,1}(\\t)*(\\n|\\r)+(\\s)*(<|&lt;){1,1}";
    private static final String XML_LINARIZATION_REPLACEMENT = "$1$5";
    private static final String XML_REMOVE_WHITESPACE_REGEX = "(>|&gt;)(\\s)*(<|&lt;)";
    private static final String XML_REMOVE_WHITESPACE_REPLACEMENT = "$1$3";

    /**
     * Instantiates a new Digest xml fixture.
     */
    public DigestXmlFixture() {
    }


    /**
     * Create digest from string string.
     *
     * @param input    the input
     * @param encoding the encoding
     * @return the string
     */
    public String createDigestFromString(String input, String encoding) {

        String data = linarizeXml(removeWhitespaceXml(removeFitNesseBrTags(input)));
        System.out.println("input string = " + data);

        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            InputStream is = new ByteArrayInputStream(data.getBytes(encoding));

            byte[] dataBytes = new byte[1024];
            int nread = 0;
            while ((nread = is.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }

            byte[] mdbytes = md.digest();

            StringBuffer sb = new StringBuffer("");
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Digests are equal boolean.
     *
     * @param digest1 the digest 1
     * @param digest2 the digest 2
     * @return the boolean
     */
    public Boolean digestsAreEqual(String digest1, String digest2) {
        return equal(digest1, digest2);
    }

    /**
     * Strings are equal boolean.
     *
     * @param input1    the input 1
     * @param encoding1 the encoding 1
     * @param input2    the input 2
     * @param encoding2 the encoding 2
     * @return the boolean
     */
    public Boolean stringsAreEqual(String input1, String encoding1, String input2, String encoding2) {
        String digest1 = createDigestFromString(input1, encoding1);
        String digest2 = createDigestFromString(input2, encoding2);
        return digestsAreEqual(digest1, digest2);
    }

    private static String linarizeXml(String xml) {
        return (xml != null) ? xml.trim().replaceAll(XML_LINARIZATION_REGEX, XML_LINARIZATION_REPLACEMENT) : null;
    }

    private static String removeWhitespaceXml(String xml) {
        return (xml != null) ? xml.trim().replaceAll(XML_REMOVE_WHITESPACE_REGEX, XML_REMOVE_WHITESPACE_REPLACEMENT) : null;
    }

    private static String removeFitNesseBrTags(String xml) {
        return xml.replace("</br>", "").replace("<br>", "").replace("</BR>", "").replace("<BR>", "").replace("<br/>", "").replace("<BR/>", "").replace("$amp;", "&").replace("&", "&amp;");
    }

}
