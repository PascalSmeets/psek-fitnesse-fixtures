package nl.psek.fitnesse.fixtures.general.xml;


import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.fixtures.general.util.DocumentUtils;
import nl.psek.fitnesse.fixtures.general.util.FileUtils;
import nl.psek.fitnesse.fixtures.general.util.OperationsFixture;
import org.apache.xmlbeans.XmlException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.findOnRegexAndReturnResult;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.matchOnPattern;


/**
 * A fixture to perform checks on and extract data from XML strings/files
 */
public class XmlNavigateFixture {

    private static final Logger LOG = LoggerFactory.getLogger(XmlNavigateFixture.class);
    /**
     * The Document.
     */
    public Document document = null;

    private Schema schema = null;


    /**
     * Instantiates a new Xml navigate fixture.
     */
    public XmlNavigateFixture() {
    }


    /**
     * Methode voor het inladen van een XML bestand op basis van de opgegeven bestandsnaam.
     * Het bestand moet hiervoor op het lokale bestandssysteem aanwezig zijn (daar waar deze
     * fixture is gedeployed en draait).
     *
     * @param bestandsnaam - naam van het xml bestand.
     * @throws IOException                  - wanneer het xml bestand niet gelezen kan worden
     * @throws SAXException                 - wanneer het bestand niet naar geldige xml geparsed kan worden
     * @throws ParserConfigurationException - wanneer er een fout in de configuratie van de fixture is gevonden

     */
    public void loadXmlFromFile(String bestandsnaam) throws IOException, SAXException, ParserConfigurationException {
        String xmlString = FileUtils.readTextFile(bestandsnaam);
        loadXmlFromString(xmlString);
    }

    /**
     * Methode voor het inladen van een XML bericht op basis van meegegeven XML String.
     *
     * @param xmlString - de String die de XML representeerd
     * @throws ParserConfigurationException the parser configuration exception
     * @throws IOException                  the io exception
     * @throws SAXException                 the sax exception

     */
    public void loadXmlFromString(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        this.document = DocumentUtils.parseDocument(xmlString);
    }

    /**
     * Methode voor het inladen van XSD schema's. Deze schema's kunnen vervolgens gebruikt worden door
     * de fixture om bijvoorbeeld een XML bestand/string tegen te valideren.
     *
     * @param bestandsnaam - De bestandsnamen van de schemas (1 of meerdere, door spatie gescheiden)
     * @throws IOException  the io exception
     * @throws SAXException the sax exception

     */
    public void loadSchemasFromFiles(String... bestandsnaam) throws IOException, SAXException {
        this.schema = DocumentUtils.loadSchemasFromFile(FileUtils.streamSources(bestandsnaam));
    }

    /**
     * Methode voor het inladen van XSD schema's, op basis van een directory naam en een regex patroon voor de bestandsnamen
     *
     * @param folder           map t.o.v./src
     * @param regexFilePattern reguliere expressie om files te selecteren
     * @throws IOException  the io exception
     * @throws SAXException the sax exception
     */
    public void loadSchemasFromFolder(String folder, String regexFilePattern) throws IOException, SAXException {
        List<String> schemas = new ArrayList<>();
        List<File> files = FileUtils.filesInFolder(folder, regexFilePattern);

        files.forEach(file -> LOG.info("Found XSD schema file: " + file.getAbsolutePath()));
        files.forEach(file -> schemas.add(file.getAbsolutePath()));
        this.schema = DocumentUtils.loadSchemasFromFile(FileUtils.streamSources(schemas.toArray(new String[schemas.size()])));
    }

    /**
     * Valideert het geladen XML bestand tegen de geladen XSD schema's. Geeft een exception
     * als 1 van beide niet is geladen.
     *
     * @return the boolean
     * @throws IOException  the io exception
     * @throws SAXException the sax exception

     */
    public boolean validateXmlToSchema() throws IOException, SAXException {

        if (this.document != null && this.schema != null) {
            return DocumentUtils.validateDocument(this.document, this.schema);
        } else {
            throw new SAXException("Document of schema is niet geladen");
        }
    }

    /**
     * Methode die de opgegeven XPATH ten opzicht van het geladen XML evalueert.
     * Het aantal gevonden nodes wordt als getal teruggegeven.
     *
     * @param xpath - xpath expressie
     * @return the int
     * @throws XPathExpressionException - indien er een foutieve xpath expressie is opgegeven
     */
    public int xpathIsTimesPresentInDocument(String xpath) throws XPathExpressionException {
        if (this.document != null) {
            return DocumentUtils.findNodes(document, xpath).getLength();
        } else {
            throw new XPathExpressionException("XML document is not loaded");
        }
    }


    /**
     * Verify xpath is times present in document boolean.
     *
     * @param xpath                the xpath
     * @param timesPresentExpected the times present expected
     * @return the boolean
     * @throws XPathExpressionException the x path expression exception
     */
    public Boolean verifyXpathIsTimesPresentInDocument(String xpath, int timesPresentExpected) throws XPathExpressionException {
        if (this.document != null) {
            if (DocumentUtils.findNodes(document, xpath).getLength() == timesPresentExpected) return true;
            else return false;
        } else {
            throw new XPathExpressionException("XML document is not loaded");
        }
    }


    /**
     * Xpath is present in document boolean.
     *
     * @param xpath the xpath
     * @return the boolean
     */
    public Boolean xpathIsPresentInDocument(String xpath) {
        if (this.document != null) {
            try {
                return (DocumentUtils.findNodes(document, xpath).getLength()) > 0;
            } catch (XPathExpressionException e) {
                throw new ConditionalException(e.getMessage());
            }
        } else {
            throw new ConditionalException("XML document is not loaded");
        }
    }


    /**
     * Gets xpath node name.
     *
     * @param xpath the xpath
     * @return the xpath node name
     * @throws XPathExpressionException the x path expression exception
     */
    public String getXpathNodeName(String xpath) throws XPathExpressionException {
        if (this.document != null) {
            if (xpathIsTimesPresentInDocument(xpath) == 1) {
                Node node = DocumentUtils.findNodes(document, xpath).item(0);

                return node.getNodeName();
            } else {
                throw new XPathExpressionException("Xpath heeft geen uniek resultaat opgeleverd");
            }
        } else {
            throw new XPathExpressionException("XML document is not loaded");
        }
    }

    /**
     * Gets xpath node local-name.
     *
     * @param xpath the xpath
     * @return the xpath node local-name
     * @throws XPathExpressionException the x path expression exception
     */
    public String getXpathNodeLocalName(String xpath) throws XPathExpressionException {
        if (this.document != null) {
            if (xpathIsTimesPresentInDocument(xpath) == 1) {
                Node node = DocumentUtils.findNodes(document, xpath).item(0);

                return node.getLocalName();
            } else {
                throw new XPathExpressionException("Xpath heeft geen uniek resultaat opgeleverd");
            }
        } else {
            throw new XPathExpressionException("XML document is not loaded");
        }
    }


    /**
     * Unique xpath is present in document boolean.
     *
     * @param xpath the xpath
     * @return the boolean
     * @throws XPathExpressionException the x path expression exception
     * @throws ConditionalException     the conditional exception
     */
    public Boolean uniqueXpathIsPresentInDocument(String xpath) throws XPathExpressionException, ConditionalException {
        if (this.document != null) {
            try {
                return (xpathIsTimesPresentInDocument(xpath) == 1);
            } catch (XPathExpressionException e) {
                throw new ConditionalException(e.getMessage());
            }
        } else {
            throw new ConditionalException("XML document is not loaded");
        }
    }


    /**
     * Xpath is not present in document boolean.
     *
     * @param xpath the xpath
     * @return the boolean
     */
    public Boolean xpathIsNotPresentInDocument(String xpath) {
        return !xpathIsPresentInDocument(xpath);
    }

    /**
     * Methode voor het ophalen van de text waarde van een node uit de XML. De XPATH
     * mag hiervoor maar 1 keer voorkomen in het bericht en de node moet een text waarde
     * bevatten.
     *
     * @param xpath the xpath
     * @return the string
     * @throws XPathExpressionException the x path expression exception
     */
    public String uniqueXpathTextValue(String xpath) throws XPathExpressionException {
        if (this.document != null) {
            if (xpathIsTimesPresentInDocument(xpath) == 1) {
                Node node = DocumentUtils.findNodes(document, xpath).item(0);
                if (node.getFirstChild() != null && node.getFirstChild().getNodeType() == Node.TEXT_NODE) {
                    return node.getFirstChild().getNodeValue();
                } else {
                    // will return null if node is empty
                    LOG.info("node '" + node.getNodeName() + "' is null or not a text node. Returning the TextContent '" + node.getTextContent() + "'.");
                    return node.getTextContent();
                }
            } else {
                throw new XPathExpressionException("Xpath heeft geen uniek resultaat opgeleverd");
            }
        } else {
            throw new XPathExpressionException("XML document is not loaded");
        }
    }

    /**
     *  similar to uniqueXpathTextValue, but with support for regular expressions
     *
     * @param xpath   the xpath
     * @param pattern the pattern
     * @return string
     * @throws XPathExpressionException the x path expression exception
     */
	public String uniqueXpathTextValueContains(String xpath, String pattern) throws XPathExpressionException {
		String textValue = "";
		if (this.document != null) {
			if (xpathIsTimesPresentInDocument(xpath) == 1) {
				Node node = DocumentUtils.findNodes(document, xpath).item(0);
				if (node.getFirstChild() != null && node.getFirstChild().getNodeType() == Node.TEXT_NODE) {
					textValue= node.getFirstChild().getNodeValue();
				} else {
					// will return null if node is empty
					LOG.info("node '" + node.getNodeName() + "' is null or not a text node. Returning the TextContent '" + node.getTextContent() + "'.");
					textValue= node.getTextContent();
				}
			} else {
				throw new XPathExpressionException("Xpath heeft geen uniek resultaat opgeleverd");
			}
		} else {
			throw new XPathExpressionException("XML document is not loaded");
		}

		return findOnRegexAndReturnResult(textValue, pattern);

	}


    /**
     * verifyUniqueXpathTextValuePresent checks if a text value is present at the specified XPath location.
     * If the XPath is not valid the XPathExpressionException is caught and false is returned.
     *
     * @param xpath   the xpath
     * @param pattern the pattern
     * @return true or false
     */
    public Boolean verifyUniqueXpathTextValuePresent(String xpath, String pattern) {
        String foundValue = null;
        try {
            foundValue = uniqueXpathTextValue(xpath);
        } catch (XPathExpressionException e) {
            return false;
        }
        return matchOnPattern(foundValue, pattern);
    }


    /**
     * Method to check the value of an element that could be empty or not present.
     * If pattern is an empty string AND the element is present this method will check that the text value is empty
     * If pattern is an empty string AND the element is not present this method will return true
     * If pattern is a string (not empty) AND the element is present this method will match the pattern with the found text value
     * If pattern is a string (not empty) AND the element is not present this method will return false
     *
     * @param xpath   xpath expression to locate an element
     * @param pattern string or regular expression (regex:  OR   regexpi:  &larr; case insensitive regex)
     * @return boolean boolean
     */
    public Boolean verifyOptionalUniqueXpathTextValuePresent(String xpath, String pattern) {
        String foundValue = null;

        if (pattern.isEmpty()) {
            try {
                foundValue = uniqueXpathTextValueOptional(xpath);
                if (foundValue != null) {
                    LOG.info(String.format("verifyOptionalUniqueXpathTextValuePresent - pattern is empty: found result '%s' for xpath '%s', starting match of '%s' with '%s'", foundValue, xpath, pattern, foundValue));
                    return matchOnPattern(foundValue, pattern);
                } else {
                    LOG.info(String.format("verifyOptionalUniqueXpathTextValuePresent - pattern is empty: found an empty string '' for xpath '%s', starting match of '%s' with EMPTY STRING", xpath, pattern));
                    foundValue = "";
                    return matchOnPattern(foundValue, pattern);
                }
            } catch (XPathExpressionException | NullPointerException e) {
                LOG.info(String.format("verifyOptionalUniqueXpathTextValuePresent - could not find optional xpath %s in the document returning TRUE", xpath));
                return true;
            }
        } else
            try {
                foundValue = uniqueXpathTextValue(xpath);
            } catch (XPathExpressionException e) {
                return false;
            }
        LOG.info(String.format("verifyOptionalUniqueXpathTextValuePresent - pattern is not empty : found result '%s' for xpath '%s', starting match of '%s' with '%s' ", foundValue, xpath, pattern, foundValue));
        return matchOnPattern(foundValue, pattern);
    }

    /**
     * reverse of verifyUniqueXpathTextValuePresent
     *
     * @param xpath   the xpath
     * @param pattern the pattern
     * @return boolean boolean
     */
    public Boolean verifyUniqueXpathTextValueNotPresent(String xpath, String pattern) {
        return !verifyUniqueXpathTextValuePresent(xpath, pattern);
    }

    /**
     * checkUniqueXpathTextValuePresent checks if a text value is present at the specified XPath location.
     * If the XPath is not valid the XPathExpressionException is caught and a ConditionalExceptionType is returned
     *
     * @param xpath         the xpath
     * @param expectedValue the expected value
     * @return true / false or ConditionalExceptionType
     */


    public Boolean checkUniqueXpathTextValuePresent(String xpath, String expectedValue) {
        String foundValue = null;
        try {
            foundValue = uniqueXpathTextValue(xpath);
        } catch (XPathExpressionException e) {
            throw new ConditionalException(e.getMessage());
        }
        return OperationsFixture.areStringValuesEqual(foundValue, expectedValue);
    }


    /**
     * Methode voor het ophalen van de text waarde van een node uit de XML. De XPATH
     * mag hiervoor maar 1 keer voorkomen in het bericht, maar als het niet voorkomt dan wordt gewoon null geretourneerd
     *
     * @param xpath the xpath
     * @return the string
     * @throws XPathExpressionException the x path expression exception
     */
    public String uniqueXpathTextValueOptional(String xpath) throws XPathExpressionException {
        if (this.document != null) {
            if (xpathIsTimesPresentInDocument(xpath) == 1) {
                Node node = DocumentUtils.findNodes(document, xpath).item(0);
                if (node.getFirstChild() != null && node.getFirstChild().getNodeType() == Node.TEXT_NODE) {
                    return node.getFirstChild().getNodeValue();
                }
            }

            return null;
        } else {
            throw new XPathExpressionException("XML document is not loaded");
        }
    }

    /**
     * Methode voor het ophalen van de text waarde van een node uit de XML. De XPATH
     * mag hiervoor maar 1 keer voorkomen in het bericht en de node moet een text waarde
     * bevatten.
     *
     * @param xpath     the xpath
     * @param attribuut the attribuut
     * @return the string
     * @throws XPathExpressionException the x path expression exception
     */
    public String uniqueXpathAttributeValue(String xpath, String attribuut) throws XPathExpressionException {
        if (this.document != null) {
            if (xpathIsTimesPresentInDocument(xpath) == 1) {
                Node node = DocumentUtils.findNodes(document, xpath).item(0);
                if (node != null) {
                    NamedNodeMap attributes = node.getAttributes();
                    if (attributes.getLength() != 0) {
                        Node namedItem = attributes.getNamedItem(attribuut);
                        if (namedItem != null) {
                            return namedItem.getNodeValue();
                        } else {
                            throw new XPathExpressionException("Geen attribuut gevonden (" + attribuut + ").");
                        }
                    } else {
                        throw new XPathExpressionException("Element heeft geen attributen.");
                    }
                } else {
                    throw new XPathExpressionException("Gevonden node gevonden.");
                }
            } else {
                throw new XPathExpressionException("Xpath heeft geen uniek resultaat opgeleverd");
            }
        } else {
            throw new XPathExpressionException("XML document is not loaded");
        }
    }

    /**
     * Geeft de XML als String terug
     *
     * @return the string
     * @throws XmlException the xml exception
     */
    public String xml() throws XmlException {
        return DocumentUtils.documentToString(this.document);
    }


}
