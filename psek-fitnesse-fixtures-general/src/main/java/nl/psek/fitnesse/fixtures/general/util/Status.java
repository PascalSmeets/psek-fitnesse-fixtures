package nl.psek.fitnesse.fixtures.general.util;

/**
 * Mapping of HTTP response codes to a constant 'success' or 'failure' value.
 *
 * @author <a href='mailto:tomstrummer+httpbuilder@gmail.com'>Tom Nichols</a>
 */
public enum Status {
	/**
	 * Any status code &gt;= 100 and &lt; 400
	 */
	SUCCESS(100, 399),
	/**
	 * Any status code &gt;= 400 and &lt; 1000
	 */
	FAILURE(400, 999);

	private final int min, max;

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}

	/**
	 * Returns true if the numeric code matches the represented status (either
	 * <code>success</code> or <code>failure</code>).  i.e.
	 * <pre>
	 * assert Status.SUCCESS.matches(200);
	 * assert Status.FAILURE.matches(404);
	 * </pre>
	 *
	 * @param code numeric HTTP code
	 * @return true if the numeric code represents this enums success or failure
	 * condition
	 */
	public boolean matches(int code) {
		return min <= code && code <= max;
	}

	private Status(int min, int max) {
		this.min = min;
		this.max = max;
	}
}