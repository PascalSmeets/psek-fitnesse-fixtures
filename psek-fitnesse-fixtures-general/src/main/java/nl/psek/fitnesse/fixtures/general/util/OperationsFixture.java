package nl.psek.fitnesse.fixtures.general.util;


import nl.psek.fitnesse.StopTestCommandException;
import nl.psek.fitnesse.CommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static nl.psek.fitnesse.fixtures.general.util.DateFixture.parseSimpledateFormat;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.*;
import static nl.psek.fitnesse.util.FitnesseSystemSettings.setStopTestOnException;


/**
 * A fixture with commonly used methods, useful as a library import in FitNesse
 *
 * @author Pascal Smeets on 15-7-2015
 */
public class OperationsFixture {

    private static final Logger LOG = LoggerFactory.getLogger(OperationsFixture.class);
    private static StringBuilder tempBuffer = new StringBuilder();

    /**
     * Instantiates a new Operations fixture.
     */
    public OperationsFixture() {
        //default constructor
    }

    /**
     * Returns the tempBuffer as is
     *
     * @return string
     */
    public static String readBuffer() {
        return tempBuffer.toString();
    }

    /**
     * Returns the tempBuffer as is
     *
     * @return parsed string - #LINEBREAK# - is replaces with the \n character
     */
    public static String readBufferWithLinebreaks() {
        return parseLinebreaks(tempBuffer.toString());
    }

    /**
     * Clears the buffer
     */
    public static void clearBuffer() {
        tempBuffer = new StringBuilder();
    }

    /**
     * Determines if the values of two objects are equal
     *
     * @param input1 first object input
     * @param input2 second object input
     * @return true or false
     */
    public static Boolean areValuesEqual(Object input1, Object input2) {
        try {
            return equal(input1, input2);
        } catch (IllegalArgumentException e) {
            throw new CommandException(e);
        }

    }

    /**
     * Are string values equal boolean.
     *
     * @param input1 the input 1
     * @param input2 the input 2
     * @return the boolean
     */
    public static Boolean areStringValuesEqual(String input1, String input2) {
        try {
            return equal(input1, input2);
        } catch (IllegalArgumentException e) {
            throw new CommandException(e);
        }
    }

    /**
     * method to stop test execution in fitnesse if set to true
     * sets the value that van be used later on
     *
     * @param value boolean
     */
    public static void stopFitnesseTestOnException(boolean value) {
        setStopTestOnException(value);
        LOG.info("Setting FitNesse to stop on exception (only works when a 'ConditionalException' is thrown in the method) as: {} ", value);
    }

    /**
     * Removes the regex #LINEBREAK# from a string and replaces it with /n
     *
     * @param input string to parse
     * @return parsed string
     */
    private static String parseLinebreaks(String input) {
        return input.replaceAll("#LINEBREAK#", "\n");
    }

    /**
     * Allows for the setting of a Java System.Property from FitNesse
     *
     * @param key   property name
     * @param value propery value
     */
	public static void setSystemProperty(String key, String value) {
        System.setProperty(key, value);
    }

	/**
	 * Allows for the setting and adding to a Java System.Property from FitNesse
	 * If the System.property does not exist it will be created
	 * Uses the getFromPropertyIfApplicable prefix syntax 'p:' for retrieveing the @valueToAdd from another System.property
	 * @param key   property name
	 * @param valueToAdd propery value
	 */
	public static void concatValueToSystemProperty(String key, String valueToAdd) {
		String storedValue = "";

		if (System.getProperties().containsKey(key)) {
			storedValue = System.getProperty(key);
		}
		String updatedValue = storedValue.concat(getFromPropertyIfApplicable(valueToAdd));

		setSystemProperty(key, updatedValue);
	}

    /**
     * Logs the specified input to System.out. Replaces the string #LINEBREAK# with a \n
     *
     * @param input string
     */
    public static void logThis(String input) {
        if (input.contains("#LINEBREAK#")) {
            input = (parseLinebreaks(input));
        }
        LOG.info(input);
        }

    /**
     * A buffer for information yo want to combine and output later (tempBuffer). Consists of a Stringbuilder.
     * Append the input to the buffer
     *
     * @param input String
     */
    public static void addToBuffer(String input) {
        tempBuffer.append(input);
    }

    /**
     * Logs the  tempBuffer as is to the logThis method
     */
    public static void logBuffer() {
        logThis(readBuffer());
    }

    /**
     * Logs the  tempBuffer as is to the logThis method
     * Clears the StringBuilder
     */
    public static void logAndClearBuffer() {
        logThis(readBuffer());
        clearBuffer();
    }

    /**
     * Match value and if false stop test. If the result of the compare is false testing will be halted by throwing a StopTestCommandException
     *
     * @param input   the input
     * @param pattern the pattern
     * @return the boolean
     * @throws StopTestCommandException if the compare result is false
     */
    public boolean matchValueAndIfFalseStopTest(String input, String pattern){
        LOG.info("Starting matchValueAndIfFalseStopTest");
        if (matchOnPattern(input, pattern)) {
            LOG.info("match = true, testing continues");
            return true;
        } else {
            LOG.info("match = false, testing stops");
            throw new StopTestCommandException("No match stopping test execution.");
        }
    }

    /**
     * Match value and if true stop test. If the result of the compare is true testing will be halted by throwing a StopTestCommandException
     *
     * @param input   the input
     * @param pattern the pattern
     * @return the boolean
     * @throws StopTestCommandException if the compare result is true
     */
    public boolean matchValueAndIfTrueStopTest(String input, String pattern){
        LOG.info("Starting matchValueAndIfTrueStopTest");
        if (!matchOnPattern(input, pattern)) {
            LOG.info("match = false, testing continues");
            return true;
        } else {
            LOG.info("match = true, testing stops");
            throw new StopTestCommandException("Found a match stopping test execution.");
        }
    }

    /**
     * method to control test executiobn → abort based on a numerical value
     *
     * @param input   the input
     * @param control the control
     * @return boolean
     */
    public boolean isNumberSmallerThanControlAndIfFalseStopTesting(int input, int control) {

		if (input < control) {
			return true;
		} else {
			throw new StopTestCommandException("Input value larger than control, stopping test execution");
		}

	}

    /**
     * Encode to base 64 string.
     *
     * @param input         the input
     * @param inputEncoding the input encoding
     * @return the string
     */
    public static String encodeToBase64(String input, String inputEncoding) {
        byte[] bytes;
        try {
            bytes = input.getBytes(inputEncoding);
        } catch (UnsupportedEncodingException e) {
            throw new CommandException("The encoding '" + inputEncoding + "' is not supported." + System.lineSeparator() + e.getMessage());
        }

        if (bytes != null) {
            return Base64.getEncoder().encodeToString(bytes);
        } else throw new CommandException("No input was converted to Base64");
    }

    /**
     * Decode from base 64 string.
     *
     * @param input          the input
     * @param outputEncoding the output encoding
     * @return the string
     */
	public static String decodeFromBase64(String input, String outputEncoding) {
        byte[] decoded = Base64.getDecoder().decode(input);

        try {
            return new String(decoded, outputEncoding);
        } catch (UnsupportedEncodingException e) {
            throw new CommandException("The encoding '" + outputEncoding + "' is not supported." + System.lineSeparator() + e.getMessage());
        }

    }


    /**
     * Decode from base 64 to file boolean.
     *
     * @param input          the input
     * @param outputFileName the output file name
     * @return the boolean
     */
    public static Boolean decodeFromBase64ToFile(String input, String outputFileName) {
        byte[] decoded = Base64.getDecoder().decode(input);

        try {
            Files.write(Paths.get(outputFileName), decoded);
            return true;
        } catch (IOException e) {
            throw new CommandException("An IOException occurred while writing the file: " + e.getMessage());
        }
    }

    /**
     * Remove fitnesse br tags string.
     *
     * @param input the input
     * @return the string
     */
    public String removeFitnesseBrTags(String input) {


        LOG.info("TIDY_XML input = {}", input);
        input = input.replace("</br>", "").replace("<br>", "").replace("</BR>", "").replace("<BR>", "").replace("<br/>", "").replace("<BR/>", "").replace("$amp;", "&").replace("&", "&amp;");

        LOG.info("TIDY_XML output = {}", input);
        return input;
    }

    /**
     * Performs a pause action using Thread.sleep .
     * Don't use this with the Slim Web Driver fixture, use the sleep command provided there.
     * <p>
     * Usage: this command is intended to specify a wait time to let a fixture wait for completion of a remote action
     *
     * @param millis the time to pause in milliseconds
     * @throws InterruptedException the interrupted exception
     */
     public void threadSleep(long millis) throws InterruptedException {
        Thread.sleep(millis);

    }

    /**
     * Basic calculations, with integers
     *
     * @param input1 the input 1
     * @param input2 the input 2
     * @return the string
     */
    public String addTwoNumbers(String input1, String input2) {
        BigInteger first = new BigInteger(input1);
        BigInteger second = new BigInteger(input2);
        return String.valueOf(first.add(second));
    }

    /**
     * Basic calculations, with decimals
     *
     * @param input1 the input 1
     * @param input2 the input 2
     * @return the string
     */
	public String addTwoDecimalNumbers(String input1, String input2) {
		BigDecimal first = new BigDecimal(input1);
		BigDecimal second = new BigDecimal(input2);
		return String.valueOf(first.add(second));
	}

    /**
     * Basic calculations, with decimals
     *
     * @param csvListOfDecimals list of string values separated by the ; (semicolon) character
     * @return the total summation of the values as a string
     */
    public String addDecimalNumbersInCsv(String csvListOfDecimals) {
        List<String> listOfStringValues = new ArrayList<>();
        List<BigDecimal> listOfBigDecimalValues = new ArrayList<>();

        listOfStringValues.addAll(Arrays.asList(csvListOfDecimals.split(";")));

        for (String stringValue : listOfStringValues) {
            listOfBigDecimalValues.add(new BigDecimal(stringValue));
        }

        BigDecimal result = listOfBigDecimalValues.stream().reduce(BigDecimal.ZERO, BigDecimal::add);

        return String.valueOf(result);

    }

    /**
     * Basic calculations, multiplication
     *
     * @param input1 the input 1
     * @param input2 the input 2
     * @return the multiplication result as a string
     */
    public String multiplyTwoNumbers(String input1, String input2)    {
        if(input1.contains(",") || input2.contains(",")){
            input1 = input1.replaceAll(",",".");
            input2 = input2.replaceAll(",",".");
        }

        BigDecimal first = new BigDecimal(input1);
        BigDecimal second = new BigDecimal(input2);
        return String.valueOf(first.multiply(second));
    }


    /**
     * Round the input on 2 decimals, uses Bigdecimal RoundingMode.HALF_UP
     *
     * @param input the input
     * @return big decimal
     */
    public static BigDecimal roundOnTwoDecimals(String input){

	    if (input.contains(".") && input.contains(",")) {
		    input = input.replace(".", "");
	    }
	    BigDecimal waarde = new BigDecimal(input.replace(",", "."));

        return waarde.setScale(2, RoundingMode.HALF_UP);
    }


    /**
     * Replace a value in a string using a regular expression pattern.
     *
     * @param input       the input
     * @param pattern     the pattern
     * @param replacement the replacement
     * @return the string
     */
    public static String replace(String input, String pattern, String replacement) {
        return new StringBuilder(input.replaceAll(pattern, replacement)).toString();
    }

    /**
     * Gets substring with regex.
     *
     * @param input the input
     * @param regex the regex
     * @return the substring with regex
     */
    public static String getSubstringWithRegex(String input, String regex) {
        return findOnRegexAndReturnResult(input, regex);
    }

    /**
     * remove trailing zeroes that do not contribute to the value
     * i.e.    2.00 → 2
     *         2.10 → 2.1
     *
     * @param input the input
     * @return string
     */
    public static String removeTrailingZeroesAndDot(String input) {
        input = input.indexOf('.') < 0 ? input : input.replaceAll("0*$","").replaceAll("\\.$","");
        return input;
    }


    /**
     * Method to clean a value for further use in FitNesse.
     *
     * @param input the value to check for null
     * @return empty String or the input
     */
    public static Object ifNullReplaceWithEmptyString(Object input) {
        if (input == null){
            return "";
        }
        return input;

    }

    /**
     * Simple method that can be used to display the value of a Variable or SlimSymbol
     *
     * @param input the input
     * @return string string
     * @deprecated please switch to giveText
     */
    @Deprecated(since = "version 2.1.6", forRemoval = true)
    public String displayValue(String input){
        return input;
    }

    /**
     * Copy of method from variablesfixture
     *
     * @param text the text
     * @return string
     */
    public String giveText(String text) {
        return text;
    }

    /**
     * Returns the number of characters in the input
     *
     * @param input the input
     * @return int int
     */
    public static int getLength(String input) {
        return input.length();
    }

    /**
     * Timestamp your test
     * <p>
     * Returns the current time in the specified format.
     *
     * @param format the format the express the time in.
     * @return the time in the specified format.
     */
    public String giveCurrentTimestampFormatted(String format) {
        SimpleDateFormat simpleDateFormat = parseSimpledateFormat(format);
        return simpleDateFormat.format(new Date(System.currentTimeMillis()));
    }

    /**
     * Give current timestamp formatted minus millis string.
     *
     * @param format the format
     * @param millis the millis
     * @return the formatted timestamp as a string
     */
    public static String giveCurrentTimestampFormattedMinusMillis(String format, long millis) {
        SimpleDateFormat simpleDateFormat = parseSimpledateFormat(format);
        return simpleDateFormat.format(new Date(System.currentTimeMillis() - millis));
    }

    /**
     * Give current timestamp formatted string.
     *
     * @param format the format in SimpleDateFormat
     * @param zone   the timezone
     * @return the formatted timestamp as a string
     */
    public static String giveCurrentTimestampFormatted(String format, String zone) {
        SimpleDateFormat simpleDateFormat = parseSimpledateFormat(format);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(zone));
        return simpleDateFormat.format(new Date(System.currentTimeMillis()));
    }
}


