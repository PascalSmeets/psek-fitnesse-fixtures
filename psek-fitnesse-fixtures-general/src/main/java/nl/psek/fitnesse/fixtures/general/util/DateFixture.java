package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.StopTestCommandException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.lang.String.valueOf;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

/**
 * A fixture to generate dates in multiple formats, convert between formats and perform calculations, for example giveDateTodayPlusNumberOfDays
 * <p>
 * Multiple commands use a date format as input parameter.
 * <p>
 * Some example date format’s are:
 * <p>
 *     dd-MM-yyyy       31–01–2016
 *     ddMMyyyy         31012016
 *     dd MMM yyyy      31 Jan 2016
 *     yyyyMMdd         20160131
 *     E d MMM yyyy G   Sun 31 Jan 2016 AD
 *     D                31
 * <p>
 *     From the java.time documentation:
 * <p>
 *     Symbol  Meaning                     Presentation      Examples
 *     ------  -------                     ------------      -------
 *     G       era                         text              AD; Anno Domini; A
 *     u       year                        year              2004; 04
 *     y       year-of-era                 year              2004; 04
 *     D       day-of-year                 number            189
 *     M/L     month-of-year               number/text       7; 07; Jul; July; J
 *     d       day-of-month                number            10
 *     Q/q     quarter-of-year             number/text       3; 03; Q3; 3rd quarter
 *     Y       week-based-year             year              1996; 96
 *     w       week-of-week-based-year     number            27
 *     W       week-of-month               number            4
 *     E       day-of-week                 text              Tue; Tuesday; T
 *     e/c     localized day-of-week       number/text       2; 02; Tue; Tuesday; T
 *     F       week-of-month               number            3
 *
 *
 * @author Pascal Smeets
 */
public class DateFixture {

    private static String dateFormat = "dd-MM-yyyy";
//    private static LocalDate today = LocalDate.now();
    private static LocalDateTime now = LocalDateTime.now();

    private static DateTimeFormatter formatXml = DateTimeFormatter.ofPattern("yyyyMMdd");


    /**
     * Instantiates a new Date fixture.
     */
     public DateFixture() {
        //default constructor
    }


//Today

    private static String format(GregorianCalendar calendar, String dateFormat) {
        SimpleDateFormat fmt = new SimpleDateFormat(dateFormat);
        fmt.setCalendar(calendar);
        return fmt.format(calendar.getTime());
    }

    private static int randBetween(int start, int end) {
        Random r = new Random();
        return start + r.nextInt(end - start);
    }

// Calculations based on today

    /**
     * Parse simpledate format simple date format.
     *
     * @param simpleDateFormat the simple date format
     * @return the simple date format
     */
    public static SimpleDateFormat parseSimpledateFormat(String simpleDateFormat) {
        SimpleDateFormat df;
        try {
            df = new SimpleDateFormat(simpleDateFormat);
        } catch (IllegalArgumentException e) {
            throw new CommandException("The supplied SimpleDateFormat %s is not a valid SimpleDateFormat. " + e.getMessage(), simpleDateFormat);
        }
        return df;
    }

    /**
     * Returns the current date in the dd-mm-yyyy format.
     *
     * @return the date.
     */
    public static String giveDateToday() {
        return giveDateToday(dateFormat);
    }

    /**
     * Give date today string.
     *
     * @param dateFmt the date fmt
     * @return the string
     */
    public static String giveDateToday(String dateFmt) {
        return now.format(getFormat(dateFmt));
    }

    /**
     * Give date today plus number of days string.
     *
     * @param numberOfDays the number of days
     * @param dateFmt      the date fmt
     * @return the string
     */
    public static String giveDateTodayPlusNumberOfDays(int numberOfDays, String dateFmt) {
        return now.plusDays(numberOfDays).format(getFormat(dateFmt));
    }

    /**
     * Give date today minus number of days string.
     *
     * @param numberOfDays the number of days
     * @param dateFmt      the date fmt
     * @return the string
     */
    public static String giveDateTodayMinusNumberOfDays(long numberOfDays, String dateFmt) {
        return now.minusDays(numberOfDays).format(getFormat(dateFmt));
    }

    /**
     * Give date today minus number of months string.
     *
     * @param numberOfMonths the number of months
     * @param dateFmt        the date fmt
     * @return the string
     */
    public static String giveDateTodayMinusNumberOfMonths(int numberOfMonths, String dateFmt) {
        return now.minusMonths(numberOfMonths).format(getFormat(dateFmt));
    }

    /**
     * Give date today plus number of months string.
     *
     * @param numberOfMonths the number of months
     * @param dateFmt        the date fmt
     * @return the string
     */
    public static String giveDateTodayPlusNumberOfMonths(int numberOfMonths, String dateFmt) {
        return now.plusMonths(numberOfMonths).format(getFormat(dateFmt));
    }

    /**
     * Give date today minus number of years string.
     *
     * @param numberOfYears the number of years
     * @param dateFmt       the date fmt
     * @return the string
     */
    public static String giveDateTodayMinusNumberOfYears(int numberOfYears, String dateFmt) {
        return now.minusYears(numberOfYears).format(getFormat(dateFmt));
    }

    // Calculations based on supplied date

    /**
     * Give date today plus number of years string.
     *
     * @param numberOfYears the number of years
     * @param dateFmt       the date fmt
     * @return the string
     */
    public static String giveDateTodayPlusNumberOfYears(int numberOfYears, String dateFmt) {
        return now.plusYears(numberOfYears).format(getFormat(dateFmt));
    }

    /**
     * Is timestamp between boolean.
     *
     * @param checkStamp       the check stamp
     * @param isAfter          the is after
     * @param isBefore         the is before
     * @param timestampsFormat the timestamps format
     * @return the boolean
     */
    public Boolean isTimestampBetween( String checkStamp, String isAfter, String isBefore, String timestampsFormat) {
        Date toCheck;
        Date after;
        Date before;
        SimpleDateFormat df = parseSimpledateFormat(timestampsFormat);

        try {
            toCheck = df.parse(checkStamp);
            after = df.parse(isAfter);
            before = df.parse(isBefore);
        }catch (ParseException e) {
            throw new CommandException(e.getMessage());
        }
        return (toCheck.after(after) && toCheck.before(before));
    }

    /**
     * Number of millis between long.
     *
     * @param firstTimestamp   the first timestamp
     * @param secondTimestamp  the second timestamp
     * @param timestampsFormat the timestamps format
     * @return the long
     */
    public long numberOfMillisBetween(String firstTimestamp, String secondTimestamp, String timestampsFormat) {
        Date firstTs;
        Date secondTs;
        SimpleDateFormat df = parseSimpledateFormat(timestampsFormat);

        try {
            firstTs = df.parse(firstTimestamp);
            secondTs = df.parse(secondTimestamp);
        }catch (ParseException e) {
            throw new CommandException(e.getMessage());
        }
        return (secondTs.getTime() - firstTs.getTime());
    }


    /**
     * Number of days between two dates.
     *
     * @param firstDay   the first day
     * @param secondDay  the second day
     * @param dateTimeFormat the date format
     * @return the long
     */
    public long numberOfDaysBetween(String firstDay, String secondDay, String dateTimeFormat) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimeFormat);

        LocalDate firstDate = LocalDate.parse(firstDay,formatter);
        LocalDate secondDate = LocalDate.parse(secondDay,formatter);

        return ChronoUnit.DAYS.between(firstDate,secondDate);
    }


    /**
     * Returns the supplied date plus a number of days in the specified output format.
     *
     * @param date         the date
     * @param numberOfDays the number of days in the future based on the supplied date.
     * @param dateFormat   the date format
     * @return the date.
     */
    public static String giveDatePlusNumberOfDays(String date, long numberOfDays, String dateFormat) {
        return getDate(date).plusDays(numberOfDays).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied date minus a number of days in the specified output format.
     *
     * @param date         the date
     * @param numberOfDays the number of days in the past based on the supplied date.
     * @param dateFormat   the date format
     * @return the date.
     */
    public static String giveDateMinusNumberOfDays(String date, long numberOfDays, String dateFormat) {
        return getDate(date).minusDays(numberOfDays).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied date plus a number of months in the specified output format.
     *
     * @param date           the date
     * @param numberOfMonths the number of months in the future based on the supplied date.
     * @param dateFormat     the date format
     * @return the date.
     */
    public static String giveDatePlusNumberOfMonths(String date, int numberOfMonths, String dateFormat) {
        return getDate(date).plusMonths(numberOfMonths).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied date minus a number of months in the specified output format.
     *
     * @param date           the date
     * @param numberOfMonths the number of months in the past based on the supplied date.
     * @param dateFormat     the date format
     * @return the date.
     */
    public static String giveDateMinusNumberOfMonths(String date, int numberOfMonths, String dateFormat) {
        return getDate(date).minusMonths(numberOfMonths).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied date plus a number of years in the specified output format.
     *
     * @param date          the date
     * @param numberOfYears the number of years in the future based on the supplied date.
     * @param dateFormat    the date format
     * @return the date.
     */
    public static String giveDatePlusNumberOfYears(String date, int numberOfYears, String dateFormat) {
        return getDate(date).plusYears(numberOfYears).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied date minus a number of years in the specified output format.
     *
     * @param date          the date
     * @param numberOfYears the number of years in the past based on the supplied date.
     * @param dateFormat    the date format
     * @return the date.
     */
    public static String giveDateMinusNumberOfYears(String date, int numberOfYears, String dateFormat) {
        return getDate(date).minusYears(numberOfYears).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied time plus a number of hours in the specified output format.
     *
     * @param time          the time
     * @param numberOfHours the number of hours in the future based on the supplied time.
     * @param dateFormat    the date format
     * @return the time.
     */
    public static String giveTimestampPlusNumberOfHours(String time, int numberOfHours, String dateFormat) {
        return getTime(time, dateFormat).plusHours(numberOfHours).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied date time a number of hours in the specified output format.
     *
     * @param time          the time
     * @param numberOfHours the number of hours in the past based on the supplied time.
     * @param dateFormat    the date format
     * @return the time.
     */
    public static String giveTimestampMinusNumberOfHours(String time, int numberOfHours, String dateFormat) {
        return getTime(time, dateFormat).minusHours(numberOfHours).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied time plus a number of Seconds in the specified output format.
     *
     * @param time            the time
     * @param numberOfSeconds the number of Seconds in the future based on the supplied time.
     * @param dateFormat      the date format
     * @return the time.
     */
    public static String giveTimestampPlusNumberOfSeconds(String time, int numberOfSeconds, String dateFormat) {
        return getTime(time, dateFormat).plusSeconds(numberOfSeconds).format(getFormat(dateFormat));
    }


    //UNIX time

    /**
     * Returns the supplied date time a number of Seconds in the specified output format.
     *
     * @param time            the time
     * @param numberOfSeconds the number of hours in the past based on the supplied time.
     * @param dateFormat      the date format
     * @return the time.
     */
    public static String giveTimestampMinusNumberOfSeconds(String time, int numberOfSeconds, String dateFormat) {
        return getTime(time, dateFormat).minusSeconds(numberOfSeconds).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied time plus a number of Millis in the specified output format.
     *
     * @param time           the time
     * @param numberOfMillis the number of Millis in the future based on the supplied time.
     * @param dateFormat     the date format
     * @return the time.
     */
    public static String giveTimestampPlusNumberOfMillis(String time, int numberOfMillis, String dateFormat) {
        return getTime(time, dateFormat).plusNanos((long)numberOfMillis* 1000000).format(getFormat(dateFormat));
    }

    /**
     * Returns the supplied date time a number of Millis in the specified output format.
     *
     * @param time           the time
     * @param numberOfMillis the number of hours in the past based on the supplied time.
     * @param dateFormat     the date format
     * @return the time.
     */
    public static String giveTimestampMinusNumberOfMillis(String time, int numberOfMillis, String dateFormat) {
        return getTime(time, dateFormat).minusNanos((long)numberOfMillis*1000000).format(getFormat(dateFormat));
    }

    /**
     * Generates a unix timestamp in milliseconds from January 1, 1970 (midnight UTC/GMT), not counting leap seconds (in ISO 8601: 1970-01-01T00:00:00Z).
     * More info on https://en.wikipedia.org/wiki/Unix_time
     * This method uses the local systems timezone!
     *
     * @return timestamp in milliseconds since January 1, 1970 (midnight UTC/GMT) in the LOCAL timezone.
     */
    public long giveUnixTimestamp() {
        Instant now = Instant.now();
        return now.toEpochMilli();
    }

// Other

    /**
     * Generates a timestamp in the format ISO_INSTANT
     *  "The ISO instant formatter that formats or parses an instant in UTC, such as '2011-12-03T10:15:30Z'."
     *
     * @return string string
     */
    public static String giveIsoInstantTimestamp() {
        return valueOf(Instant.now());

    }

    /**
     * Converts a unix timestamp in milliseconds to the specified output format. Method assumes the local time zone is used.
     *
     * @param unixTime input unix timestamp (local timezone)
     * @param dateFmt output dateTime format
     * @return date (or dateTime) in the specified format
     */
    public static String convertUnixTimeToFormat(long unixTime, String dateFmt) {
        String iso8601Time = Instant.ofEpochMilli(unixTime).toString();
        DateTimeFormatter frmt = DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.systemDefault());
        LocalDateTime input = LocalDateTime.parse(iso8601Time, frmt);
        return input.format(getFormat(dateFmt));
    }

    /**
     * Converts an input date / date timestamp to unix time. Method assumes the local time zone is used.
     *
     * @param dateTime dateStamp or dateTimeStamp (local timezone)
     * @param dateFmt input date format
     * @return unix timeStamp
     */
    public long convertDateTimeToUnixTime(String dateTime, String dateFmt) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFmt);
        sdf.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = sdf.parse(dateTime);
        } catch (ParseException e) {
            throw new CommandException(e.getMessage());
        }
        return date.getTime();
    }

    /**
     * generates a random date between two supplied dates, output is in the SimpleDateFormat format that is specified
     *
     * @param start      a year yyyy ex. 1950
     * @param end        a year yyyy ex. 2010
     * @param dateFormat SimpleDateFormat ex yyyyMMdd or dd-MM-yyyy
     * @return string string
     */
    public static String generateRandomDateBetweenYears(int start, int end, String dateFormat) {
        GregorianCalendar gc = new GregorianCalendar();
        int year = randBetween(start, end);
        gc.set(Calendar.YEAR, year);
        int dayOfYear = randBetween(1, gc.getActualMaximum(Calendar.DAY_OF_YEAR));
        gc.set(Calendar.DAY_OF_YEAR, dayOfYear);
        return format(gc, dateFormat);
    }

    /**
     * generate a random date between 2 specified dates
     *
     * @param year the year
     * @param startMonth the starting month (can be the same as endMonth)
     * @param endMonth the ending month (can be the same as startMonth)
     * @param dateFormat the date format for the retuned random date
     * @return string
     */
    public static String generateRandomDateBetweenMonths(int year, int startMonth, int endMonth, String dateFormat) {
        if(endMonth < startMonth){
            throw new StopTestCommandException("De eindmaand (3e parameter) moet groter of gelijk zijn dan de startmaand (2e parameter)");
        }
        LocalDate initialEnd = LocalDate.of(year, endMonth, 1);

        LocalDate startDate = LocalDate.of(year, startMonth, 1);
        LocalDate endDate = initialEnd.with(lastDayOfMonth());

        int days = (int) ChronoUnit.DAYS.between(startDate, endDate);
        int random = randBetween(0, days);

        LocalDate randomDayBetweenMonths = startDate.plusDays(random);
        LocalDateTime dateTime = LocalDateTime.of(randomDayBetweenMonths, LocalTime.now());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return dateTime.format(formatter);

    }

    /**
     * Generate random date between now and a number of days in the past string.
     *
     * @param daysinthepast the daysinthepast
     * @param dateFormat    the date format
     * @return the string
     */
    public String generateRandomDateBetweenNowAndANumberOfDaysInThePast(int daysinthepast, String dateFormat) {

        LocalDate now = LocalDate.now();
        LocalDate past = now.minusDays(daysinthepast);


        int random = randBetween(0, daysinthepast);

        LocalDate randomDateInThePast = past.plusDays(random);

        //add current timestamp to generated date
        LocalDateTime dateTime = LocalDateTime.of(randomDateInThePast, LocalTime.now());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return dateTime.format(formatter);

    }


    /**
     * Add time to timestamp string.
     *
     * @param inputTimestamp the input timestamp
     * @param inputFormat    the input format
     * @param amount         the amount
     * @param chronoUnit     the chrono unit
     * @param outputFormat   the output format
     * @return the string
     */
    public static String addTimeToTimestamp(String inputTimestamp, String inputFormat, int amount, ChronoUnit chronoUnit, String outputFormat) {
        Instant instant = LocalDateTime.parse(
                inputTimestamp, DateTimeFormatter.ofPattern(inputFormat))
                .atZone(ZoneId.systemDefault())
                .toInstant();
        instant = instant.plus(amount,chronoUnit);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(outputFormat).withZone(ZoneId.systemDefault());
        return formatter.format(instant);
    }


    /**
     * Convert date from to format string.
     *
     * @param oldDate   the old date
     * @param oldFormat the old format
     * @param newFormat the new format
     * @return the string
     */
    public static String convertDateFromToFormat(String oldDate, String oldFormat, String newFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(oldFormat);
        Date date = null;
        try {
            date = sdf.parse(oldDate);
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage());
        }
        if (newFormat.contains("HH:mm:ss.SSS") && !oldFormat.contains("HH")) {
            sdf.applyPattern("HHmmssSSS");
            if ( sdf.format(date).equals("000000000"))
                newFormat = newFormat.replace("HH:mm:ss.SSS", "23:59:59.999");
        }
        sdf.applyPattern(newFormat);
        return sdf.format(date);
    }

    private static LocalDate getDate(String value) {
        return LocalDate.parse(value, formatXml);
    }

    private static LocalDateTime getTime(String value, String dateFormat) {
        return LocalDateTime.parse(value, DateTimeFormatter.ofPattern(dateFormat));
    }

    private static DateTimeFormatter getFormat(String dateFormat) {
        return DateTimeFormatter.ofPattern(dateFormat);
    }

    /**
     * Method to fetch the last day of the Previous month
     * @param dateFormat   date format
     * @return the string
     */
    public static String getLastDayofPreviousMonth(String dateFormat) {
        return LocalDate.now().withDayOfMonth(1).minusDays(1).format(getFormat(dateFormat));
    }
}
