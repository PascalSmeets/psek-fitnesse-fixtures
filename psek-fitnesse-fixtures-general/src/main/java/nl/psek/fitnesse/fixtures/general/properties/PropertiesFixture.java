package nl.psek.fitnesse.fixtures.general.properties;

import nl.psek.fitnesse.fixtures.util.EncryptionUtil;
import nl.psek.fitnesse.fixtures.util.FileFinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * A fixture for loading a properties file and retrieving the entry's, with support for encrypted entries using the EncryptionUtil
 *
 * @author Ronald Mathies
 */

public class PropertiesFixture {

    private static final Logger LOG = LoggerFactory.getLogger(PropertiesFixture.class);
    private Properties properties = null;
    private static final String PROPERTY_ENCRYPTED_PREFIX = "ENCRYPTED";
    private static String encryptionSecretSystemPropertyKeyName = "encryption.password";
    private boolean useTestcaseDefinedPassword = false;
    private String encryptionPassword;

    /**
     * Load the given properties file, the file can be anywhere on the user.dir
     * path. It will use the FileFinder to recursively search the path for the given file.
     *
     * @param file the properties file.
     */

    public PropertiesFixture(String file) {

        FileFinder.findFileInUserDirectory(file, file1 -> {
            try {
                properties = new Properties();
                properties.load(new FileReader(file1));
            } catch (IOException e) {
                throw new IllegalArgumentException("PropertiesFixture cannot load file '" + file1 + "', please make sure the file exists and is on the classpath. ");
            }
        });

        if (properties == null) {
            try {
                properties = new Properties();
                properties.load(new FileReader(file));
            } catch (IOException e) {
                throw new IllegalArgumentException("PropertiesFixture cannot load file '" + file + "', please make sure the file exists and is on the classpath. ");
            }
        }
    }

    /**
     * Returns the string value of the given property key. If the property is encrypted (using EncryptionUtils) the value is decrypted and returned.
     * <p>
     * By default the encryption password is taken from the System.property with key `encryption.password`, specified by the field `encryptionSecretSystemPropertyKeyName`.
     * Override this behaviour by setting the key value itself
     *
     * @param key the key to search for.
     * @return the string value.
     * @throws java.lang.IllegalArgumentException when the property key cannot be found.
     */

    public String getStringProperty(String key) {
        if (properties.containsKey(key)) {
            String value = properties.getProperty(key);

            if (value.startsWith(PROPERTY_ENCRYPTED_PREFIX)) {
                return getEncryptedProperty(value.substring(PROPERTY_ENCRYPTED_PREFIX.length()));
            } else return value;
        }

        throw new IllegalArgumentException("PropertiesFixture cannot get property with key '" + key + "', please make sure the property with that key exists.");
    }

    /**
     * Returns the int value of the given property key.
     *
     * @param key the key to search for.
     * @return the int value.
     * @throws java.lang.IllegalArgumentException when the property key cannot be found or the property
     *                                            key doesn't contain a integer value.
     */

    public int getIntProperty(String key) {
        try {
            return Integer.parseInt(getStringProperty(key));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("PropertiesFixture cannot get int property value with key '" + key + "', please make sure the property contains an integer value.");
        }
    }

    private String getEncryptedProperty(String value) {
        String pass;

        if (System.getProperty(encryptionSecretSystemPropertyKeyName) != null) {  //if the password is defined by a System.property use this one, default key name = 'encryption.password', can be overridden using `setPasswordPropertyKey`
            LOG.info("Using password from system.property with key = " + encryptionSecretSystemPropertyKeyName + " (password is not logged)");
            pass = System.getProperty(encryptionSecretSystemPropertyKeyName);
        } else if (useTestcaseDefinedPassword) {   //if defined by user in testcase use this password
            LOG.info("Using password from testcase (password is not logged)");
            pass = this.encryptionPassword;
        } else {
            throw new IllegalArgumentException("The encryption password could not be determined");
        }

        EncryptionUtil util = new EncryptionUtil();
        util.setPassword(pass);
        return util.decode(value);
    }

    public void setPassword(String encryptionPassword) {
        useTestcaseDefinedPassword = true;
        this.encryptionPassword = encryptionPassword;
    }

    public static void setPasswordPropertyKey(String key) {
        encryptionSecretSystemPropertyKeyName = key;
    }

}
