package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.StopTestCommandException;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;

/**
 * A simple fixture to POST files to an upload form
 */
public class SimpleHttpPoster {

	private static final Logger LOG = LoggerFactory.getLogger(SimpleHttpPoster.class);
	private static int TIMEOUTSECONDS = 20;

	/**
	 * Reads a file line by line to a string with Charset = UTF-8
	 * @param filePath the file to be read
	 * @return String file contents
	 */
	private static String readLineByLineJava8(String filePath) {
		StringBuilder contentBuilder = new StringBuilder();

		try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			throw new StopTestCommandException(e.getMessage());
		}

		return contentBuilder.toString();
	}

	/**
	 * Method to POST a file as a Multipart Part to a webservice with customisable timeout
	 *
	 * @param uri            the uri
	 * @param textFilePath   the text file path
	 * @param timeoutSeconds the custom timeout
	 * @return http statuscode
	 */
	public int postMultipartFileToUri(String uri, String textFilePath, int timeoutSeconds){
		this.TIMEOUTSECONDS = timeoutSeconds;

		return postMultipartFileToUriWithFailCheck(uri, textFilePath, true);

	}

	/**
	 * Method to POST a file as a Multipart Part to a webservice with customisable timeout, max number of retries
	 *
	 * @param uri            the uri
	 * @param textFilePath   the text file path
	 * @param timeoutSeconds the custom timeout
	 * @param maxTries       the max number of tries the operation will be attempted
	 * @return http statuscode
	 */
	public int postMultipartFileToUriWithRetry(String uri, String textFilePath, int timeoutSeconds, int maxTries) {
		this.TIMEOUTSECONDS = timeoutSeconds;

		int statuscode = postMultipartFileToUriWithFailCheck(uri, textFilePath, false);

		int tryCount = 1;
		int totalTryCount = 1;

		while (Status.FAILURE.matches(statuscode) && tryCount < maxTries) {
			tryCount++;
			LOG.info("postMultipartFileToUriWithRetry, try = " + tryCount);
			totalTryCount = tryCount;
			statuscode = postMultipartFileToUriWithFailCheck(uri, textFilePath, false);
		}

		LOG.info("postMultipartFileToUriWithRetry, total tries until succesful or max = " + totalTryCount);
		return statuscode;

	}


	/**
	 * Method to POST a file as a Multipart Part to a webservice
	 *
	 * @param uri          the location of the webservice
	 * @param textFilePath the path to the file that will be sent, uses UTF-8
	 * @param throwErrorOnFail
	 * @return http statuscode
	 * @throws ConditionalException the conditional exception
	 */
	public int postMultipartFileToUriWithFailCheck(String uri, String textFilePath, boolean throwErrorOnFail) throws ConditionalException {
		String contents = readLineByLineJava8(textFilePath);

		LOG.info("BEGIN:      REQUEST TO SEND: ");
		LOG.info(" + METHOD  = post");
		LOG.debug(" + URI     = " + uri);
		LOG.info(" + TIMEOUT = " + TIMEOUTSECONDS + " seconds");
		LOG.info(" + FILE    = " + textFilePath);
		LOG.info(" + INHOUD  = " + contents);


		OkHttpClient client = new OkHttpClient.Builder()
				.connectTimeout(TIMEOUTSECONDS, TimeUnit.SECONDS)
				.writeTimeout(TIMEOUTSECONDS, TimeUnit.SECONDS)
				.readTimeout(TIMEOUTSECONDS,TimeUnit.SECONDS)
				.build();

		RequestBody filebody = RequestBody.create(MediaType.get("text/xml"), contents);

		RequestBody multipartBody = new MultipartBody.Builder()
				.setType(MultipartBody.FORM)
				.addFormDataPart("file", Paths.get(textFilePath).getFileName().toString(), filebody)
				.build();

		Request request = new Request.Builder()
				.url(getFromPropertyIfApplicable(uri))
				.post(multipartBody)
				.build();
		LOG.info("END:        REQUEST TO SEND. ");
		LOG.info("BEGIN:                RECEIVED RESPONSE: ");

		try (Response response = client.newCall(request).execute()) {

			int responsecode = response.code();
			String responsebody = response.body().string();
			long tx = response.sentRequestAtMillis();
			long rx = response.receivedResponseAtMillis();

			LOG.info(" + RESPONSE CODE = " + responsecode);
			LOG.info(" + RESPONSE BODY = " + responsebody);
			LOG.info(" + RESPONSE TIME = " + (rx - tx)+" ms");

			if (throwErrorOnFail) {
			if (!response.isSuccessful()) {
				throw new StopTestCommandException("Unexpected code " + response);
			}
			}
			LOG.info("END:                  RECEIVED RESPONSE. ");
			return response.code();
		} catch (IOException e) {
			throw new ConditionalException(e.getMessage());
		}
	}
}
