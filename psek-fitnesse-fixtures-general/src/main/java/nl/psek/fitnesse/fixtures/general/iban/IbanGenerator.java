package nl.psek.fitnesse.fixtures.general.iban;

import nl.psek.fitnesse.ConditionalException;

import java.math.BigInteger;
import java.util.Random;

/**
 * A fixture to generate an International Bank Account Number (IBAN)
 *
 * @author Pascal Smeets https://nl.wikipedia.org/wiki/International_Bank_Account_Number
 */
public class IbanGenerator {

    // Range for the accountNumberGenerator
    private static final int LOW = 100000000;
    private static final int HIGH = 999999900;

    /**
     * Constructs a new IBAN Generator.
     */
    public IbanGenerator() {
        //default constructor
    }

    /**
     * Generates an IBAN for accountNumbers between 10 and 30 numerical characters.
     *
     * @param countryCode   option NL or DK
     * @param bankCode      4 alpha characters
     * @param accountNumber 10 - 30 numerical characters, for NL or DK length = 10
     * @return IBAN number as String
     * @throws ConditionalException the conditional exception
     */
    public static String generateIBAN(String countryCode, String bankCode, String accountNumber) {
        if (accountNumber.length() < 10 || accountNumber.length() > 30) {
            throw new ConditionalException("The accountnumber needs to be between 10 and 30 number character long");
        }

        BigInteger numberToCheck = new BigInteger(getNumericValuesFromString(bankCode) + accountNumber + getNumericValuesFromString(countryCode) + "00");
        BigInteger modulusValue = new BigInteger("97");
        int remainder = numberToCheck.mod(modulusValue).intValue();
        String controlValue = String.valueOf(98 - remainder);

        if (controlValue.length() < 2) {
            return countryCode + "0" + controlValue + bankCode + accountNumber;
        } else return countryCode + controlValue + bankCode + accountNumber;
    }

    /**
     * Generates an IBAN using a 10 digit generated accountNumber that is a valid Dutch accountnumber.
     *
     * @param countryCode option NL or DK
     * @param bankCode    4 alpha characters
     * @return IBAN number as String
     */
    public static String generateIBAN(String countryCode, String bankCode) {
        String accountNumber = generateAccountNumber();
        return generateIBAN(countryCode, bankCode, accountNumber);
    }


    /**
     * Validates a Dutch IBAN of 18 characters long
     * Using format NLkk BBBB CCCC CCCC CK:
     * NL countryCode
     * kk controlValue
     * BBBB bankCode
     * CCCC CCCC CK accountNumber (including controlvalue)
     *
     * @param iban the iban
     * @return boolean
     * @throws ConditionalException the conditional exception
     */
    public Boolean validateDutchIban(String iban) {
        if (iban.length() != 18) {
            throw new ConditionalException("Supplied IBAN is not 18 character long.");
        }

        String countryCode = iban.substring(0, 2);
        String controlValue = iban.substring(2, 4);
        String bankCode = iban.substring(4, 8);
        String accountNumber = iban.substring(8);

        BigInteger numberToCheck = new BigInteger(getNumericValuesFromString(bankCode) + accountNumber + getNumericValuesFromString(countryCode) + controlValue);
        BigInteger modulusValue = new BigInteger("97");

        return (numberToCheck.mod(modulusValue).intValue() == 1);
    }

    /**
     * Gets numeric value from character.
     *
     * @param string the string
     * @return the numeric value from character
     */
    public static String getNumericValueFromCharacter(String string) {
        return String.valueOf(Character.getNumericValue(string.toLowerCase().charAt(0)));
    }

    /**
     * Gets numeric values from string.
     *
     * @param string the string
     * @return the numeric values from string
     * @throws ConditionalException the conditional exception
     */
    public static String getNumericValuesFromString(String string) {

        if (string.length() == 4) {
            String bc1 = string.substring(0, 1);
            String bc2 = string.substring(1, 2);
            String bc3 = string.substring(2, 3);
            String bc4 = string.substring(3, 4);

            return getNumericValueFromCharacter(bc1) + getNumericValueFromCharacter(bc2) + getNumericValueFromCharacter(bc3) + getNumericValueFromCharacter(bc4);
        }

        if (string.length() == 2) {
            String cc1 = string.substring(0, 1);
            String cc2 = string.substring(1, 2);

            return getNumericValueFromCharacter(cc1) + getNumericValueFromCharacter(cc2);
        } else
            throw new ConditionalException("This only works with a BankCode of 4 characters or a Countrycode of 2 characters, you supplied '%s' characters", string.length());
    }


    /**
     * Generates a valid accountnumber to use in an IBAN. Accountnumber is a valid Dutch accountnumber.
     *
     * @return string
     * @throws ConditionalException the conditional exception
     */
    public static String generateAccountNumber() {
        int possibleAccountNumber = giveRandomStartingNumber();

        for (; ; ++possibleAccountNumber) {
            if (checkElfProef(possibleAccountNumber)) {
                return String.valueOf("0" + possibleAccountNumber);
            }

            if (possibleAccountNumber > HIGH) {
                throw new ConditionalException("No accountNumber was found.");
            }
        }
    }

    private static int giveRandomStartingNumber() {
        return new Random().nextInt(HIGH - LOW) + LOW;
    }


    private static boolean checkElfProef(int possibleAccountNumber) {
        if (String.valueOf(possibleAccountNumber).length() == 9) {
            int a, b, c, d, e, f, g, h, i, pos = 0;

            a = giveNumberOnPosition(possibleAccountNumber, pos++);
            b = giveNumberOnPosition(possibleAccountNumber, pos++);
            c = giveNumberOnPosition(possibleAccountNumber, pos++);
            d = giveNumberOnPosition(possibleAccountNumber, pos++);
            e = giveNumberOnPosition(possibleAccountNumber, pos++);
            f = giveNumberOnPosition(possibleAccountNumber, pos++);
            g = giveNumberOnPosition(possibleAccountNumber, pos++);
            h = giveNumberOnPosition(possibleAccountNumber, pos++);
            i = giveNumberOnPosition(possibleAccountNumber, pos);

            int resultaat = (9 * a) + (8 * b) + (7 * c) + (6 * d) + (5 * e) + (4 * f) + (3 * g) + (2 * h) + (1 * i);

            return resultaat % 11 == 0;
        } else throw new ConditionalException("Het opgegeven nummer bevat geen 9 karakters");
    }


    private static int giveNumberOnPosition(int possibleAccountNumber, int position) {
        String number = String.valueOf(possibleAccountNumber);
        String substring = number.substring(position, position + 1);
        return Integer.parseInt(substring);
    }
}
