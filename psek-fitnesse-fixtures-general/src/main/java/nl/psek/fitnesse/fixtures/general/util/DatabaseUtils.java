package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.fixtures.general.properties.PropertiesFixture;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;

/**
 * Helper class used in nl.psek.fitnesse.fixtures.general.pdf.PdfTextReaderFixture
 *
 * Created by Pascal on 18-1-2017.
 */
public class DatabaseUtils {

    private static final String CONNECTION_DATABASE_DRIVER = "database.driver";
    private static final String CONNECTION_DATABASE_URL = "database.url";
    private static final String CONNECTION_DATABASE_USERNAME = "database.username";
    private static final String CONNECTION_DATABASE_PASSWORD = "database.password";  //used to pass the password from a property file to this class

    private Map<String, Connection> connections = new HashMap<>();

    private List<String> parameters = new ArrayList<>();

    @Deprecated
    public void createConnection(String name, String properties) {
        Connection connection = createAConnection(properties);
        connections.put(name, connection);
    }

    /**
     * Creates a new database connection with the specified name the credentials are taken directly OR from system.properties
     *
     * @param name an alias for this connection
     * @param databaseDriver the database driver name as a string
     * @param url the URL to connect to, provided als plain text or with prefix 'p:' with a string that points to a System.property
     * @param username the username, provided als plain text or with prefix 'p:' with a string that points to a System.property
     * @param password the password , provided als plain text or with prefix 'p:' with a string that points to a System.property
     */
    public void createConnection(String name, String databaseDriver, String url, String username, String password) {
        Connection connection = createAConnection(databaseDriver, url, username, password);
        connections.put(name, connection);
    }

    @Deprecated
    public void createConnectionUsingSystenProperties(String name, String databaseDriver, String systemPropertyForUrl, String systemPropertyForUsername, String systemPropertyForPassword) {
        Connection connection = createAConnectionUsingSystemProperties(databaseDriver,systemPropertyForUrl, systemPropertyForUsername, systemPropertyForPassword);
        connections.put(name, connection);
    }


    private Connection findConnectionByName(String name) {
        Connection connection = null;
        if (connections.containsKey(name)) {
            connection = connections.get(name);
        } else {
        throw new ConditionalException("No database connection found with name '%s'.", name);
     }
        return connection;
    }


    public InputStream getBlobAsStream(String name, String sql) {
        Connection connection = findConnectionByName(name);

        if (connection != null) {
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            setPreparedStatementParameters(statement);

                try (ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                if (Types.BLOB == resultSet.getMetaData().getColumnType(1)) {
                    return resultSet.getBlob(1).getBinaryStream();
                } else {
                    throw new ConditionalException("The column that was found is not a BLOB");
                }
            } else {
                throw new ConditionalException("No rows were found");
            }
                }
        } catch (SQLException e) {
            throw new ConditionalException("Failed to execute query '%s' due to the following '%s'.", sql, e.getMessage());
        }
     }
        return null;
    }


    private void setPreparedStatementParameters(PreparedStatement statement) throws SQLException {
        for (int i = 0; i < parameters.size(); i++) {
            statement.setObject(i + 1, parameters.get(i));
        }
        parameters.clear();
    }

    @Deprecated
    private Connection createAConnection(String properties) {
        PropertiesFixture propertiesFixture = new PropertiesFixture(properties);

        String databaseDriver = propertiesFixture.getStringProperty(CONNECTION_DATABASE_DRIVER);
        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new ConditionalException("Cannot find database driver class '%s'.", databaseDriver);
        }

        String databaseUrl = propertiesFixture.getStringProperty(CONNECTION_DATABASE_URL);
        String databaseUsername = propertiesFixture.getStringProperty(CONNECTION_DATABASE_USERNAME);
        String databasePassword = propertiesFixture.getStringProperty(CONNECTION_DATABASE_PASSWORD);

        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException sqlException) {
            throw new ConditionalException("Cannot open connection to '%s' because '%s'.", databaseUrl, sqlException.getMessage());
        }
    }


    private Connection createAConnection(String databaseDriver, String url, String username, String password) {

        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new ConditionalException("Cannot find database driver class '%s'.", databaseDriver);
        }


        String databaseUrl = getFromPropertyIfApplicable(url);
        String databaseUsername = getFromPropertyIfApplicable(username);
        String databasePassword = getFromPropertyIfApplicable(password);

        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException sqlException) {
            throw new ConditionalException("Cannot open connection to '%s' because '%s'.", databaseUrl, sqlException.getMessage());
        }
    }


    private Connection createAConnectionUsingSystemProperties(String databaseDriver, String systemPropertyForUrl, String systemPropertyForUsername, String systemPropertyForPassword) {

        try {
            Class.forName(databaseDriver);
        } catch (ClassNotFoundException classNotFoundException) {
            throw new ConditionalException("Cannot find database driver class '%s'.", databaseDriver);
        }


        String databaseUrl = System.getProperty(systemPropertyForUrl);
        String databaseUsername = System.getProperty(systemPropertyForUsername);
        String databasePassword = System.getProperty(systemPropertyForPassword);

        try {
            return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException sqlException) {
            throw new ConditionalException("Cannot open connection to '%s' because '%s'.", databaseUrl, sqlException.getMessage());
        }
    }




    /**
     * Closes a database connection with the specified name.
     *
     * @param name the name of the database connection.
     */

    public void closeConnection(String name) {
        Connection connection = findConnectionByName(name);
        if (connection != null) {
        try {
            connection.close();
        } catch (SQLException sqlException) {
            // Ignore this message.
        } finally {
            connections.remove(name);
        }
    }
    }

}
