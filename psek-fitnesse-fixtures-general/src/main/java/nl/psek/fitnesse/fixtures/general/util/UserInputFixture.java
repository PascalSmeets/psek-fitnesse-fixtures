package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.ConditionalException;

import javax.swing.*;

/**
 * A fixture thats can get user input during test execution, useful for demonstration purposes.
 *
 * Created by Pascal on 28-5-2015.
 */
public class UserInputFixture {


    /**
     * Instantiates a new User input fixture.
     */
    public UserInputFixture() {
        //default constructor
    }

    /**
     * Option to ask the user for input using a Dialog and return the value to fitnesse
     *
     * @param message the message
     * @return input string
     */


    public String askUserInput(String message) {
        return askUserInput(message, 0, 0);

    }

    /**
     * Option to ask the user for input using a Dialog and return the value to fitnesse, it is possible to specify the min and max length of the input
     *
     * @param message   the message
     * @param minLength the min length
     * @param maxLength the max length
     * @return input string
     */


    public String askUserInput(String message, int minLength, int maxLength) {

        String input = JOptionPane.showInputDialog(message).trim();

        if (minLength != 0 && input.length() < minLength) {
                throw new ConditionalException("De opgegeven waarde '" + input + "' moet minimaal '" + minLength + "' karakters lang zijn.");
            }

        if (maxLength != 0 && input.length() > maxLength) {
                throw new ConditionalException("De opgegeven waarde '" + input + "' mag maximaal '" + maxLength + "' karakters lang zijn.");
            }
        if (input.length() != 0) {
            return input;
        }

        throw new ConditionalException("Er is geen input gevonden");
    }


}
