package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.fixtures.selenium.SeleniumFixtureDownloadUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.containsPattern;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.matchOnPattern;

/**
 * Helper class for nl.psek.fitnesse.fixtures.general.pdf.PdfTextReaderFixture
 */
public class PDFUtils {

    /**
     * Ik laad een PDF bestand uit een {@code InputStream}
     *
     * @param is the is
     * @return pd document
     * @throws IOException the io exception
     */
    public static PDDocument loadPdfFromStream(InputStream is) throws IOException {
        return PDDocument.load(is);
    }

    /**
     * Load pdf pd document.
     *
     * @param file the file
     * @return the pd document
     * @throws IOException the io exception
     */
    public static PDDocument loadPDF(File file) throws IOException {
        return PDDocument.load(file);
    }

    /**
     * Load pdf pd document.
     *
     * @param url the url
     * @return the pd document
     * @throws IOException the io exception
     */
    public static PDDocument loadPdf(URL url) throws IOException {
        SeleniumFixtureDownloadUtil downloader = new SeleniumFixtureDownloadUtil();
        return PDDocument.load(downloader.download(url));
    }

    /**
     * Ik filter de tekst uit een PDF bestand. En geef dit terug
     * als String. De volgorde waarin ik de tekst ophaal wordt getracht op
     * basis van de positie van de tekst in het document te worden bepaald.
     *
     * @param pdf              the pdf
     * @param sorteerOpPositie zet op true als de tekst op positie moet worden bepaald.
     * @return string
     * @throws IOException the io exception
     */
    public static String pdfText(PDDocument pdf, boolean sorteerOpPositie) throws IOException {
        PDFTextStripper pdfStripper = new PDFTextStripper();
        pdfStripper.setSortByPosition(sorteerOpPositie);

        return pdfStripper.getText(pdf);
    }

    /**
     * Ik geef een array van gevonden tekst regels uit de PDF terug.
     *
     * @param pdf the pdf
     * @return string [ ]
     * @throws Exception the exception
     */
    public static String[] pdfTextLines(PDDocument pdf) throws Exception {
        String[] tekstRegels = pdfText(pdf, false).split("\n");
        for (int i = 0; i < tekstRegels.length; i++) {
            tekstRegels[i] = tekstRegels[i].replace("\r", "").trim();
        }

        return tekstRegels;
    }

    /**
     * Ik geef alleen true terug als de tekst in de PDF een regel
     * bevat die overeenkomt met de opgegeven tekst
     *
     * @param pdf     the pdf
     * @param pattern the pattern
     * @return boolean
     * @throws Exception the exception
     */
    public static boolean pdfContainsLine(PDDocument pdf, String pattern) throws Exception {
        boolean patternFound = false;
        List<String> l = Arrays.asList(pdfTextLines(pdf));


        for (int i = 0; i < l.size(); i++) {
            String s = l.get(i);
            if (matchOnPattern(s, pattern)) {
                patternFound = true;
                break;

            }
        }

        return patternFound;
    }

    /**
     * Controleert of een regel begint met de aangeleverde tekst
     *
     * @param pdf   the pdf
     * @param tekst the tekst
     * @return the boolean
     * @throws Exception the exception
     */
    public static boolean pdfLineStartsWith(PDDocument pdf, String tekst) throws Exception {
        boolean textFound = false;
        for (String r : pdfTextLines(pdf)) {
            if (r.startsWith(tekst)) {
                textFound = true;
                break;
            }
        }
        return textFound;
    }

    /**
     * Controleert of een regel de aangeleverde tekst bevat
     *
     * @param pdf     the pdf
     * @param pattern the pattern
     * @return the boolean
     * @throws Exception the exception
     */
    public static boolean pdfLineContains(PDDocument pdf, String pattern) throws Exception {
        boolean patternFound = false;
        for (String r : pdfTextLines(pdf)) {
            if (containsPattern(r, pattern)) {
                patternFound = true;
                break;
            }
        }
        return patternFound;
    }

    /**
     * Controleert of een regel met de aangeleverde eindigt
     *
     * @param pdf   the pdf
     * @param tekst the tekst
     * @return the boolean
     * @throws Exception the exception
     */
    public static boolean pdfLineEndsOn(PDDocument pdf, String tekst) throws Exception {
        boolean textFound = false;
        for (String r : pdfTextLines(pdf)) {
            if (r.endsWith(tekst)) {
                textFound = true;
                break;
            }
        }
        return textFound;
    }


    /**
     * Ik geef alleen true terug als de tekst in de PDF een regel
     * bevat die overeenkomt met de opgegeven tekst, ongeacht de casing van het woord.
     *
     * @param pdf   the pdf
     * @param regel the regel
     * @return boolean
     * @throws Exception the exception
     */
    public static boolean pdfContainsLineCaseInsensitive(PDDocument pdf, String regel) throws Exception {
        for (String tekstRegel : pdfTextLines(pdf)) {
            if (tekstRegel.equalsIgnoreCase(regel)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Ik filter de tekst uit een PDF en geef op basis
     * van opgegeven regelnummer de bijbehorende tekst terug.
     *
     * @param pdf         the pdf
     * @param regelnummer the regelnummer
     * @return string
     * @throws Exception the exception
     */
    public static String pdfTextLine(PDDocument pdf, int regelnummer) throws Exception {
        String[] regels = pdfTextLines(pdf);
        if (regelnummer > 0 && regelnummer <= regels.length) {
            return regels[(regelnummer - 1)];
        } else {
            throw new CommandException("regelnummer buiten bereik");
        }
    }

    /**
     * Ik filter de tekst uit een PDF en geef op basis
     * van opgegeven regelnummer de bijbehorende tekst terug.
     *
     * @param pdf               the pdf
     * @param zoekVanafTekst    the zoek vanaf tekst
     * @param zoekTotTekst      the zoek tot tekst
     * @param teVerwachtenTekst the te verwachten tekst
     * @return boolean
     * @throws Exception the exception
     */
    public static boolean pdfContainsTextBetweenTwoTexts(PDDocument pdf, String zoekVanafTekst, String zoekTotTekst, String teVerwachtenTekst) throws Exception {
        return true;
    }

    /**
     * Pdf contains text between two texts on one page boolean.
     *
     * @param pdf               the pdf
     * @param paginaNr          the pagina nr
     * @param zoekVanafTekst    the zoek vanaf tekst
     * @param zoekTotTekst      the zoek tot tekst
     * @param teVerwachtenTekst the te verwachten tekst
     * @return the boolean
     * @throws Exception the exception
     */
    public static boolean pdfContainsTextBetweenTwoTextsOnOnePage(PDDocument pdf, int paginaNr, String zoekVanafTekst, String zoekTotTekst, String teVerwachtenTekst) throws Exception {
        String teDoorzoekenTekst = giveTextFromPage(pdf, paginaNr);
        int startIndex = teDoorzoekenTekst.indexOf(zoekVanafTekst);
        if (startIndex < 0) {
            throw new CommandException("zoek vanaf tekst niet gevonden");
        }
        startIndex += zoekVanafTekst.length();
        int endIndex = teDoorzoekenTekst.indexOf(zoekTotTekst, startIndex);
        if (endIndex < 0) {
            throw new CommandException("zoek tot tekst niet gevonden");
        }
        if (startIndex > endIndex) {
            throw new CommandException("eindtekst eerder gevonden dan de begin tekst om tussen te zoeken");
        }

        return teDoorzoekenTekst.substring(startIndex, endIndex).contains(teVerwachtenTekst);

    }

    /**
     * Pdf contains text between two texts in one block on one page boolean.
     *
     * @param pdf                the pdf
     * @param paginaNr           the pagina nr
     * @param zoekVanafTekst     the zoek vanaf tekst
     * @param blokNrZoekVanTekst the blok nr zoek van tekst
     * @param zoekTotTekst       the zoek tot tekst
     * @param teVerwachtenTekst  the te verwachten tekst
     * @return the boolean
     * @throws Exception the exception
     */
    public static boolean pdfContainsTextBetweenTwoTextsInOneBlockOnOnePage(PDDocument pdf, int paginaNr, String zoekVanafTekst, int blokNrZoekVanTekst, String zoekTotTekst, String teVerwachtenTekst) throws Exception {
        int teller = 1;
        String teDoorzoekenTekst = giveTextFromPage(pdf, paginaNr);
        int startIndex = 0;
        for (int i = 0; i < blokNrZoekVanTekst; i++) {
            startIndex = teDoorzoekenTekst.indexOf(zoekVanafTekst, startIndex);
            if (startIndex < 0) {
                throw new CommandException("zoek vanaf tekst niet gevonden");
            }
            startIndex += zoekVanafTekst.length();
        }
        int endIndex = teDoorzoekenTekst.indexOf(zoekTotTekst, startIndex);
        if (endIndex < 0) {
            throw new CommandException("zoek tot tekst niet gevonden");
        }
        if (startIndex > endIndex) {
            throw new CommandException("eindtekst eerder gevonden dan de begin tekst om tussen te zoeken");
        }

        return teDoorzoekenTekst.substring(startIndex, endIndex).contains(teVerwachtenTekst);

    }

    /**
     * Give text from page string.
     *
     * @param pdf      the pdf
     * @param paginaNr the pagina nr
     * @return the string
     * @throws IOException the io exception
     */
    public static String giveTextFromPage(PDDocument pdf, int paginaNr) throws IOException {
        PDFTextStripper stripper = new PDFTextStripper();
        stripper.setSortByPosition(true);
        if (paginaNr > 0 && paginaNr <= pdf.getNumberOfPages()) {
            stripper.setStartPage(paginaNr);
            stripper.setEndPage(paginaNr);
            return stripper.getText(pdf);
        } else {
            throw new CommandException("Pagina nummer buiten bereik van pdf");
        }
    }

    /**
     * Ik geef de tekst terug uit het PDF bestand, waarbij ik alleen kijk naar een
     * opgegeven visueel gebied. Dit gebied kun je defineren door x,y,breedte en hoogte
     * op te geven, en het pagina nummer in de pdf waar dit gebied zich bevind.
     *
     * @param pdf      the pdf
     * @param paginaNr the pagina nr
     * @param x        the x
     * @param y        the y
     * @param width    the width
     * @param height   the height
     * @return string
     * @throws Exception the exception
     */
    public static String pdfTextFromRegion(PDDocument pdf, int paginaNr, int x, int y, int width, int height) throws Exception {
        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
        stripper.setSortByPosition(true);

        Rectangle rect = new Rectangle(x, y, width, height);
        stripper.addRegion("class1", rect);

        List allPages = (List) pdf.getDocumentCatalog().getPages();
        if (paginaNr > 0 && paginaNr <= allPages.size()) {
            PDPage page = (PDPage) allPages.get((paginaNr - 1));
            stripper.extractRegions(page);

            return stripper.getTextForRegion("class1");
        } else {
            throw new CommandException("Pagina nummer buiten bereik van pdf");
        }
    }

    /**
     * Close pdf.
     *
     * @param pdf the pdf
     * @throws IOException the io exception
     */
    public static void closePDF(PDDocument pdf) throws IOException {
        pdf.close();
    }

    /**
     * Pdf text present on a page boolean.
     *
     * @param pdf               the pdf
     * @param paginaNr          the pagina nr
     * @param teVerwachtenTekst the te verwachten tekst
     * @return the boolean
     * @throws Exception the exception
     */
    public static boolean pdfTextPresentOnAPage(PDDocument pdf, int paginaNr, String teVerwachtenTekst) throws Exception {
        return giveTextFromPage(pdf, paginaNr).contains(teVerwachtenTekst);
    }

    /**
     * Count times present int.
     *
     * @param pdf                              the pdf
     * @param teZoekenTekstDatGeteldMoetWorden the te zoeken tekst dat geteld moet worden
     * @return the int
     * @throws Exception the exception
     */
    public static int countTimesPresent(PDDocument pdf, String teZoekenTekstDatGeteldMoetWorden) throws Exception {
        String pdfTekst = pdfText(pdf, false);

        if (teZoekenTekstDatGeteldMoetWorden == null || teZoekenTekstDatGeteldMoetWorden.length() == 0) {
            throw new CommandException("te zoeken tekst heeft geen waarde");
        }

        int aantal = 0;
        int index = 0;
        while (index < pdfTekst.length() && ((index = pdfTekst.indexOf(teZoekenTekstDatGeteldMoetWorden, index)) >= 0)) {
            aantal++;
            index += teZoekenTekstDatGeteldMoetWorden.length();
        }
        return aantal;

    }

}
