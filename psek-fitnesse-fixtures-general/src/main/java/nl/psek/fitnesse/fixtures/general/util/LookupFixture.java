package nl.psek.fitnesse.fixtures.general.util;

import java.util.HashMap;
import java.util.Map;

/**
 * A fixture to give FitNesse some form of conditional execution, useful in generic scenario's.
 *
 *     |scenario   |use button and check message _|button                   |
 *     |push fixture                                                        |
 *     |start      |Lookup Fixture                                          |
 *     |addEntry;  |continue                      |Continue to the next page|
 *     |addEntry;  |abort                         |Abort action and return  |
 *     |$message=  |findEntry;                    |@button                  |
 *     |pop fixture                                                         |
 *     |click;     |id=@button                                              |
 *     |verifyText;|id=message                    |$message                 |
 *
 *      !|script                                                             |
 *      |note                         |navigate to a webpage with the buttons|
 *      |use button and check message;|abort                                 |
 *      |note                         |navigate back to the webpage          |
 *      |use button and check message;|continue                              |
 *
 * Created by Pascal on 20-11-2015.
 */
public class LookupFixture {

    private Map<String, String> entries = new HashMap<>();
    private String defaultvalue = null;


    /**
     * Instantiates a new Lookup fixture.
     */
    public LookupFixture() {
        //default constructor
    }

    /**
     * Specifies the default that will be returned if the specified key in findEntry(key) is not found
     *
     * @param value the value
     */
    public void setDefaultIfNotFound(String value) {
        defaultvalue = value;
    }


    /**
     * Add entry.
     *
     * @param label the label
     * @param value the value
     */
    public void addEntry(String label, String value) {
        entries.put(label, value);
    }

    /**
     * Find entry string.
     *
     * @param label the label
     * @return the string
     */
    public String findEntry(String label) {
        if (entries.get(label) != null) {
            return entries.get(label);
        } else if (defaultvalue != null) {
            return defaultvalue;
        } else {
            return entries.get(label);
        }
    }


    /**
     * Clear entries.
     */
    public void clearEntries() {
        entries.clear();
        defaultvalue = null;
    }

}
