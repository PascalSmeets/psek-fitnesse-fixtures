package nl.psek.fitnesse.fixtures.general.pdf;

import nl.psek.fitnesse.ConditionalException;
import nl.psek.fitnesse.fixtures.general.util.DatabaseUtils;
import nl.psek.fitnesse.fixtures.general.util.FileUtils;
import nl.psek.fitnesse.fixtures.general.util.PDFUtils;
import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Base64;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;

/**
 * A fixture that can read a PDF file (local or from a url) and extract text from it.
 */
public class PdfTextReaderFixture {

    private PDDocument pdf = null;
    private static final String ERROR_LOADING_DOCUMENT = "Error loading PDF document '%s', the following exception occurred '%s'";


    /**
     * Instantiates a new Pdf text reader fixture.
     */
    public PdfTextReaderFixture() {
        //default constructor
    }


    /**
     * Load pdf from file boolean.
     *
     * @param file the file
     * @return the boolean
     */
    public boolean loadPdfFromFile(String file) {
        try {
            pdf = PDFUtils.loadPDF(FileUtils.file(file));
            return pdf != null;

        } catch (IOException e) {
            throw new ConditionalException(ERROR_LOADING_DOCUMENT, file, e.getMessage());
        }
    }


    /**
     * Load pdf from url boolean.
     *
     * @param url the url
     * @return the boolean
     */
    public boolean loadPdfFromUrl(String url) {
        try {
            pdf = PDFUtils.loadPdf(new URL(url));
            return pdf != null;
        } catch (IOException e) {
            throw new ConditionalException(ERROR_LOADING_DOCUMENT, url, e.getMessage());
        }
    }


    /**
     * Load pdf from url boolean.
     *
     * @param url      the url
     * @param username the username
     * @param password the password
     * @return the boolean
     */
	public boolean loadPdfFromUrl(String url, final String username, String password) {

		final String pwd = getFromPropertyIfApplicable(password);

        Authenticator auth = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
				return (new PasswordAuthentication(username, pwd.toCharArray()));
            }
        };
        Authenticator.setDefault(auth);
        return loadPdfFromUrl(url);
    }


    /**
     * Load pdf from stream boolean.
     *
     * @param is the is
     * @return the boolean
     */
    public boolean loadPdfFromStream(InputStream is) {
        try {
            pdf = PDFUtils.loadPdfFromStream(is);
            return pdf != null;

        } catch (IOException e) {
            throw new ConditionalException("Error loading PDF document from inputstream, the following exception occurred '%s'", e.getMessage());
        }
    }


    /**
     * Load pdf from base 64 string boolean.
     *
     * @param base64Encoded the base 64 encoded
     * @return the boolean
     */
    public boolean loadPdfFromBase64String(String base64Encoded) {
        byte[] decoded = Base64.getDecoder().decode(base64Encoded);
        InputStream stream = new ByteArrayInputStream(decoded);
        loadPdfFromStream(stream);
        return pdf != null;
    }


    /**
     * Load pdf from database boolean.
     *
	 * @param databaseDriver the database driver
	 * @param url            the URL to connect to, provided als plain text or with prefix 'p:' with a string that points to a System.property
	 * @param username       the username, provided als plain text or with prefix 'p:' with a string that points to a System.property
	 * @param password       the password , provided als plain text or with prefix 'p:' with a string that points to a System.property
     * @param sql        the sql
     * @return the boolean
     */
	public boolean loadPdfFromDatabase(String databaseDriver, String url, String username, String password, String sql) {
        try {
            DatabaseUtils connection = new DatabaseUtils();
			connection.createConnection("mydb", databaseDriver, url,username,password);
            pdf = PDFUtils.loadPdfFromStream(connection.getBlobAsStream("mydb", sql));
            connection.closeConnection("mydb");
            return pdf != null;
        } catch (IOException e) {
			throw new ConditionalException(ERROR_LOADING_DOCUMENT, e.getMessage());
        }
    }


    /**
     * Close pdf.
     */
    public void closePdf() {
        try {
            PDFUtils.closePDF(pdf);

        } catch (IOException e) {
            throw new ConditionalException("Error closing PDF document, the following exception occurred '%s'", e.getMessage());
        }
    }


    /**
     * Pdf to string string.
     *
     * @param sortByPosition the sort by position
     * @return the string
     */
    public String pdfToString(boolean sortByPosition) {
        try {
            return PDFUtils.pdfText(pdf, sortByPosition);

        } catch (IOException e) {
            throw new ConditionalException("Error converting PDF to plain text '%s'", e.getMessage());
        }
    }


    /**
     * Pdf to text lines string [ ].
     *
     * @return the string [ ]
     */
    public String[] pdfToTextLines() {
        String[] result = null;
        try {
            result = PDFUtils.pdfTextLines(pdf);
        } catch (Exception e) {
            throw new ConditionalException("Error converting PDF to plain text lines '%s'", e.getMessage());
        }
        return result;
    }


    /**
     * Pdf contains text line boolean.
     *
     * @param text the text
     * @return the boolean
     */
    public boolean pdfContainsTextLine(String text) {
        try {
            return PDFUtils.pdfContainsLine(pdf, text);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of the following line of text '%s', the following exception occurred '%s'", text, e.getMessage());
        }
    }


    /**
     * Pdf contains text line case insensitive boolean.
     *
     * @param text the text
     * @return the boolean
     */
    public boolean pdfContainsTextLineCaseInsensitive(String text) {
        try {
            return PDFUtils.pdfContainsLineCaseInsensitive(pdf, text);

        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of the following line (case insensitive) of text '%s', the following exception occurred '%s'", text, e.getMessage());
        }
    }


    /**
     * Pdf contains text line with prefix boolean.
     *
     * @param prefix the prefix
     * @return the boolean
     */
    public boolean pdfContainsTextLineWithPrefix(String prefix) {
        try {
            return PDFUtils.pdfLineStartsWith(pdf, prefix);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of a line with the prefix '%s', the following exception occurred '%s'", prefix, e.getMessage());
        }
    }

    /**
     * Controleert of een regel met de aangeleverde eindigt
     *
     * @param suffix the suffix
     * @return the boolean
     */
    public boolean pdfContainsTextLineWithSuffix(String suffix) {
        try {
            return PDFUtils.pdfLineEndsOn(pdf, suffix);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of a line with the suffix '%s', the following exception occurred '%s'", suffix, e.getMessage());
        }
    }


    /**
     * Pdf contains text line that contains text boolean.
     *
     * @param text the text
     * @return the boolean
     */
    public boolean pdfContainsTextLineThatContainsText(String text) {
        try {
            return PDFUtils.pdfLineContains(pdf, text);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of a line that contains the text '%s', the following exception occurred '%s'", text, e.getMessage());
        }
    }


    /**
     * Pdf contains text between on page boolean.
     *
     * @param page         the page
     * @param fromText     the from text
     * @param toText       the to text
     * @param expectedText the expected text
     * @return the boolean
     */
    public boolean pdfContainsTextBetweenOnPage(int page, String fromText, String toText, String expectedText) {
        try {
            return PDFUtils.pdfContainsTextBetweenTwoTextsOnOnePage(pdf, page, fromText, toText, expectedText);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of the text '%s' on page '%s' between '%s' and '%s', the following exception occurred '%s'", expectedText, page, fromText, toText, e.getMessage());
        }
    }


    /**
     * Pdf contains text between on page inside block boolean.
     *
     * @param page            the page
     * @param fromText        the from text
     * @param fromTextBlockNr the from text block nr
     * @param toText          the to text
     * @param expectedText    the expected text
     * @return the boolean
     */
    public boolean pdfContainsTextBetweenOnPageInsideBlock(int page, String fromText, int fromTextBlockNr, String toText, String expectedText) {
        try {
            return PDFUtils.pdfContainsTextBetweenTwoTextsInOneBlockOnOnePage(pdf, page, fromText, fromTextBlockNr, toText, expectedText);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of the text '%s' on page '%s' between '%s' from block '%s' and '%s', the following exception occurred '%s'", expectedText, page, fromText, fromTextBlockNr, toText, e.getMessage());
        }
    }


    /**
     * Pdf not contains text between on page boolean.
     *
     * @param page           the page
     * @param fromText       the from text
     * @param toText         the to text
     * @param unexpectedText the unexpected text
     * @return the boolean
     */
    public boolean pdfNotContainsTextBetweenOnPage(int page, String fromText, String toText, String unexpectedText) {
        try {
            return !PDFUtils.pdfContainsTextBetweenTwoTextsOnOnePage(pdf, page, fromText, toText, unexpectedText);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the absence of the text '%s' on page '%s' between '%s' and '%s', the following exception occurred '%s'", unexpectedText, page, fromText, toText, e.getMessage());
        }
    }


    /**
     * Pdf contains text on page boolean.
     *
     * @param page         the page
     * @param expectedText the expected text
     * @return the boolean
     */
    public boolean pdfContainsTextOnPage(int page, String expectedText) {
        try {
            return PDFUtils.pdfTextPresentOnAPage(pdf, page, expectedText);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the existence of the text '%s' on page '%s', the following exception occurred '%s'", expectedText, page, e.getMessage());
        }
    }


    /**
     * Pdf count text presence int.
     *
     * @param expectedText the expected text
     * @return the int
     */
    public int pdfCountTextPresence(String expectedText) {
        try {
            return PDFUtils.countTimesPresent(pdf, expectedText);
        } catch (Exception e) {
            throw new ConditionalException("Error checking PDF for the number of times that the text '%s' is in the document, the following exception occurred '%s'", expectedText, e.getMessage());
        }
    }


    /**
     * Write pdf to file.
     *
     * @param filename the filename
     */
    public void writePdfToFile(String filename) {
        try {

            File file = new File(filename);
            if (!file.exists()) {
                FileUtils.createFile(file);
            }
            this.pdf.save(filename);

        } catch (IOException e) {
            throw new ConditionalException("Error writing PDF to file '%s', the following exception occurred '%s'", e.getMessage());
        }
    }
}
