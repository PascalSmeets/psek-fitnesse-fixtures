package nl.psek.fitnesse.fixtures.general.text;

import nl.psek.fitnesse.CommandException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.containsPatternNoInputLogging;

/**
 * A fixture to read the last n-lines from a textfile,
 * and perform checks on the returned lines,
 * this can be used to perform checks in a logfile.
 *
 * Created by Pascal on 7-12-2015.
 */
public class TextReaderFixture {

    private int linesToRead = 10;
    private int linesInFile;
    private File fileToRead;
    private String fileEncoding = "UTF-8";


    public TextReaderFixture() {
        //default constructor
    }


    public void setLinesToRead(int amount) {
        linesToRead = amount;
    }


    public void setFileToRead(String fileName, String encoding) throws IOException {
        fileToRead = new File(fileName);
        fileEncoding = encoding;
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(fileName))) {
            linesInFile = (int) reader.lines().count();
    }
    }


    public void setFileToRead(String fileName) throws IOException {
        setFileToRead(fileName, fileEncoding);
    }


    public String[] returnLines() {
        return readLastLinesFromfile(linesToRead);
    }


    public String findText(String text) {
        String[] lines = readLastLinesFromfile(linesToRead);
        for (String line : lines) {
            if (containsPatternNoInputLogging(line, text)) {
                return line;
            }
        }
        return "";
    }


    public Boolean isTextPresentOnLine(String text) {
        String[] lines = readLastLinesFromfile(linesToRead);
        boolean found = false;
        for (String line : lines) {
            if (containsPatternNoInputLogging(line, text)) {
                found = true;
            }
        }
        return found;
    }

    private String[] readLastLinesFromfile(int maxlines) {
        try {
            // If a file has less than maxLines the value is reset to the number of lines in the file
            if (linesInFile < maxlines) {
                maxlines = linesInFile;
            }
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileToRead), "UTF-8"))) {

            String[] lines = new String[maxlines];
            int count = 0;
            String line = null;
            while ((line = reader.readLine()) != null) {
                lines[count % lines.length] = line;
                count++;
            }
            return lines;
            }
        } catch (IOException e) {
            throw new CommandException(e.getMessage());
        }
    }

}
