package nl.psek.fitnesse.fixtures.general.json;

import nl.psek.fitnesse.CommandException;
import org.json.JSONObject;

/**
 * A fixture to build a json string in a test, it's preferable to generate a json messsage, but this will do the job is generating is not an option.
 *
 * Created by Pascal Smeets on 02-03-2018.
 */
public class BuildJsonStringFixture {

    private StringBuilder builder;
    private boolean typeIsObject = false;
    private boolean typeIsArray = false;


    public void startNewJsonObjectMessage() {
        typeIsObject = true;
        builder = new StringBuilder();
        builder.append("{");

    }

    public void startNewJsonObjectMessage(String objectName) {
        startNewJsonObjectMessage();
        builder.append("\"" + objectName + "\": {");
    }


    public void startNewJsonArrayMessage() {
        typeIsArray = true;
        builder = new StringBuilder();
        builder.append("[");

    }

    // Add key value pairs

    public void addString(String name, String value) {
        builder.append("\"" + name + "\":\"" + value + "\",");  // needs comma as last character for trimLastComma() to work
    }


    public void addInt(String name, int value) {
        builder.append("\"" + name + "\":" + value + ",");  // needs comma as last character for trimLastComma() to work
    }


    public void addBoolean(String name, String input) {
        String value = "";
        if (input.equalsIgnoreCase("true")) {
            value = "true";
        } else if (input.equalsIgnoreCase("false")) {
            value = "false";
        } else {
            throw new CommandException("The supplied input boolean could not be converted to true or false, you supplied the text: '" + input + "'");
        }
        builder.append("\"" + name + "\":" + value + ",");  // needs comma as last character for trimLastComma() to work
    }

    public void addNull(String name) {
        builder.append("\"" + name + "\": null,");  // needs comma as last character for trimLastComma() to work
    }

    // Json objects

    public void startObject() {
        builder.append("{");
    }


    public void startObject(String objectName) {
        builder.append("\"" + objectName + "\":{");
    }


    public void endObject() {
        trimLastComma();
        builder.append("},");
    }

    public void endObject(String unusedTagOnlyForReferenceInTestCase) {
        //this method is used for logging in testcases,the parameter is only for display onscreen
        endObject();
    }

    //Json arrays

    public void startArray() {
        builder.append("[");
    }

    public void startArray(String arrayName) {
        builder.append("\"" + arrayName + "\":[");
    }


    public void endArray() {
        trimLastComma();
        builder.append("],");
    }

    public void endArray(String unusedTagOnlyForReferenceInTestCase) {
        //this method is used for logging in testcases,the parameter is only for display onscreen
        endArray();
    }

    // build the Json string

    public String generateJson() {
        trimLastComma();
        if (typeIsObject) {
            builder.append("}");
        } else if (typeIsArray) {
            builder.append("]");
        }

        //using JSONObject minimizes the json string (removes unnecessary whitespace)
        return new JSONObject(builder.toString()).toString();
    }

    // helper methods
    private void trimLastComma() {
        if (builder.toString().endsWith(",")) {
            builder.setLength(builder.length() - 1);
        }
    }
}
