package nl.psek.fitnesse.fixtures.general.json;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.PathNotFoundException;
import nl.psek.fitnesse.ConditionalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.containsPattern;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.matchOnPattern;


/**
 * The JsonNavigate Fixture can be used to perform checks on and extract data from Json strings or files.
 * <p>
 * [Jayway JsonPath](https://github.com/jayway/JsonPath) is used in this fixture.
 */
public class JsonNavigateFixture {

    private static final Logger LOG = LoggerFactory.getLogger(nl.psek.fitnesse.fixtures.general.json.JsonNavigateFixture.class);
    Object document = null;
    private boolean logFoundValues = false;


    public JsonNavigateFixture() {
        //default constructor
    }


    /**
     * Loads a json string into a json object that will be used by other methods.
     * Supposes UTF-8 encoding
     *
     * @param jsonString input string
     */
    public void loadJsonFromString(String jsonString) {
        this.document = Configuration.defaultConfiguration().jsonProvider().parse(new ByteArrayInputStream(jsonString.getBytes()), StandardCharsets.UTF_8.name());
    }

    /**
     * Loads a json byte[] into a json object that will be used by other methods.
     * Uses utf-8 encoding
     *
     * @param jsonBytes the byte[] containg the json string
     */
    public void loadJsonFromUTF8ByteArray(byte[] jsonBytes) {
        this.document = Configuration.defaultConfiguration().jsonProvider().parse(new ByteArrayInputStream(jsonBytes), StandardCharsets.UTF_8.name());
        LOG.info(document.toString());

    }

    /**
     * Loads a json file into a json object that will be used by other methods.
     *
     * @param jsonFileName the input file
     * @param encoding     the input file encoding
     */
    public void loadJsonFromFile(String jsonFileName, String encoding) {
        try {
            Path path = Paths.get(jsonFileName);
            byte[] content = Files.readAllBytes(path);

            this.document = Configuration.defaultConfiguration().jsonProvider().parse(new ByteArrayInputStream(content), String.valueOf(Charset.forName(encoding)));
        } catch (IOException e) {
            LOG.info(e.getMessage());
        }
    }

    /**
     * Counts the number of times a jsonpath is present
     *
     * @param path path to a specific element
     * @return number of times the element is present as int
     */
    public int countNumberOfTimesPresent(String path) {
        Configuration conf = Configuration.builder().options(Option.ALWAYS_RETURN_LIST).build();
        List<String> list = JsonPath.using(conf).parse(document).read(path);

        return list.size();
    }

    /**
     * Find the value of a specific JSON element using JsonPath in the specified JSON text string
     * More information https://github.com/jayway/JsonPath
     * You van use a path like:
     * $.store.book[0].title
     * or the bracket–notation
     * $['store']['book'][0]['title']
     *
     * @param path path to a specific element
     * @return the value that was found OR empty string if the found element has value null
     * @throws ConditionalException - StopTestCommandException if the path is not found and if FitnesseSystemSettings.stopTestOnException=true
     * @throws ConditionalException - CommandException  if the path is not found and if FitnesseSystemSettings.stopTestOnException=false
     */
    public Object retrieveValueOfJsonElement(String path) {
        Object result = null;
        try {
            if (!JsonPath.isPathDefinite(path)) {
                LOG.info("JsonPath has multiple possible results, returning the whole set.");
            }
            result = JsonPath.read(document, path);
            result = cleanResult(result.toString());

            if (logFoundValues){
            LOG.info("Found value for path '" + path + "' is: " + result.toString());
            }

            return result;
        } catch (NullPointerException e) {
            LOG.info("Caught NullPointerException, this is occurring because the value of the element that was read is 'null'. If the path was not found a PathNotFoundException would occur.");
            LOG.info("Returning an empty string to FitNesse for path: " + path);
            return "";

        } catch (PathNotFoundException e) {
            throw new ConditionalException("Het json element '" + path + "' is niet gevonden. [PathNotFoundException]");
        }
    }

    /**
     * Retrieves the value of a json string or boolean element, if the element is not present this returns an empty string.
     *
     * @param path jsonpath to search for
     * @return empty string is the element is not present
     */
    public Object retrieveValueOfJsonElementOptional(String path) {
        try {
            return retrieveValueOfJsonElement(path);
        } catch (Exception e) {
            //json element was niet gevonden, in deze optionele methode gaat dat goed, geeft een lege string terug
            return "";
        }
    }


    /**
     * Retrieves the value of a json string or boolean element, if the element is not present this returns an empty string.
     *
     * @param path jsonpath to search for
     * @return found value if the element is present
     * @throws ConditionalException - StopTestCommandException if the path is not found and if FitnesseSystemSettings.stopTestOnException=true
     * @throws ConditionalException - CommandException  if the path is not found and if FitnesseSystemSettings.stopTestOnException=false
     */
    public Integer retrieveValueOfJsonIntElement(String path) {
        Integer result = null;
        try {
            if (!JsonPath.isPathDefinite(path)) {
                LOG.info("JsonPath has multiple possible results, returning the whole set.");
            }
            result = JsonPath.read(document, path);
            if(logFoundValues) {
            LOG.info("Found value for path '" + path + "' is: " + result);
            }
        } catch (PathNotFoundException e) {
            throw new ConditionalException("Het json element '" + path + "' is niet gevonden. [PathNotFoundException]");
        }
        return result;
    }


    /**
     * Check if the value of a specific JSON element is equal to the regex pattern
     * uses retrieveValueOfJsonElement
     *
     * @param path    jsonpath to search for
     * @param pattern expected regex pattern that is present at path
     * @return true or when false:
     * @throws ConditionalException with the found value is not equal to the expected value
     */
    public Boolean checkValueOfJsonElement(String path, String pattern) {
        Object foundValue = retrieveValueOfJsonElement(path);
        if (matchOnPattern(foundValue.toString(), pattern)) {
            return true;
        }
        throw new ConditionalException(" foundValue = '%s', but expectedValue was '%s'", foundValue, pattern);
    }

    /**
     * Check if the value of a specific JSON element is equal to the regex pattern value
     * uses retrieveValueOfJsonElement
     *
     * @param path    path to a specific element
     * @param pattern expected regex pattern that is present at path
     * @return true or false
     */
    public Boolean verifyValueOfJsonElement(String path, String pattern) {
        return matchOnPattern(retrieveValueOfJsonElement(path).toString(), pattern);

    }

    /**
     * Verifies that the jsonpath result contains a specific regex pattern.
     *
     * @param path    jsonpath to search for
     * @param pattern expected regex pattern that is present at path
     * @return true or false
     */
    public boolean verifyJsonElementContainsString(String path, String pattern) {
        boolean result = false;
        try {
            result = containsPattern(retrieveValueOfJsonElement(path).toString(), pattern);
        } catch (PathNotFoundException e) {
            throw new ConditionalException(e.getMessage());
        }
        return result;
    }


    /**
     * Controleer of het aangegeven pad aanwezig is in het JSON object.
     * Als het pad niet aanwezig is wordt er een PathNotFoundException teruggegeven die we afvangen.
     * Als er gezocht wordt met een indefinite pad dan komt er ALTIJD een list terug, dat ziet er zo uit[]
     * Als er gezocht wordt met een indefinite pad en er komt een list terug met een lege string erin ziet dat er zo uit [""]
     * <p>
     * Als er dus gezocht wordt met een indefinite pad en er wordt niets gevonden komt er een lege list terug []. Op deze lege list filteren we, daarvoor geven we false terug.
     * <p>
     * https://github.com/jayway/JsonPath
     * A path is indefinite if it contains:
     * <p>
     * {@code
     * .. - a deep scan operator
     * ?(<expression>) - an expression
     * [<number>, <number> (, <number>)] - multiple array indexes
     * }
     *
     * @param path jsonpath to search for
     * @return true of false
     */
    public Boolean verifyJsonElementPresent(String path) {
        try {
            String result = JsonPath.read(document, path).toString();

            if (isTheJsonPathResultAnEmptyList(result)) {
                LOG.info("Er is een response van JsonPath, maar die was: " + result + ". Dat is de default bij een niet aanwezig element als er gezocht wordt met een indefinite pad. Het element '" + path + "' is dus niet aanwezig.");
                return false;
            }
            return true;                        // als er geen PathNotFoundException optreed dan is het pad dus gevonden
        } catch (NullPointerException e) {
            LOG.info("Caught NullPointerException, this is occurring because the value of the element that was read is 'null'. If the path was not found a PathNotFoundException would occur.");
            LOG.info("Returning true for path: " + path);
            return true;

        } catch (PathNotFoundException e) {
            LOG.info("Er is een PathNotFoundException opgetreden omdat het element '" + path + "' niet gevonden is.");
            return false;
        }
    }


    /**
     * Verifies that a json element is not present.
     * Reverse of verifyJsonElementPresent
     *
     * @param path jsonpath to search for
     * @return true or false
     */
    public Boolean verifyJsonElementNotPresent(String path) {
        return !verifyJsonElementPresent(path);
    }

    /**
     * Checks that the result is an empty list
     * if the input matches [] the result is true
     *
     * @param result string input
     * @return true or false
     */
    private Boolean isTheJsonPathResultAnEmptyList(String result) {
        return result.equalsIgnoreCase("[]");
    }

    /**
     * Cleans the returned result, if it is contained in brackets and doublequotes ["RESULT"] OR in brackets [RESULTS] these will be removed
     *
     * @param input result string, parsed json response
     * @return cleaned string
     */
    private String cleanResult(String input) {

        LOG.debug("Input result = " + input);
        if (input.startsWith("[\"") && input.endsWith("\"]")) {

            LOG.debug("CLEANUP RESULT: result is contained in brackets and double quotes, removing both.");
            input = input.substring(2);
            input = input.substring(0, (input.length() - 2));

        } else if (input.startsWith("[") && input.endsWith("]")) {

            LOG.debug("CLEANUP RESULT: result is contained in brackets, removing brackets.");
            input = input.substring(1);
            input = input.substring(0, (input.length() - 1));
        } else {
            LOG.debug("CLEANUP RESULT: result is not contained in brackets OR brackets and double quotes, return result as-is");
        }

        LOG.debug("Parsed result = " + input);
        return input;
    }

    /**
     * Enables the logging of the values found by retrieveValueOfJsonElement and retrieveValueOfJsonIntElement to the console, default = no logging
     */
    public void enableLoggingOfFoundValues(){
        logFoundValues = true;
    }

    /**
     * Disables the logging of the values found by retrieveValueOfJsonElement and retrieveValueOfJsonIntElement to the console, default = no logging
     */
    public void disableLoggingOfFoundValues(){
        logFoundValues = false;
    }

}
