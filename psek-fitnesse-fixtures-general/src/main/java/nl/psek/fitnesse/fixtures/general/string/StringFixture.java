package nl.psek.fitnesse.fixtures.general.string;

import nl.psek.fitnesse.CommandException;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.containsPatternNoInputLogging;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.findOnRegexAndReturnResult;

/**
 * A fixture that can be used to verify and manipulate strings.
 *
 * @author Ronald Mathies
 */
public class StringFixture {

    private StringBuilder builder = new StringBuilder();

    /**
     * Constructs a new String Fixture with the initial value set.
     *
     * @param value the initial value to work with.
     */
    public StringFixture(String value) {
        builder.append(value);
    }

    /**
     * appends a string.
     *
     * @param value the value
     */
    public void appendValue(String value) {
        if (value != null && !value.isEmpty()) {
            builder.append(value);
        }
    }

    /**
     * Clears the buffer completely.
     *
     * @return the boolean
     */
    public boolean clearBuffer() {
        builder = new StringBuilder();
        return true;
    }

    /**
     * Returns the value.
     *
     * @return the string
     */
    public String result() {
        return builder.toString();
    }

    /**
     * Replaces the pattern with the replacement in the buffer.
     *
     * @param pattern     the pattern to look for.
     * @param replacement the replacement value.
     */
    public void replaceAll(String pattern, String replacement) {
        String value = builder.toString();
        builder = new StringBuilder(value.replaceAll(pattern, replacement));
    }

    /**
     * Verify string contains boolean.
     *
     * @param pattern the pattern
     * @return the boolean
     */
    public boolean verifyStringContains(String pattern) {
        return containsPatternNoInputLogging(builder.toString(), pattern);
    }


    /**
     * Verify string does not contain boolean.
     *
     * @param pattern the pattern
     * @return the boolean
     */
    public boolean verifyStringDoesNotContain(String pattern) {
        return !verifyStringContains(pattern);
    }

    /**
     * convert a string to lowercase
     *
     * @return lowercase string
     */
    public String resultToLowerCase() {
        return builder.toString().toLowerCase();
    }

    /**
     * convert a string to uppercase
     *
     * @return uppercase string
     */
    public String resultToUpperCase() {
        return builder.toString().toUpperCase();
    }


    /**
     * Gets substring with regex.
     *
     * @param regex the regex
     * @return the substring with regex
     */
    public String getSubstringWithRegex(String regex) {
        return findOnRegexAndReturnResult(result(), regex);
    }

    /**
     * Returns a new string that is a substring of this string.
     *
     * @param start the start index.
     * @param end   the end index.
     * @return the string
     */
    public String substring(String start, String end) {
        String value = builder.toString();

        int beginIndex;
        int endIndex;

        try {
            beginIndex = Integer.parseInt(start);
        } catch (NumberFormatException nfe) {
            throw new CommandException("The start value is not a numeric value.");
        }

        try {
            endIndex = Integer.parseInt(end);
        } catch (NumberFormatException nfe) {
            throw new CommandException("The end value is not a numeric value.");
        }

        if (beginIndex < 0) {
            throw new CommandException("De start '%s' can not be a negative value.", start);
        }
        if (endIndex > value.length()) {
            throw new CommandException("The end '%s' cannot be larger then the length '%d' of the value.", end, value.length());
        }
        int subLen = endIndex - beginIndex;
        if (subLen < 0) {
            throw new CommandException("The end '%s' cannot be before the start '%s'", end, start);
        }

        return value.substring(beginIndex, endIndex);
    }


    /**
     * Substring from string.
     *
     * @param start the start
     * @return the string
     */
    public String substringFrom(String start) {
        return substring(start, String.valueOf(builder.length()));
    }


    /**
     * Substring to string.
     *
     * @param end the end
     * @return the string
     */
    public String substringTo(String end) {
        return substring(String.valueOf(0), end);
    }

}
