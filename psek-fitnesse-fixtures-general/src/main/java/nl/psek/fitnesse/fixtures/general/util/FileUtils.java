package nl.psek.fitnesse.fixtures.general.util;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
import nl.psek.fitnesse.CommandException;
import nl.psek.fitnesse.StopTestCommandException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

import static java.nio.file.Files.createTempDirectory;
import static nl.psek.fitnesse.fixtures.util.FileFinder.findFileInUserDirectory;
import static nl.psek.fitnesse.fixtures.util.FixtureOperations.getFromPropertyIfApplicable;

/**
 * A fixture that can be used to generate a (temporary) file, write and read to files and copy files (including over SMB)
 */
public class FileUtils {

    private static Log logger = LogFactory.getLog(nl.psek.fitnesse.fixtures.general.util.FileUtils.class);
    /**
     * The constant LF.
     */
    public static final char LF = '\n';
    /**
     * The constant CR.
     */
    public static final char CR = '\r';

    /**
     * The Temp dir with prefix.
     */
    static Path tempDirWithPrefix;

    /**
     * Default constructor
     */
    public FileUtils() {
        //Default constructora
    }

    /**
     * File file.
     *
     * @param filename the filename
     * @return the file
     */
    public static File file(String filename) {
        return new File(filename);
    }

    /**
     * Creates a temporary UTF-8 file
     *
     * @param filenamePrefix filename part added to the beginning of the name of the temporary file
     * @param filenameSuffix filename part added to the end of the name of the temporary file
     * @param filecontents   String content to be written as a file
     * @return the complete file path as a string
     * @throws StopTestCommandException if an IOException occurs
     */
    public static String createTempFile(String filenamePrefix, String filenameSuffix, String filecontents) {
        if(!filenameSuffix.startsWith(".")){
            filenameSuffix = "." + filenameSuffix;
        }
        Path tempFile = null;
        try {
            tempDirWithPrefix = createTempDirectory("fileutiltemp");
            tempFile = Files.createTempFile(tempDirWithPrefix, filenamePrefix, filenameSuffix);
        } catch (IOException e) {
            throw new StopTestCommandException("Error creating tempfile: " + e.getMessage());
        }
        Charset charset = Charset.forName("UTF-8");

        try {
            BufferedWriter writer = Files.newBufferedWriter(tempFile, charset);
            writer.write(filecontents);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new StopTestCommandException("Writing to tempfile error: " + e.getMessage());
        }
        return tempFile.toAbsolutePath().toString();
    }

    /**
     * Creates a temporary binary file
     *
     * @param filenamePrefix filename part added to the beginning of the name of the temporary file
     * @param filenameSuffix filename part added to the end of the name of the temporary file
     * @param filecontents   byte[] content to be written as a file
     * @return the complete file path as a string
     * @throws StopTestCommandException if an IOException occurs
     */
    public static String createBinaryTempFile(String filenamePrefix, String filenameSuffix, byte[] filecontents){
        if(!filenameSuffix.startsWith(".")){
            filenameSuffix = "." + filenameSuffix;
        }
        Path tempFile = null;
        try {
            tempDirWithPrefix = createTempDirectory("fileutiltemp");
            tempFile = Files.createTempFile(tempDirWithPrefix, filenamePrefix, filenameSuffix);
        } catch (IOException e) {
            throw new StopTestCommandException("Error creating tempfile: " + e.getMessage());
        }

        try {
            Files.write(tempFile, filecontents);
        } catch (IOException e) {
            throw new StopTestCommandException("Writing to tempfile error: " + e.getMessage());
        }
        return tempFile.toAbsolutePath().toString();
    }

    /**
     * Verifies that a file exists
     *
     * @param location the location
     * @return the boolean
     */
    public boolean verifyFileExists(String location) {
        Path path = Paths.get(location);
        return path.toFile().exists();
    }

    /**
     * Verify smb file exists boolean.
     *
     * @param location       the location
     * @param domain         the domain
     * @param domainUser     the domain user
     * @param domainPassword the domain password
     * @return the boolean
     */
    public boolean verifySMBFileExists(String location, String domain, String domainUser, String domainPassword) {
        if (!location.startsWith("smb:")) {
            location = "smb:" + location;
        }
        try {
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(getFromPropertyIfApplicable(domain), getFromPropertyIfApplicable(domainUser), getFromPropertyIfApplicable(domainPassword));
            SmbFile sFile = new SmbFile(location, auth);
            return sFile.exists();
        } catch (IOException e) {
            throw new CommandException("Something went wrong verify the file " + e.getMessage());
        }
    }

    /**
     * Gets file size.
     *
     * @param location the location
     * @return the file size
     */
    public long getFileSize(String location) {
        Path path = Paths.get(location);
        try {
            return Files.size(path);
        } catch (IOException e) {
            throw new CommandException("File was not found " + e.getMessage());
        }
    }

    /**
     * Copies a file from source to target
     *
     * @param sourceFile      the source file
     * @param destinationFile the destination file
     */
    public void copyFile(String sourceFile, String destinationFile) {
        Path source = Paths.get(sourceFile);
        Path target = Paths.get(destinationFile);

        try {
            Files.copy(source, new FileOutputStream(target.toFile()));
        } catch (IOException e) {
            throw new CommandException("Something went wrong copying the file " + e.getMessage());
        }
    }

    /**
     * Copy file to smb share.
     *
     * @param sourceFile      the source file
     * @param destinationFile the destination file
     * @param domain          the domain
     * @param domainUsername  the domain username
     * @param domainPassword  the domain password
     */
    public void copyFileToSMBShare(String sourceFile, String destinationFile, String domain, String domainUsername, String domainPassword) {
        Path source = Paths.get(sourceFile);

        if (!destinationFile.startsWith("smb:")) {
            destinationFile = "smb:" + destinationFile;
        }

        try {
            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(getFromPropertyIfApplicable(domain), getFromPropertyIfApplicable(domainUsername), getFromPropertyIfApplicable(domainPassword));
            SmbFile sFile = new SmbFile(destinationFile, auth);
            SmbFileOutputStream sfos = new SmbFileOutputStream(sFile);
            Files.copy(source, sfos);
            sfos.close();
        } catch (IOException e) {
            throw new CommandException("Something went wrong copying the file " + e.getMessage());
        }
    }

    /**
     * Ik lees zoek een bestand op het filesysteem en probeer
     * deze als tekstbestand te lezen. Vervolgens geef ik de
     * gelezen tekst als String terug.
     * <p>
     * Ik verwijder de laatste line separator uit de teruggegeven string
     *
     * @param filename the filename
     * @return string string
     * @throws IOException the io exception
     */
    public static String readTextFile(String filename) throws IOException {
        Path path = Paths.get(filename);

        String content = "";
        try {
            content = new String(Files.readAllBytes(path));
        } catch (IOException ex) {
            throw new StopTestCommandException(ex.getMessage());
        }

        return chomp(content);
    }

    /**
     * Ik lees zoek een bestand op het filesysteem en probeer
     * deze als tekstbestand met UTF-8 encoding te lezen. Vervolgens geef ik de
     * gelezen tekst als String terug.
     * <p>
     * Ik verwijder de laatste line separator uit de teruggegeven string
     *
     * @param filename the filename
     * @return string string
     * @throws IOException the io exception
     */
    public static String readTextFileAsUtf8(String filename) throws IOException {
        Path path = Paths.get(filename);

        String content = "";
        try {
            content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            throw new StopTestCommandException(ex.getMessage());
        }

        return chomp(content);
    }

    /**
     * Write line to text file.
     *
     * @param filename         the filename
     * @param textLineToAppend the text line to append
     * @throws IOException the io exception
     */
    public static void writeLineToTextFile(String filename, String textLineToAppend) throws IOException {
        writeLineToTextFile(filename, textLineToAppend, true);
    }

    /**
     * Write line to text file.
     *
     * @param filename         the filename
     * @param textLineToAppend the text line to append
     * @param append           boolean toggle to append to the file (append=true) or to overwrite (append=false)
     */
    public static void writeLineToTextFile(String filename, String textLineToAppend, boolean append) {
        Path path = Paths.get(filename);

        if (append) {
            try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.APPEND)) {
				writer.newLine();
                writer.write(textLineToAppend);
            } catch (IOException e) {
                throw new StopTestCommandException(e.getMessage());
            }
        } else {

            try (BufferedWriter writer = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING)) {
				writer.newLine();
                writer.write(textLineToAppend);
            } catch (IOException e) {
                throw new StopTestCommandException(e.getMessage());
            }

        }
    }

    /**
     * Read lines in file list.
     *
     * @param filename the filename
     * @return the list
     * @throws IOException the io exception
     */
    public static List<String> readLinesInFile(String filename) throws IOException {
        ArrayList<String> lineList = new ArrayList<>();
        File file = new File(filename);
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                lineList.add(currentLine);
            }
        }
        return lineList;
    }

    /**
     * Stream sources stream source [ ].
     *
     * @param bestandsNamen the bestands namen
     * @return the stream source [ ]
     * @throws IOException the io exception
     */
    public static StreamSource[] streamSources(String[] bestandsNamen) throws IOException {
        StreamSource[] sources = new StreamSource[bestandsNamen.length];
        for (int x = 0; x < bestandsNamen.length; x++) {
            File f = new File(bestandsNamen[x]);
            if (f.exists()) {
                sources[x] = new StreamSource(new File(bestandsNamen[x]));
            } else {
                throw new IOException("File met naam " + bestandsNamen[x] + " niet gevonden");
            }
        }
        return sources;
    }

    /**
     * Files in folder list.
     *
     * @param dir              the dir
     * @param regexFilePattern the regex file pattern
     * @return the list
     * @throws IOException the io exception
     */
    public static List<File> filesInFolder(File dir, String regexFilePattern) throws IOException {
        if (dir.exists()) {
            DirFilenameFilter filter = new DirFilenameFilter(regexFilePattern);
            return Arrays.asList(dir.listFiles(filter));
        } else {
            throw new IOException("Ongeldige directory");
        }
    }

    /**
     * Files in folder list.
     *
     * @param directory        the directory
     * @param regexFilePattern the regex file pattern
     * @return the list
     * @throws IOException the io exception
     */
    public static List<File> filesInFolder(String directory, String regexFilePattern) throws IOException {
        File dir = new File(directory);
        return filesInFolder(dir, regexFilePattern);
    }

    /**
     * Ik ben een {@link FilenameFilter} die op basis van
     * een bestandsnaam pattern kan gebruikt worden.
     *
     */
    public static class DirFilenameFilter implements FilenameFilter {

        private String pattern;

        /**
         * Instantiates a new Dir filename filter.
         *
         * @param pattern the pattern
         */
        public DirFilenameFilter(String pattern) {
            this.pattern = pattern;
        }

        public boolean accept(File dir, String name) {
            return name.matches(pattern);
        }

    }

    /**
     * Ik kopieer een bestand van bron naar bestemming.
     *
     * @param sourceFile      the source file
     * @param destinationFile the destination file
     * @return boolean boolean
     * @throws IOException the io exception
     */
    public static boolean copyFileTo(File sourceFile, File destinationFile) throws IOException {
        logger.info("Kopieren bestand: bron [" + sourceFile.getAbsolutePath() + "] doel [" + destinationFile.getAbsolutePath() + "]");
        try (FileInputStream fis = new FileInputStream(sourceFile)) {
            createFile(destinationFile);
            return writeStreamToFile(fis, destinationFile);
        }
    }

    /**
     * Write stream to file boolean.
     *
     * @param inputStream the input stream
     * @param file        the file
     * @return the boolean
     * @throws IOException the io exception
     */
    public static boolean writeStreamToFile(InputStream inputStream, File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file, false)) {
            byte[] buffer = new byte[256];
            while (inputStream.read(buffer) != -1) {
                fos.write(buffer);
            }
            inputStream.close();
            fos.flush();
        }
        return file.isFile();
    }

    /**
     * Ik maak een nieuw bestand aan als deze nog niet bestaat.
     *
     * @param file the file
     * @throws IOException the io exception
     */
    public static void createFile(File file) throws IOException {
        if (!file.exists()) {
            File parent = file.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            file.createNewFile();
        }
    }

    /**
     * Delete een file als deze bestaat
     *
     * @param filename the filename
     */
    public void deleteFile(String filename) {
        Path path = Paths.get(filename);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new CommandException(e.getMessage());
        }
    }

    /**
     * Gets date part.
     *
     * @param date         the date
     * @param calendarPart the calendar part
     * @return the date part
     */
    public static int getDatePart(Date date, int calendarPart) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        return cal.get(calendarPart);
    }

    /**
     * Ik stel een array van {@code java.io.File} samen met bestanden in de opgegeven
     * directory. Als er een deepSearch wordt gedaan doe ik dit recursief voor
     * subdirectories. De totale platgeslagen array met File's geef ik vervolgens
     * mee aan de {@link #fileSearch(File[], FileEvaluator) fileSearch()} methode, en geef het resultaat daarvan weer terug.
     *
     * @param directory     the directory
     * @param deepSearch    the deep search
     * @param fileEvaluator the file evaluator
     * @param filter        the filter
     * @return list list
     * @throws IOException the io exception
     */
    public static List<File> fileSearch(File directory, boolean deepSearch, FileEvaluator
            fileEvaluator, FilenameFilter filter) throws IOException {
        if (directory.isDirectory()) {
            File[] files = null;
            if (deepSearch) {
                files = recursiveDirectoryList(directory, filter);
            } else {
                files = directory.listFiles(filter);
            }
            return fileSearch(files, fileEvaluator);
        } else {
            throw new IOException("Invalid or nonexisting directory");
        }
    }

    private static File[] recursiveDirectoryList(File dir, FilenameFilter filter) throws IOException {
        ArrayList<File> cache = new ArrayList<>();
        for (File f : dir.listFiles(filter)) {
            if (f.isDirectory()) {
                recursiveDirectoryList(f, filter);
            } else {
                cache.add(f);
            }
        }
        return cache.toArray(new File[cache.size()]);
    }

    /**
     * Ik zoek een {@code File} in een {@code File[]} array. Dit
     * doe ik door de meegeleverde {@link FileEvaluator} te
     * voorzien van een {@link File} uit de array en vervolgens
     * de {@link FileEvaluator#valid() valid()} methode op de evaluator aan te roepen.
     * vervolgens met zoeken.
     *
     * @param fileArray     the file array
     * @param fileEvaluator the file evaluator
     * @return list list
     * @throws IOException the io exception
     */
    public static List<File> fileSearch(File[] fileArray, FileEvaluator fileEvaluator) throws IOException {
        logger.info("File search within " + fileArray.length + " files. There are " + fileEvaluator.getCriteria().size() + " criteria loaded.");
        ArrayList<File> foundFiles = new ArrayList<>();

        for (File f : fileArray) {
            fileEvaluator.setFile(f);
            if (fileEvaluator.valid()) {
                foundFiles.add(f);
            }
        }
        return foundFiles;
    }

    /**
     * Read file string.
     *
     * @param fileName the file name
     * @param encoding the encoding
     * @return the string
     * @throws IOException the io exception
     */
    public static String readFile(final String fileName, final String encoding) throws IOException {
        final Path path = find(fileName);

        return new String(Files.readAllBytes(path), encoding);
    }

    private static Path find(final String fileName) {
        final Path[] messagePath = new Path[1];

        findFileInUserDirectory(fileName, file -> {
            try {
                messagePath[0] = file.toPath();
            } catch (Exception ex) {
                throw new CommandException(ex);
            }
        });

        return messagePath[0];
    }

    /**
     * Remove the last line separator from the input string
     *
     * @param str input string
     * @return cleanded string
     */
    private static String chomp(String str) {
        if (isEmpty(str)) {
            return str;
        }

        if (str.length() == 1) {
            char ch = str.charAt(0);
            if (ch == CR || ch == LF) {
                return "";
            }
            return str;
        }

        int lastIdx = str.length() - 1;
        char last = str.charAt(lastIdx);

        if (last == LF) {
            if (str.charAt(lastIdx - 1) == CR) {
                lastIdx--;
            }
        } else if (last != CR) {
            lastIdx++;
        }
        return str.substring(0, lastIdx);
    }

    /**
     * Checks for empty
     *
     * @param str input string
     * @return boolean
     */
    private static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

}