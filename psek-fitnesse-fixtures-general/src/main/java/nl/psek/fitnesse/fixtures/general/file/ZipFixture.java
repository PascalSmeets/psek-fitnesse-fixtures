package nl.psek.fitnesse.fixtures.general.file;

import nl.psek.fitnesse.CommandException;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A fixture to compress a folder to a zip file, can be used to generate a zip file of test artifacts
 *
 * Created by pascal on 02/07/16.
 */
public class ZipFixture {
    public ZipFixture() {
    }

    public void zipDir(String folder, String filename) throws IOException {
        Path path = Paths.get(folder);

        if (!Files.isDirectory(path)) {
            throw new CommandException("Path must be a directory.");
        }

        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filename));

        try (ZipOutputStream out = new ZipOutputStream(bos)) {
            addZipDir(out, path.getFileName(), path);
        }
    }

    private Path buildPath(final Path root, final Path child) {
        if (root == null) {
            return child;
        } else {
            return Paths.get(root.toString(), child.toString());
        }
    }

    private void addZipDir(final ZipOutputStream out, final Path root, final Path dir) throws IOException {
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path child : stream) {
                Path entry = buildPath(root, child.getFileName());
                if (Files.isDirectory(child)) {
                    addZipDir(out, entry, child);
                } else {
                    out.putNextEntry(new ZipEntry(entry.toString()));
                    Files.copy(child, out);
                    out.closeEntry();
                }
            }
        }
    }

}



