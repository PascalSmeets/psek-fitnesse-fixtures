package nl.psek.fitnesse.fixtures.general.bsn;

import nl.psek.fitnesse.ConditionalException;

import java.util.Random;

import static nl.psek.fitnesse.fixtures.util.FixtureOperations.equal;

/**
 * A fixture to generate a Dutch Burger Service Nummer (BSN).
 *
 * more info on BSN: https://www.rijksoverheid.nl/onderwerpen/privacy-en-persoonsgegevens/burgerservicenummer-bsn
 *
 * @author Ronald Mathies
 * @author Pascal Smeets
 */
public class BsnGenerator {

    private static final int LOW = 100000000;
    private static final int HIGH = 999999988;

    /**
     * Constructs a new BSN Generator.
     */
    public BsnGenerator() {
        //default constructor
    }

    /**
     * Returns a random BSN number.
     * In the range 100000000 - 999999988
     *
     * @return the string
     * @throws ConditionalException the conditional exception
     */


    public String generateBsn() {
        int possibleBsn = giveRandomStartingNumber();

        for (; ; ++possibleBsn) {
            if (checkElfProef(String.valueOf(possibleBsn))) {
                return String.valueOf(possibleBsn);
            }

            if (possibleBsn > HIGH) {
                throw new ConditionalException("Geen BSN gevonden.");
            }
        }
    }


    /**
     * Generate bsn in range string.
     *
     * @param begin the begin
     * @param end   the end
     * @return the string
     * @throws ConditionalException the conditional exception
     */
    public String generateBsnInRange(int begin, int end) {
        int possibleBsn = giveRandomStartingNumber(begin, end);

        for (; ; ++possibleBsn) {
            if (checkElfProef(String.valueOf(possibleBsn))) {
                return String.valueOf(possibleBsn);
            }

            if (possibleBsn > end) {
                throw new ConditionalException("Geen BSN gevonden.");
            }
        }
    }


    /**
     * Usefull in testdata generation.
     * In some cases a generated bsn is not usable. A check on the generated bsn can be performed end the results can be passed to this method.
     * If the passed values are equal a new bsn will be generated, otherwise the original bsn will be returned
     *
     * @param originalBsn the original bsn
     * @param valueOne    the value one
     * @param valueTwo    the value two
     * @return original bsn or a newly generated bsn
     * @throws ConditionalException the conditional exception
     */
    public String generateBsnAgainIfConditionIsEqual(String originalBsn, String valueOne, String valueTwo) {
        if (equal(valueOne, valueTwo)) {
            return generateBsn();
        } else return originalBsn;
    }


    /**
     * Generate bsn in range again if condition is equal string.
     *
     * @param originalBsn the original bsn
     * @param begin       the begin
     * @param end         the end
     * @param valueOne    the value one
     * @param valueTwo    the value two
     * @return the string
     * @throws ConditionalException the conditional exception
     */
    public String generateBsnInRangeAgainIfConditionIsEqual(String originalBsn, int begin, int end, String valueOne, String valueTwo) {
        if (equal(valueOne, valueTwo)) {
            return generateBsnInRange(begin, end);
        } else return originalBsn;
    }


    private int giveRandomStartingNumber() {
        return new Random().nextInt(HIGH - LOW) + LOW;
    }

    private int giveRandomStartingNumber(int begin, int end) {
        return new Random().nextInt(end - begin) + begin;
    }

    /**
     * Checks the supplied number using the '11-proef voor een bsn'
     * https://nl.wikipedia.org/wiki/Burgerservicenummer
     * Als het burgerservicenummer wordt voorgesteld door ABCDEFGHI, dan moet:
     * (9 × A) + (8 × B) + (7 × C) + (6 × D) + (5 × E) + (4 × F) + (3 × G) + (2 × H) + (-1 × I) moet een veelvoud van 11 zijn.
     *
     * @param bsn the bsn
     * @return true or false. If the number supplied does not have a length of 9 characters an exception.
     * @throws ConditionalException the conditional exception
     */


    public boolean checkElfProef(String bsn) {
        if (bsn.length() == 9) {
            int a, b, c, d, e, f, g, h, i, pos = 0;

            a = giveNumberOnPosition(bsn, pos++);
            b = giveNumberOnPosition(bsn, pos++);
            c = giveNumberOnPosition(bsn, pos++);
            d = giveNumberOnPosition(bsn, pos++);
            e = giveNumberOnPosition(bsn, pos++);
            f = giveNumberOnPosition(bsn, pos++);
            g = giveNumberOnPosition(bsn, pos++);
            h = giveNumberOnPosition(bsn, pos++);
            i = giveNumberOnPosition(bsn, pos);

            int resultaat = (9 * a) + (8 * b) + (7 * c) + (6 * d) + (5 * e) + (4 * f) + (3 * g) + (2 * h) + (-1 * i);

            return resultaat % 11 == 0;
        } else {
            throw new ConditionalException("Het opgegeven nummer bevat geen 9 karakters");
        }
    }

    private int giveNumberOnPosition(String bsn, int position) {
        String substring = bsn.substring(position, position + 1);
        return Integer.parseInt(substring);
    }

}
