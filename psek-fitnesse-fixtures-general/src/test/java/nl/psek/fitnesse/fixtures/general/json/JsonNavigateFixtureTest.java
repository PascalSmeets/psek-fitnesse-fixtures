package nl.psek.fitnesse.fixtures.general.json;

import nl.psek.fitnesse.CommandException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Created by Pascal on 19-8-2015.
 */
public class JsonNavigateFixtureTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String JSON =
            "[" +
                    "   {" +
                    "      \"apiVersion\":[\"1.0\", \"2.0\"]," +
                    "      \"meta\":{" +
                    "         \"disclaimer\":\"By using this API you comply with the KvK-terms defined on www.kvk.nl/apidisclaimer\"" +
                    "      }" +
                    "   }" +
                    "]";

    private String JSON2 =
            "{\n" +
                    "    \"store\": {\n" +
                    "        \"book\": [\n" +
                    "            {\n" +
                    "                \"category\": \"reference\",\n" +
                    "                \"author\": \"Nigel Rees\",\n" +
                    "                \"title\": \"Sayings of the Century\",\n" +
                    "                \"price\": 8.95\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"category\": \"fiction\",\n" +
                    "                \"author\": \"Evelyn Waugh\",\n" +
                    "                \"title\": \"Sword of Honour\",\n" +
                    "                \"price\": 12.99\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"category\": \"fiction\",\n" +
                    "                \"author\": \"Herman Melville\",\n" +
                    "                \"title\": \"Moby Dick\",\n" +
                    "                \"isbn\": \"0-553-21311-3\",\n" +
                    "                \"price\": 8.99\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"category\": \"fiction\",\n" +
                    "                \"author\": \"J. R. R. Tolkien\",\n" +
                    "                \"title\": \"The Lord of the Rings\",\n" +
                    "                \"isbn\": \"0-395-19395-8\",\n" +
                    "                \"price\": 22.99\n" +
                    "            }\n" +
                    "        ],\n" +
                    "        \"bicycle\": {\n" +
                    "            \"color\": \"red\",\n" +
                    "            \"price\": 19.95\n" +
                    "        }\n" +
                    "    },\n" +
                    "    \"expensive\": 10\n" +
                    "}";


    @Test
    public void testLaadJsonUitString() throws Exception {
        JsonNavigateFixture fixture = new JsonNavigateFixture();
        fixture.loadJsonFromString(JSON);
        Object value = fixture.retrieveValueOfJsonElement("$[0].apiVersion[0]");
        assertEquals("1.0", value);
        Object value2 = fixture.retrieveValueOfJsonElement("$[0].apiVersion[1]");
        assertEquals("2.0", value2);

    }

    @Test
    public void testJsonContainsString() throws Exception {
        JsonNavigateFixture fixture = new JsonNavigateFixture();
        fixture.loadJsonFromString(JSON);
        assertTrue(fixture.verifyJsonElementContainsString("$[0].meta.disclaimer", "apidisclaimer"));
        assertFalse(fixture.verifyJsonElementContainsString("$[0].meta.disclaimer", "APIDISCLAIMER"));
        assertFalse(fixture.verifyJsonElementContainsString("$[0].meta.disclaimer", "apidisKlaimer"));
        thrown.expect(CommandException.class);
        assertTrue(fixture.verifyJsonElementContainsString("[0].meta.disKlaimer", "apidisclaimer"));
    }

    @Test
    public void testCheckValueOfJsonElement() throws Exception {
        JsonNavigateFixture fixture = new JsonNavigateFixture();
        fixture.loadJsonFromString(JSON2);
        // dot notation
        assertTrue(fixture.checkValueOfJsonElement("$.store.book[1].author", "Evelyn Waugh"));
        // bracket notation
        assertTrue(fixture.checkValueOfJsonElement("$['store']['book'][1]['author']", "Evelyn Waugh"));
    }

    @Test
    public void testRetrieveValueOfJsonElement() throws Exception {
        JsonNavigateFixture fixture = new JsonNavigateFixture();
        fixture.loadJsonFromString(JSON2);
        Object value = fixture.retrieveValueOfJsonElement("expensive");
        assertEquals("10", value);
        Object value2 = fixture.retrieveValueOfJsonElement("$.expensive");
        assertEquals("10", value2);
        Object value3 = fixture.retrieveValueOfJsonElement("$.store.book[2].author");
        assertEquals("Herman Melville", value3);
        Object value4 = fixture.retrieveValueOfJsonElement("$.store.book[3].isbn");
        assertEquals("0-395-19395-8", value4);
        thrown.expect(CommandException.class);
        fixture.retrieveValueOfJsonElement("$..yes.author.bla");
        fixture.retrieveValueOfJsonElement("$.yes.author.bla");

    }

}