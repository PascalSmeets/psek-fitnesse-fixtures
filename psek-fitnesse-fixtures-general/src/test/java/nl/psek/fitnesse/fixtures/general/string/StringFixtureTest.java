package nl.psek.fitnesse.fixtures.general.string;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by pascal on 18-03-2017.
 */
public class StringFixtureTest {
    @Test
    public void substringFrom() throws Exception {

        StringFixture fixture = new StringFixture("0123456789");
        assertTrue(fixture.substringFrom("5").equals("56789"));

    }

    @Test
    public void substringTo() throws Exception {

        StringFixture fixture = new StringFixture("0123456789");
        assertTrue(fixture.substringTo("5").equals("01234"));


    }

}