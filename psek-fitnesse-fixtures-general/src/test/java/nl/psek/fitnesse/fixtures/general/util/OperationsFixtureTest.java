package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.CommandException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertTrue;

public class OperationsFixtureTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    @Test
    public void checkAreValuesEqual() throws Exception {
        Object object1 = "tekst1";
        Object object2 = "tekst1";
        assertTrue(OperationsFixture.areValuesEqual(object1, object2));
    }

    @Test
    public void checkAreValuesEqualThrowsExceptionWithNull() throws CommandException {
        thrown.expect(CommandException.class);
        Object object1 = "tekst1";
        assertTrue(OperationsFixture.areValuesEqual(object1, null));
    }

}
