package nl.psek.fitnesse.fixtures.general.json;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuildJsonStringFixtureTest {

    BuildJsonStringFixture fixture = new BuildJsonStringFixture();

    @Test
    public void createJsonStringTest() {

        fixture.startNewJsonObjectMessage();
        fixture.addString("first", "element");
        fixture.addString("second", "element");
        fixture.addInt("number", 1);

        fixture.startObject("type");
        fixture.addString("id", "12345-aa");
        fixture.addBoolean("active", "false");
        fixture.endObject();

        fixture.startArray("phoneNumber");
        fixture.startObject();
        fixture.addString("type", "home");
        fixture.addString("number", "0123456789");
        fixture.endObject();
        fixture.startObject();
        fixture.addString("type", "home");
        fixture.addString("number", "11223344");
        fixture.endObject();
        fixture.endArray();
        fixture.addBoolean("test", "false");

        String generated = fixture.generateJson();
        String provided = "{\"number\":1,\"phoneNumber\":[{\"number\":\"0123456789\",\"type\":\"home\"},{\"number\":\"11223344\",\"type\":\"home\"}],\"test\":false,\"type\":{\"active\":false,\"id\":\"12345-aa\"},\"first\":\"element\",\"second\":\"element\"}";

        assertEquals(provided, generated);

    }

}