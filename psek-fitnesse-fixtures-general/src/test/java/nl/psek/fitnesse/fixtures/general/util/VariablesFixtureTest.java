package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.CommandException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class VariablesFixtureTest {

    public static VariablesFixture fixture = new VariablesFixture();

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void convertNumberCharactersTest() {
        String input = "12345";
        assertEquals("conversion of only number chars to alpha", "BCDEF", (fixture.convertNumberCharsToAlpha(input)));
    }

    @Test
    public void converMixedCharactersToalphaTest() {
        String input = "Z2345";
        assertEquals("conversion of a mixed char string to alpha", "ZCDEF", (fixture.convertNumberCharsToAlpha(input)));
    }

    @Test
    public void converAlphaCharactersToalphaTest() {
        String input = "AbCdEfG";
        assertEquals("conversion of a alpha char string to alpha", "AbCdEfG", (fixture.convertNumberCharsToAlpha(input)));
    }

    @Test
    public void converEmptyStringToalphaTest() {
        String input = "";
        exception.expect(CommandException.class);
        fixture.convertNumberCharsToAlpha(input);
    }

    @Test
    public void converUnsupportedCharactersToalphaTest() {
        String input = "12AB-+!!@@#";
        exception.expect(CommandException.class);
        fixture.convertNumberCharsToAlpha(input);
    }

    @Test
    public void unicodeStringTest() {
        assertEquals("string is the alphabet in lowercase", "abcdefghijklmnopqrstuvwxyz", fixture.generateUnicodeCodepointStringFromWithLength(97, 26));
    }


}
