package nl.psek.fitnesse.fixtures.general.util;

import nl.psek.fitnesse.StopTestCommandException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.nio.charset.StandardCharsets;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class KeepassFixtureTest {

	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Test
	public void IfTheSystemProperyContainingThePassphraseCanNotBeReadWeThrowAnException() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("The system property containing the passphrase has not been set: IDontExist");

		KeepassFixture kp = new KeepassFixture("bla", "IDontExist", false);
	}

	@Test
	public void IfTheKeepassStoreCanNotBeFoundWeThrowAnException() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("The keepass datastore could not be found: bla");
		System.setProperty("IDoExist", "aStringValue");

		KeepassFixture kp = new KeepassFixture("bla", "IDoExist", false);
	}

	@Test
	public void IfTheKeepassStoreCanNotBeLoadedWeThrowAnException() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("The keepass datastore could not be loaded: src/test/resources/keepassfixture/unloadable.kdbx");

		System.setProperty("IDoExist", "aStringValue");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unloadable.kdbx", "IDoExist", false);
	}

	@Test
	public void IfTheKeepassStoreIsEmptyWeDoNotThrowAnException() {
		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest_empty.kdbx", "passphrase", false);
	}

	@Test
	public void IfTheKeepassStoreContainsDuplicateKeysWeThrownAnException() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("The supplied key is not unique in the database, but present 2 times");

		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest_duplicates.kdbx", "passphrase", false);
		kp.getCredential("testkey");
	}

	@Test
	public void IfTheEntryContainsAnEmptyValueWeThrowAnExceptionIfThatValueIsRequestedCredential() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("The requested result from the entry was empty");

		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);
		kp.getCredential("testkey.empty");
	}

	@Test
	public void IfTheEntryContainsAnEmptyValueWeThrowAnExceptionIfThatValueIsRequestedUsername() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("The requested result from the entry was empty");

		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);
		kp.getUsername("testkey.empty");
	}

	@Test
	public void IfTheEntryContainsAnEmptyValueWeThrowAnExceptionIfThatValueIsRequestedUrl() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("The requested result from the entry was empty");

		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);
		kp.getUrl("testkey.empty");
	}

	@Test
	public void UsingAFileThatIsNotPresentThrowsAnException() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("Could not read fom file: src/test/resources/keepassfixture/test-credentials-nothere.txt");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "src/test/resources/keepassfixture/test-credentials-nothere.txt", true);
	}

	@Test
	public void UsingAFileWithNoPassphraseThrowsAnException() {
		exceptionRule.expect(StopTestCommandException.class);
		exceptionRule.expectMessage("Passphrase is not present in file: src/test/resources/keepassfixture/test-credentials-empty.txt");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "src/test/resources/keepassfixture/test-credentials-empty.txt", true);
	}

	@Test
	public void UsingAFileWithThePassphraseWorks() {
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "src/test/resources/keepassfixture/test-credentials-file.txt", true);
	}

	@Test
	public void WeCanGetAUsernameFromTheKeepassStore() {
		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);

		assertThat("Username can be loaded", kp.getUsername("testkey.complete"), equalTo("testusername"));
	}

	@Test
	public void WeCanGetACredentialFromTheKeepassStore() {
		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);

		assertThat("Username can be loaded", kp.getCredential("testkey.complete"), equalTo("testcredential"));
	}

	@Test
	public void WeCanGetAUrlFromTheKeepassStore() {
		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);

		assertThat("Username can be loaded", kp.getUrl("testkey.complete"), equalTo("https://me:logsin@example.com"));
	}

	@Test
	public void WeCanGetACustomStringPropertyTheKeepassStore() {
		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);

		assertThat("Username can be loaded", kp.getField("testkey.complete", "testfield1"), equalTo("testfield1value"));

	}

	@Test
	public void WeCanGetAFileAttachmentFromTheKeepassStore(){
		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);

		assertThat("The loaded bytes contain the file contents", new String( kp.getBinaryProperty("testkey.with.attachment.storedasbinary","attachment.txt"), StandardCharsets.UTF_8), equalTo("test attachement file contents"));
	}

	@Test
	public void WeCanStoreResultsInsystemProperties() {
		System.setProperty("passphrase", "unittest_password");
		KeepassFixture kp = new KeepassFixture("src/test/resources/keepassfixture/unittest.kdbx", "passphrase", false);

		kp.storeUsernameInProperty("testkey.complete", "sp1");
		assertThat(System.getProperty("sp1").toString(), equalTo("testusername"));

		kp.storeCredentialInProperty("testkey.complete", "sp2");
		assertThat(System.getProperty("sp2").toString(), equalTo("testcredential"));

		kp.storeUrlInProperty("testkey.complete", "sp3");
		assertThat(System.getProperty("sp3").toString(), equalTo("https://me:logsin@example.com"));

		kp.storeFieldInProperty("testkey.complete", "testfield1", "sp4");
		assertThat(System.getProperty("sp4").toString(), equalTo("testfield1value"));

	}

}
