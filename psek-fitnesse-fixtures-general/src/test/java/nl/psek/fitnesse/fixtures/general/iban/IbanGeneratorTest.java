package nl.psek.fitnesse.fixtures.general.iban;

import nl.psek.fitnesse.CommandException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

/**
 * @author Pascal Smeets
 */
public class IbanGeneratorTest {


    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGenerateIBAN() throws Exception {
        String countryCode = "NL";
        String bankCode = "INGB";
        String accountNumber = "0001234567";
        String iban = IbanGenerator.generateIBAN(countryCode, bankCode, accountNumber);
        assertEquals("NL20INGB0001234567", iban);

        String countryCode2 = "GB";
        String bankCode2 = "WEST";
        String accountNumber2 = "12345698765432";

        String iban2 = IbanGenerator.generateIBAN(countryCode2, bankCode2, accountNumber2);
        assertEquals("GB82WEST12345698765432", iban2);

    }


    @Test
    public void getNumericValueFromCharTest() {
        String input = "a";
        assertEquals("10", IbanGenerator.getNumericValueFromCharacter(input));
    }

    @Test
    public void getNumericValuesFromStringTest() {
        String input1 = "aB";
        String input2 = "aBcD";
        assertEquals("1011", IbanGenerator.getNumericValuesFromString(input1));
        assertEquals("10111213", IbanGenerator.getNumericValuesFromString(input2));
    }

    @Test
    public void getNumericValuesFromStringThrowsCommandExceptionTest() throws CommandException {
        thrown.expect(CommandException.class);
        String input1 = "a";
        String input2 = "abcde";
        assertEquals("1011", IbanGenerator.getNumericValuesFromString(input1));
        assertEquals("10111213", IbanGenerator.getNumericValuesFromString(input2));
    }
}
