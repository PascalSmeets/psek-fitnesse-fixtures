package nl.psek.fitnesse.fixtures.general.text;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Pascal on 7-12-2015.
 */
public class TextReaderFixtureTest {

    @Test
    public void testTailNormalFile() throws Exception {
        TextReaderFixture tr = new TextReaderFixture();

        //10 lines from the end are read bij default, file has 11 lines
        tr.setFileToRead("src/test/resources/TextReaderFixtureTest.log");
        // Pink on line 3 is found
        assertTrue(tr.isTextPresentOnLine("Pink"));
        // White on line 1 is not found because the line is not read
        assertFalse(tr.isTextPresentOnLine("White"));
    }

    @Test
    public void testTailSmallFile() throws Exception {
        TextReaderFixture tr = new TextReaderFixture();

        //10 lines from the end are read bij default, but file has 3 lines
        tr.setFileToRead("src/test/resources/TextReaderFixtureTest_small.log");
        // Pink on line 3 is found, and no exception is thrown
        assertTrue(tr.isTextPresentOnLine("Pink"));

    }
}