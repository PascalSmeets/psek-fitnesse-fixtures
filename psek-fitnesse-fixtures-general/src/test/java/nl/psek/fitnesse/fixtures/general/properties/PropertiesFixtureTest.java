package nl.psek.fitnesse.fixtures.general.properties;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PropertiesFixtureTest {

    @Test
    public void checkGetStringProperty() throws Exception {
        PropertiesFixture properties = new PropertiesFixture("root.properties");
        assertEquals("Root property should be true.", "true", properties.getStringProperty("root"));
    }

    @Test
    public void checkGetIntProperty() throws Exception {
        PropertiesFixture properties = new PropertiesFixture("child.properties");
        assertEquals("Value property should be 1.", 1, properties.getIntProperty("value"));
    }
}