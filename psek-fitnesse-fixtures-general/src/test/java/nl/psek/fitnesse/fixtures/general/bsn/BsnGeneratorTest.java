package nl.psek.fitnesse.fixtures.general.bsn;

import nl.psek.fitnesse.CommandException;
import org.junit.Test;

import static org.junit.Assert.*;

public class BsnGeneratorTest {

    BsnGenerator generator = new BsnGenerator();

    @Test
    public void checkValidElfProefTest() {

        // test numbers are from https://www.rvig.nl/documenten/richtlijnen/2018/09/20/test-burgerservicenummers-bsn-en-a-nummers-inclusief-omnummertabel
        assertTrue("valid BSN passes 11-proef", generator.checkElfProef("000000085"));
        assertTrue("valid BSN passes 11-proef", generator.checkElfProef("999998614"));
        assertFalse("non-valid BSN passes 11-proef", generator.checkElfProef("999998620"));
    }

    @Test(expected = CommandException.class)
    public void checkInputElfProefTest() {
        generator.checkElfProef("99998620");
    }

}